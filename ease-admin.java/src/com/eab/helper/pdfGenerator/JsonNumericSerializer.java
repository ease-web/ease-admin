package com.eab.helper.pdfGenerator;

import java.lang.reflect.Type;

import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public class JsonNumericSerializer implements JsonSerializer<Double> {

    public JsonElement serialize(Double src, Type typeOfSrc, JsonSerializationContext context) {
        if (src == Math.rint(src)) {
            return new JsonPrimitive(src.intValue());
        } else if (src == src.longValue()) {
            return new JsonPrimitive(src.longValue());
        }
        return new JsonPrimitive(src);
    }

}
