package com.eab.listener;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import com.eab.common.Log;
import com.eab.dao.audit.AudUserLog;
import com.eab.dao.dynamic.RecordLock;

public class SessionListener implements HttpSessionListener {

	private static int totalActiveSessions;
		
	public static int getTotalActiveSession(){
		return totalActiveSessions;
	}
	
	@Override
	public void sessionCreated(HttpSessionEvent arg0) {
		totalActiveSessions++;
		Log.debug("{sessionCreated} - add one session into counter, current active(" + totalActiveSessions + ")");
	}
	
	@Override
	public void sessionDestroyed(HttpSessionEvent arg0) {
		totalActiveSessions--;
		Log.debug("{sessionDestroyed} - deduct one session from counter, current active(" + totalActiveSessions + ")");
		String sessionID = null;
		AudUserLog audit = new AudUserLog();
		
		try {
			// Mark Logout Time into audit trail
			sessionID = arg0.getSession().getId();
			boolean status = audit.setLogoutTime(sessionID);
			
			//clear record lock
			RecordLock rl = new RecordLock();
			boolean removeRecord = rl.removeRecordLockBySession(sessionID);
			
			Log.debug("{sessionDestroyed} - Set Logout Time is " + status);
		} catch(Exception e) {
			Log.debug("Fail to mark Logout Time......."+sessionID);
		} finally {
			audit = null;
		}
	}	
}
