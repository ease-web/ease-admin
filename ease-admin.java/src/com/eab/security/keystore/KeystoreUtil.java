package com.eab.security.keystore;

import java.io.FileInputStream;
import java.io.InputStream;
import java.security.Key;
import java.security.KeyStore;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import sun.misc.BASE64Decoder;

import com.eab.common.Function;
import com.eab.common.Log;

public class KeystoreUtil {
	private final static String CRYPTOKEY_ALGORITHM = "TripleDES";
	private final static String CRYPTOKEY_TRANSFORMATION = "TripleDES/ECB/PKCS5Padding";
	private final static String KEYSTORE_TYPE = "JCEKS";
	private final static String KEYSTORE_ALIAS = "key.121.eabsystems";
	private final static String KEYSTORE_PIN = "ko7K5x2V";	//Example: AAbb1122
	
	public static Key read(String keystoreFile) {
		Key key = null;
		KeyStore ks = null;
		InputStream readStream = null;
		String pin = KEYSTORE_PIN;
		
		try {
			char[] password = new char[pin.length()];
			pin.getChars(0, pin.length(), password, 0);
			
			ks = KeyStore.getInstance(KEYSTORE_TYPE);
			readStream = new FileInputStream(keystoreFile);
			ks.load(readStream, password);
			key = ks.getKey(KEYSTORE_ALIAS, password);
			readStream.close();
		} catch(Exception e) {
			Log.error(e);
		}
		
		return key;
	}
	
	public static SecretKey decryptPrimaryKey(String key, String keystoreFile) throws Exception {
		String encryptText = null;
		String pin = KEYSTORE_PIN;
		char[] password = new char[pin.length()];
		pin.getChars(0, pin.length(), password, 0);

		KeyStore ks = KeyStore.getInstance(KEYSTORE_TYPE);
		InputStream readStream = new FileInputStream(keystoreFile);
		ks.load(readStream, password);
		Key key2 = ks.getKey(KEYSTORE_ALIAS, password);
		readStream.close();
		
		BASE64Decoder decoder = new BASE64Decoder();
		byte[] key1 = decoder.decodeBuffer(key);
		
		Cipher cipher = Cipher.getInstance(CRYPTOKEY_TRANSFORMATION);
	    cipher.init(Cipher.DECRYPT_MODE, key2);

	    byte[] primaryKey = cipher.doFinal(key1);
	    Log.debug("Primary Key: " + Function.ByteToHex(primaryKey));
	    
	    return new SecretKeySpec(primaryKey, 0, primaryKey.length, CRYPTOKEY_ALGORITHM);
	}
}
