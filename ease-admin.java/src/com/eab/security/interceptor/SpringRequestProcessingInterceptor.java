package com.eab.security.interceptor;

import java.lang.reflect.Method;
import java.util.Enumeration;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.eab.common.Log;
import com.eab.security.CharBlacklist;

public class SpringRequestProcessingInterceptor extends HandlerInterceptorAdapter {

	@Override
    public boolean preHandle(HttpServletRequest request,
    					HttpServletResponse response, Object handler) throws Exception {
        long startTime = System.currentTimeMillis();
        Log.debug("{preHandle} Request URL::" + request.getRequestURL().toString() + ":: Start Time=" + System.currentTimeMillis());
        request.setAttribute("startTime", startTime);
        //if returned false, we need to make sure 'response' is sent
        return true;
    }
	
	@Override
    public void postHandle(HttpServletRequest request,
            			HttpServletResponse response, Object handler,
            			ModelAndView modelAndView) throws Exception {
		boolean enable = false;
		
		Log.debug("{postHandle} Request URL::" + request.getRequestURL().toString() + " Sent to Handler :: Current Time=" + System.currentTimeMillis());
        //we can add attributes in the modelAndView and use that in the view page
		
		if (enable) {
			// Security: Check data passing to JSP does not include any blacklist special character.
			// It is only limited String Value in Spring Model Object.
			// If using React.js + Reflux, you may need turn it off.
			if (modelAndView != null) {
				
				ModelMap mm = modelAndView.getModelMap();
				if (mm != null) {
					for(Entry<String, Object> entry : mm.entrySet()) {
						boolean hasChange = false;
						String text = null;
						String key = entry.getKey();
						Object value = entry.getValue();
						Log.debug("{postHandle} Attribute: key=" + key + ", value=" + value);
						if ( value instanceof String ) {
							text = CharBlacklist.encode((String) value);
							if (!value.equals(text)) {
								hasChange = true;
								Log.debug("{postHandle} Attribute: original=" + value);
								Log.debug("{postHandle} Attribute: update=" + text);
							}
						}
						
						// Set new value into ModelMap
						if (hasChange) {
							mm.put(key, text);		// Updated Text String
						}
					}
				}
			}
		}
    }
	
	@Override
    public void afterCompletion(HttpServletRequest request,
            HttpServletResponse response, Object handler, Exception ex)
            throws Exception {
        long startTime = (Long) request.getAttribute("startTime");
        Log.debug("{afterCompletion} Request URL::" + request.getRequestURL().toString() + ":: End Time=" + System.currentTimeMillis());
        Log.debug("{afterCompletion} Request URL::" + request.getRequestURL().toString() + ":: Time Taken=" + (System.currentTimeMillis() - startTime));
    }
}
