/* This source has been formatted by an unregistered SourceFormatX */
/* If you want to remove this info, please register this shareware */
/* Please visit http://www.textrush.com to get more information    */
/*
 * Created by IntelliJ IDEA.
 * User: Administrator
 * Date: 2002 - 4 - 27
 * Time: 15:26:35
 * To change template for new class use
 * Code Style | Class Templates options ( Tools | IDE Options ) .
 */
package com.eab.util.jobrunner;

import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Timer;
import java.util.TimerTask;

import com.eab.biz.batch.BatRunIpRefresh;
import com.eab.common.Log;


/**
 * Each job is a java thread represent by this Job class.
 * there is Timer object in this class to execute the job itself
 */
public class Job extends TimerTask {
    /**
     *Timer object to excute the job
     */
    private Timer myTimer = null;

    /**
     * The object of which the method to call
     */
    private Object runnableObject = null;

    /**
     * The tyoe of running schedule, possible value:
     * "once-a-week", "once-a-day", "fixed-delay"
     */
    private String periodType = null;

    /**
     *The first start time of job
     */
    private String startTime = null;

    /**
     * The delay time (in milliscend) for the first start of the job
     */
    private String startDelay = null;

    /**
     * The delay time (in millisecond) for the job of fixed-delay type
     */
    private String delay = null;

    /**
     *The name of the class to run
     */
    private String className = null;

    /**
     * Name of the method of property runnableObject, of which we will
     * call to execute the job
     */
    private String methodName = null;

    /**
     *
     * @return String
     */
    public String getPeriodType() {
        return this.periodType;
    }

    /**
     *
     * @param periodType String
     */
    public void setPeriodType(String periodType) {
        this.periodType = periodType;
    }

    /**
     *
     * @return String
     */
    public String getStartTime() {
        return this.startTime;
    }

    /**
     *
     * @param startTime String
     */
    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    /**
     *
     * @return String
     */
    public String getStartDelay() {
        return this.startDelay;
    }

    /**
     *
     * @param startDelay String
     */
    public void setStartDelay(String startDelay) {
        this.startDelay = startDelay;
    }

    /**
     *
     * @return String
     */
    public String getDelay() {
        return this.delay;
    }

    /**
     *
     * @param delay String
     */
    public void setDelay(String delay) {
        this.delay = delay;
    }

    /**
     *
     * @return String
     */
    public String getClassName() {
        return this.className;
    }

    /**
     *
     * @param className String
     */
    public void setClassName(String className) {
        this.className = className;
    }

    /**
     *
     * @return String
     */
    public String getMethodName() {
        return this.methodName;
    }

    /**
     *
     * @param methodName String
     */
    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    /**
     * Instantiate the Timer object , add this job to this Timer by
     * calling the schedule method of the Timer.Start the Timer
     */
    public void startup() {
        myTimer = new Timer();

        try {
            if (this.getPeriodType() == null) {
                throw new Exception(this.getClassName() + "."
                    + this.getMethodName() + "() " + "type:"
                    + this.getPeriodType()
                    + " error:periodType is null!");
            }

            if (this.getPeriodType().trim().equalsIgnoreCase("once-a-week")) {
                setTimerOAW(myTimer);
            }

            if (this.periodType.trim().equalsIgnoreCase("once-a-day")) {
                setTimerOAD(myTimer);
            }

            if (this.periodType.trim().equalsIgnoreCase("fixed-delay")) {
                setTimerFD(myTimer);
            }

            if (!(this.getPeriodType().trim().equalsIgnoreCase("fixed-delay"))
                    && !(this.getPeriodType()
                        .trim().equalsIgnoreCase("once-a-day"))
                    && !(this.getPeriodType()
                        .trim().equalsIgnoreCase("once-a-week"))) {
                throw new Exception(this.getClassName() + "."
                    + this.getMethodName() + "() " + "type:"
                    + this.getPeriodType()
                    + " error:periodType is not correct!");
            }
        } catch (Exception e) {
        	Log.error("{Jobrunner Job} -----------------> Error ------------->" + e);

            Log.error("{Jobrunner Job} -----------------> Error ------------->" + this.getClassName() + "." + this.getMethodName() + "() " + "type:" + this.getPeriodType() + " can not be started");
        }
    }

    /**
     *
     * @param myTimer Timer
     * @throws Exception wqefrwe
     */
    private void setTimerOAW(Timer myTimer) throws Exception {
        if (this.getStartTime() != null) {
            int weekDay = stringToWeekDay(this.getStartTime()
                                              .substring(0,
                        this.getStartTime().indexOf(',')));

            if (weekDay == 0) {
                throw new Exception(this.getClassName() + "."
                    + this.getMethodName() + "() " + "type:"
                    + this.getPeriodType()
                    + " error:week day of startTime is not correct");
            }

            SimpleDateFormat formatter = new SimpleDateFormat(
                    "HH:mm:ss");
            Date dstartTime = formatter.parse(this.getStartTime()
                                   .substring(this.getStartTime()
                                              .indexOf(',')
                        + 1));
            Date currentDT = new Date(System.currentTimeMillis());
            Calendar calendar = new GregorianCalendar();
            calendar.setTime(currentDT);

            int month = calendar.get(calendar.MONTH);
            int year = calendar.get(calendar.YEAR);
            int day = calendar.get(calendar.DAY_OF_MONTH);
            calendar.setTime(dstartTime);
            calendar.set(calendar.MONTH, month);
            calendar.set(calendar.YEAR, year);
            calendar.set(calendar.DAY_OF_MONTH, day);

            if (calendar.get(calendar.DAY_OF_WEEK) == weekDay) {
                if (currentDT.getTime() > calendar.getTime().getTime()) {
                    calendar.add(calendar.DAY_OF_MONTH, 7);
                }
            }

            if (calendar.get(calendar.DAY_OF_WEEK) > weekDay) {
                calendar.add(calendar.DAY_OF_MONTH, 7);
                calendar.set(calendar.DAY_OF_WEEK, weekDay);
            }

            if (calendar.get(calendar.DAY_OF_WEEK) < weekDay) {
                calendar.set(calendar.DAY_OF_WEEK, weekDay);
            }

            Log.info("{Jobrunner setTimerOAW} -----------------> " + this.getClassName() + "." + this.getMethodName() + "() " + "type:"+ this.getPeriodType() + " startTime="+ calendar.getTime());

            myTimer.schedule(this, calendar.getTime(),
                1000 * 3600 * 24 * 7);
        } else {
            throw new Exception(this.getClassName() + "."
                + this.getMethodName() + "() " + "type:"
                + this.getPeriodType() + " error:startTime is null");
        }
    }

    /**
     *
     * @param input String
     * @return int
     */
    private int stringToWeekDay(String input) {
        int i = 0;

        if (input.trim().equalsIgnoreCase("SUN")) {
            i = 1;
        }

        if (input.trim().equalsIgnoreCase("MON")) {
            i = 2;
        }

        if (input.trim().equalsIgnoreCase("TUE")) {
            i = 3;
        }

        if (input.trim().equalsIgnoreCase("WED")) {
            i = 4;
        }

        if (input.trim().equalsIgnoreCase("THU")) {
            i = 5;
        }

        if (input.trim().equalsIgnoreCase("FRI")) {
            i = 6;
        }

        if (input.trim().equalsIgnoreCase("SAT")) {
            i = 7;
        }

        return i;
    }

    /**
     *
     * @param myTimer Timer
     * @throws Exception ption
     */
    private void setTimerOAD(Timer myTimer) throws Exception {
        if (this.getStartTime() != null) {
            SimpleDateFormat formatter = new SimpleDateFormat(
                    "HH:mm:ss");
            Date dstartTime = formatter.parse(this.getStartTime());
            Date currentDT = new Date(System.currentTimeMillis());
            Calendar calendar = new GregorianCalendar();
            calendar.setTime(currentDT);

            int month = calendar.get(calendar.MONTH);
            int year = calendar.get(calendar.YEAR);
            int day = calendar.get(calendar.DAY_OF_MONTH);
            calendar.setTime(dstartTime);
            calendar.set(calendar.MONTH, month);
            calendar.set(calendar.YEAR, year);
            calendar.set(calendar.DAY_OF_MONTH, day);

            if (currentDT.getTime() > calendar.getTime().getTime()) {
                calendar.add(calendar.DAY_OF_MONTH, 1);
            }

            Log.info("{Jobrunner setTimerOAD} -----------------> " + this.getClassName() + "." + this.getMethodName() + "() " + "type:"+ this.getPeriodType() + " startTime="+ calendar.getTime());
            
            myTimer.schedule(this, calendar.getTime(),
                1000 * 3600 * 24);
        } else {
            throw new Exception(this.getClassName() + "."
                + this.getMethodName() + "() " + "type:"
                + this.getPeriodType() + " error:startTime is null");
        }
    }

    /**
     *
     * @param myTimer Timer
     * @throws Exception err
     */
    private void setTimerFD(Timer myTimer) throws Exception {
        Exception e = null;

        if (this.getStartTime() != null) {
            SimpleDateFormat formatter = new SimpleDateFormat(
                    "yyyy-MM-dd HH:mm:ss");
            Date dstartTime = formatter.parse(this.getStartTime());

            if (this.getStartDelay() != null) {
                long lstartDelay = Long.decode(this.getStartDelay())
                                       .longValue();

                if (this.getDelay() != null) {
                    long ldeley = Long.decode(this.getDelay())
                                      .longValue();
                    dstartTime.setTime(dstartTime.getTime() + lstartDelay);
                    Log.info("{Jobrunner setTimerFD} -----------------> " + this.getClassName() + "." + this.getMethodName() + "() " + "type:"+ this.getPeriodType() + " startTime="+dstartTime);

                    myTimer.schedule(this, dstartTime, ldeley);
                } else {
                    e = new Exception(this.getClassName() + "."
                            + this.getMethodName() + "() " + "type:"
                            + this.getPeriodType()
                            + " error:delay is null");
                }
            } else {
                if (this.getDelay() != null) {
                    long ldeley = Long.decode(this.getDelay()).longValue();
                    Log.info("{Jobrunner setTimerFD} -----------------> " + this.getClassName() + "." + this.getMethodName() + "() " + "type:"+ this.getPeriodType() + " startTime="+dstartTime);

                    myTimer.schedule(this, dstartTime, ldeley);
                } else {
                    e = new Exception(this.getClassName() + "."
                            + this.getMethodName() + "() " + "type:"
                            + this.getPeriodType()
                            + " error:delay is null");
                }
            }
        } else {
            if (this.getStartDelay() != null) {
                long lstartDelay = Long.decode(this.getStartDelay())
                                       .longValue();

                if (this.getDelay() != null) {
                    long ldeley = Long.decode(this.getDelay()).longValue();
                    Log.info("{Jobrunner setTimerFD} -----------------> " + this.getClassName() + "." + this.getMethodName() + "() " + "type:"+ this.getPeriodType() + " startTime="+new Date(System.currentTimeMillis() + lstartDelay));

                    myTimer.schedule(this, lstartDelay, ldeley);
                } else {
                    e = new Exception(this.getClassName() + "."
                            + this.getMethodName() + "() " + "type:"
                            + this.getPeriodType()
                            + " error:delay is null");
                }
            } else {
                if (this.getDelay() != null) {
                    long ldeley = Long.decode(this.getDelay()).longValue();
                    
                    Log.info("{Jobrunner setTimerFD} -----------------> " + this.getClassName() + "." + this.getMethodName() + "() " + "type:"+ this.getPeriodType() + " startTime="+new Date(System.currentTimeMillis()));
                    
                    myTimer.schedule(this, 0, ldeley);
                } else {
                    e = new Exception(this.getClassName() + "."
                            + this.getMethodName() + "() " + "type:"
                            + this.getPeriodType()
                            + " error:delay is null");
                }
            }
        }

        if (e != null) {
        	Log.error("{Jobrunner setTimerFD} -----------------> " + e);
        }
    }

    /**
     *Call the specified method of the specified class described in
     * the configuration XML file
     */
    public void run() {

		BatRunIpRefresh batip = new BatRunIpRefresh();

		try {
		if(batip.updateServerIp())
		{	
			Log.info("{Jobrunner Job Begin} -----------------> " + this.getClassName() + "." + this.getMethodName() );
        try {
            Class executeClass = null;
            Method executeMethod = null;

            if (runnableObject != null) {
                executeClass = runnableObject.getClass();
            } else {
                executeClass = Class.forName(className);
                this.runnableObject = executeClass.newInstance();
            }

            if (this.getMethodName() != null) {
                executeMethod = executeClass.getDeclaredMethod(methodName, null);
            } else {
                throw new Exception("error:methodName is null");
            }

            executeMethod.invoke(this.runnableObject, null);

            Log.info("{Jobrunner Job End Success} -----------------> " + this.getClassName() + "." + this.getMethodName() );

        } catch (Exception e) {

            Log.error("{Jobrunner Job End Error} -----------------> " + this.getClassName() + "." + this.getMethodName() );
            Log.error("{Jobrunner Job End Error} -----------------> " + e );
        }

        if (this.getPeriodType().trim().equalsIgnoreCase("once-a-week")) {
        	Log.info("{Jobrunner Job Ignore Csse} -----------------> " + this.getClassName() + "." + this.getMethodName() + "() job next excute in " + new Date(System.currentTimeMillis()  + (1000 * 3600 * 24 * 7)).toString() );
        }

        if (this.periodType.trim().equalsIgnoreCase("once-a-day")) {
        	Log.info("{Jobrunner Job Ignore Csse} -----------------> " + this.getClassName() + "." + this.getMethodName() + "() job next excute in " + new Date(System.currentTimeMillis()  + (1000 * 3600 * 24 * 7)).toString() );
        }

        if (this.periodType.trim().equalsIgnoreCase("fixed-delay")) {
        	Log.info("{Jobrunner Job Ignore Csse} -----------------> " + this.getClassName() + "." + this.getMethodName() + "() job next excute in " + new Date(System.currentTimeMillis()  + (1000 * 3600 * 24 * 7)).toString() );
        }

    }
	} catch (Exception e) {
		e.printStackTrace();
	}
   }		
}
