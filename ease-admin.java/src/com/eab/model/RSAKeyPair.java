package com.eab.model;

import java.security.PrivateKey;
import java.security.PublicKey;

public class RSAKeyPair {
	private PublicKey publicKey;

	private PrivateKey privateKey;
	
	private String htmlUsedModulus;
	
	public PublicKey getPublicKey() {
		return publicKey;
	}

	public void setPublicKey(PublicKey publicKey) {
		this.publicKey = publicKey;
	}

	public PrivateKey getPrivateKey() {
		return privateKey;
	}

	public void setPrivateKey(PrivateKey privateKey) {
		this.privateKey = privateKey;
	}

	public String getHtmlUsedModulus() {
		return htmlUsedModulus;
	}

	public void setHtmlUsedModulus(String htmlUsedModulus) {
		this.htmlUsedModulus = htmlUsedModulus;
	}
}
