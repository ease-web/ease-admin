package com.eab.model.dynTemplate;

public class DynUI {
	String module;
	String uiName;
	String dataTable;
	String searchField;
	String primaryKey;
	String mstView;
	String filterByCom;
	String primaryID;
	String trxSectionId;
	String nameId;
	String templateConcat;
	
	
	public String getTemplateConcat() {
		return templateConcat;
	}

	public void setTemplateConcat(String templateConcat) {
		this.templateConcat = templateConcat;
	}

	public String getNameId() {
		return nameId;
	}

	public void setNameId(String nameId) {
		this.nameId = nameId;
	}
	String hasLangFilter, hasCompFilter;
	
	public boolean hasLangFilter() {
		return "Y".equalsIgnoreCase(hasLangFilter);
	}
	
	public void setLangFilter(String langFilter){
		hasLangFilter = langFilter;
	}
	
	public boolean hasCompFilter() {
		return "Y".equalsIgnoreCase(hasCompFilter);
	}
	
	public void setCompFilter(String langFilter){
		hasCompFilter = langFilter;
	}
	
	public String getModule() {
		return module;
	}
	public void setModule(String module) {
		this.module = module;
	}
	public String getUiName() {
		return uiName;
	}
	public void setUiName(String uiName) {
		this.uiName = uiName;
	}
	public String getDataTable() {
		return dataTable;
	}
	public void setDataTable(String dataTable) {
		this.dataTable = dataTable;
	}
	public String getSearchField() {
		return searchField;
	}
	public void setSearchField(String searchField) {
		this.searchField = searchField;
	}
	public String getPrimaryKey() {
		return primaryKey;
	}
	public void setPrimaryKey(String primaryKey) {
		this.primaryKey = primaryKey;
	}
	public String getMstView() {
		return mstView;
	}
	public void setMstView(String mstView) {
		this.mstView = mstView;
	}
	
	public String getPrimaryID() {
		return primaryID;
	}
	public void setPrimaryID(String primaryID) {
		this.primaryID = primaryID;
	}
	public String getTrxSectionID() {
		return trxSectionId;
	}
	public void setTrxSectionID(String trxSectionId) {
		this.trxSectionId = trxSectionId;
	}
	

	
}
