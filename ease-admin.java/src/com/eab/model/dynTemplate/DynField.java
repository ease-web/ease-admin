package com.eab.model.dynTemplate;

public class DynField {
	String module, 
	id, 
	key, 
	title, 
	type,  
	optionsLookUp, 
	lookUpField, 
	present,
	hints,
	mandatory,
	value;
	
	int seq;
	
	String hasLangFilter, hasCompFilter;
	
	public boolean getMandatory() {
		return "Y".equalsIgnoreCase(mandatory);
	}
	
	public void setMandatory(String mandatory){
		this.mandatory = mandatory;
	}
	
	public boolean hasLangFilter() {
		return "Y".equalsIgnoreCase(hasLangFilter);
	}
	
	public void setLangFilter(String langFilter){
		hasLangFilter = langFilter;
	}
	
	public boolean hasCompFilter() {
		return "Y".equalsIgnoreCase(hasCompFilter);
	}
	
	public void setCompFilter(String langFilter){
		hasCompFilter = langFilter;
	}
	
	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getType() {
		return type;
	}

	public String getHints() {
		return hints;
	}

	public void setHints(String hints) {
		this.hints = hints;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getSeq() {
		return seq;
	}

	public void setSeq(int seq) {
		this.seq = seq;
	}

	public String getOptionsLookUp() {
		return optionsLookUp;
	}

	public void setOptionsLookUp(String optionsLookUp) {
		this.optionsLookUp = optionsLookUp;
	}

	public String getLookUpField() {
		return lookUpField;
	}

	public void setLookUpField(String lookUpField) {
		this.lookUpField = lookUpField;
	}

	public String getPresent() {
		return present;
	}

	public void setPresent(String present) {
		this.present = present;
	}
	
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
