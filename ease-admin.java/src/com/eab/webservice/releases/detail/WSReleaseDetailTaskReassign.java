package com.eab.webservice.releases.detail;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.eab.common.JsonConverter;
import com.eab.common.Log;
import com.eab.json.model.Content;
import com.eab.json.model.Response;
import com.eab.biz.releases.ReleaseDetailMgr;
import com.eab.biz.releases.ReleaseDetailTaskMgr;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import org.json.*;

/**
 * Servlet implementation class WSReleaseDetailTask
 */
@WebServlet("/Releases/Detail/Reassign")
public class WSReleaseDetailTaskReassign extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public WSReleaseDetailTaskReassign() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Do not support GET method
		doPost(request, response);//TODO: to be removed after testing
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = null;
		List<Object> taskList = new ArrayList<>();
		String releaseID = "";
		
		int recordStart = 0;
        int pageSize = 0;
        String releaseDetailId = "";
		String sortBy = "";
		String sortDir = "";
		
		try {				
			ReleaseDetailTaskMgr releaseDetailTask = new ReleaseDetailTaskMgr(request);
			
			// Get Parameter
			try {
				String param = request.getParameter("p0");				
								
				if (param != null && !param.isEmpty()) {
					String resultStr = new String(Base64.getDecoder().decode(param.replace(" ", "+")), "UTF-8");
										
					JSONObject resultJSON = new JSONObject(resultStr);
					   				
					if (resultJSON.has("keyList"))						
						releaseID = (String) resultJSON.get("keyList");		
					
					//Task List
					if (resultJSON.has("rows")){						
						try {
							JSONArray jsonArray = (JSONArray)resultJSON.get("rows");
							
							for(Object obj : jsonArray){
								taskList.add(obj);
							}				
						} catch(Exception e){
							taskList = releaseDetailTask.getTaskList(Integer.parseInt(releaseID));	 	
						}
					}
					String reloadParam = request.getParameter("p10");
					if (reloadParam != null && !reloadParam.isEmpty()) {
						reloadParam = new String(Base64.getDecoder().decode(reloadParam.replace(" ", "+")), "UTF-8");
						JSONObject jsonObj = new JSONObject(reloadParam);
						JSONObject value = jsonObj.getJSONObject("value");
											
						//From release main page
						if (value.has("releaseID"))
							releaseDetailId = value.get("releaseID").toString();
						
						//From task selection
						if (value.has("value"))
							releaseDetailId = (String) value.get("value");
						
						//From table sorting
						if (value.has("id"))
							releaseDetailId = (String) value.get("id");
						
	                    if (value.has("recordStart"))
	                        recordStart = (int) value.get("recordStart");
	                    
	                    if (value.has("pageSize"))
	                        pageSize = (int) value.get("pageSize");
	                    
						if (value.has("sortBy"))
							sortBy = (String) value.get("sortBy");
						
						if (value.has("sortDir"))
							sortDir = (String) value.get("sortDir");
					}
				}
			} catch (Exception e) {
				 Log.error(e);
			}
											
			
			ReleaseDetailMgr release = new ReleaseDetailMgr(request);
			Gson gson = new GsonBuilder().enableComplexMapKeySerialization().setPrettyPrinting().create();
			Response _result = gson.fromJson(releaseDetailTask.ReassignTask(Integer.parseInt(releaseID), taskList), Response.class);
			Response _response = release.getJson(releaseDetailId, false, sortBy, sortDir, recordStart, pageSize, false);
			Content content = _response.getContent();
			JsonObject values = JsonConverter.convertObj(content.getValues()).getAsJsonObject();
			values.addProperty("actionType", "change");
			content.setValues(values);
			_result.setContent(content);
			_result.setPage(null);
        
			//Return Json
			response.setCharacterEncoding("utf-8");
	        response.setContentType("application/json; charset=utf-8");
			response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
			response.setDateHeader("Expires", 0);
			response.addHeader("Cache-Control", "post-check=0, pre-check=0");
			response.setHeader("Pragma", "no-cache");			
			
			out = response.getWriter();
			out.print(gson.toJson(_result));
			out.flush();
			out.close();
		} catch(Exception e) {
			Log.error(e);
		} finally {
			
		}
	}
}

