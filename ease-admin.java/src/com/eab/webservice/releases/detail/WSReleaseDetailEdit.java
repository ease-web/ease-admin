package com.eab.webservice.releases.detail;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import com.eab.biz.releases.ReleaseDetailMgr;
import com.eab.common.Log;
import com.eab.json.model.Response;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@WebServlet("/Releases/Detail/Edit")
public class WSReleaseDetailEdit extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public WSReleaseDetailEdit() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);//TODO: to be removed after testing
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = null;
		Response resp = null;
		String releaseID = "";
		String releaseName = "";
		Date targetDate = null;
		String remark = "";
		Gson gson = new GsonBuilder().enableComplexMapKeySerialization().setPrettyPrinting().create();
		
		try {
			
			try {
				String param = request.getParameter("p0");
				if (param != null && !param.isEmpty()) {
					String resultStr = new String(Base64.getDecoder().decode(param.replace(" ", "+")), "UTF-8");
					JSONObject resultJSON = new JSONObject(resultStr);
					
					if (resultJSON.has("keyList"))
						releaseID = (String)resultJSON.get("keyList");
					
					if (resultJSON.has("releaseName"))
						releaseName = (String)resultJSON.get("releaseName");
															
					try {
						if (resultJSON.has("targetDate")) {
							SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
							targetDate = sdf.parse((String) resultJSON.getString("targetDate"));
						}
					} catch(Exception e) {
						if (resultJSON.has("targetDate"))
							targetDate = new Date(resultJSON.getLong("targetDate"));
					}
					
					if (resultJSON.has("remark"))
						remark = (String)resultJSON.get("remark");					
				}
			} catch (Exception e) {
				 Log.error(e);
			}
						
			ReleaseDetailMgr release = new ReleaseDetailMgr(request);
			resp = release.updateRelease(releaseID, releaseName, targetDate, remark);
				
			//Return Json
			response.setCharacterEncoding("utf-8");
	        response.setContentType("application/json; charset=utf-8");
			response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
			response.setDateHeader("Expires", 0);
			response.addHeader("Cache-Control", "post-check=0, pre-check=0");
			response.setHeader("Pragma", "no-cache");			
			
			out = response.getWriter();
			out.print(gson.toJson(resp));
			out.flush();
			out.close();
		} catch(Exception e) {
			Log.error(e);
		} finally {
			
		}		
	}	
}
