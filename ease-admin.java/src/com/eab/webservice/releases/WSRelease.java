package com.eab.webservice.releases;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Base64;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import com.eab.biz.releases.ReleaseMgr;
import com.eab.common.Log;
import com.eab.json.model.Response;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@WebServlet("/Releases")
public class WSRelease extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public WSRelease() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Do not support GET method
		doPost(request, response);//TODO: to be removed after testing
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = null;
		String statusFilter = "";
		String criteria = "";
        int recordStart = 0;
        int pageSize = 0;
		String sortBy = "";
		String sortDir = "";
		Response resp = null;
		Gson gson = new GsonBuilder().enableComplexMapKeySerialization().setPrettyPrinting().create();
		try {
			
			// Get Parameter
			try {
				String param = request.getParameter("p0");
				if (param != null && !param.isEmpty()) {
					String resultStr = new String(Base64.getDecoder().decode(param.replace(" ", "+")), "UTF-8");
					JSONObject resultJSON = new JSONObject(resultStr);
										
					if (resultJSON.has("statusFilter"))
						statusFilter = (String) resultJSON.get("statusFilter");
										
					if (resultJSON.has("criteria"))
						criteria = (String) resultJSON.get("criteria");
					
                    if (resultJSON.has("recordStart"))
                        recordStart = (int) resultJSON.get("recordStart");
                    
                    if (resultJSON.has("pageSize"))
                        pageSize = (int) resultJSON.get("pageSize");
                    
					if (resultJSON.has("sortBy"))
						sortBy = (String) resultJSON.get("sortBy");
					
					if (resultJSON.has("sortDir"))
						sortDir = (String) resultJSON.get("sortDir");
				}
			} catch (Exception e) {
				 Log.error(e);
			}
						
			//Get Json from release manager
			ReleaseMgr release = new ReleaseMgr(request);
			resp = release.getJson(criteria, statusFilter, recordStart, pageSize, sortBy, sortDir,  "", "", "", null);
   
			//Return Json
			response.setCharacterEncoding("utf-8");
	        response.setContentType("application/json; charset=utf-8");
			response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
			response.setDateHeader("Expires", 0);
			response.addHeader("Cache-Control", "post-check=0, pre-check=0");
			response.setHeader("Pragma", "no-cache");			
			
			out = response.getWriter();
			out.print(gson.toJson(resp));
			out.flush();
			out.close();
		} catch(Exception e) {
			Log.error(e);
		} finally {
			
		}
	}
}
