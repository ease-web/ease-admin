package com.eab.webservice.settings;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.Base64;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

import com.eab.biz.settings.SettingsManager;
import com.eab.common.Log;
import com.eab.json.model.Response;
import com.eab.webservice.dynamic.DynamicFunction;
import com.google.gson.Gson;

/**
 * Servlet implementation class ChangeCompany
 */
@WebServlet("/Settings/ChangeCompany")
public class WSChangeCompany extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public WSChangeCompany() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Log.debug("ChangeCompany");
		Response resp = null;
		String param = request.getParameter("p0");
		Log.debug("ChangeCompany:para: "+param);
		Log.debug("MySettings");
		if (param != null && !param.isEmpty()) {
			String resultStr = new String(Base64.getDecoder().decode(param.replace(" ", "+")), "UTF-8");
			JSONObject resultJSON = new JSONObject(resultStr);			
			SettingsManager  sm = new SettingsManager(request);	
			resp = sm.changeCompany(resultJSON.getString("company"));
		}						
		response.setCharacterEncoding("utf-8");
		response.setContentType("application/json; charset=utf-8");
		response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
		response.setDateHeader("Expires", 0);
		response.addHeader("Cache-Control", "post-check=0, pre-check=0");
		response.setHeader("Pragma", "no-cache");
		PrintWriter out = response.getWriter();
		Gson gson = new Gson();
		out.append(gson.toJson(resp));	
		 
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
