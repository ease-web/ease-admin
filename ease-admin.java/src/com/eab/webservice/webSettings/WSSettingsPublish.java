package com.eab.webservice.webSettings;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Base64;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONObject;

import com.eab.biz.webSettings.WebSettingsManager;
import com.eab.common.Constants;
import com.eab.common.Language;
import com.eab.common.Log;
import com.eab.json.model.Response;
import com.eab.model.profile.UserPrincipalBean;
import com.google.gson.Gson;

@WebServlet("/WebSettings/Publish")
public class WSSettingsPublish extends HttpServlet {

	private static final long serialVersionUID = 1L;
	Language langObj;
	UserPrincipalBean principal;
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public WSSettingsPublish() {
	    super();
	    // TODO Auto-generated constructor stub
	}
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
	    this.principal = (UserPrincipalBean) session.getAttribute(Constants.USER_PRINCIPAL);
		
		String p0 = request.getParameter("p0") ;
		
		String valStr = new String(Base64.getDecoder().decode(p0), "UTF-8");
		
		JSONObject data = new JSONObject(valStr);

	    WebSettingsManager mgr = new WebSettingsManager(request);
	    Response resp = mgr.publish(data);
	    mgr.close();
		response.setCharacterEncoding("utf-8");
		response.setContentType("application/json; charset=utf-8");
		response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
		response.setDateHeader("Expires", 0);
		response.addHeader("Cache-Control", "post-check=0, pre-check=0");
		response.setHeader("Pragma", "no-cache");
		PrintWriter out =  null;
		try {
			out = response.getWriter();
			Gson gson = new Gson();
			String ret = gson.toJson(resp); 
//			Log.debug("end add request:" + ret);
			out.append(ret);
		} catch (IOException e) {
			Log.error(e);
		} finally {
			if (out != null) {
				out.flush();
				out.close();
			}
		}

		//		vc.tableView(request, response);
	}
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
}
