package com.eab.webservice.tasks;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Base64;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.eab.common.Function;
import com.eab.common.Function2;
import com.eab.common.Log;
import com.eab.dao.tasks.Tasks;
import com.eab.model.profile.UserPrincipalBean;
import com.eab.security.LoginExclusion;
import com.eab.biz.tasks.TaskMgr;

import org.json.*;

/**
 * Servlet implementation class WSTask
 */
@WebServlet("/Tasks/Redirect")
public class WSTaskRedirect extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public WSTaskRedirect() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Do not support GET method
		doPost(request, response);//TODO: to be removed after testing
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = null;
		String outputJSON;
		String taskID = "";
		String backFuncCode = "";
		String releaseID = "";
		
		try {
			try {
				String param = request.getParameter("p0");
				
				if (param != null && !param.isEmpty()) {
					String resultStr = new String(Base64.getDecoder().decode(param.replace(" ", "+")), "UTF-8");
					JSONObject resultJSON = new JSONObject(resultStr);
										
					//Task ID
					if (resultJSON.has("taskID")){    
						taskID = Integer.toString((int)resultJSON.get("taskID"));
					}			
					
					//Function code for back page
					if (resultJSON.has("releaseID")){    
						releaseID = (String)resultJSON.get("releaseID");
					}
				}
				
			} catch (Exception e) {
				 Log.error(e);
			}
								
			TaskMgr task = new TaskMgr(request);
			
			UserPrincipalBean p = Function.getPrincipal(request);
			LoginExclusion exclude = new LoginExclusion(p);
			Tasks tasksDAO = new Tasks();

			//Validate Login Session
			Map<String, Object> keys = tasksDAO.getRedirectKeys(p, taskID);
			String func_code = keys.get("func_code").toString();
			Function2 func2 = new Function2();
			String funcUri = func2.getFuncUri(func_code);
			boolean canAccess = false;
			if(!funcUri.equals(""))
				canAccess = exclude.canAccess(request, funcUri); 
			
			if (!canAccess) {				 
				HttpSession session = request.getSession();
				session.setAttribute("errorMessage", "VULNERABILITY_CSRF_TOKEN");
				session.setAttribute("errorStatusCode", 10403);	//403
				response.sendRedirect(request.getContextPath() + "/Error");
				String section = "Request Filter";
				Log.info("{" + section +"} Security Issue (CSRF Token) ....... User will be redirected to error page.");
			}else{
				outputJSON = task.pageRedirect(taskID, backFuncCode, releaseID);
        
			//Return Json
			response.setCharacterEncoding("utf-8");
	        response.setContentType("application/json; charset=utf-8");
			response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
			response.setDateHeader("Expires", 0);
			response.addHeader("Cache-Control", "post-check=0, pre-check=0");
			response.setHeader("Pragma", "no-cache");			
			
			out = response.getWriter();
			out.print(outputJSON);
			out.flush();
			out.close();
			}
		} catch(Exception e) {
			Log.error(e);
		} finally {
			
		}
	}
}
