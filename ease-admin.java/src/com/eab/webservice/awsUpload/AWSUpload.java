package com.eab.webservice.awsUpload;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Base64;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import javax.ws.rs.Consumes;

import com.eab.common.Constants;
import com.eab.common.EnvVariable;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.util.Streams;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONObject;

import com.eab.common.Log;
import com.eab.json.model.Response;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * Servlet implementation class AWSUpload
 */
@WebServlet("/AWSUpload")
@MultipartConfig(fileSizeThreshold=1024*1024*1000,  // 1000 MB 
maxFileSize=1024*1024*1000,       // 50 MB
maxRequestSize=1024*1024*2000)    // 100 MB
public class AWSUpload extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		Log.info("AWSUpload");
		Response resp 	= null;

		JSONObject resultJSON = null;
		JSONObject actionJSON = null;
		String fileName = "";
		byte[] ipaFile = null;
		Long ipaFileLength = null;
		try {
			
			boolean isMultipart = ServletFileUpload.isMultipartContent(request);
			if (isMultipart) {
				DiskFileItemFactory factory = new DiskFileItemFactory();
				// Configure a repository (to ensure a secure temp location is used)
				ServletContext servletContext = this.getServletConfig().getServletContext();
				File repository = (File) servletContext.getAttribute("javax.servlet.context.tempdir");
				factory.setRepository(repository);

				
				// Create a new file upload handler
				ServletFileUpload upload = new ServletFileUpload(factory);

				// Parse the request
				List<FileItem> fileItems = upload.parseRequest(request);
			    for (FileItem item : fileItems) {
			        if (item.isFormField()) {
			        	switch(item.getFieldName()) {
			        		case "fileName":
			        			fileName = item.getString();
			        			break;
			        		default:
			        			break;
			        	}
			        } else {
			        	switch (item.getFieldName()) {
			        		case "ipaFile":
			        			ipaFile = item.get();
			        			ipaFileLength = item.getSize();
			        			break;
			        		default:
			        			break;
			        	}
			        }
			    }
			}
			Log.info("AWSUpload file name : " + fileName);
			Log.info("AWSUpload file length : " + ipaFileLength);
			String base_url = EnvVariable.get("INTERNAL_API_DOMAIN");
			String url = base_url + "/upload/ios";
			int timeout = 60;
			HttpClient client = HttpClientBuilder.create().setDefaultRequestConfig(RequestConfig.custom().setConnectTimeout(timeout * 1000).setConnectionRequestTimeout(timeout * 1000).setSocketTimeout(timeout * 1000).build()).build();
		    HttpPost httpPost = new HttpPost(url);
		 
		    MultipartEntityBuilder builder = MultipartEntityBuilder.create();
			builder.addTextBody("length", ipaFileLength.toString());
		    builder.addTextBody("version", fileName);
		    builder.addBinaryBody("file", ipaFile, ContentType.APPLICATION_OCTET_STREAM, ".ipa");
		 
		    HttpEntity multipart = builder.build();
		    httpPost.setEntity(multipart);
		    
		    try {
		    	Log.info("Call Ease Api ......");
		    	ExecutorService executorService = Executors.newSingleThreadExecutor();
		    	AWSUploadRunable run = new AWSUploadRunable();
		    	run.setHttpPost(httpPost);
		    	executorService.execute(run);
		    	executorService.shutdown();
		    } catch (Exception Error ) {
		    	Log.error(Error);
		    }
		    
			AWSUploadService AWSUploadService = new AWSUploadService(request);
			JsonObject requestBody = new JsonObject();
			resp = AWSUploadService.callPostApi("/retrieve/versions", requestBody, request);

			response.setCharacterEncoding("utf-8");
			response.setContentType	("application/json; charset=utf-8");
			response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
			response.setDateHeader("Expires", 0);
			response.addHeader("Cache-Control", "post-check=0, pre-check=0");
			response.setHeader("Pragma", "no-cache");
			PrintWriter out = response.getWriter();
			Gson gson = new Gson();
			String responseStr = gson.toJson(resp);
			responseStr = responseStr.replaceAll("\"IS_UPLOADING\":\"N\"", "\"IS_UPLOADING\":\"Y\"");
			JsonObject responseJson = new JsonParser().parse(responseStr).getAsJsonObject();
			out.append(responseJson.toString());
		} catch(Exception e) {
			Log.error(e);
			PrintWriter out = response.getWriter();
			JsonObject responseBody = new JsonObject();
			responseBody.addProperty("exceptionCaught", true);
			out.append(responseBody.toString());
		} 
	}

}
