package com.eab.webservice.awsUpload;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.eab.biz.bcp.BcpRequest;
import com.eab.biz.bcp.BcpCommon;
import com.eab.common.*;
import com.eab.couchbase.CBServer;
import com.eab.dao.common.BasicWSClass;
import com.eab.dao.common.Functions;

import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import com.eab.json.model.AppBar;
import com.eab.json.model.AppBarTitle;
import com.eab.json.model.Content;
import com.eab.json.model.Field;
import com.eab.json.model.Option;
import com.eab.json.model.Page;
import com.eab.json.model.Response;
import com.eab.json.model.SearchCondition;
import com.eab.json.model.Template;
import com.eab.model.MenuList;
import com.eab.model.bcp.BcpBean;
import com.eab.model.profile.UserPrincipalBean;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import static com.eab.dao.report.Report.getPickerOptions;
import static com.eab.dao.report.Report.getChannels;

public class AWSUploadService extends BasicWSClass{

	public AWSUploadService(HttpServletRequest request) {
		super(request);
	}
	
	public Response createResp(JSONObject paramJSON, JSONObject action, JsonObject displayValues, HttpServletRequest request) {
		Response resp 		= null;
		
		BcpRequest bcpRequest = null;			
		

		String resultStr	= null;
		String errorInput	= null;	// Validation error	
	
		
		try { 
			Functions funcDao = new Functions();
			// AppBar
//			AppBar appBar = getAppBar("/BCProcessing", AppBarTitle.Types.LABEL, null, funcDao.getFunction("/BCProcessing", request), null, null);
			
			Map<String,Object> appbardata = new HashMap<String,Object>();
			
			Map<String,Object> level1_obj = new HashMap<String,Object>();
			level1_obj.put("id", "/AWSUpload");
			level1_obj.put("types", AppBarTitle.Types.LABEL);
			level1_obj.put("value", "/AWSUpload");
			level1_obj.put("primary", funcDao.getFunction("/AWSUpload", request));
			level1_obj.put("options", null);
			
			
			
			Map<String,Object> appbar_action = new HashMap<String,Object>();
			appbar_action.put("id", "searchString");
			appbar_action.put("type", "searchButton");
			appbar_action.put("value", langObj.getNameDesc("BCP.FUNC.POLICY_NUMBERS").toString());
			appbar_action.put("title", langObj.getNameDesc("SEARCH").toString());
			appbar_action.put("options", null);
			
			level1_obj.put("appbar_action", appbar_action);
			appbardata.put("level1", level1_obj);
			
			AppBar appBar = getAppBar(appbardata);
			Page pageObject 	= new Page("/AWSUpload", "AWSUpload");
			
			// create template
			
			//Prepare Table template
			JSONObject template_obj = new JSONObject();
			
			// List id, List name, List type, List width
			template_obj.put("List1", Arrays.asList("action", "ACTION", "text", "auto"));
			template_obj.put("List2", Arrays.asList("status", "STATUS", "text", "150px"));
			template_obj.put("List3", Arrays.asList("updateBy", "UPD_BY", "text", "210px"));
			template_obj.put("List4", Arrays.asList("upd_date", "LAST_UPDATED", "text", "200px"));
			
			List<Field> tableTemplate = new ArrayList<>();;
			
			List<Field> fields 	= new ArrayList<>();
			
			Template template = new Template("action", tableTemplate, "table", "");
			
			Content content = new Content(template, displayValues, displayValues);
			

			String tokenID 	= Token.CsrfID(request.getSession());
			String tokenKey = Token.CsrfToken(request.getSession());
			
			if(action != null && action.has("action")) {
				if(action.getString("action").equals("CHANGE_PAGE")) {
					Map<String,Object> action_chain = null;
					if (action.has("action_chain")) {
						action_chain = actionChain(action.getJSONObject("action_chain"));
						

					}
					resp = new Response(Constants.ActionTypes.CHANGE_PAGE, action_chain, null, 
							pageObject, tokenID, tokenKey, appBar, content);
				}
				
			} else {
				resp = new Response(Constants.ActionTypes.CHANGE_CONTENT, null, 
						pageObject, tokenID, tokenKey, appBar, content);
			}
	
			
		} catch (Exception e) {
			Log.error(e);
			resp = new Response(Constants.ActionTypes.PAGE_REDIRECT, "/Error", null);
		}

		return resp;
	}
	
	
	public Response callPostApi(String path, JsonObject body, HttpServletRequest request) {
		JsonObject responseBody = new JsonObject();
		try {
				Log.info("Goto callPostApi");
				
				String base_url = EnvVariable.get("INTERNAL_API_DOMAIN");
				String url = base_url + path;

				HttpPost httpPost = new HttpPost(url);
				// Headers
				httpPost.addHeader("content-type", "application/json");				

				// StringEntity
				StringEntity stringEntity = new StringEntity(body.toString());
				httpPost.setEntity(stringEntity);			
				
				HttpClient httpclient = null;
				int timeout = 60;
				httpclient = HttpClientBuilder.create().setDefaultRequestConfig(RequestConfig.custom().setConnectTimeout(timeout * 1000).setConnectionRequestTimeout(timeout * 1000).setSocketTimeout(timeout * 1000).build()).build();
				HttpResponse response = httpclient.execute(httpPost);
				
				String json = EntityUtils.toString(response.getEntity());
				
				responseBody = new JsonParser().parse(json).getAsJsonObject();
				
				return createResp(null, null, responseBody, request);
				
		} catch (Exception ex) {
			return createResp(null, null, responseBody, request);
		}
	}
	
}
