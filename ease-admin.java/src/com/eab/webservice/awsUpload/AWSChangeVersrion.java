package com.eab.webservice.awsUpload;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Base64;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.eab.common.Constants;
import org.json.JSONObject;

import com.eab.biz.bcp.BcpIndex;
import com.eab.common.Log;
import com.eab.json.model.Response;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

/**
 * Servlet implementation class AWSUpload
 */
@WebServlet("/AWSChangeVersion")
public class AWSChangeVersrion extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AWSChangeVersrion() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Log.info("AWSChangeVersion");
		Response resp 	= null;

		
		try {
			JSONObject resultJSON;
			String awsVersionId = null;
			String param = request.getParameter("p0") ;
			if (param != null && !param.isEmpty()) {
				String resultStr = new String(Base64.getDecoder().decode(param.replace(" ", "+")), "UTF-8");
				resultJSON = new JSONObject(resultStr);	
				awsVersionId = resultJSON.getString("version");
			}
			
			
			Log.info("AWS Status: vesrion ID :: "+awsVersionId);
			
			AWSUploadService AWSUploadService = new AWSUploadService(request);
			JsonObject actionJSON = new JsonObject();
			actionJSON.addProperty("versionAwsId", awsVersionId);
			resp = AWSUploadService.callPostApi("/manage/versions", actionJSON, request);

			response.setCharacterEncoding("utf-8");
			response.setContentType	("application/json; charset=utf-8");
			response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
			response.setDateHeader("Expires", 0);
			response.addHeader("Cache-Control", "post-check=0, pre-check=0");
			response.setHeader("Pragma", "no-cache");
			PrintWriter out = response.getWriter();
			Gson gson = new Gson();
			out.append(gson.toJson(resp));
		} catch(Exception e) {
			Log.error(e);
		} 
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
