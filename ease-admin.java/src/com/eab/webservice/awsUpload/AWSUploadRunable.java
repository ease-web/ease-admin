package com.eab.webservice.awsUpload;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import com.eab.common.Log;


public class AWSUploadRunable implements Runnable {	
	private HttpPost httpPost;

	@Override
	public void run() {
		try {
			Log.info("Landing AWS Upload runable");
			int timeout = 60;
			HttpClient client = HttpClientBuilder.create().setDefaultRequestConfig(RequestConfig.custom().setConnectTimeout(timeout * 1000).setConnectionRequestTimeout(timeout * 1000).setSocketTimeout(timeout * 1000).build()).build();
			
			HttpResponse uploadResponse = client.execute(httpPost);
			Log.info("Upload Response" + uploadResponse.getEntity().toString());
		    Log.info("EASE-API finsihed the file upload ...");
		} catch (Exception err) {
			Log.error("AWS Upload Exception");
			Log.error(err);
		}
	}
	
	public void setHttpPost(HttpPost httpPost) {
		this.httpPost = httpPost;
	}
	
	public HttpPost getPostBody() {
		return this.httpPost;
	}
}
