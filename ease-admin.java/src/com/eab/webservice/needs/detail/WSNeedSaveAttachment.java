package com.eab.webservice.needs.detail;


import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Base64;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import com.eab.biz.dynamic.CompanyManager;
import com.eab.biz.dynamic.DynamicManager;
import com.eab.biz.needs.NeedAttachmentMgr;
import com.eab.biz.needs.NeedDetailMgr;
import com.eab.biz.products.ProductCoverMgr;
import com.eab.biz.products.ProductMgr;
import com.eab.biz.upload.UploadManager;
import com.eab.common.Function;
import com.eab.common.Log;
import com.eab.json.model.Response;
import com.eab.json.model.UploadResponse;
import com.eab.model.profile.UserPrincipalBean;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

/**
 * Servlet implementation class saveBNC
 */
@WebServlet("/Needs/Detail/SaveAttachment")
public class WSNeedSaveAttachment extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public WSNeedSaveAttachment() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */ 
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Log.debug("SaveAttachment");
		NeedAttachmentMgr nam = new NeedAttachmentMgr(request);
		Gson gson = new GsonBuilder().enableComplexMapKeySerialization().setPrettyPrinting().create();
		UploadResponse resp = nam.saveAttach();
		response.setCharacterEncoding("utf-8");
		response.setContentType("application/json; charset=utf-8");
		response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
		response.setDateHeader("Expires", 0);
		response.addHeader("Cache-Control", "post-check=0, pre-check=0");
		response.setHeader("Pragma", "no-cache");
		PrintWriter out = response.getWriter();
		NeedDetailMgr needDetailMgr = new NeedDetailMgr(request);
		String outputString="";
		if(resp.isComplete()&&resp.isTeminate()){
			Response _resp = null;
		
			try {
				_resp = needDetailMgr.changePage(resp.getSelectPageId(), resp.getItemCode(), resp.getVersion(), resp.getChannelCode() , ("N0001".equals(resp.getItemCode())||"NeedsSelection".equals(resp.getCurPageId()))?true:false);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			 JSONObject o1 = new JSONObject(gson.toJson(_resp));
			 JSONObject o2 = new JSONObject(gson.toJson(resp));
			 for(String key: o2.keySet()){
				 o1.put(key, o2.get(key));
			 }
			 outputString=o1.toString();
		}else{
			outputString= gson.toJson(resp);    
		}
		  
		
		out.append(outputString);	
		
		
	}

}

 
