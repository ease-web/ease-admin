package com.eab.webservice.bcp;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Base64;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.eab.common.Constants;
import org.json.JSONObject;

import com.eab.biz.bcp.BcpDetail;
import com.eab.common.Log;
import com.eab.json.model.Response;
import com.google.gson.Gson;

/**
 * Servlet implementation class WSBCProcessing
 */
@WebServlet("/BCProcessing/Detail")
public class WSBCProcessingDetail extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public WSBCProcessingDetail() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Log.debug("BCProcessingDetail");
		Response resp 	= null;

		
		try {
			String type = null;
			String policyNumber   = null;
			
			JSONObject resultJSON = null;
			
			if(request.getParameter("p0") instanceof String && request.getParameter("p1") instanceof String) {
				type	= request.getParameter("p0");
				policyNumber   = request.getParameter("p1");
			} else {
				String param = request.getParameter("p0") ;
				if (param != null && !param.isEmpty()) {
					String resultStr = new String(Base64.getDecoder().decode(param.replace(" ", "+")), "UTF-8");
					resultJSON = new JSONObject(resultStr);	
					type = resultJSON.getString("type");
					policyNumber = resultJSON.getString("policyNumber");
				}
			}
			

//			String module 	= request.getParameter("p1");

			
//			JSONObject resultJSON = null;
			
			// Get Parameter
//			if (param != null && !param.isEmpty()) {
//				String resultStr = new String(Base64.getDecoder().decode(param.replace(" ", "+")), "UTF-8");
//				resultJSON = new JSONObject(resultStr);	
//				
//			}
			
//			boolean init = true;
//			if(bcpfun != null) {
//				init = false;
//			}
//
			BcpDetail bcpm = new BcpDetail(request);
			resp = bcpm.getBCPDetail(request, type, policyNumber);

			response.setCharacterEncoding("utf-8");
			response.setContentType	("application/json; charset=utf-8");
			response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
			response.setDateHeader("Expires", 0);
			response.addHeader("Cache-Control", "post-check=0, pre-check=0");
			response.setHeader("Pragma", "no-cache");
			PrintWriter out = response.getWriter();
			Gson gson = new Gson();
			out.append(gson.toJson(resp));
		} catch(Exception e) {
			Log.error(e);
		} 
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
