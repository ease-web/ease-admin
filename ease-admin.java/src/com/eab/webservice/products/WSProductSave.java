package com.eab.webservice.products;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.util.Base64;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import com.eab.biz.common.CommonManager;
import com.eab.biz.dynamic.CompanyManager;
import com.eab.biz.products.ProductBuilderMgr;
import com.eab.biz.products.ProductFormulaMgr;
import com.eab.biz.products.ProductGridMgr;
import com.eab.biz.products.ProductMgr;
import com.eab.common.Constants;
import com.eab.common.Function;
import com.eab.common.Language;
import com.eab.common.Log;
import com.eab.dao.products.Products;
import com.eab.json.model.Response;
import com.eab.json.model.UploadResponse;
import com.eab.model.products.ProductAttachBean;
import com.eab.model.profile.UserPrincipalBean;
import com.eab.model.tasks.TaskBean;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Servlet implementation class WSSave
 */
@WebServlet("/Products/Detail/Save")
public class WSProductSave extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public WSProductSave() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Do not support GET method
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ProductBuilderMgr productBuilderManager = new ProductBuilderMgr(request);
		ProductMgr productMgr = new ProductMgr(request);
		UploadResponse resp = productBuilderManager.saveProduct();
		
		response.setCharacterEncoding("utf-8");
		response.setContentType("application/json; charset=utf-8");
		response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
		response.setDateHeader("Expires", 0);
		response.addHeader("Cache-Control", "post-check=0, pre-check=0");
		response.setHeader("Pragma", "no-cache");
		PrintWriter out = response.getWriter();
		Gson gson = new Gson();
		Response _resp = null;
		try { 
			if(resp.isChange()){
				_resp=productMgr.getProdDetail(resp.getItemCode(), resp.getVersion(), resp.getSelectPageId());
			}else{
				_resp=productMgr.changePage(resp.getCurPageId(), resp.getSelectPageId(), resp.getItemCode(), resp.getVersion());
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		 JSONObject o1 = new JSONObject(gson.toJson(_resp));
		 JSONObject o2 = new JSONObject(gson.toJson(resp));
		 for(String key : o2.keySet()){
			 o1.put(key, o2.get(key));
		 }
		 out.append(o1.toString());	
		

	}

}
