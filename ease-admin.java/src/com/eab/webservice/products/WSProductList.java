package com.eab.webservice.products;

import java.io.IOException;
import java.util.Base64;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import com.eab.biz.products.ProductMgr;
import com.eab.common.Log;
import com.eab.json.model.ProductListCondition;
import com.eab.json.model.Response;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Servlet implementation class WSProductList
 */
@WebServlet("/Products")
public class WSProductList extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public WSProductList() {
		super();

	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			Gson gson = new GsonBuilder().create();
			ProductListCondition productListCondition = null;
			
			// Get Parameter
			String param = request.getParameter("p0");
			if (param != null && !param.isEmpty()) {
				String resultStr = new String(Base64.getDecoder().decode(param.replace(" ", "+")), "UTF-8");
				if (resultStr != null && !resultStr.isEmpty()) {
					productListCondition = gson.fromJson(resultStr, ProductListCondition.class);									
				}
			}

			// ProductManager
			ProductMgr productManager = new ProductMgr(request);
			Response rs = productManager.productList(productListCondition);

			// Print response
			response.setCharacterEncoding("utf-8");
			response.setContentType("application/json; charset=utf-8");
			response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
			response.setDateHeader("Expires", 0);
			response.addHeader("Cache-Control", "post-check=0, pre-check=0");
			response.setHeader("Pragma", "no-cache");

			response.getWriter().append(gson.toJson(rs));
		} catch (Exception e) {
			Log.error(e);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
