package com.eab.webservice;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.eab.common.CookieManager;
import com.eab.common.Log;

/**
 * Servlet implementation class WSClear
 */
@WebServlet("/Clear")
public class WSClear extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public WSClear() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		CookieManager cookie = null;
		try {
			cookie = new CookieManager();
			cookie.removeCookieAtResponse("sessionexpiry", request.getServerName(), "/", response);
			Log.debug("{/Clear} Done.");
		} catch(Exception e) {
			///TODO
		} finally {
			cookie = null;
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
