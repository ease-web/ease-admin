package com.eab.webservice.beneIllus;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import com.eab.biz.beneIllus.BeneIllusMgr;
import com.eab.common.Log;
import com.eab.json.model.Response;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@WebServlet("/BeneIllus/Clone")
public class WSBeneIllusClone extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public WSBeneIllusClone() {
		super();
		// TODO Auto-generated constructor stub
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String paramCompCode = null;
		List<String> paramBiCodes = new ArrayList<>();
		
		// Get Parameter
		try {
			String param = request.getParameter("p0");
			if (param != null && !param.isEmpty()) {
				String resultStr = new String(Base64.getDecoder().decode(param.replace(" ", "+")), "UTF-8");
				JSONObject resultJSON = new JSONObject(resultStr);
				
				if (resultJSON.has("compCode")) {
					paramCompCode = (String) resultJSON.get("compCode");
				}
				JSONArray rows = new JSONArray();
				if (resultJSON.has("rows")) {
					rows = resultJSON.getJSONArray("rows");
				}
				
				// Fetch rows
				for (int i = 0; i < rows.length(); i++) {
					String row = (String) rows.get(i);
					paramBiCodes.add(row);
				}

				BeneIllusMgr biMgr = new BeneIllusMgr(request);
				Response rs = biMgr.cloneBi(paramBiCodes, paramCompCode);
				Gson gson = new GsonBuilder().enableComplexMapKeySerialization().setPrettyPrinting().create();
				
				// Print response
				response.setCharacterEncoding("utf-8");
		        response.setContentType("application/json; charset=utf-8");
				response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
				response.setDateHeader("Expires", 0);
				response.addHeader("Cache-Control", "post-check=0, pre-check=0");
				response.setHeader("Pragma", "no-cache");
				response.getWriter().append(gson.toJson(rs));
			}
		} catch (Exception e) {
			Log.error(e);
		}

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
