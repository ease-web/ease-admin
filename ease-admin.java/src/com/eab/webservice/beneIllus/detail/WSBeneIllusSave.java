package com.eab.webservice.beneIllus.detail;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import com.eab.biz.beneIllus.BeneIllusAttachMgr;
import com.eab.biz.beneIllus.BeneIllusMgr;
import com.eab.common.Log;
import com.eab.json.model.Response;
import com.eab.json.model.UploadResponse;
import com.google.gson.Gson;

/**
 * Servlet implementation class WSSave
 */
@WebServlet("/BeneIllus/Detail/SaveAttachment")
public class WSBeneIllusSave extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public WSBeneIllusSave() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Do not support GET method
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		BeneIllusAttachMgr bam = new BeneIllusAttachMgr(request);
		UploadResponse resp = bam.saveAttach();
		response.setCharacterEncoding("utf-8");
		response.setContentType("application/json; charset=utf-8");
		response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
		response.setDateHeader("Expires", 0);
		response.addHeader("Cache-Control", "post-check=0, pre-check=0");
		response.setHeader("Pragma", "no-cache");
		PrintWriter out = response.getWriter();
		Gson gson = new Gson();
		
		String outputString="";
		if(resp.isComplete()&&resp.isTeminate()){
			BeneIllusMgr biDetailMgr = new BeneIllusMgr(request);
			Response _resp = null;
			try { 
				if(resp.isChange()){
					_resp=biDetailMgr.getBiDetail(resp.getItemCode(), resp.getVersion(), resp.getSelectPageId());
				}else{
					_resp=biDetailMgr.changePage(resp.getCurPageId(), resp.getSelectPageId(), resp.getItemCode(), resp.getVersion());
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			 JSONObject o1 = new JSONObject(gson.toJson(_resp));
			 JSONObject o2 = new JSONObject(gson.toJson(resp));
			 for(String key: o2.keySet()){
				 o1.put(key, o2.get(key));
			 }
			 outputString=o1.toString();
		}else{
			outputString= gson.toJson(resp);    
		}
		  
		
		out.append(outputString);	
	}

}
