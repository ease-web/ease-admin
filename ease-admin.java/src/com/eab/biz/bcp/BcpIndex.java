package com.eab.biz.bcp;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.eab.biz.bcp.BcpRequest;
import com.eab.biz.bcp.BcpCommon;
import com.eab.common.*;
import com.eab.couchbase.CBServer;
import com.eab.dao.common.Functions;

import org.json.JSONObject;

import com.eab.json.model.AppBar;
import com.eab.json.model.AppBarTitle;
import com.eab.json.model.Content;
import com.eab.json.model.Field;
import com.eab.json.model.Option;
import com.eab.json.model.Page;
import com.eab.json.model.Response;
import com.eab.json.model.SearchCondition;
import com.eab.json.model.Template;
import com.eab.model.MenuList;
import com.eab.model.bcp.BcpBean;
import com.eab.model.profile.UserPrincipalBean;
import com.google.gson.JsonObject;

import static com.eab.dao.report.Report.getPickerOptions;
import static com.eab.dao.report.Report.getChannels;

public class BcpIndex extends BcpCommon{

	public BcpIndex(HttpServletRequest request) {
		super(request);
	}



	public List<Map<String,Object>> getApiStatusByPolicyNumber(JSONObject paramJSON) throws SQLException {
		
		String polNum = "";
		String errorInput = null;	// Validation error
		
		if (paramJSON != null){
			if(paramJSON.has("criteria") && paramJSON.get("criteria") instanceof String) {
				polNum = paramJSON.getString("criteria");
			}
			if(paramJSON.has("policyNumber") && paramJSON.get("policyNumber") instanceof String) {
				polNum = paramJSON.getString("policyNumber");
			}
			
			// Validate input polNo
			if (!isValidPolicyNumberFormat(polNum)) {
				polNum = "";
			}
		}
		
		BcpRequest bcpRequest = new BcpRequest(request);

		
		return bcpRequest.getApplicationApiStatus(polNum);
	}
	
	public Response getBcpApiStatus(JSONObject paramJSON, JSONObject action, HttpServletRequest request) {
		Response resp 		= null;
		
		BcpRequest bcpRequest = null;			
		

		String resultStr	= null;
		String errorInput	= null;	// Validation error	
	
		
		try { 
			Functions funcDao = new Functions();
			// AppBar
//			AppBar appBar = getAppBar("/BCProcessing", AppBarTitle.Types.LABEL, null, funcDao.getFunction("/BCProcessing", request), null, null);
			
			Map<String,Object> appbardata = new HashMap<String,Object>();
			
			Map<String,Object> level1_obj = new HashMap<String,Object>();
			level1_obj.put("id", "/BCProcessing");
			level1_obj.put("types", AppBarTitle.Types.LABEL);
			level1_obj.put("value", "/BCProcessing");
			level1_obj.put("primary", funcDao.getFunction("/BCProcessing", request));
			level1_obj.put("options", null);
			
			
			
			Map<String,Object> appbar_action = new HashMap<String,Object>();
			appbar_action.put("id", "searchString");
			appbar_action.put("type", "searchButton");
			appbar_action.put("value", langObj.getNameDesc("BCP.FUNC.POLICY_NUMBERS").toString());
			appbar_action.put("title", langObj.getNameDesc("SEARCH").toString());
			appbar_action.put("options", null);
			
			level1_obj.put("appbar_action", appbar_action);
			appbardata.put("level1", level1_obj);
			
			AppBar appBar = getAppBar(appbardata);
			Page pageObject 	= new Page("/BCProcessing", "BCProcessing");
			
			// create template
			
			//Prepare Table template
			JSONObject template_obj = new JSONObject();
			
			// List id, List name, List type, List width
			template_obj.put("List1", Arrays.asList("action", "ACTION", "text", "auto"));
			template_obj.put("List2", Arrays.asList("status", "STATUS", "text", "150px"));
			template_obj.put("List3", Arrays.asList("updateBy", "UPD_BY", "text", "210px"));
			template_obj.put("List4", Arrays.asList("upd_date", "LAST_UPDATED", "text", "200px"));
			
//			List<Field> tableTemplate = getApiStatusTableTemplate();
			
			List<Field> tableTemplate = getTableTemplate(template_obj);
			
			List<Field> fields 	= new ArrayList<>();
			fields.add(new Field("submit", "button", "BCP.FUNC.SUBMIT"));
			
			Template template = new Template("action", tableTemplate, "table", "BCP.FUNC.ACTION");
			template.setDetailItems(fields);

			if(paramJSON != null) {
				if(paramJSON.has("resubmit") && paramJSON.get("resubmit") instanceof Boolean ) {
					if(paramJSON.getBoolean("resubmit")) {
						bcpRequest = new BcpRequest(request);
						resultStr = bcpRequest.resubmit(paramJSON);
					}
					
				}
			}
			
			//prepare table values				
			List<Map<String,Object>> actionStatusList = getApiStatusByPolicyNumber(paramJSON);

			// translation
			List<String> nameList = new ArrayList<String>();
			template.getNameCodes(nameList);
			appBar.getNameCodes(nameList);
			
			
			BcpBean values = new BcpBean(actionStatusList, resultStr);
			Content content = new Content(template, values, values);

			
//			Map<String, String> nameMap = new HashMap<String, String>();
//			nameMap = Function2.getTranslateMap(request, nameList);
//			template.translate(nameMap);
//			appBar.translate(nameMap);
			

			String tokenID 	= Token.CsrfID(request.getSession());
			String tokenKey = Token.CsrfToken(request.getSession());
			
			if(action != null && action.has("action")) {
				if(action.getString("action").equals("CHANGE_PAGE")) {
					Map<String,Object> action_chain = null;
					if (action.has("action_chain")) {
						action_chain = actionChain(action.getJSONObject("action_chain"));
						

					}
					resp = new Response(Constants.ActionTypes.CHANGE_PAGE, action_chain, null, 
							pageObject, tokenID, tokenKey, appBar, content);
				}
				
			} else {
				resp = new Response(Constants.ActionTypes.CHANGE_CONTENT, null, 
						pageObject, tokenID, tokenKey, appBar, content);
			}
	
			
		} catch (Exception e) {
			Log.error(e);
			resp = new Response(Constants.ActionTypes.PAGE_REDIRECT, "/Error", null);
		}

		return resp;
	}
	
	public Response submitApiRequest(JSONObject paramJSON) {
		Response resp = null;
		
		return resp;
	}
	
	
	
}
