package com.eab.biz.common;

import java.util.Base64;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONObject;

import com.eab.common.Constants;
import com.eab.common.Language;
import com.eab.common.Log;
import com.eab.common.Token;
import com.eab.common.Constants.ActionTypes;
import com.eab.dao.common.Functions;
import com.eab.json.model.AppBarTitle;
import com.eab.json.model.Content;
import com.eab.json.model.Dialog;
import com.eab.json.model.Field;
import com.eab.json.model.Response;
import com.eab.model.profile.UserPrincipalBean;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

public class CommonManager {
	HttpServletRequest request;
	Gson gson;
	public CommonManager(HttpServletRequest request){
		this.request = request;
		this.gson = new GsonBuilder().enableComplexMapKeySerialization().setPrettyPrinting().create();
	}
	
	public void setAppbarPrimary(AppBarTitle appBarTitle, String defaultValue){
		setAppbarPrimary(appBarTitle, defaultValue, null);
	}
	
	
	
	public void setAppbarPrimary(AppBarTitle appBarTitle, String defaultValue, String channelName){
		try{
			String backKeyStr = request.getParameter("p10");
			Functions funcDao = new Functions();
			JSONObject backKey = null;
			
			if(backKeyStr != null && !backKeyStr.isEmpty()) {
				backKeyStr = new String(Base64.getDecoder().decode(backKeyStr.replace(" ", "+")), "UTF-8");
				backKey = new JSONObject(backKeyStr);
			} 
			
			if(backKey == null && (defaultValue != null && !defaultValue.equals(""))){
				backKey = new JSONObject();
				backKey.put("id", defaultValue);
				backKey.put("title", funcDao.getFunction(defaultValue, request));
			}
			
			if(backKey != null){
				String id = backKey.getString("id");
				String title = backKey.has("title")?backKey.getString("title"):funcDao.getFunction(id, request);
				
				if(channelName!=null) 
					title += " (" + channelName + ")";
				
				appBarTitle.setBackPage(null, title);
			}
		}catch(Exception e){
			Log.error(e);
		}
	}
	
	public String backPage(){
		
		String tokenID = Token.CsrfID(request.getSession());
		String tokenKey = Token.CsrfToken(request.getSession());
		return gson.toJson(new Response(ActionTypes.BACK_PAGE, null, null, tokenID, tokenKey, null, null));
	}
	
	public String reloadPage(){
		String tokenID = Token.CsrfID(request.getSession());
		String tokenKey = Token.CsrfToken(request.getSession());
		return gson.toJson(new Response(ActionTypes.RELOAD_PAGE, null, null, tokenID, tokenKey, null, null));
	}
	
	public boolean isChangePage(){
		try{
			String actionTypeStr = request.getParameter("p11");
			if (actionTypeStr != null && !actionTypeStr.isEmpty()) {
				actionTypeStr = new String(Base64.getDecoder().decode(actionTypeStr.replace(" ", "+")), "UTF-8");
				JSONObject actionType =new JSONObject(actionTypeStr);
				if(actionType.getString("action").equals(Constants.ActionTypes.CHANGE_PAGE))
					return true;
			}
		}catch(Exception e){
			Log.error(e);
		}
		return false;
	}
	
	public String returnMsg(String title, String msg){
		// Action
		String action = ActionTypes.SHOW_DIALOG;

		// Token
		String tokenID = Token.CsrfID(request.getSession());
		String tokenKey = Token.CsrfToken(request.getSession());
		Language langObj = new Language(((UserPrincipalBean) request.getSession().getAttribute(Constants.USER_PRINCIPAL)).getUITranslation());
		Dialog dialog = new Dialog(null, title, msg, null, new Field(null, "cancel", langObj.getNameDesc("OK")));
		
		
		return gson.toJson(new Response(action, tokenID, tokenKey, dialog));
	}
}
