package com.eab.biz.dynamic;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import com.eab.common.Constants;
import com.eab.common.Language;
import com.eab.common.Log;
import com.eab.dao.profile.User;
import com.eab.json.model.Dialog;
import com.eab.json.model.Field;
import com.eab.model.dynamic.RecordLockBean;
import com.eab.model.profile.UserBean;
import com.eab.model.profile.UserPrincipalBean;

public class LockManager {
	
	HttpServletRequest request;

	public LockManager(HttpServletRequest request) {
		this.request = request;
	}
	
	public Dialog lockDialog(RecordLockBean rlb){
		// create dialog
		User user = new User();
		String msg = "RECORD.LOCKED.MSG";
		try {
			Language langObj;
			HttpSession session;
			UserPrincipalBean principal;
			session = request.getSession();
			principal = (UserPrincipalBean) session.getAttribute(Constants.USER_PRINCIPAL);
			langObj = new Language(principal.getUITranslation());			
			UserBean ub = user.get(rlb.getUserCode(), request);
			msg =  langObj.getNameDesc("USER.IS.EDITING");
			msg = msg.replace("{1}", langObj.getNameDesc("USER"));
			msg = msg.replace("{2}", ub.getUserName());
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			Log.error(e);
		}
		String dialogId = "lockDialog";
		String dialogTitle = "RECORD.LOCKED";
		Field negative = new Field("ok", "button", "BUTTON.OK");
		Dialog dialog = new Dialog(dialogId, dialogTitle, msg, null, negative, 30);
		dialog.translate(request);
		return dialog;
	}

	

}
