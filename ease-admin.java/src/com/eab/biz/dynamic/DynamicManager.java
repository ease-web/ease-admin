package com.eab.biz.dynamic;

import java.sql.Blob;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.util.StringUtils;

import com.eab.common.Constants;
import com.eab.common.CryptoUtil;
import com.eab.common.Function;
import com.eab.common.Function2;
import com.eab.common.Log;
import com.eab.common.Token;
import com.eab.dao.common.Functions;
import com.eab.dao.dynamic.DynamicDAO;
import com.eab.dao.dynamic.RecordLock;
import com.eab.dao.dynamic.TemplateDAO;
import com.eab.database.jdbc.DBAccess;
import com.eab.database.jdbc.DBManager;
import com.eab.database.jdbc.DataType;
import com.eab.json.model.AppBar;
import com.eab.json.model.AppBarTitle;
import com.eab.json.model.Content;
import com.eab.json.model.Dialog;
import com.eab.json.model.Field;
import com.eab.json.model.Page;
import com.eab.json.model.Response;
import com.eab.json.model.Template;
import com.eab.json.model.Field.Types;
import com.eab.model.dynamic.DynMultiLangField;
import com.eab.model.dynamic.DynamicUI;
import com.eab.model.dynamic.RecordLockBean;
import com.eab.model.profile.UserPrincipalBean;
import com.eab.webservice.dynamic.DynTableController;
import com.eab.webservice.dynamic.DynamicFunction;
import com.google.gson.JsonObject;

public class DynamicManager {
	HttpServletRequest request;
	public DynamicManager(HttpServletRequest request){
		this.request = request;
	}

	public Response returnDialog(String dialogMsg, String dialogId,  String dialogTitle){	 
		String tokenID = Token.CsrfID(request.getSession());
		String tokenKey = Token.CsrfToken(request.getSession());
		Field negative = new Field("ok", "button", "BUTTON.OK");
		Dialog dialog = new Dialog(dialogId, dialogTitle, dialogMsg, null, negative);	
		dialog.translate(request);
		Response resp = new Response(Constants.ActionTypes.SHOW_DIALOG, tokenID, tokenKey, dialog);	
		return resp;
	}
	
	public boolean hasApproveRight(String table, String key, String value){
		boolean hasRight = false;
		 
		UserPrincipalBean principal = Function.getPrincipal(request);
		if(principal.getUserType().equals("A")){
			//is admin
			return true;
		}else{
			DynamicDAO dao = new DynamicDAO();
			String userInDB = dao.getModifyUser(table, key, value);
			if(userInDB != null && !principal.getUserCode().equals(userInDB))
				return true;
		}
		return hasRight;
	}
	public Response getDynTable(){
		DynTableController dc = new DynTableController();
		return dc.getDynTable(request);
	}
	
	public void setAppbarPrimary(AppBarTitle appBarTitle, String defaultValue){
		try{
			String backKey = request.getParameter("p10");
			if((backKey == null || backKey.equals("")) && (defaultValue != null && !defaultValue.equals(""))){
				backKey = defaultValue;
			}
			if(backKey != null && !backKey.equals("")){
				Functions funcDao = new Functions();
				appBarTitle.setBackPage(backKey, funcDao.getFunction(backKey, request));
			}
		}catch(Exception e){
			Log.error(e);
		}
	}
	
	public Response getAddViews(){
		String module = request.getParameter("p1");
		JSONObject template = null;	
		JSONObject values = new JSONObject();
		
		HttpSession session = request.getSession();
		DynamicUI ui = null;	
		
		Response resp = null;
		DynamicDAO dynamic = new DynamicDAO();
		
		if (module != null && !module.isEmpty()) {
			DBManager dbm = null;
			Connection conn = null;	
			try {
				conn = DBAccess.getConnection();
				dbm = new DBManager();
				
				UserPrincipalBean principal = Function.getPrincipal(request);
				
				if (module != null && !module.isEmpty()) {
					ui = session.getAttribute(Constants.SessionKeys.CURRENT_MODULE) != null ? (DynamicUI) session.getAttribute(Constants.SessionKeys.CURRENT_MODULE) : null;
					if (ui == null || !ui.getModule().equals(module)) {
						ui = dynamic.getDynamicUI(module, conn);
						session.setAttribute(Constants.SessionKeys.CURRENT_MODULE, ui);
					}								
					// get dynamic template
					try {
						template = session.getAttribute(Constants.SessionKeys.CURRENT_TEMPLATE) != null ? (JSONObject) session.getAttribute(Constants.SessionKeys.CURRENT_TEMPLATE) : null;
					} catch (Exception ex) {
						template = null;
					}
					if (template == null || !module.equals(template.getString("id"))) {
						TemplateDAO templateDao = new TemplateDAO();
						template = templateDao.getTemplate(module, ui, conn);
						session.setAttribute(Constants.SessionKeys.CURRENT_TEMPLATE, template);
					}
				}
				
				//new record
				JSONObject newTemplate = DynamicFunction.getDetailTemplate(template);
				newTemplate.put("type", "add");
				newTemplate.put("title", (ui.getModule() + ".NEW.TITLE").toUpperCase());
			
				values = emptyData(template.getJSONArray("items"), ui.getDataTable(), dbm, conn);
				String secTitle =  (ui.getModule() + ".NEW.MASTER.TITLE").toUpperCase();
				values.put("primaryName", secTitle);
				values.put("isNew", true);
				
				if(values.has("compCode") && !module.equals("company")){
					values.put("compCode", principal.getCompCode());
				}
								
				JsonObject valuesObject = DynamicFunction.convertJsonToGson(values);
				Template templateOb = DynamicFunction.getTemplateFromJson(request,newTemplate);

				Content content = new Content(templateOb, valuesObject, valuesObject);
												
				Page pageObject = new Page("/Dynamic/Add", ui.getModule());
				
				JSONObject appBarOb = dynamic.genAppBarByViewType(conn, ui, principal, "add");
				AppBar appBar = DynamicFunction.getAppBarFromJson(appBarOb);
				
				if(values.has("primaryName"))
					appBar.getTitle().setSecondary(values.getString("primaryName"));
				
				//translate appbar
				try{
					List<String> barNameList = new ArrayList<String>();
					appBar.getNameCodes(barNameList);
					Map<String, String> lmap = Function2.getTranslateMap(request, barNameList);
					appBar.translate(lmap);
				}catch(Exception e){
					Log.error(e);
				}
				
				// Token
				String tokenID = Token.CsrfID(request.getSession());
				String tokenKey = Token.CsrfToken(request.getSession());
						
				resp = new Response(Constants.ActionTypes.CHANGE_CONTENT, null, pageObject, tokenID, tokenKey, appBar, content);	
				dbm.clear();
			}catch(Exception e){
				Log.debug("getAddViews");
				Log.error(e);
			}finally{
				try {
					conn.close();
				} catch (SQLException e) {
					Log.error(e);
				}
				dbm = null						;
				conn = null;
			}
		}
		return resp;
	}
	private JSONObject emptyData(JSONArray templateItems, String table, DBManager dbm, Connection conn){
		String sql = "select * from " + table;
		ResultSet rs = null;
		JSONObject emptyData = new JSONObject();
		try{
			dbm.clear();
			rs = dbm.select(sql, conn);
			int	size = templateItems.length();
			for(int i = 0; i<size; i++ ){
				try {
					JSONObject item = templateItems.getJSONObject(i);
					String key = item.getString("id");
					
					//text with multi lang
					if(item.has("type") && item.getString("type").endsWith(Types.MULTTEXT)){
						JSONObject multiText = new JSONObject();
						multiText.put("en","");
						multiText.put("zh-tw","");
						emptyData.put(key, multiText);
					}else{
					
						String value = "";
						switch(key){
							case "action":
								value = "N";
								break;
							case "status":
								value = "A";
								break;
							default:
								value = "";
								break;
						}
						emptyData.put(key,value);
					}
				} catch (JSONException e) {
					Log.error(e);
				}
				
			}
		}catch(Exception e){
			
		}	
		return emptyData;
	}
	public Response getDetails(HttpServletRequest request, String path){
		HttpSession session = request.getSession();
		JSONObject template = null, detailTemplate;	
		DynamicDAO dynamic = new DynamicDAO();
		JSONObject values = new JSONObject();
		String id = request.getParameter("p0");
		String module = request.getParameter("p1");
		DynamicUI ui = null;
		
		
		Response resp = null;
		
		if (id != null && !id.isEmpty()) {
			DBManager dbm = null;
			Connection conn = null;	
			try {				
				conn = DBAccess.getConnection();
				dbm = new DBManager();
				
				UserPrincipalBean principal = Function.getPrincipal(request);
				
				if (module != null && !module.isEmpty()) {
					ui = session.getAttribute(Constants.SessionKeys.CURRENT_MODULE) != null ? (DynamicUI) session.getAttribute(Constants.SessionKeys.CURRENT_MODULE) : null;
					if (ui == null || !ui.getModule().equals(module)) {
						ui = dynamic.getDynamicUI(module, conn);
						session.setAttribute(Constants.SessionKeys.CURRENT_MODULE, ui);
					}							
					// get dynamic template
					try {
						template = session.getAttribute(Constants.SessionKeys.CURRENT_TEMPLATE) != null ? (JSONObject) session.getAttribute(Constants.SessionKeys.CURRENT_TEMPLATE) : null;
					} catch (Exception ex) {
						template = null;
					}
					if (template == null || !module.equals(template.getString("id"))) {
						TemplateDAO templateDao = new TemplateDAO();
						template = templateDao.getTemplate(module, ui, conn);
						session.setAttribute(Constants.SessionKeys.CURRENT_TEMPLATE, template);
					}		
					
					detailTemplate = DynamicFunction.getDetailTemplate(template);
					
					dbm.clear();
					String sqlStr = "select * from "+ ui.getDataTable() + " where " + ui.getPrimaryKey() + " = ? ";
					dbm.param(id, DataType.TEXT);
					if(ui.getCompanyFilter() && !"company".equals(ui.getModule())){
						sqlStr += " and " + Constants.COL_COMP_CODE + "=?";
						dbm.param(principal.getCompCode(), DataType.TEXT);
					}
					
					//set values			
					values = getDetailsData(dbm, conn, sqlStr, detailTemplate, ui, id);
					values.put("lang", principal.getLangCode());
					values.put("id", id);
					
					JSONObject newTemplate = new JSONObject(detailTemplate.toString());
					newTemplate.put("type", "details");
					newTemplate.put("title", (module + ".details.title").toUpperCase());
					//parse values			
					JsonObject valuesObject = DynamicFunction.convertJsonToGson(values);
					//make content
					
					Template templateModel = DynamicFunction.getTemplateFromJson(request,newTemplate);
				
					Content content = new Content(templateModel, valuesObject, valuesObject);
													
					Page pageObject = new Page(path, ui.getModule());
					
					
					JSONObject appbarJson = null;
					
					if(isAppBarHasDelete(module, id)){
						appbarJson = dynamic.genAppBarByViewType(conn, ui, principal, "details", null);
					}else{
						appbarJson = dynamic.genAppBarByViewType(conn, ui, principal, "details_no_delete", null);
					}
					AppBar appBar = DynamicFunction.getAppBarFromJson(appbarJson);
					appBar.getTitle().setId("/Dynamic");
					if(values.has("primaryName"))
						appBar.getTitle().setSecondary(values.getString("primaryName"));
					
					//translate appbar
					try{
						List<String> barNameList = new ArrayList<String>();
						appBar.getNameCodes(barNameList);
						Map<String, String> lmap = Function2.getTranslateMap(request, barNameList);
						appBar.translate(lmap);
					}catch(Exception e){
						//cant translate app bar
					}
					
					// Token
					String tokenID = Token.CsrfID(request.getSession());
					String tokenKey = Token.CsrfToken(request.getSession());
					
					resp = new Response(Constants.ActionTypes.CHANGE_CONTENT, null, pageObject, tokenID, tokenKey, appBar, content);	

				}

				dbm.clear();
			} catch (Exception e) {				
				Log.error(e);
				String error = e.getMessage();
				Log.debug("ERROR: " + error);
				resp = new Response(Constants.ActionTypes.PAGE_REDIRECT, "/Error", null);
			} finally {
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					Log.error(e);
				}
				dbm = null;
				conn = null;
			}
		}
		
		return resp;
	}
	
	public List<String> getLangList(){
		List<String> langList = new ArrayList<String>();
		langList.add("en");
		langList.add("zh-tw");		
		return langList;
	}
	
	public JSONObject getDetailsData(DBManager dbm, Connection conn, String sql, JSONObject template, DynamicUI ui, String keyValue){
		String primaryName = ui.getPrimaryKey();
		JSONObject values = new JSONObject();
		JSONArray items = template.getJSONArray("items");
		ResultSet rs = null; 
		try{
			rs = dbm.select(sql, conn);	
			ResultSetMetaData rsmd = rs.getMetaData();
			int columnCount = rsmd.getColumnCount();
			ArrayList<String> columns = new ArrayList<String>();
		    for (int i = 1; i < columnCount + 1; i++) {
				String columnName = rsmd.getColumnName(i).toLowerCase();
				columns.add(columnName);
		    }
		    HashMap<String, Object> map = new HashMap();
		    while (rs.next()) {				
				for (String columnName : columns) {		
					Log.debug("columns:" + columnName );
					switch(columnName){
					case "create_date":
						java.sql.Date time1 = rs.getDate(columnName);
						if(time1 != null)
							values.put("createDate", time1.getTime());
						break;
					case "modify_date":
						java.sql.Date time2 = rs.getDate(columnName);
						if(time2 != null)
							values.put("modifyDate", time2.getTime());
						break;
					case "approv_date":
						java.sql.Date time3 = rs.getDate(columnName);
						if(time3 != null)
							values.put("aproveDate", time3.getTime());
						break;
					case "raw_data":
						Clob clob = rs.getClob(columnName);
						if(clob != null)
							map.put(columnName, Function.convertClobToJson(clob));
						break;
					default:
						String value = rs.getString(columnName);
					    map.put(columnName, value);
					    break;
					}
				}
				//get sec title
				Log.debug("------->primaryName: "+primaryName);
				if(primaryName != null){
					String secTitle = rs.getString(primaryName);
					if(secTitle != null && !secTitle.equals(""))
						values.put("primaryName", secTitle);
				}
		    	// get related user name
				Map<String, String> userMap = new HashMap<String, String>();
				Map<String, String> nameMap = new HashMap<String, String>();

				String createBy = rs.getString("create_by");
				if (createBy != null && !createBy.isEmpty()) {
					userMap.put("createBy", createBy);
					if (!nameMap.containsKey(createBy)) {
						nameMap.put(createBy, createBy);
					}
				}
				
				try {
					String modifyBy = rs.getString("modify_by");
					if (modifyBy != null && !modifyBy.isEmpty()) {
						userMap.put("modifyBy", modifyBy);
						if (!nameMap.containsKey(modifyBy)) {
							nameMap.put(modifyBy, modifyBy);
						}
					}
					
					if (!StringUtils.isEmpty(ui.getTempTable())) { 
						String approveBy = rs.getString("approv_by");
						if (approveBy != null && !approveBy.isEmpty()) {
							userMap.put("approvBy", approveBy);
							if (!nameMap.containsKey(approveBy)) {
								nameMap.put(approveBy, approveBy);
							}
						}
					}
				} catch(Exception ex) {
					// ignore fail case
				}
				if (userMap.size() > 0) {
					String names = "";
					dbm.clear();
					for (String name : nameMap.keySet()) {
						names += (names.isEmpty()?"":", ")+dbm.param(name, DataType.TEXT);
					}
					
					String sqlData = "SELECT user_code, user_name FROM sys_user_mst WHERE user_code in ("+ names +")";
					ResultSet rs2 = dbm.select(sqlData, conn);
					
					while (rs2.next()) {
						nameMap.put(rs2.getString("user_code"), rs2.getString("user_name"));
					}
					for (Map.Entry<String,String> item : userMap.entrySet()) {
						values.put(item.getKey(), nameMap.get(item.getValue()));
					}
					rs2.close();
				}
		    }

			for(int i=0; i<items.length(); i++){			
				JSONObject item = items.getJSONObject(i);
				String  id = item.getString("id");
				String key = item.getString("key").toLowerCase();
				if(item.has("type") && Types.MULTTEXT.equals(item.getString("type"))){
					UserPrincipalBean principal = Function.getPrincipal(request);	
					
					DynamicDAO dm = new DynamicDAO();
					DynMultiLangField dynMultiLangField = dm.getDynMultiLangField(ui.getModule());
					String langTxtTable  = dynMultiLangField.getMstTable();
					values.put(id, getMulText(langTxtTable, ui, keyValue, principal, dbm, conn));
				} else {
					if(item.has("subType") && item.get("subType") != null && "PASSWORD".equals(item.getString("subType").toUpperCase())){
						values.put(id, Constants.DUMMY_PASSWORD);
					}
					else
						values.put(id, map.get(key));
				}							
			}
				

			rs.close();
		}catch(Exception e){
			Log.error("approve----->error");
			Log.error(e);	
		}finally{
			dbm.clear();
		}		
//		Log.error("approve N----->"+values.toString());
		return values;
	}
	
	public JSONObject getMulText(String table, DynamicUI ui, String id, UserPrincipalBean principal, DBManager dbm, Connection conn){
		JSONObject mulText = new JSONObject();
		String compCode = principal.getCompCode();
		dbm.clear();
		try{
			String cond = null;
			if(ui.getCompanyFilter() && !"company".equals(ui.getModule())){
				cond = (cond == null) ? "comp_code = ?" : cond + " and comp_code = ?";
				dbm.param(compCode, DataType.TEXT);
			}
			
			//primary key
			cond = (cond == null) ? ui.getPrimaryKey() + " = ?" : cond + " and " + ui.getPrimaryKey() + " = ?";
			dbm.param(id, DataType.TEXT);
			
			String sql = "select lang_code, name_desc from " + table + " where " + cond;
			ResultSet rs = dbm.select(sql, conn);
			JSONObject langValuesInDB = new JSONObject();
			if(rs != null){
				while(rs.next()){
					langValuesInDB.put(rs.getString("lang_code"), rs.getString("name_desc"));
				}
			}
			
			List<String> langList = getLangList();
			int langSize = langList.size();
			for(int j = 0; j < langSize; j++){
				String lang = langList.get(j);
				if(langValuesInDB.has(lang)){
					mulText.put(lang, langValuesInDB.get(lang));
				}else{
					mulText.put(lang, "");
				}
			}			
			
		}catch(Exception e){
			
		}finally{
			dbm.clear();
		}
		
		return mulText;
		
	}
	
	
	public JSONObject getMstValues(DBManager dbm, Connection conn, DynamicUI ui, JSONObject template, String keyValue){
		Log.debug("get mst data");
		return getValues(dbm, conn, ui, template, keyValue, true);
	}
	
	public JSONObject getTmpValues(DBManager dbm, Connection conn, DynamicUI ui, JSONObject template, String keyValue){
		Log.debug("get tmp data");
		return getValues(dbm, conn, ui, template, keyValue, false);
	}
	
	public JSONObject getValues(DBManager dbm, Connection conn, DynamicUI ui, JSONObject template, String keyValue, boolean isMst){
		String primaryName = ui.getPrimaryName();
		String keyCol = ui.getPrimaryKey();		
		String table = null;
		if(isMst)
			table = ui.getDataTable();
		else
			table = ui.getTempTable();
		
				
		JSONObject values = new JSONObject();
		JSONArray items = template.getJSONArray("items");
		ResultSet rs = null; 
		try{
			String sql = "select * from " + table + " where " + keyCol + " = " + dbm.param(keyValue, DataType.TEXT);
			//check if comp_code is also its primary key

			if(ui.getCompanyFilter() && !"company".equals(ui.getModule())){
				UserPrincipalBean principal = Function.getPrincipal(request);
				sql += " and comp_code = "+dbm.param(principal.getCompCode(), DataType.TEXT);
			}
			
			rs = dbm.select(sql, conn);	
			ResultSetMetaData rsmd = rs.getMetaData();
			int columnCount = rsmd.getColumnCount();
			ArrayList<String> columns = new ArrayList<String>();
		    for (int i = 1; i < columnCount + 1; i++) {
				String columnName = rsmd.getColumnName(i).toLowerCase();
				columns.add(columnName);
		    }
		    HashMap<String, String> map = new HashMap();
		    while (rs.next()) {				
				for (String columnName : columns) {			
					switch(columnName){
						case "create_date":
							java.sql.Date timeC = rs.getDate("create_date");
							if(timeC != null)
								values.put("createDate", timeC.getTime());
							break;
						case "modify_date":
							java.sql.Date timeM = rs.getDate("modify_date");
							if(timeM != null)
								values.put("modifyDate", timeM.getTime());
							break;
						case "approv_date":
							java.sql.Date timeA = rs.getDate("approv_date");
							if(timeA != null)
								values.put("approvDate", timeA.getTime());
							break;
						default:
							String value = rs.getString(columnName);
						    map.put(columnName, value);
						    break;
					}
					
				}
				//get sec title
				Log.debug("------->primaryName: "+primaryName);
				if(primaryName != null){
					String secTitle = rs.getString(primaryName);
					if(secTitle != null && !secTitle.equals(""))
						values.put("primaryName", secTitle);
				}
		    	// get related user name
				Map<String, String> userMap = new HashMap<String, String>();
				Map<String, String> nameMap = new HashMap<String, String>();

				String createBy = rs.getString("create_by");
				if (createBy != null && !createBy.isEmpty()) {
					userMap.put("createBy", createBy);
					if (!nameMap.containsKey(createBy)) {
						nameMap.put(createBy, createBy);
					}
				}
				String modifyBy = rs.getString("modify_by");
				if (modifyBy != null && !modifyBy.isEmpty()) {
					userMap.put("modifyBy", modifyBy);
					if (!nameMap.containsKey(modifyBy)) {
						nameMap.put(modifyBy, modifyBy);
					}
				}
				String approveBy = rs.getString("approv_by");
				if (approveBy != null && !approveBy.isEmpty()) {
					userMap.put("approvBy", approveBy);
					if (!nameMap.containsKey(approveBy)) {
						nameMap.put(approveBy, approveBy);
					}
				}
				if (userMap.size() > 0) {
					String names = "";
					dbm.clear();
					for (String name : nameMap.keySet()) {
						names += (names.isEmpty()?"":", ")+dbm.param(name, DataType.TEXT);
					}
					
					String sqlData = "SELECT user_code, user_name FROM sys_user_mst WHERE user_code in ("+ names +")";
					ResultSet rs2 = dbm.select(sqlData, conn);
					
					while (rs2.next()) {
						nameMap.put(rs2.getString("user_code"), rs2.getString("user_name"));
					}
					for (Map.Entry<String,String> item : userMap.entrySet()) {
						values.put(item.getKey(), nameMap.get(item.getValue()));
					}
					rs2.close();
				}
		    }

				for(int i=0; i<items.length(); i++){		
					JSONObject item = items.getJSONObject(i);
					String  id = item.getString("id");
					String key = item.getString("key");
					
					if(item.has("type") && Types.MULTTEXT.equals(item.getString("type"))){
						UserPrincipalBean principal = Function.getPrincipal(request);	
						DynamicDAO dm = new DynamicDAO();
						DynMultiLangField dynMultiLangField = dm.getDynMultiLangField(ui.getModule());
						String langTxtTable  = isMst ? dynMultiLangField.getMstTable() : dynMultiLangField.getTmpTable();
						values.put(id, getMulText(langTxtTable, ui, keyValue, principal, dbm, conn));
						//break;
					}else{
						if(item.has("subType") && item.get("subType") != null && "PASSWORD".equals(item.getString("subType").toUpperCase())){
							values.put(id, Constants.DUMMY_PASSWORD);
						}
						else
							values.put(id, map.get(key));
					}
														
				}
				

			rs.close();
		}catch(Exception e){
			Log.error("approve----->error");
			Log.error(e);	
		}finally{
			dbm.clear();
		}		
//		Log.error("approve N----->"+values.toString());
		return values;
	}

	public Response getEditView(HttpServletRequest request, String path){
		
		String id = request.getParameter("p0");
		String module = request.getParameter("p1");
		Response resp = null;
		
		RecordLock rlE = new RecordLock();
		RecordLockBean rlbE = rlE.newRecordLockBean(module, id);
		rlE.getRecordLock(request, rlbE);
		if(!rlbE.canAccess()){
			resp = getDetails(request, "details");
			LockManager lm = new LockManager(request);
			resp.setDialog(lm.lockDialog(rlbE));
			return resp;
		}else{
			if(!rlbE.isCurrentUserAccess())
				rlE.addRecordLock(request, rlbE);
		}
		JSONObject template = null;	
		JSONObject values = new JSONObject();
		
		HttpSession session = request.getSession();
		DynamicUI ui = null;	
		Connection conn = null;	
		DBManager dbm = null;
		DynamicDAO dynamic = new DynamicDAO();
		
			try {
				conn = DBAccess.getConnection();
				dbm = new DBManager();
					ui = session.getAttribute(Constants.SessionKeys.CURRENT_MODULE) != null ? (DynamicUI) session.getAttribute(Constants.SessionKeys.CURRENT_MODULE) : null;
					if (ui == null || !ui.getModule().equals(module)) {
						ui = dynamic.getDynamicUI(module, conn);
						session.setAttribute(Constants.SessionKeys.CURRENT_MODULE, ui);
					}
								
					// get dynamic template
					try {
						template = session.getAttribute(Constants.SessionKeys.CURRENT_TEMPLATE) != null ? (JSONObject) session.getAttribute(Constants.SessionKeys.CURRENT_TEMPLATE) : null;
					} catch (Exception ex) {
						template = null;
					}
					if (template == null || !module.equals(template.getString("id"))) {
						TemplateDAO tempalteDAO = new TemplateDAO();
						template = tempalteDAO.getTemplate(module,ui, conn);
						session.setAttribute(Constants.SessionKeys.CURRENT_TEMPLATE, template);
					}		
					
					// get data
					JSONObject newTemplate = new JSONObject(template.toString());
					String sqlMst = "select * from "+ ui.getDataTable() + " where " + ui.getPrimaryKey() + "= ? " ;
					dbm.param(id, DataType.TEXT);
					if(ui.getCompanyFilter() && !"company".equals(ui.getModule())){
						UserPrincipalBean principal = Function.getPrincipal(request);
						sqlMst +=" and comp_code=?";
						dbm.param(principal.getCompCode(), DataType.TEXT);
					}
					
					values = getDetailsData(dbm, conn, sqlMst, newTemplate, ui, id);
					
					values.put("id", id);
					UserPrincipalBean principal = Function.getPrincipal(request);
					values.put("lang", principal.getLangCode());
					values.put("action", "E");
										
					//add title
					newTemplate.put("title", (module+".edit.title").toUpperCase());
					//template type					
					newTemplate.put("type", "edit");
																	
					JsonObject valuesObject = DynamicFunction.convertJsonToGson(values);
					
					Template templateOb = DynamicFunction.getTemplateFromJson(request, newTemplate);
					// refine fields 
					for (Field item : templateOb.getItems()) {
						if ("edit".equals(templateOb.getType()) && !item.isAllowUpdate()) {
							item.setDisabled(true);
						}
					}
					Content content = new Content(templateOb, valuesObject, valuesObject);
					Page pageObject = new Page(path, ui.getModule());
					
					//get appBar
					JSONObject appBarOb = dynamic.genAppBarByViewType(conn, ui, principal, "edit");
					AppBar appBar = DynamicFunction.getAppBarFromJson(appBarOb);
					appBar.getTitle().setId("/Dynamic");
					//add the dyn sec title
					if(values.has("primaryName"))
						appBar.getTitle().setSecondary(values.getString("primaryName"));
					
					//translate appbar
					try{
						List<String> barNameList = new ArrayList<String>();
						appBar.getNameCodes(barNameList);
						Map<String, String> lmap = Function2.getTranslateMap(request, barNameList);
						appBar.translate(lmap);
					}catch(Exception e){
						Log.error(e);
					}
					
					// Token
					String tokenID = Token.CsrfID(request.getSession());
					String tokenKey = Token.CsrfToken(request.getSession());							
					resp = new Response(Constants.ActionTypes.CHANGE_CONTENT, null, pageObject, tokenID, tokenKey, appBar, content);	

				}
			 catch (Exception e) {
			Log.error(e);
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				Log.error(e);
			}
			conn = null;
			dbm = null;
		}	
		return resp;
	}
	public  boolean hasApproveRight(DBManager dbm, DynamicUI ui, String id) throws SQLException {
		boolean hasRight = true;
		UserPrincipalBean principal = Function.getPrincipal(request);
		String userCode = principal.getUserCode();
		DynamicDAO dynamicDao = new DynamicDAO();
		hasRight = dynamicDao.hasApproveRight(dbm, ui, id, userCode);
		return hasRight;
	}
	
	public boolean isAppBarHasDelete(String module, String id){
		boolean result = true;
		if("environment".equals(module)){
			DynamicDAO dy = new DynamicDAO();
			result = dy.isEnvDeleteAble(id);
		}
		return result;
	}
	
	public String encryptPassword(String password){
		UserPrincipalBean principal = Function.getPrincipal(request);
		String compCode = principal.getCompCode();
		CryptoUtil crypto = new CryptoUtil();
		return crypto.encryptUserData(compCode, password);
	}
}
