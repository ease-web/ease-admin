package com.eab.biz.system;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import com.eab.biz.settings.SettingsManager;
import com.eab.common.Email;
import com.eab.common.Function;
import com.eab.common.Log;
import com.eab.dao.profile.User;
import com.eab.dao.system.ResetPasswordTrx;
import com.eab.database.jdbc.DBAccess;
import com.eab.json.model.Result;
import com.eab.model.profile.UserBean;

public class ResetPassword {
	private static final String TEMPLATE_CODE = "PASSWORD_RESET";
	private static final String HOST_URL = "HOST_URL";
	private static final String EMAIL_URL = "EMAIL_RESET_URL";
	private static final String EMAIL_FROM = "EMAIL_ADDR";
	private static final String EMAIL_SUBJ = "EMAIL_RESET_SUBJECT";
	private static final String VAR_USER_NAME = "USER_NAME";
	private static final String VAR_URL = "RESET_URL";
	private static final int TOKON_LENGTH = 30;

	public boolean apply(String userCode, String userName, String email, String langCode, String funcCode) {
		try {
			UserBean userInfo = new UserBean();
			userInfo.setUserCode(userCode);
			userInfo.setUserName(userName);
			userInfo.setEmail(email);

			return apply(userCode, langCode, funcCode, userInfo);
		} catch (Exception e) {
			Log.error(e);
			return false;
		}

	}

	public boolean apply(String userCode, String langCode, String funcCode, HttpServletRequest request) {
		try {
			// Check exist user
			User userObj = new User();
			UserBean userInfo = userObj.get(userCode, request);
			return apply(userCode, langCode, funcCode, userInfo);
		} catch (Exception e) {
			Log.error(e);
			return false;
		}

	}

	public boolean apply(String userCode, String langCode, String funcCode, UserBean userInfo) {
		boolean output = false;
		Email email = null;
		String emailContents = null;
		boolean emailStatus = false;

		try {

			if (userInfo != null) {
				email = new Email();

				// Get Email Template
				emailContents = email.getTemplate(TEMPLATE_CODE, langCode);
				Log.debug("(ResetPassword apply) emailContents=\n" + emailContents);

				// Fill in User Name into Email Template
				emailContents = emailContents.replaceAll(VAR_USER_NAME, userInfo.getUserName());

				// Generate and Fill in Reset Password URL
				String token = Function.GenerateRandomASCII(TOKON_LENGTH);
				String url = SystemParameters.get(HOST_URL) + SystemParameters.get(EMAIL_URL) + token;
				Log.debug("(ResetPassword apply) URL=" + url);
				emailContents = emailContents.replaceAll(VAR_URL, url);

				// Insert Email transaction
				String trxNo = userInfo.getUserCode() + "-" + String.valueOf((new Date()).getTime());
				Log.debug("(ResetPassword apply) trxNo=" + trxNo);
				String emailFrom = SystemParameters.get(EMAIL_FROM);
				Log.debug("(ResetPassword apply) emailFrom=" + emailFrom);
				String emailTo = userInfo.getEmail();
				Log.debug("(ResetPassword apply) emailTo=" + emailTo);
				String subject = SystemParameters.get(EMAIL_SUBJ);
				Log.debug("(ResetPassword apply) subject=" + subject);

				trxNo = email.createTransaction(langCode, emailFrom, emailTo, subject, emailContents, funcCode, TEMPLATE_CODE, userInfo.getUserCode());

				if (trxNo != null && trxNo.length() > 0) {
					// Insert Password Reset transaction
					addRequest(token, userInfo.getUserCode(), langCode, userInfo.getUserCode());

					// Send Email
					// Request sending email
					emailStatus = email.send(trxNo, userInfo.getUserCode());
					/// TODO: any action if email status is false.

					output = true;
				}
			}

		} catch (Exception e) {
			Log.error(e);
		} finally {
			userInfo = null;
			email = null;
		}

		return output;
	}

	public boolean addRequest(String token, String userCode, String langCode, String loginUserCode) {
		boolean output = false;
		boolean hasError = false;
		ResetPasswordTrx dbo = null;

		try {
			dbo = new ResetPasswordTrx();

			// check data is not empty
			if (token == null || token.length() == 0)
				hasError = true;
			else if (userCode == null || userCode.length() == 0)
				hasError = true;
			else if (langCode == null || langCode.length() == 0)
				hasError = true;
			else if (loginUserCode == null || loginUserCode.length() == 0)
				hasError = true;

			// insert data into database
			if (!hasError) {
				// Clear request of same user preventing duplicated request.
				dbo.removeByUserCode(userCode);

				output = dbo.insert(token, userCode, langCode, loginUserCode);
			} else {
				output = false;
			}
		} catch (Exception e) {
			Log.error(e);
		} finally {
			dbo = null;
		}

		return output;
	}

	public HashMap<String, String> checkRequest(String token) {
		String output = null;
		HashMap<String, String> resultSet = null;
		ResetPasswordTrx dbo = null;
		boolean hasError = false;
		HashMap<String, String> reqDS = null;

		try {
			dbo = new ResetPasswordTrx();

			if (token == null || token.length() == 0) {
				hasError = true;
			}

			if (!hasError) {
				reqDS = dbo.selectByToken(token);

				return reqDS;
				/*
				 * if (reqDS != null && reqDS.size() > 0) { output = reqDS.get("USER_CODE"); }
				 */
			}
		} catch (Exception e) {
			Log.error(e);
		} finally {
			dbo = null;
			resultSet = null;
			reqDS = null;
		}

		return null;
	}

	public boolean setNewPassword(HttpServletRequest request, String token, String userCode, String password) throws SQLException {
		boolean output = false;
		Connection conn = null;
		ResetPasswordTrx dbo = null;
		boolean hasError = false;

		try {
			conn = DBAccess.getConnection();
			conn.setAutoCommit(false);
			dbo = new ResetPasswordTrx();

			
			 

			// Update User Password (use same database connection)
			if (!hasError) {
				SettingsManager settingMgr = new SettingsManager(request);
				Result state= settingMgr.changePassword(userCode, password);
				if(state.getSuccess()){				
				// Remove Reset Transaction (use same database connection)				
					if (!hasError && !dbo.removeByToken(token, conn)) { hasError = true; }
				}else
					hasError = true;
			}

			// Update Password History?
			/// TODO: Wait for Change Password function providing method to update user password.

			// Commit or Rollback?
			if (!hasError) {
				conn.commit();
				output = true;
			} else {
				conn.rollback();
			}

		} catch (SQLException e) {
			Log.error(e);
			conn.rollback();
			throw e;
		} catch (Exception e) {
			conn.rollback();
			Log.error(e);
		} finally {
			if (conn != null && !conn.isClosed())
				conn.close();
			conn = null;
		}

		return output;
	}
}
