package com.eab.biz.system;

import java.sql.SQLException;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import com.eab.biz.settings.SettingsManager;
import com.eab.common.Log;
import com.eab.model.system.PasswordCheckType;

public class PasswordPolicy {
	private static int PASSWORD_MIN_LENGTH = 8;
	private static int PASSWORD_REUSE_HISTORY = 2;
	private static final String PASSWORD_ALPHANUMERIC_PATTERN = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,20}$";
	private static final String PASSWORD_ALPHA_PATTERN = "^(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{0,20}$";
	private static final String PASSWORD_NUMERIC_PATTERN = "^(?=.*\\d)[0-9a-zA-Z]{0,20}$";
	/***
	 * ?= �V means apply the assertion condition, meaningless by itself, always work with other combination
	 * Whole combination is means, 6 to 20 characters string with at least one digit, one upper case letter, one lower case letter and one special symbol (��@#$%��). This regular expression pattern is very useful to implement a strong and complex password.
	 * 
	 * Example: ((?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{6,20})
	 * 
	 * (?=.*\d)			#   must contains one digit from 0-9
	 * (?=.*[a-z])		#   must contains one lowercase characters
	 * (?=.*[A-Z])		#   must contains one uppercase characters
	 * (?=.*[@#$%])		#   must contains one special symbols in the list "@#$%"
	 * .				#   match anything with previous condition checking
	 * {6,20}			#   length at least 6 characters and maximum of 20
	 */
	
	public static int validate(HttpServletRequest request, String password, String userCode) {
		
		try {
			if (password != null) {
				if (!checkLength(password)) {																	//Password must contains at least 8 characters.
					return PasswordCheckType.Invalid_Length;
				} else if (!checkLetter(password)) {															//Password must contains upper case and lower case character.
					return PasswordCheckType.Invalid_Contain_Alpha;
				} else if (!checkNumeric(password)) {															//Password must contains numeric digits.
					return PasswordCheckType.Invalid_Contain_Num;
				} else if (!Pattern.compile(PASSWORD_ALPHANUMERIC_PATTERN).matcher(password).matches()) {		//Password must contains upper case and lower case character and numeric digits.
					return PasswordCheckType.Invalid_Contain_AlphaNum;					
				} else if (!checkUser(userCode , password)) {													//New password must not include Login ID pattern.
					return PasswordCheckType.Invalid_UserCode_Pattern;
				} else if (!checkHistory(request, userCode, password)) {										//New password must not be same as Last 2 password in Password History.
					return PasswordCheckType.Invalid_History;
				}
			} else {
				return PasswordCheckType.Invalid_Other;
			}
		} catch(Exception e) {
			Log.error(e);
		}
		
		return PasswordCheckType.Valid;
	}
	
	public static boolean checkLength(String password) {
		if (password.length() < PASSWORD_MIN_LENGTH) {
			return false;
		} else {
			return true;
		}
	}
	
	public static boolean checkLetter(String password) {
		if (!Pattern.compile(PASSWORD_ALPHA_PATTERN).matcher(password).matches()) {
			return false;
		} else {
			return true;
		}
	}

	public static boolean checkNumeric(String password) {
		if (!Pattern.compile(PASSWORD_NUMERIC_PATTERN).matcher(password).matches()) {
			return false;
		} else {
			return true;
		}
	}

	public static boolean checkHistory(HttpServletRequest request, String password, String userCode) throws SQLException {
		SettingsManager settingMgr = new SettingsManager(request);
		if (!settingMgr.checkPasswordHistory(userCode, password)) {
			return false;
		} else {
			return true;
		}
	}

	public static boolean checkUser(String password, String userCode) {
		if (userCode != null && password.toUpperCase().indexOf(userCode.toUpperCase()) >= 0) {
			return false;
		} else {
			return true;
		}
	}
}
