package com.eab.biz.products;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONObject;

import com.eab.common.Constants;
import com.eab.common.JsonConverter;
import com.eab.common.Language;
import com.eab.common.Log;
import com.eab.dao.products.Products;
import com.eab.json.model.Condition;
import com.eab.json.model.Dialog;
import com.eab.json.model.DialogItem;
import com.eab.json.model.Field;
import com.eab.json.model.Option;
import com.eab.json.model.Trigger;
import com.eab.json.model.Field.PresentationTypes;
import com.eab.json.model.Field.Types;
import com.eab.model.profile.UserPrincipalBean;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

public class ProductAlternationOptionMgr {
	HttpServletRequest request;
	UserPrincipalBean principal;
	Language langObj;
	Gson gson;

	public ProductAlternationOptionMgr(HttpServletRequest request) {
		super();
		this.request = request;
		principal = (UserPrincipalBean) request.getSession().getAttribute(Constants.USER_PRINCIPAL);
		langObj = new Language(principal.getUITranslation());
		gson = new GsonBuilder().enableComplexMapKeySerialization().setPrettyPrinting().create();
	}
	
	public void addSection(List<Field> sections, JsonObject values, String prodCode, int version, boolean isReadOnly){
		try{
			Log.debug("---------------------[ProductAlternationOptionMgr - start adding section]--------------------------");
			List<Field> prodAltOptItems = new ArrayList<Field>(); 
		 
			// alternationOption item - smoking
			List<Option> alterOpts = new ArrayList<Option>();
			alterOpts.add(new Option(langObj.getNameDesc("ALLOW_PREMIUM_HOLIDAY"), "allwPremHoliday"));
			alterOpts.add(new Option(langObj.getNameDesc("ALLOW_PARTIAL_WITHDRAW"), "allwPartialWithdraw"));
			alterOpts.add(new Option(langObj.getNameDesc("ALLOW_PARTIAL_RETURN"), "allwPartialReturn"));
			alterOpts.add(new Option(langObj.getNameDesc("ALLOW_PERIODIC_WITHDRAW"), "allwPeriodicWithdraw"));
			alterOpts.add(new Option(langObj.getNameDesc("ALLOW_TOP_UP_PREMIUM"), "allwTopupPrem"));
			alterOpts.add(new Option(langObj.getNameDesc("ALLOW_LUMP_SUM_PREMIUM"), "allwLumpsumPrem"));
			prodAltOptItems.add(new Field("alterOption", Types.CHECKBOXGROUP, Types.NUMBER, langObj.getNameDesc("ALTER_OPTION"), 1100, "100%", PresentationTypes.COLUMN2, false, isReadOnly, alterOpts, null, null));
			
			Field sectionProdAltOption = new Field(Constants.ProductPage.ALT_OPTION, Types.SECTION, null, true, prodAltOptItems);
	
			sections.add(sectionProdAltOption);
			
			Log.debug("---------------------[ProductAlternationOptionMgr - end adding section]--------------------------");
			Log.debug("---------------------[ProductAlternationOptionMgr - start adding values]--------------------------");
			Products prodDAO = new Products();
			String sectionId = Constants.ProductPage.ALT_OPTION;
			JSONObject prodSection = prodDAO.getProdSections(prodCode, principal, version, sectionId).get(sectionId);
			if(prodSection != null){
				values.add(sectionId, JsonConverter.convertObj(prodSection));
			}
			Log.debug("---------------------[ProductAlternationOptionMgr - end adding values]--------------------------");
		}catch(Exception e){
			Log.error(e);
		}
	}
	
	

}
