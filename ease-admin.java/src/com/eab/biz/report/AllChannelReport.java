package com.eab.biz.report;

import com.eab.common.Constants;
import com.eab.common.EnvVariable;
import com.eab.common.Function;
import com.eab.common.Log;
import com.eab.couchbase.CBServer;
import com.eab.dao.report.AllChannelReportDao;
import com.eab.dao.report.Report;
import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.*;

import static com.eab.biz.report.AllChannelReportService.*;
import static com.eab.common.Function.jsonObject2Object;
import static com.eab.common.Function.jsonObject2String;
import static java.util.Objects.isNull;

public class AllChannelReport {

    public String genAllChannelReport(String generatedBy, String startDate, String endDate, String dateType, String selectedChannel ,String paasword ) throws ParseException{

        AllChannelReportService allChannelReportService = new AllChannelReportService();
        AllChannelReportProvider allChannelReportProvider = new AllChannelReportProvider();

        String gatewayURL = EnvVariable.get("GATEWAY_URL");
        String gatewayPort = EnvVariable.get("GATEWAY_PORT");
        String 	gatewayDBName = EnvVariable.get("GATEWAY_DBNAME");
        String gatewayUser = EnvVariable.get("GATEWAY_USER");
        String gatewayPW = EnvVariable.get("GATEWAY_PW");
        CBServer cb = new CBServer(gatewayURL,gatewayPort,gatewayDBName,gatewayUser,gatewayPW);


        ReportUtil reportUtil = new ReportUtil();

        String startTime  = Long.toString(ReportUtil.dateStr2Long(startDate, 0,0,0,0));
        String endTime  = Long.toString(ReportUtil.dateStr2Long(endDate, 23,59,59, 999000000));

        Log.debug("submiss searchStartTime:"+startTime + " searchEndTime:"+endTime);

        ArrayList<String> appIds = new ArrayList<String>();
        ArrayList<String> polIds = new ArrayList<String>();
        ArrayList<String> agentCodes = new ArrayList<String>();
        ArrayList<String> managerCodes = new ArrayList<String>();
        ArrayList<String> fnIds = new ArrayList<String>();

        JSONObject appList = cb.getDoc("_design/main/_view/allChannelAppCases?startkey=[\"01\","+startTime+"]&endkey=[\"01\","+endTime+"]&stale=false");
        JSONArray result = appList!=null?appList.getJSONArray("rows"):new JSONArray();
        JSONObject polFnMap =new JSONObject();

        JSONObject agentHierarchys  = Report.getAgentHierarchys();
        //get IDs
        for (int i = 0; i < result.length(); i++) {
            JSONObject row = result.getJSONObject(i).getJSONObject("value");
            if(row.has("id"))
                appIds.add(row.getString("id"));
            if(row.has("parentId"))
                appIds.add(row.getString("parentId"));
            if(row.has("policyNumber") && row.get("policyNumber") instanceof String)
                polIds.add(row.getString("policyNumber"));
            Object agentCode = jsonObject2Object(row, "agentCode"); 
            if(agentCode!=null){
                agentCodes.add((String)agentCode);
            }

            Object fnId = jsonObject2Object(row, "fnId");
            if(fnId != null){
                fnIds.add((String)fnId);
                if(row.has("policyNumber") && row.get("policyNumber") instanceof String)
                    polFnMap.put(row.getString("policyNumber"), (String)fnId);
            }
        }

        //get Mapping Template
        JSONObject channels = cb.getDoc("channels");
        JSONObject paymentTmpl = cb.getDoc("paymentTmpl");
        JSONObject riskRating  =  cb.getDoc("riskRating");

        //get Required JsonObjects
        JSONArray bundleAppStatus =  cb.getDocsByBatch(cb, "bundleApp","[\"01\",\"application\",\"", appIds, "\"]", "false");
        JSONArray policys = cb.getDocsByBatch(cb, "allChannelApprovalCases","[\"01\",\"", polIds, "\"]", "false");
        JSONArray agents = cb.getDocsByBatch(cb, "agentDetails","[\"01\",\"agentCode\",\"", agentCodes, "\"]", "false");

        for(int i = 0; i < agentCodes.size(); i++){
            JSONObject agent = reportUtil.getValue(agents, "agentCode", agentCodes.get(i) );
            Object managerCode = jsonObject2Object(agent, "managerCode");
            if(managerCode!=null){
                managerCodes.add((String)managerCode);
            }
        }
        JSONArray managers = cb.getDocsByBatch(cb, "agentDetails","[\"01\",\"agentCode\",\"", managerCodes, "\"]", "false");
        JSONArray validBundles = cb.getDocsByBatch(cb, "validBundleById","[\"01\",\"", fnIds, "\"]", "false");


        //handle cka-S
        ArrayList<String> naIds = new ArrayList<String>();
        for( int i =0; i< validBundles.length() ;i++){
            JSONObject validBundle = validBundles.getJSONObject(i);
            Object naId = jsonObject2Object(validBundle, "value.fna.naid");
            if(naId != null && naId instanceof String )
                naIds.add((String)naId);
        }

        JSONArray fnas = cb.getDocsByBatch(cb, "naById","[\"01\",\"", naIds, "\"]", "false");

        JSONObject ckas = new JSONObject();
        ArrayList<String> polNoWithRiskProfile = new ArrayList<String>();
        for( int i =0; i< validBundles.length() ;i++){
            JSONObject validBundle = validBundles.getJSONObject(i);
            Object passCka = null;
            Object naId = jsonObject2Object(validBundle, "value.fna.naid");
            if(naId != null && naId instanceof String ){
                Object fna = ReportUtil.getValue(fnas, "id", (String)naId);
                if(fna != null && fna instanceof JSONObject){
                    Object productType = jsonObject2Object((JSONObject)fna, "productType.prodType");
                    if(productType != null && productType instanceof String ){
                        String prodType = (String) productType;
                       if (prodType.indexOf("investLinkedPlans") > -1)
                           passCka = jsonObject2Object((JSONObject)fna, "ckaSection.owner.passCka");
                        if (prodType.indexOf("investLinkedPlans") > -1 || prodType.indexOf("participatingPlans") > -1)
                            polNoWithRiskProfile.add(validBundle.getString("id"));
                    }

                }
            }
            ckas.put(validBundle.getString("id"), passCka);
        }

        //handle cka-E

        List<Object[]> resultList = new ArrayList<Object[]>();

        if(null != appList){
            JSONArray appListRows = (JSONArray)appList.get("rows");
            int colNum = 87;
            for(int i = 0 ; i < appListRows.length(); i++){
                JSONObject appListRow = (JSONObject) appListRows.get(i);
                String appId = (String) appListRow.get("id");

                JSONObject application = appListRow.getJSONObject("value");

                if(null != application){
                    String policyNumber =  application.has("policyNumber") && application.get("policyNumber") instanceof String ?(String) application.get("policyNumber") : null;

                    JSONObject policy = null;
                    if(policyNumber !=null)
                        policy = reportUtil.getValue(policys, "approvalCaseId", policyNumber);
                    String parentId = application.has("parentId") && application.get("parentId") instanceof String ?(String) application.get("parentId") : null;


                    //status of case filtering
                    String appStatus= reportUtil.getValueString(bundleAppStatus, "applicationDocId", "appStatus", parentId==null?appId:parentId);
                    Object bundleIsValid= reportUtil.getValueItem(bundleAppStatus, "applicationDocId", "isValid", parentId==null?appId:parentId);
                     if(!(appStatus != null && (appStatus.equals("SUBMITTED") ||  (appStatus.equals("APPLYING") && bundleIsValid!=null && (boolean)bundleIsValid)) ))
                         continue;

                    //Channel filtering
                    String channel= jsonObject2String(application,"channel");
                    if(!selectedChannel.equals("all") ){
                        if(!channel.equals(selectedChannel))
                            continue;
                    }

                    //Date range filtering
                    String channelTitle = getChannelValue(channels, channel, "title");
                    String channelType = getChannelValue(channels, channel, "type");
                    boolean isFA =false;
                    boolean isAgnecy =false;
                    if(channelType!=null && channelType.equals("FA"))
                        isFA =true;
                    if(channelType!=null && channelType.equals("AGENCY"))
                        isAgnecy =true;

                    Object caseCreationDate= jsonObject2Object(application,"applicationStartedDate");
                    Object agentCaseSubmissionDate= jsonObject2Object(application,"applicationSubmittedDate");
                    Object supervisorApprovalDate= (policy != null)?jsonObject2Object(policy,"supervisorApproveRejectDate") :null;
                    Object approveRejectDate=  (policy != null)? jsonObject2Object(policy,"approveRejectDate"):null;
                    Object _superApprovalDt = null;
                    Object _fmApprovalDt = null;
                    if(isAgnecy){
                        _superApprovalDt = approveRejectDate;
                    }else if(isFA){
                        _superApprovalDt = supervisorApprovalDate;
                        _fmApprovalDt = approveRejectDate;
                    }
                    long longSuperApproveDate = -1;
                    if(_superApprovalDt != null && _superApprovalDt instanceof Double)
                        longSuperApproveDate = (long)(double)_superApprovalDt;
                    long longApproveRejectDate = -1;
                    if(_fmApprovalDt != null &&_fmApprovalDt instanceof Double)
                        longApproveRejectDate = (long)(double)_fmApprovalDt;
                    long longStartTime = Long.parseLong(startTime);
                    long longEndTime= Long.parseLong(endTime);
                    long longSubmittDate = -1;
                    if(agentCaseSubmissionDate instanceof Double)
                        longSubmittDate = (long)(double)agentCaseSubmissionDate;
                    if(dateType.equals("submitt")) {
                        if (longSubmittDate < 0 || longStartTime > longSubmittDate || longSubmittDate > longEndTime)
                            continue;
                    }

                    long approvalcompareDate =-1;
                    if(selectedChannel.equals("all")){
                        approvalcompareDate = longSuperApproveDate;
                    }else{
                        if(isFA)
                            approvalcompareDate = longApproveRejectDate;
                        else
                            approvalcompareDate = longSuperApproveDate;
                    }

                    if(dateType.equals("approve")) {
                        if (approvalcompareDate < 0 || (longStartTime > approvalcompareDate) || (approvalcompareDate > longEndTime))
                            continue;
                    }

                    //extract data
                    String agentCode= jsonObject2String(application,"agentCode");
                    JSONObject agent = reportUtil.getValue(agents, "agentCode", agentCode );

                    Object caseStatus= allChannelReportService.getStatusOfCaseAllChannel(appStatus, policys, policyNumber);

                    //proposer info
                    Object proposerIc= jsonObject2Object(application,"proposerIc");
                    String proposerDocumentType= jsonObject2String(application,"pDocType");
                    String proposerDocumentTypeOther= jsonObject2String(application,"pDocTypeOther");

                    String pDocType =  mapDocType(proposerDocumentType);
                    if(proposerDocumentType.equals("other"))
                        pDocType =proposerDocumentTypeOther;

                    Object proposerName= jsonObject2Object(application,"proposerName");
                    Object proposerMobileNo= allChannelReportService.handleMobileNumber(jsonObject2String(application,"proposerMobileCountryCode"),jsonObject2Object(application,"proposerMobileNo"));

                    Object proposerEmailAddress= jsonObject2Object(application,"proposerEmailAddress");

                    //insured info
                    Object lifeAssuredIc= jsonObject2Object(application,"lifeAssuredIc");
                    String lifeAssuredDocumentType= jsonObject2String(application,"insDocType");
                    String lifeAssuredDocumentTypeOther= jsonObject2String(application,"insDocTypeOther");
                    String iDocType =  mapDocType(lifeAssuredDocumentType) ;
                    if(lifeAssuredDocumentType.equals("other"))
                        iDocType = lifeAssuredDocumentTypeOther;

                    Object lifeAssuredName= jsonObject2Object(application,"lifeAssuredName");



                    //approval info
                    String agentName= jsonObject2String(application,"agentName");
                    String managerName= "";
                    String managerCode= "";
                    String directorName= "";
                    Object caseApprovedBy= (policy != null)?  jsonObject2Object(policy,"approveRejectManagerName"):null;;
                    Object firmName= (policy != null)?  jsonObject2Object(policy,"faFirmName"):null;

                    Object proxyStartDate= "";
                    Object proxyEndDate= "";

                    if(null != agentCode && !"".equals(agentCode)){
                        JSONObject agentProfile = reportUtil.getValue(agents, "agentCode", agentCode );
                        if(agentProfile != null && agentProfile.has("profileId")) {
                            String profileId = jsonObject2String(agentProfile, "profileId");

                            JSONObject agentUpline = ReportUtil.getAgentUpline(profileId, agentCode, agentHierarchys);
                            if(agentUpline != null ) {
                                if(agentUpline.has("manager"))
                                    managerName = agentUpline.getString("manager");
                                if(agentUpline.has("director"))
                                    directorName = agentUpline.getString("director");
                                if(agentUpline.has("managerCode"))
                                    managerCode = agentUpline.getString("managerCode");

                                if(managers != null) {
                                    JSONObject managerProfile = reportUtil.getValue(managers, "agentCode", managerCode);
                                    if (managerProfile != null && managerProfile.has("profileId")) {
                                        String mgrProfileId = jsonObject2String(managerProfile, "profileId");
                                        JSONObject targetRange = new JSONObject();
                                        if (approvalcompareDate > -1) {
                                            targetRange = AllChannelReportService.getProxyDateRange(mgrProfileId, approvalcompareDate);
                                        }

                                        if (targetRange != null && targetRange.has("DISPLAY_START") && targetRange.has("DISPLAY_END")) {
                                            proxyStartDate = targetRange.getString("DISPLAY_START");
                                            proxyEndDate = targetRange.getString("DISPLAY_END");

                                        }
                                    }
                                }
                            }
                        }
                    }

                    //quotation+basic plan info
                    Object productName="";
                    Object premiumAmountBasicPlan="";
                    Object sumAssuredBasicPlan="";
                    Object planList = jsonObject2Object(application,"plans");
                    if(planList !=  null && planList instanceof JSONArray){
                        JSONObject plan = (JSONObject)((JSONArray)planList).get(0);
                        if(plan != null){
                            productName = jsonObject2Object(plan,"covName.en");
                            sumAssuredBasicPlan = jsonObject2Object(plan,"sumInsured");
                            premiumAmountBasicPlan = jsonObject2Object(plan,"premium");
                        }
                    }
                    String payMode= jsonObject2String(application,"premiumFrequency");
                    String premType = payMode.equalsIgnoreCase("L")?"SP":"RP";
                    String premFreq = ReportUtil.mapPayMode((String)payMode);
                    Object topUp="";//TODO
                    Object rsp= jsonObject2Object(application,"rspAmount");
                    rsp = rsp !=null && rsp.equals("-")? "" : rsp;
                    String rspFrequency= jsonObject2String(application,"rspPayFreq.en");
                    rspFrequency = rspFrequency.equals("-")? "" : rspFrequency;
                    String paymentMethod= ReportUtil.getPaymentMethodName(paymentTmpl,jsonObject2String(application,"paymentMethod"));
                    Object currency= jsonObject2Object(application,"currency");

                    //cka
                    Object cka = null;
                    Integer riskProfile = null;

                    if(jsonObject2String(application,"productLine").equals("IL")){
                        if (polFnMap.has(policyNumber)) {
                            if (ckas.has(polFnMap.getString(policyNumber))) {
                                Object passCka = ckas.get(polFnMap.getString(policyNumber));
                                if (passCka != null && passCka instanceof String) {
                                    String cksStr = (String) passCka;
                                    if (cksStr.equals("Y"))
                                        cka = "Pass";
                                    else if (cksStr.equals("N"))
                                        cka = "Fail";
                                }
                            }
                        }

                    }

                    String approvalStatus = ReportUtil.getValueString(policys, "approvalCaseId", "approvalStatus", policyNumber);

                    if (approvalStatus != null && approvalStatus.equals("A")
                            && polFnMap.has(policyNumber) && polNoWithRiskProfile.indexOf(polFnMap.getString(policyNumber)) > -1) {
                        if(application.has("riskProfile") && application.get("riskProfile") instanceof Integer)
                        riskProfile =  application.getInt("riskProfile");
                    }



                    Object dateOfRoadshow= jsonObject2Object(application,"dateOfRoadshow");
                    Object venue= jsonObject2Object(application,"venue");
                    String jointFieldWorkCase= null;
                    if(policy != null)
                        jointFieldWorkCase = jsonObject2String(policy,"jointFieldWorkCase").equals("Y")? "Yes":"No";
                    Object purposeOfJointFieldWork= (policy != null)? jsonObject2Object(policy,"purposeOfJointFieldWork"):null;;

                    //selected client info
                    Object dob=null;
                    Object educationLevel=null;

                    Object trustedIndividualName=null;
                    Object trustedIndividualMobileNo=null;
                    Object dateOfCall=null;
                    Object personContacted=null;
                    Object mobileNo=null;
                    Object commentsEnetredBySupervsior=null;
                    String langs = "";
                    int selClientReqCnt = 0;
                    //logic for selected client (1. age >=62 2. lang no English  3. edu = "below"
                    Object age = jsonObject2Object(application,"pAge");
                    if(age!=null && age instanceof Integer && (Integer)age>=62)
                        selClientReqCnt++;
                    String lang = jsonObject2String(application,"language");
                    String[] languages = ((String)lang).split(",");
                    ArrayList<String> arrListLang = new ArrayList<String>(Arrays.asList(languages));
                    if(arrListLang.indexOf("en") < 0)
                        selClientReqCnt++;
                    String eduLv = jsonObject2String(application,"educationLevel");
                    if( eduLv.equals("below"))
                        selClientReqCnt++;

                    // only selected client will display below values
                    if( !isFA && selClientReqCnt>=2){
                         dob= jsonObject2Object(application,"dob");
                         educationLevel= getEducationLevel(eduLv);
                         //lang

                         String languageOther= jsonObject2String(application,"languageOther");
                         langs = getLanguage(lang);
                         langs = languageOther !=null ? langs + " "+ languageOther : langs;

                         trustedIndividualName= jsonObject2Object(application,"trustedIndividualName");
                         trustedIndividualMobileNo=  allChannelReportService.handleMobileNumber(jsonObject2String(application,"trustedIndividualMobileCountryCode"),jsonObject2Object(application,"trustedIndividualMobileNo"));
                         dateOfCall= (policy != null)? jsonObject2Object(policy,"dateOfCall"):null;;
                         personContacted = (policy != null)?  jsonObject2Object(policy,"personContacted"):null;;
                         mobileNo = null;
                         if(policy !=null )
                             mobileNo = allChannelReportService.handleMobileNumber(jsonObject2String(policy,"mobileCountryCode"),jsonObject2Object(policy,"mobileNo"));


                         commentsEnetredBySupervsior= (policy != null)? jsonObject2Object(policy,"approveComment"):null;
                    }

                    //CDA:TODO
                    Object approvalStatusByCDA="";
                    Object commentByCDA="";
                    Object approvalDateByCDA="";

                    String rop= jsonObject2String(application,"rop").equals("Y")? "Yes":"No";

                    //hide fields by channel type
                    if(isFA ){
                        riskProfile=null;
                        dateOfRoadshow=null;
                        rop = null;
                        approvalStatusByCDA=null;
                    }
                    if(isAgnecy){
                        firmName =null;
                    }

                    //insert value with mapping title
                    Object[] record = new Object[colNum];
                    int num = 0;
                    record[num++]  = caseStatus;
                    record[num++]  = policyNumber;
                    record[num++]  = caseCreationDate;
                    record[num++]  = agentCaseSubmissionDate;
                    record[num++]  = _superApprovalDt;
                    record[num++]  = _fmApprovalDt;
                    record[num++]  = pDocType;
                    record[num++]  = proposerIc;
                    record[num++]  = proposerName;
                    record[num++]  = iDocType;
                    record[num++]  = lifeAssuredIc;
                    record[num++]  = lifeAssuredName;
                    record[num++]  = proposerMobileNo;
                    record[num++]  = proposerEmailAddress;
                    record[num++]  = agentName;
                    record[num++]  = managerName;
                    record[num++]  = directorName;
                    record[num++]  = caseApprovedBy;
                    record[num++]  = firmName;
                    record[num++]  = channelTitle;
                    record[num++]  = proxyStartDate;
                    record[num++]  = proxyEndDate;
                    record[num++]  = productName;
                    record[num++]  = premiumAmountBasicPlan;
                    record[num++]  = sumAssuredBasicPlan;
                    record[num++]  = premType;
                    record[num++]  = premFreq;
                    record[num++]  = topUp;
                    record[num++]  = rsp;
                    record[num++]  = rspFrequency;
                    record[num++]  = paymentMethod;
                    record[num++]  = currency;
                    record[num++]  = cka ;
                    record[num++]  =  riskProfile != null    ?getRiskProfile(riskRating,(int)riskProfile) :null;
                    record[num++]  = dateOfRoadshow;
                    record[num++]  = venue;
                    record[num++]  = jointFieldWorkCase;
                    record[num++]  = purposeOfJointFieldWork;
                    record[num++]  = dob;
                    record[num++]  = educationLevel;
                    record[num++]  = langs;
                    record[num++]  = trustedIndividualName;
                    record[num++]  = trustedIndividualMobileNo;
                    record[num++]  = dateOfCall;
                    record[num++]  = personContacted;
                    record[num++]  = mobileNo;
                    record[num++]  = commentsEnetredBySupervsior;
                    record[num++]  = approvalStatusByCDA;
                    record[num++]  = commentByCDA;
                    record[num++]  = approvalDateByCDA;
                    record[num++]  = rop;


                    //for riders info
                    if(planList !=  null && planList instanceof JSONArray){
                        JSONArray planListArr =  (JSONArray)planList;
                        for( int j = 1; j< planListArr.length();j++){
                            JSONObject plan = (JSONObject)(planListArr.get(j));
                            if(plan != null){
                                record[num++] = jsonObject2Object(plan,"covName.en");
                                record[num++] = jsonObject2Object(plan,"premium");
                                record[num++] = jsonObject2Object(plan,"sumInsured");
                            }

                        }
                    }
                    //insert null value for remaining fields
                    for(int j = num; j < colNum; j++){
                        record[num++] = null;
                    }

                    resultList.add(record);
                }
            }

            //sort by diff dates depending on date type
            Collections.sort(resultList, new Comparator<Object[]>(){
                @Override
                public int compare(Object[] objArray1, Object[] objArray2){
                    int sortColIdx = 3;
                    String selChannelType = (!selectedChannel.equals("all")) ? getChannelValue(channels, selectedChannel, "type") :"";

                    switch(dateType){
                        case "appStart":
                            sortColIdx = 2;
                            break;
                        case "submitted":
                            sortColIdx = 3;
                            break;
                        case "approve":
                            sortColIdx = 4;
                             if(selChannelType.equals("FA"))
                                 sortColIdx = 5;
                            break;
                    }

//                    boolean is1Empty = (null == objArray1[sortColIdx]);
//                    boolean is2Empty = (null == objArray2[sortColIdx]);
//
//                    if(is1Empty && !is2Empty){
//                        return 1;
//                    }else if(!is1Empty && is2Empty){
//                        return -1;
//                    }else if(is1Empty && is2Empty){
//                        return 0;
//                    }

                    Object obj1 = objArray1[sortColIdx] !=null && objArray1[sortColIdx] instanceof  Double? objArray1[sortColIdx] : (double)-1;
                    Object obj2 = objArray2[sortColIdx]!=null && objArray2[sortColIdx] instanceof  Double? objArray2[sortColIdx] : (double)-1;
                    return ((Double)obj1).compareTo(((Double)obj2));
                }
            });

        }

        Object[][] records = new Object[resultList.size()][];
        for(int i=0;i<resultList.size();i++){
              records[i] = resultList.get(i);
        }
        //time zone handling

        records = allChannelReportService.handleReportTimeFormat(records);


        Calendar cal = Calendar.getInstance();
        Function.changeTimezone(cal, Constants.TIMEZONE_ID);
        String genDate = Function.cal2Str(cal);
        return allChannelReportProvider.createAllChannelReport(records,generatedBy, genDate, paasword);
    }






}
