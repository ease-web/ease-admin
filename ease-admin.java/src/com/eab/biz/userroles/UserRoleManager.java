package com.eab.biz.userroles;

import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.eab.biz.common.CommonManager;
import com.eab.biz.settings.SettingsManager;
import com.eab.common.ActionFailureException;
import com.eab.common.Constants;
import com.eab.common.Function;
import com.eab.common.Function2;
import com.eab.common.Log;
import com.eab.common.Token;
import com.eab.dao.dynamic.DynamicDAO;
import com.eab.dao.dynamic.TemplateDAO;
import com.eab.dao.userroles.UserRolesDAO;
import com.eab.database.jdbc.DBAccess;
import com.eab.database.jdbc.DataType;
import com.eab.json.model.AppBar;
import com.eab.json.model.AppBarTitle;
import com.eab.json.model.Content;
import com.eab.json.model.Field;
import com.eab.json.model.ListContent;
import com.eab.json.model.Page;
import com.eab.json.model.Response;
import com.eab.json.model.SearchCondition;
import com.eab.json.model.Template;
import com.eab.model.MenuList;
import com.eab.model.dynamic.DynamicUI;
import com.eab.model.profile.UserPrincipalBean;
import com.eab.security.SysPrivilegeMgr;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class UserRoleManager {

	String[] ids = new String[]{"compCode", "roleCode", "roleName", "status", "createDate", "createBy", "modifyDate", "modifyBy", "approveDate", "approveBy"};
	String[] keys = new String[]{"comp_code", "role_code", "role_name", "status", "create_date", "create_by", "modify_date", "modify_by", "approv_date", "approv_by"};
	boolean[] mandate = new boolean[]{true, true, true, true, false, false, false, false, false, false};
//	String[] ids = null; 
//	String[] keys = null;

	String[] rightIds = new String[]{"roleCode", "compCode", "funcCode", "status", "createDate", "createBy", "modifyDate", "modifyBy", "approveDate", "approveBy"};
	String[] rightKeys = new String[]{"role_code", "comp_code", "func_code", "status", "create_date", "create_by", "modify_date", "modify_by", "approv_date", "approv_by"};
	
	String module = "userRole";
	Map<String, String> idMap = null;
	Map<String, String> keyMap = null;
	

	Connection conn = null;
	UserRolesDAO dao = null;
	HttpServletRequest request = null;
	HttpSession session = null;
	
	UserPrincipalBean principal = null;

	public UserRoleManager(HttpServletRequest request) {
		super();
		this.request = request;
		this.session = request.getSession();
		try {
			conn = DBAccess.getConnection();
			dao = new UserRolesDAO(conn, ids, keys, rightIds, rightKeys);
		} catch (SQLException e) {
		}		
		
		
		idMap = new HashMap<String, String>();
		keyMap = new HashMap<String, String>();
		
		for (int i = 0; i < this.ids.length; i++) {
			idMap.put(ids[i], keys[i]);
			keyMap.put(keys[i], ids[i]);
		}

		// get principal
		principal = Function.getPrincipal(request);
	}
	
	public Response getTableView() {
		return getTableView(true);
	}
	
	public Response returnTableView() {
		return getTableView(false);
	}
	/**
	 * @param request
	 * @return Response
	 */
	public Response getTableView(boolean hasSearch) {
		SearchCondition cond = null;
		if(hasSearch){
			cond = SearchCondition.RetrieveSearchCondition(request);		
		}
		else{
			//return from details page and has no other param from request have to create.
			cond = new SearchCondition();
		}
			
		Response resp = null;
		String error = null;
		DynamicDAO dynamic = new DynamicDAO();
		//filter by status first	
		String statusStr = cond.getStatusFilter();
		String sortBy = cond.getSortBy();
		String sortDir = cond.getSortDir();

		int offset = cond.getRecordStart();
		int size = cond.getPageSize();

		try {
			
			if (principal == null) {
				throw new ActionFailureException("Session time out.", true, true);
			}
			String compCode = principal.getCompCode();

			String templateType = "table";
			List<String> additionalNames = Arrays.asList("MT.FOOTER");
			List<String> names = new ArrayList<String>();
			names.addAll(additionalNames);
			
			// get dynamic ui
			DynamicUI ui = session.getAttribute(Constants.SessionKeys.CURRENT_MODULE) != null ? (DynamicUI) session.getAttribute(Constants.SessionKeys.CURRENT_MODULE) : null;
			if (ui == null || !module.equals(ui.getModule())) {
				ui = dynamic.getDynamicUI(module, conn);
				session.setAttribute(Constants.SessionKeys.CURRENT_MODULE, ui);
			}
						
			// get dynamic template
			Template template = null;
			JsonObject tempJson = null;
			Object cTemp = session.getAttribute(Constants.SessionKeys.CURRENT_TEMPLATE);
			if (cTemp != null && cTemp instanceof JsonObject) {
				tempJson = (JsonObject) cTemp;
				Gson gson = new Gson();
				template = gson.fromJson(tempJson, Template.class);
			}
			
			if (template == null || !module.equals(template.getId())) {
				tempJson = new JsonObject();
				TemplateDAO tempalteDao =  new TemplateDAO();
				template = tempalteDao.getFields(ui, principal.isRegional(), principal.getUserCode(), compCode, conn, tempJson);
				session.setAttribute(Constants.SessionKeys.CURRENT_TEMPLATE, tempJson);
			}
			
			template.setType(templateType);
			
//			initKeyMap(tempJson);
			// convert sort by
			if (sortBy == null || sortBy.isEmpty()) {
				sortBy = "modify_date";
				sortDir = "D";
				cond.setSortBy("modifyDate");
				cond.setSortDir("D");
			} else {
				if (idMap.containsKey(sortBy)) { 
					sortBy = idMap.get(sortBy);
				}
			}
			
			// get granded uri list to filter the app bar
			SysPrivilegeMgr spMgr = new SysPrivilegeMgr();
			List<String> uriList = new ArrayList<String>();
			spMgr.getGrandedURIList(principal.getRoleCode(), uriList);
			
			// get app bar
			AppBar appBar = dynamic.genAppBar(conn, ui, templateType, uriList);
			
			List<Field> multiSelect =  dynamic.getActions(conn, ui, "multiSelect", uriList);
			
			if (multiSelect != null) {	
				appBar.getActions().add(multiSelect);
			}
			appBar.setValues(cond);

			// translate
			appBar.getNameCodes(names);
			template.getNameCodes(names);
			Map<String, String> lmap = Function2.getTranslateMap(request, names);
			appBar.translate(lmap);
			template.translate(lmap);
			
			JsonObject langMap = new JsonObject();
			for (String name : additionalNames) {
				langMap.addProperty(name, lmap.get(name));
			}			

			
			// get result
			String filterKey = cond.getSearchKey();
			String curFilterKey = session.getAttribute(Constants.SessionKeys.FILTER_KEY) != null ? (String) session.getAttribute(Constants.SessionKeys.FILTER_KEY) : null;

			JsonArray result = null;
			if (offset > 0 && filterKey.equals(curFilterKey)) {
				Object temp = session.getAttribute(Constants.SessionKeys.SEARCH_RESULT);
				result = temp != null && temp instanceof JsonArray ? (JsonArray) temp: null;
			} 

			if (result == null) {
//				boolean isAdmin = Function.isCurrentUserAdmin(request);
				result = dao.getRoleMaster(cond, statusStr, sortBy, sortDir, tempJson.getAsJsonArray("items"), ui, compCode);
				session.setAttribute(Constants.SessionKeys.FILTER_KEY, filterKey);
				session.setAttribute(Constants.SessionKeys.SEARCH_RESULT, result);
			}

			JsonArray shortedList = new JsonArray();
			for (int j = offset; j< offset + size && j<result.size(); j++) {
				shortedList.add(result.get(j));
			}
			ListContent list = new ListContent(shortedList, result.size());
			list.setIsMore(offset > 0);

			Content content = new Content(template, list);
			content.setLangMap(langMap);
			
			// Page
			Page page = new Page("/UserRoles", lmap.get("USERROLE.TITLE"));

			// Token
			String tokenID = Token.CsrfID(session);
			String tokenKey = Token.CsrfToken(session);

			if (cond.isUseDefault()) {
				resp = new Response(Constants.ActionTypes.CHANGE_PAGE, null, page, tokenID, tokenKey, appBar, content);
			} else {
				resp = new Response(Constants.ActionTypes.CHANGE_CONTENT, null, page, tokenID, tokenKey, appBar, content);
			}
			
		} catch (Exception ex) {
			Log.error(ex);
			error = ex.getMessage();
			resp = new Response(Constants.ActionTypes.PAGE_REDIRECT, "/Error", null);
		} finally {
			try {
				if (conn != null && !conn.isClosed()) conn.close();
			} catch (SQLException e) {
				// ignorable
			} finally {
				conn = null;
			}
		}

		return resp;
	}
	
	public Response genAddNew() {
		Response resp = null;
		String error = null;
		DynamicDAO dynamic = new DynamicDAO();
		try {
			
			if (principal == null) {
				throw new ActionFailureException("Session time out.", true, true);
			}
			String compCode = principal.getCompCode();

			String templateType = "add";

//			List<String> additionalNames = Arrays.asList("MT.FOOTER");
			List<String> names = new ArrayList<String>();
//			names.addAll(additionalNames);
			
			// get dynamic ui
			DynamicUI ui = session.getAttribute(Constants.SessionKeys.CURRENT_MODULE) != null ? (DynamicUI) session.getAttribute(Constants.SessionKeys.CURRENT_MODULE) : null;
			if (ui == null || !module.equals(ui.getModule())) {
				ui = dynamic.getDynamicUI(module, conn);
				session.setAttribute(Constants.SessionKeys.CURRENT_MODULE, ui);
			}
			
			// get dynamic template
			Template template = null;
			JsonObject tempJson = null;
			Object cTemp = session.getAttribute(Constants.SessionKeys.CURRENT_TEMPLATE);
			if (cTemp != null && cTemp instanceof JsonObject) {
				tempJson = (JsonObject) cTemp;
				Gson gson = new Gson();
				template = gson.fromJson(tempJson, Template.class);
			}
			
			if (template == null || !module.equals(template.getId())) {
				tempJson = new JsonObject();
				TemplateDAO tempalteDao =  new TemplateDAO();
				template = tempalteDao.getFields(ui, principal.isRegional(), principal.getUserCode(), compCode, conn, tempJson);
				session.setAttribute(Constants.SessionKeys.CURRENT_TEMPLATE, tempJson);
			}
			
			template.setType(templateType);
			template.setTitle("ROLE.ADD.TITLE");
			
//			initKeyMap(tempJson);

			for (Field item : template.getItems()) {
				if ("rights".equals(item.getId())) {
					item.setItems(dao.genRightMap(null));
					break;
				}
			}

			// get granded uri list to filter the app bar
			SysPrivilegeMgr spMgr = new SysPrivilegeMgr();
			List<String> uriList = new ArrayList<String>();
			spMgr.getGrandedURIList(principal.getRoleCode(), uriList);
			
			// get app bar
			AppBar appBar = dynamic.genAppBar(conn, ui, templateType, uriList);

			appBar.getTitle().setSecondary("ROLE.NEW_ROLE");
			
			// translate
			template.getNameCodes(names);
			appBar.getNameCodes(names);
			Map<String, String> lmap = Function2.getTranslateMap(request, names);
			template.translate(lmap);
			appBar.translate(lmap);
			
			
//			Map<String, String> langMap = new HashMap<String, String>();
//			for (String name : additionalNames) {
//				langMap.put(name, lmap.get(name));
//			}
//			
			// get details
			Content content = new Content(template, null);
			
			JsonObject changedValues = new JsonObject();
			changedValues.addProperty("isNew", true);
			
			content.setChangedValues(changedValues);
			
			// Page
			Page page = new Page("/UserRoles/Details", "User Role Master");

			// Token
			String tokenID = Token.CsrfID(session);
			String tokenKey = Token.CsrfToken(session);

			resp = new Response(Constants.ActionTypes.CHANGE_PAGE, null, page, tokenID, tokenKey, appBar, content);
			
		} catch (Exception ex) {
			Log.error(ex);
			error = ex.getMessage();
			resp = new Response(Constants.ActionTypes.PAGE_REDIRECT, "/Error", null);
		} finally {
			try {
				if (conn != null && !conn.isClosed()) conn.close();
			} catch (SQLException e) {
				// ignorable
			} finally {
				conn = null;
			}

		}

		return resp;
	}

	public Response getDetailView() {
		Response resp = null;
		String error = null;
		DynamicDAO dynamic = new DynamicDAO();
		String rid = request.getParameter("p0");
	
		try {
			if (rid == null || rid.isEmpty()) {
			//	Log.debug("load user role details by id:" + rid);
				throw new Exception("not user role id found: " + rid);
			}
			
			String templateType = null;
			
			if (principal == null) {
				throw new ActionFailureException("Session time out.", true, true);
			}
			String compCode = principal.getCompCode();

			List<String> additionalNames = Arrays.asList("DYN.DELETE.DATEBY", "DYN.CURRENT.DATEBY", "DYN.CHANGED.DATEBY", "DYN.CURRENT", "DYN.CHANGED");
			List<String> names = new ArrayList<String>();
			names.addAll(additionalNames);
			
			// get dynamic UI
			DynamicUI ui = session.getAttribute(Constants.SessionKeys.CURRENT_MODULE) != null ? (DynamicUI) session.getAttribute(Constants.SessionKeys.CURRENT_MODULE) : null;
			if (ui == null || !module.equals(ui.getModule())) {
				ui = dynamic.getDynamicUI(module, conn);
				session.setAttribute(Constants.SessionKeys.CURRENT_MODULE, ui);
			}
			
			// get dynamic template
			Template template = null;
			JsonObject tempJson = null;
			Object cTemp = session.getAttribute(Constants.SessionKeys.CURRENT_TEMPLATE);
			if (cTemp != null && cTemp instanceof JsonObject) {
				tempJson = (JsonObject) cTemp;
				Gson gson = new Gson();
				template = gson.fromJson(tempJson, Template.class);
			}
			
			if (template == null || !module.equals(template.getId())) {
				tempJson = new JsonObject();
				TemplateDAO tempalteDao =  new TemplateDAO();
				template = tempalteDao.getFields(ui, principal.isRegional(), principal.getUserCode(), compCode, conn, tempJson);
				session.setAttribute(Constants.SessionKeys.CURRENT_TEMPLATE, tempJson);
			}
			
//			initKeyMap(tempJson);
			// get record values
			List<JsonObject> roles = dao.getRoleById(ui, tempJson.getAsJsonArray("items"), rid, compCode);
			
			JsonObject roleMst = null;
			JsonObject roleTmp = null;
			
			String Action = null;
			String UpdBy = null;
			
			for (JsonObject role : roles) {
				if (role.has("action") && !role.get("action").isJsonNull()) {
					String action = role.get("action").getAsString();
					if (action != null && !action.isEmpty()) {
						Action = action;
						if (role.has("updateBy") && !role.get("updateBy").isJsonNull()) {
							UpdBy = role.get("updateBy").getAsString();
							role.remove("updateBy");
						}
						if (roleTmp != null) {
							throw new Exception("retrieve corrupted");
						}
						roleTmp = role;
					} else {
						if (roleMst != null) {
							throw new Exception("retrieve corrupted");
						}
						roleMst = role;
					}
				} else {
					if (roleMst != null) {
						throw new Exception("retrieve corrupted");
					}
					roleMst = role;
				}
			}
			
			if (roleMst == null && roleTmp == null) {
				throw new Exception("retrieve corrupted");
			}
			
			// AppBar - Title
			String secondary = "No name";
			if (roleMst != null && roleMst.has("roleName") && !roleMst.get("roleName").isJsonNull()) { 
				secondary = roleMst.get("roleName").getAsString();
			} else if (roleTmp != null && roleTmp.has("roleName") && !roleTmp.get("roleName").isJsonNull()) {
				secondary = roleTmp.get("roleName").getAsString();
			}
			
			AppBarTitle appBarTitle = new AppBarTitle("levelTwo", "", null, secondary);
			CommonManager cMgr = new CommonManager(request);
			cMgr.setAppbarPrimary(appBarTitle, "/UserRoles");
			
			List<List<Field>> appBarActions = new ArrayList<>();
			String pageTitle = "";

			// get granded uri list to filter the app bar
			SysPrivilegeMgr spMgr = new SysPrivilegeMgr();
			List<String> uriList = new ArrayList<String>();
			spMgr.getGrandedURIList(principal.getRoleCode(), uriList);
			
			// AppBar - Actions
			if (Action == null || Action.isEmpty()) {
				templateType = "details";
				List<Field> acts = dynamic.getActions(conn, ui, templateType, uriList);
				if (acts != null) {
					appBarActions.add(acts);
				}
				pageTitle = "ROLE.VIEW.TITLE";
			} else if ("N".equals(Action)) {
				templateType = "add";
				if ("A".equals(principal.getUserType()) || !principal.getUserCode().equals(UpdBy)) {
					List<Field> aacts = dynamic.getActions(conn, ui, "approve", uriList);
					if (aacts != null) {
						appBarActions.add(aacts);
					}
				}
				List<Field> acts = dynamic.getActions(conn, ui, templateType, uriList);
				if (acts != null) {
					appBarActions.add(acts);
				}
				pageTitle = "ROLE.ADD.TITLE";
			} else if ("E".equals(Action)) {
				templateType = "edit";
				if ("A".equals(principal.getUserType()) || !principal.getUserCode().equals(UpdBy)) {
					List<Field> aacts = dynamic.getActions(conn, ui, "approve", uriList);
					if (aacts != null) {
						appBarActions.add(aacts);
					}else{
						Log.debug("no acitons");
					}
				}
				List<Field> acts = dynamic.getActions(conn, ui, templateType, uriList);
				if (acts != null) {
					appBarActions.add(acts);
				}
				pageTitle = "ROLE.EDIT.TITLE";
			} else if ("D".equals(Action)) {
				templateType = "delete";
				if ("A".equals(principal.getUserType()) || !principal.getUserCode().equals(UpdBy)) {
					List<Field> aacts = dynamic.getActions(conn, ui, "approve", uriList);
					if (aacts != null) {
						appBarActions.add(aacts);
					}
				}
				List<Field> acts = dynamic.getActions(conn, ui, templateType, uriList);
				if (acts != null) {
					appBarActions.add(acts);
				}
				pageTitle = "ROLE.DEL.TITLE";
			}
			
			// refine fields 
			for (Field item : template.getItems()) {
				if ("rights".equals(item.getId())) {
					item.setItems(dao.genRightMap(null));
				}
				if ("edit".equals(templateType) && !item.isAllowUpdate()) {
					item.setDisabled(true);
				}
			}
			
			// AppBar
			AppBar appBar = new AppBar(appBarTitle, appBarActions);
			
			template.setTitle(pageTitle);
			template.setType(templateType);

			// translate
			appBar.getNameCodes(names);
			template.getNameCodes(names);
			Map<String, String> lmap = Function2.getTranslateMap(request, names);
			appBar.translate(lmap);
			template.translate(lmap);

			JsonObject langMap = new JsonObject();
			for (String name : additionalNames) {
				langMap.addProperty(name, lmap.get(name));
			}			
			
			Log.debug("mst:" + (roleMst != null && !roleMst.isJsonNull()?roleMst:""));
			Log.debug("tmp:" + (roleTmp != null && !roleTmp.isJsonNull()?roleTmp:""));

			Content content = new Content(template, roleMst, roleTmp);
			content.setLangMap(langMap);
			
			// Page
			Page page = new Page("/UserRoles/Details", "User Role Master");
	
			// Token
			String tokenID = Token.CsrfID(session);
			String tokenKey = Token.CsrfToken(session);
	
			
			resp = new Response(Constants.ActionTypes.CHANGE_PAGE, null, page, tokenID, tokenKey, appBar, content);
			
		} catch (Exception ex) {
			Log.error(ex);
			error = ex.getMessage();
			resp = new Response(Constants.ActionTypes.PAGE_REDIRECT, "/Error", null);
		} finally {
			try {
				if (conn != null && !conn.isClosed()) conn.close();
			} catch (SQLException e) {
				// ignorable
			} finally {
				conn = null;
			}
		}
	

		return resp;
	}
//
//	private List<Field> genRoleFields(UserRolesDAO dao, String Action) throws SQLException {
//		List<Field> fields = new ArrayList<Field>();
//		
//		Field code = new Field("roleCode", "ROLE.CODE", "text", 0, 1, null);
//		if (!"N".equals(Action)) {
//			code.setDisabled(true);
//		}
//		code.setMax(50);
//		code.setMandatory(true);
//		fields.add(code);
//		
//		Field name = new Field("roleName", "ROLE.NAME", "text", 0, 2, null);
//		name.setMax(50);
//		name.setMandatory(true);
//		fields.add(name);
//
//		Field rights =new Field("rights", "list", null); 
//		
//		List<Field> items = dao.genRightMap(null);
//		rights.setItems(items);
//		
//		fields.add(rights);
//		return fields;
//	}

	public Response genEdit() {
		Response resp = null;
		
		String rid = request.getParameter("p0");
		DynamicDAO dynamic = new DynamicDAO();
		try {
			if (rid == null || rid.isEmpty()) {
			//	Log.debug("load user role details by id:" + rid);
				throw new Exception("not user role id found: " + rid);
			}
			
			if (principal == null) {
				throw new ActionFailureException("Session time out.", true, true);
			}
			String compCode = principal.getCompCode();
			String templateType = "edit";

			List<String> additionalNames = Arrays.asList("DYN.DELETE.DATEBY", "DYN.CURRENT.DATEBY", "DYN.CHANGED.DATEBY", "DYN.CURRENT", "DYN.CHANGED");
			List<String> names = new ArrayList<String>();
			names.addAll(additionalNames);
			
			// get dynamic ui
			DynamicUI ui = session.getAttribute(Constants.SessionKeys.CURRENT_MODULE) != null ? (DynamicUI) session.getAttribute(Constants.SessionKeys.CURRENT_MODULE) : null;
			if (ui == null || !module.equals(ui.getModule())) {
				ui = dynamic.getDynamicUI(module, conn);
				session.setAttribute(Constants.SessionKeys.CURRENT_MODULE, ui);
			}
			
			// get dynamic template
			Template template = null;
			JsonObject tempJson = null;
			Object cTemp = session.getAttribute(Constants.SessionKeys.CURRENT_TEMPLATE);
			if (cTemp != null && cTemp instanceof JsonObject) {
				tempJson = (JsonObject) cTemp;
				Gson gson = new Gson();
				template = gson.fromJson(tempJson, Template.class);
			}
			
			if (template == null || !module.equals(template.getId())) {
				tempJson = new JsonObject();
				TemplateDAO tempalteDao =  new TemplateDAO();
				template = tempalteDao.getFields(ui, principal.isRegional(), principal.getUserCode(), compCode, conn, tempJson);
				session.setAttribute(Constants.SessionKeys.CURRENT_TEMPLATE, tempJson);
			}
			
			template.setType(templateType);
			
//			initKeyMap(tempJson);
			
			// get record
			
			for (Field item : template.getItems()) {
				if ("rights".equals(item.getId())) {
					item.setItems(dao.genRightMap(null));
				}
				if ("edit".equals(templateType) && !item.isAllowUpdate()) {
					item.setDisabled(true);
				}
			}
			
			List<JsonObject> roles = dao.getRoleById(ui, tempJson.getAsJsonArray("items"), rid, compCode);
			
			JsonObject roleMst = null;
			
			if (roles.size() > 0) {
				roleMst = roles.get(0);
			}
			
			if (roleMst == null) {
				throw new Exception("not record found:" + rid);
			}

			String secondary = "No name";
			if (roleMst.has("roleName") && !roleMst.get("roleName").isJsonNull()) { 
				secondary = roleMst.get("roleName").getAsString();
			}
			
			// get granded uri list to filter the app bar
			SysPrivilegeMgr spMgr = new SysPrivilegeMgr();
			List<String> uriList = new ArrayList<String>();
			spMgr.getGrandedURIList(principal.getRoleCode(), uriList);
			
			// AppBar - Title
			AppBarTitle appBarTitle = new AppBarTitle("/UserRoles", "levelTwo", "", "ROLE.TITLE", null, secondary);
			List<List<Field>> appBarActions = new ArrayList<>();
			appBarActions.add(dynamic.getActions(conn, ui, templateType, uriList));

			// AppBar
			AppBar appBar = new AppBar(appBarTitle, appBarActions);
			
			
			String pageTitle = "ROLE.EDIT.TITLE";
			template.setTitle(pageTitle);
			
			// translate
			appBar.getNameCodes(names);
			template.getNameCodes(names);
			Map<String, String> lmap = Function2.getTranslateMap(request, names);
			appBar.translate(lmap);
			template.translate(lmap);
			
			JsonObject langMap = new JsonObject();
			for (String name : additionalNames) {
				langMap.addProperty(name, lmap.get(name));
			}			
			
			roleMst.addProperty("isNew", true);

			Content content = new Content(template, roleMst, roleMst);
			content.setLangMap(langMap);
			// Page
			Page page = new Page("/UserRoles/Details", "User Role Master");
	
			// Token
			String tokenID = Token.CsrfID(session);
			String tokenKey = Token.CsrfToken(session);
	
			resp = new Response(Constants.ActionTypes.CHANGE_CONTENT, null, page, tokenID, tokenKey, appBar, content);			
		} catch (Exception ex) {
			Log.error(ex);
			
			resp = new Response(Constants.ActionTypes.PAGE_REDIRECT, "/Error", null);
		} finally {
			try {
				if (conn != null && !conn.isClosed()) conn.close();
			} catch (SQLException e) {
				// ignorable
			} finally {
				conn = null;
			}
		}
	
		return resp;
	}


	public Response genDelete() {
		Response resp = null;
		DynamicDAO dynamic = new DynamicDAO();
		String rid = request.getParameter("p0");
	
		try {
			if (rid == null || rid.isEmpty()) {
			//	Log.debug("load user role details by id:" + rid);
				throw new Exception("not user role id found: " + rid);
			}

			if (principal == null) {
				throw new ActionFailureException("Session time out.", true, true);
			}
			String compCode = principal.getCompCode();
			String templateType = "delete";

			List<String> additionalNames = Arrays.asList("DYN.CURRENT.TITLE", "DYN.CHANGED.TITLE");
			List<String> names = new ArrayList<String>();
			names.addAll(additionalNames);
			
			// get dynamic ui
			DynamicUI ui = session.getAttribute(Constants.SessionKeys.CURRENT_MODULE) != null ? (DynamicUI) session.getAttribute(Constants.SessionKeys.CURRENT_MODULE) : null;
			if (ui == null || !module.equals(ui.getModule())) {
				ui = dynamic.getDynamicUI(module, conn);
				session.setAttribute(Constants.SessionKeys.CURRENT_MODULE, ui);
			}
			
			// get dynamic template
			Template template = null;
			JsonObject tempJson = null;
			Object cTemp = session.getAttribute(Constants.SessionKeys.CURRENT_TEMPLATE);
			if (cTemp != null && cTemp instanceof JsonObject) {
				tempJson = (JsonObject) cTemp;
				Gson gson = new Gson();
				template = gson.fromJson(tempJson, Template.class);
			}
			
			if (template == null || !module.equals(template.getId())) {
				tempJson = new JsonObject();
				TemplateDAO tempalteDao =  new TemplateDAO();
				template = tempalteDao.getFields(ui,principal.isRegional(), principal.getUserCode(), compCode, conn, tempJson);
				session.setAttribute(Constants.SessionKeys.CURRENT_TEMPLATE, tempJson);
			}
			
			template.setType(templateType);
			
//			initKeyMap(tempJson);
			
			if (!dao.deleteRole(rid, compCode, principal.getUserCode()) || !dao.insertAudit(rid, compCode)) { // Action 'D' for delete
				throw new Exception("insertion error!!");
			}
			
			// back to table view
			// Page
			Page page = new Page("/UserRoles", null);

			// Token
			String tokenID = Token.CsrfID(session);
			String tokenKey = Token.CsrfToken(session);

			resp = new Response(Constants.ActionTypes.CHANGE_PAGE, null, page, tokenID, tokenKey, null, null);

		} catch (Exception ex) {
			Log.error(ex);
			
			resp = new Response(Constants.ActionTypes.PAGE_REDIRECT, "/Error", null);
		} finally {
			try {
				if (conn != null && !conn.isClosed()) conn.close();
			} catch (SQLException e) {
				// ignorable
			} finally {
				conn = null;
			}
		}
	
		return resp;
	}

	public Response saveAdd() {
		Response resp = null;
		DynamicDAO dynamic = new DynamicDAO();
		try {

			String p0 = request.getParameter("p0");
			JsonObject roleJson = null;
			if (p0 != null && !p0.isEmpty()) {
				p0 = new String(Base64.getDecoder().decode(p0.replace(" ", "+")), "UTF-8");
				JsonParser parser = new JsonParser();
				roleJson = parser.parse(p0).getAsJsonObject();
			} else {
				throw new Exception("missing para!!!");
			}
			
			if (principal == null) {
				throw new ActionFailureException("Session time out.", true, true);
			}
			String compCode = principal.getCompCode();

			// get dynamic ui
			DynamicUI ui = session.getAttribute(Constants.SessionKeys.CURRENT_MODULE) != null ? (DynamicUI) session.getAttribute(Constants.SessionKeys.CURRENT_MODULE) : null;
			if (ui == null || !module.equals(ui.getModule())) {
				ui = dynamic.getDynamicUI(module, conn);
				session.setAttribute(Constants.SessionKeys.CURRENT_MODULE, ui);
			}
			
			// get dynamic template
			Template template = null;
			JsonObject tempJson = null;
			Object cTemp = session.getAttribute(Constants.SessionKeys.CURRENT_TEMPLATE);
			if (cTemp != null && cTemp instanceof JsonObject) {
				tempJson = (JsonObject) cTemp;
				Gson gson = new Gson();
				template = gson.fromJson(tempJson, Template.class);
			} else { // 
				throw new Exception("Invalid action.");
			}
			
			// valiate
			List<Field> fields = template.getItems();
			for (Field field : fields) {
				String id = field.getId();
				if (field.isMandatory() && !(roleJson.has(id) && !roleJson.get(id).isJsonNull())) {
					throw new ActionFailureException("Missing mandatary field(s).", true, true);
				}
			}
			
			String rid = roleJson.get( keyMap.get(ui.getPrimaryKey()) ).getAsString();
			
			boolean isNew = false;
			// isNew is set when press Add button only 
			if (roleJson.has("isNew") && !roleJson.get("isNew").isJsonNull()) {
				isNew = roleJson.get("isNew").getAsBoolean();
			}

			HashMap<String, String> condMap = new HashMap<String, String>();
			condMap.put(ui.getPrimaryKey(), rid);
			condMap.put("comp_code", principal.getCompCode());
			
			if (Function2.hasRecord(ui.getDataTable(), condMap, dao.getManager(), conn)) {
				throw new ActionFailureException("ERROR.DUPLICATR_CODE", true, true);
			} else if (Function2.hasRecord(ui.getTempTable(), condMap, dao.getManager(), conn)) {
				if (isNew) { // if it is new and a temp record already exist, it is a duplicate
					throw new ActionFailureException("ERROR.DUPLICATR_CODE", true, true);
				} else {
					// else it is editing a waiting approve new record
					if (!dao.saveUpdate(rid, compCode, roleJson, principal.getUserCode()) || !dao.insertAudit(rid, compCode)) {
						throw new Exception("update new error!!");
					}
				}
			} else {
				if (isNew) { // if it is new and not temp record found
					if (!dao.saveInsert(rid, compCode, roleJson, principal.getUserCode(), null, null, "N") || !dao.insertAudit(rid, compCode)) { // N for New 
						throw new Exception("insert new error!!");
					}
				} else { // otherwise, id changed
//					throw new Exception("insertion error!!");
					String oid = roleJson.get("id").getAsString();
					condMap.put(ui.getPrimaryKey(), oid);
					if (Function2.hasRecord(ui.getTempTable(), condMap, dao.getManager(), conn)) {
						if (!dao.insertAudit(oid, compCode, principal.getUserCode(), "X") 
								|| !dao.removeTmp(oid, compCode)) {
							throw new Exception("changed new record id error!!");
						} else {
							if (!dao.saveInsert(rid, compCode, roleJson, principal.getUserCode(), null, null, "N") || !dao.insertAudit(rid, compCode)) { // N for New 
								throw new Exception("changed new record id error!!");
							}
						}
					} else {
						throw new Exception("changed new record id error!!");
					}
				}
			}
			
			// back to table view			
			// Page
			Page page = new Page("/UserRoles", null);

			// Token
			String tokenID = Token.CsrfID(session);
			String tokenKey = Token.CsrfToken(session);

			resp = new Response(Constants.ActionTypes.CHANGE_PAGE, null, page, tokenID, tokenKey, null, null);

		} catch (ActionFailureException ex) {
			Log.error(ex);
			resp = new Response(Constants.ActionTypes.PAGE_REDIRECT, "/Error", null);
		} catch (Exception ex) {
			Log.error(ex);
			
			resp = new Response(Constants.ActionTypes.PAGE_REDIRECT, "/Error", null);
		} finally {
			try {
				if (conn != null && !conn.isClosed()) conn.close();
			} catch (SQLException e) {
				// ignorable
			} finally {
				conn = null;
			}
		}
	
		return resp;
	}


	public Response saveEdit() {
		Response resp = null;
		DynamicDAO dynamic = new DynamicDAO();
		try {

			String p0 = request.getParameter("p0");
			JsonObject roleJson = null;
			if (p0 != null && !p0.isEmpty()) {
				p0 = new String(Base64.getDecoder().decode(p0.replace(" ", "+")), "UTF-8");
				JsonParser parser = new JsonParser();
				roleJson = parser.parse(p0).getAsJsonObject();
			} else {
				throw new Exception("missing para!!!");
			}

			if (principal == null) {
				throw new ActionFailureException("Session time out.", true, true);
			}
			String compCode = principal.getCompCode();

			// get dynamic ui
			DynamicUI ui = session.getAttribute(Constants.SessionKeys.CURRENT_MODULE) != null ? (DynamicUI) session.getAttribute(Constants.SessionKeys.CURRENT_MODULE) : null;
			if (ui == null || !module.equals(ui.getModule())) {
				ui = dynamic.getDynamicUI(module, conn);
				session.setAttribute(Constants.SessionKeys.CURRENT_MODULE, ui);
			}
			
			// get dynamic template
			Template template = null;
			JsonObject tempJson = null;
			Object cTemp = session.getAttribute(Constants.SessionKeys.CURRENT_TEMPLATE);
			if (cTemp != null && cTemp instanceof JsonObject) {
				tempJson = (JsonObject) cTemp;
				Gson gson = new Gson();
				template = gson.fromJson(tempJson, Template.class);
			} else { // 
				throw new Exception("Invalid action.");
			}

			// valiate
			List<Field> fields = template.getItems();
			for (Field field : fields) {
				String id = field.getId();
				if (field.isMandatory() && !(roleJson.has(id) && !roleJson.get(id).isJsonNull())) {
					throw new ActionFailureException("Missing mandatary field(s).", true, true);
				}
			}
	
			boolean isNew = false;
			// isNew is set when press Edit button only 
			if (roleJson.has("isNew") && !roleJson.get("isNew").isJsonNull()) {
				isNew = roleJson.get("isNew").getAsBoolean();
			}

			String rid = roleJson.get( keyMap.get(ui.getPrimaryKey()) ).getAsString();
			
			HashMap<String, String> condMap = new HashMap<String, String>();
			condMap.put(ui.getPrimaryKey(), rid);
			condMap.put("comp_code", principal.getCompCode());
			
			if (Function2.hasRecord(ui.getDataTable(), condMap, dao.getManager(), conn)) {
				if (Function2.hasRecord(ui.getTempTable(), condMap, dao.getManager(), conn)) {
					if (isNew) { // if it is new and a temp record already exist, it is a duplicate
						throw new Exception("edit error!!");
					} // else update the temp record
					else {
						if (!dao.saveUpdate(rid, compCode, roleJson, principal.getUserCode()) || !dao.insertAudit(rid, compCode)) {
							throw new Exception("update error!!");
						}
					}
				} 
				else if (isNew) { // if it is a new edit, save by insert new edit
					String CreateBy = (String) Function2.getSingleColumnFromRecord(ui.getDataTable(), "create_by", ui.getPrimaryKey(), rid, DataType.TEXT, dao.getManager(), conn);					
					Date CreateDate = (Date) Function2.getSingleColumnFromRecord(ui.getDataTable(), "create_date", ui.getPrimaryKey(), rid, DataType.DATE, dao.getManager(), conn);					
					if (!dao.saveInsert(rid, compCode, roleJson, principal.getUserCode(), CreateDate, CreateBy, "E") || !dao.insertAudit(rid, compCode)) { // E for Edit
						throw new Exception("edit error!!");
					}
				} else {
					throw new Exception("edit error!!");
				}
			} else {
				throw new Exception("edit error!!");
			}
			
			// Page
			Page page = new Page("/UserRoles", null);

			// Token
			String tokenID = Token.CsrfID(session);
			String tokenKey = Token.CsrfToken(session);

			resp = new Response(Constants.ActionTypes.CHANGE_PAGE, null, page, tokenID, tokenKey, null, null);

		} catch (ActionFailureException ex) {
			// TODO:: error message handling
			Log.error(ex);
			resp = new Response(Constants.ActionTypes.PAGE_REDIRECT, "/Error", null);
		} catch (Exception ex) {
			Log.error(ex);
			
			resp = new Response(Constants.ActionTypes.PAGE_REDIRECT, "/Error", null);
		} finally {
			try {
				if (conn != null && !conn.isClosed()) conn.close();
			} catch (SQLException e) {
				// ignorable
			} finally {
				conn = null;
			}
		}
	
		return resp;
	}


	public Response approve() {
		Response resp = null;
		DynamicDAO dynamic = new DynamicDAO();
		String rid = request.getParameter("p0");
	
		try {
			if (rid == null || rid.isEmpty()) {
			//	Log.debug("load user role details by id:" + rid);
				throw new Exception("not user role id found: " + rid);
			}
		
			if (principal == null) {
				throw new ActionFailureException("Session time out.", true, true);
			}
			
			// check if maker is save as checker
			String UpdateBy = (String)Function2.getSingleColumnFromRecord("sys_user_role_mst_tmp", "modify_by", "role_code", rid, DataType.TEXT, dao.getManager(), conn);					
			if (!(!principal.getUserCode().equals(UpdateBy) || Constants.USERTYPE_ADMIN.equals(principal.getUserType()))) {
				throw new ActionFailureException("ERROR.INVAILD_MAC_ACCESS", true, true);
			}
			
			String compCode = principal.getCompCode();

			List<String> additionalNames = Arrays.asList("DYN.CURRENT.TITLE", "DYN.CHANGED.TITLE");
			List<String> names = new ArrayList<String>();
			names.addAll(additionalNames);
			
			// get dynamic ui
			DynamicUI ui = session.getAttribute(Constants.SessionKeys.CURRENT_MODULE) != null ? (DynamicUI) session.getAttribute(Constants.SessionKeys.CURRENT_MODULE) : null;
			if (ui == null || !module.equals(ui.getModule())) {
				ui = dynamic.getDynamicUI(module, conn);
				session.setAttribute(Constants.SessionKeys.CURRENT_MODULE, ui);
			}
			
			// get dynamic template
			Template template = null;
			JsonObject tempJson = null;
			Object cTemp = session.getAttribute(Constants.SessionKeys.CURRENT_TEMPLATE);
			if (cTemp != null && cTemp instanceof JsonObject) {
				tempJson = (JsonObject) cTemp;
				Gson gson = new Gson();
				template = gson.fromJson(tempJson, Template.class);
			}
			
			if (template == null || !module.equals(template.getId())) {
				tempJson = new JsonObject();
				TemplateDAO tempalteDao =  new TemplateDAO();
				template = tempalteDao.getFields(ui, principal.isRegional(), principal.getUserCode(), compCode, conn, tempJson);
				session.setAttribute(Constants.SessionKeys.CURRENT_TEMPLATE, tempJson);
			}
			
//			initKeyMap(tempJson);

			String Action = dao.getRoleAction(rid, compCode);
			if ("N".equals(Action)) {
				if (!dao.approveAdd(rid, compCode, principal.getUserCode()) 
						|| !dao.insertAudit(rid, compCode, principal.getUserCode(), "A") // A for approve Add
						|| !dao.removeTmp(rid, compCode)) {
					throw new Exception("insertion error!!");
				}
			} else if ("E".equals(Action)) {
				if (!dao.approveEdit(rid, compCode, principal.getUserCode()) 
						|| !dao.insertAudit(rid, compCode, principal.getUserCode(), "U") // U for approve update
						|| !dao.removeTmp(rid, compCode)) {
					throw new Exception("insertion error!!");
				}
			} else if ("D".equals(Action)) {
				if (!dao.approveDelete(rid, compCode, principal.getUserCode()) 
						|| !dao.insertAudit(rid, compCode, principal.getUserCode(), "R") // R for approve delete 
						|| !dao.removeTmp(rid, compCode)) {
					throw new Exception("insertion error!!");
				}
			} else {
				throw new Exception("insertion error!!");
			}
			
			// Page
			Page page = new Page("/UserRoles", null);

			// Token
			String tokenID = Token.CsrfID(session);
			String tokenKey = Token.CsrfToken(session);
			
			//resp = new Response(Constants.ActionTypes.CHANGE_PAGE, null, page, tokenID, tokenKey, null, null);
			UserRoleManager urMgr = new UserRoleManager(request);
			resp = urMgr.returnTableView();
			//if of role current user changed, get new menu
			UserPrincipalBean principal = Function.getPrincipal(request);
			String userRole = principal.getRoleCode();
			if(rid.equals(userRole)){
				SettingsManager sm = new SettingsManager(request);
				MenuList menuList = sm.getMenu(request.getSession(), userRole);
				resp.setMenuList(menuList.getList());
				resp.setPath("/Home");
				resp.setAction(Constants.ActionTypes.PAGE_REDIRECT);
			}
			
			
		} catch (Exception ex) {
			Log.error(ex);
			resp = new Response(Constants.ActionTypes.PAGE_REDIRECT, "/Error", null);
		} finally {
			try {
				if (conn != null && !conn.isClosed()) conn.close();
			} catch (SQLException e) {
				// ignorable
			} finally {
				conn = null;
			}
		}
	
		return resp;
	}
	
	public Response revert() {
		Response resp = null;
		DynamicDAO dynamic = new DynamicDAO();
		String rid = request.getParameter("p0");
	
		try {
			if (rid == null || rid.isEmpty()) {
				// revert a new record, do nothing
			}
			else {
				
				if (principal == null) {
					throw new ActionFailureException("Session time out.", true, true);
				}
				String compCode = principal.getCompCode();
	
				List<String> additionalNames = Arrays.asList("DYN.CURRENT.TITLE", "DYN.CHANGED.TITLE");
				List<String> names = new ArrayList<String>();
				names.addAll(additionalNames);
				
				// get dynamic ui
				DynamicUI ui = session.getAttribute(Constants.SessionKeys.CURRENT_MODULE) != null ? (DynamicUI) session.getAttribute(Constants.SessionKeys.CURRENT_MODULE) : null;
				if (ui == null || !module.equals(ui.getModule())) {
					ui = dynamic.getDynamicUI(module, conn);
					session.setAttribute(Constants.SessionKeys.CURRENT_MODULE, ui);
				}
				
				// get dynamic template
				Template template = null;
				JsonObject tempJson = null;
				Object cTemp = session.getAttribute(Constants.SessionKeys.CURRENT_TEMPLATE);
				if (cTemp != null && cTemp instanceof JsonObject) {
					tempJson = (JsonObject) cTemp;
					Gson gson = new Gson();
					template = gson.fromJson(tempJson, Template.class);
				}
				
				if (template == null || !module.equals(template.getId())) {
					tempJson = new JsonObject();
					TemplateDAO tempalteDao =  new TemplateDAO();
					template = tempalteDao.getFields(ui, principal.isRegional(), principal.getUserCode(), compCode, conn, tempJson);
					session.setAttribute(Constants.SessionKeys.CURRENT_TEMPLATE, tempJson);
				}
				
	//			initKeyMap(tempJson);
	
				String Action = dao.getRoleAction(rid, compCode);
				
				if (Action != null) {
					if (!dao.insertAudit(rid, compCode, principal.getUserCode(), "X") // X for reject 
							|| !dao.removeTmp(rid, compCode)) {
						throw new Exception("insertion error!!");
					}	
				} else {
					// just close
				}
			}
			
			// Page
			Page page = new Page("/UserRoles", null);

			// Token
			String tokenID = Token.CsrfID(session);
			String tokenKey = Token.CsrfToken(session);

			resp = new Response(Constants.ActionTypes.CHANGE_PAGE, null, page, tokenID, tokenKey, null, null);
			
		} catch (Exception ex) {
			Log.error(ex);
			resp = new Response(Constants.ActionTypes.PAGE_REDIRECT, "/Error", null);
		} finally {
			try {
				if (conn != null && !conn.isClosed()) conn.close();
			} catch (SQLException e) {
				// ignorable
			} finally {
				conn = null;
			}
		}
	
		return resp;
	}

	public Response cloneRole() {
		Response resp;
		String param = request.getParameter("p0");
		String newCompCode = null;
//		String newCompCode = request.getParameter("p9");
		DynamicDAO dynamic = new DynamicDAO();
		try {

			if (principal == null) {
				throw new ActionFailureException("Session time out.", true, true);
			}
			
			JsonArray rolesArray = null;
			if (param != null && !param.isEmpty()) {
				param = new String(Base64.getDecoder().decode(param.replace(" ", "+")), "UTF-8");
//				JSONObject paraJSON = new JSONObject(paraStr);
				Log.debug("param = " + param);
				JsonParser parser = new JsonParser();
				JsonObject paramJson = parser.parse(param).getAsJsonObject();

				//Task List
				if (paramJson.has("rows")){    
					rolesArray = paramJson.getAsJsonArray("rows");
				}
			}
			
			if (rolesArray == null || rolesArray.isJsonNull()) {
			//	Log.debug("load user role details by id:" + rid);
				throw new Exception("Not user role id found");
			}
			
			String compCode = principal.getCompCode();
			String templateType = "add";

//			List<String> additionalNames = Arrays.asList("DYN.DELETE.DATEBY", "DYN.CURRENT.DATEBY", "DYN.CHANGED.DATEBY", "DYN.CURRENT", "DYN.CHANGED");
			List<String> names = new ArrayList<String>();
//			names.addAll(additionalNames);
			
			// get dynamic ui
			DynamicUI ui = session.getAttribute(Constants.SessionKeys.CURRENT_MODULE) != null ? (DynamicUI) session.getAttribute(Constants.SessionKeys.CURRENT_MODULE) : null;
			if (ui == null || !module.equals(ui.getModule())) {
				ui = dynamic.getDynamicUI(module, conn);
				session.setAttribute(Constants.SessionKeys.CURRENT_MODULE, ui);
			}
			
			// get dynamic template
			Template template = null;
			JsonObject tempJson = null;
			Object cTemp = session.getAttribute(Constants.SessionKeys.CURRENT_TEMPLATE);
			if (cTemp != null && cTemp instanceof JsonObject) {
				tempJson = (JsonObject) cTemp;
				Gson gson = new Gson();
				template = gson.fromJson(tempJson, Template.class);
			}
			
			if (template == null || !module.equals(template.getId())) {
				tempJson = new JsonObject();
				TemplateDAO templateDao =  new TemplateDAO();
				template = templateDao.getFields(ui, principal.isRegional(), principal.getUserCode(), compCode, conn, tempJson);
				session.setAttribute(Constants.SessionKeys.CURRENT_TEMPLATE, tempJson);
			}
			
//			initKeyMap(tempJson);
			
			List<String> newRids = new ArrayList<String>();
			
			if (newCompCode == null || newCompCode.isEmpty()) {
				newCompCode = principal.getCompCode();
			}
			

			// clone records 
			for (int r = 0; r <rolesArray.size(); r++) {
				String rid = rolesArray.get(r).getAsString();
				int trial = 1;

				HashMap<String, String> condMap = new HashMap<String, String>();
				condMap.put(ui.getPrimaryKey(), rid + trial);
				condMap.put("comp_code", principal.getCompCode());
				
				while (Function2.hasRecord(ui.getMasterView(), condMap, dao.getManager(), conn)) {
					trial++;
					condMap.put(ui.getPrimaryKey(), rid + trial);
				}
				
				List<JsonObject> roles = dao.getRoleById(ui, tempJson.getAsJsonArray("items"), rid, compCode);
				for (JsonObject rr : roles) {
					if (!rr.has("action") || rr.get("action").isJsonNull()) { // the active record
						rr.addProperty("roleName", rr.get("roleName").getAsString()+" "+trial); // override the record name						
						dao.saveInsert(rid + trial, newCompCode, rr, principal.getUserCode(), null, null, "N");
						newRids.add(rid + trial);
					}
				}
			}
			
			// get and view first new role
			String nextRole = newRids.get(0);
			
			List<JsonObject> roles = dao.getRoleById(ui, tempJson.getAsJsonArray("items"), nextRole, compCode);
			
			JsonObject roleMst = null;
			JsonObject roleTmp = null;
			
			if (roles.size() == 1) {
				roleTmp = roles.get(0);
			}
			
			if (roleTmp == null) {
				throw new Exception("retrieve corrupted");
			}
			
			// AppBar - Title
			String secondary = "No name";
			if (roleTmp.has("roleName") && !roleTmp.get("roleName").isJsonNull()) {
				secondary = roleTmp.get("roleName").getAsString();
			}
			
			AppBarTitle appBarTitle = new AppBarTitle("/UserRoles", "levelTwo", "", "ROLE.TITLE", null, secondary);
			List<List<Field>> appBarActions = new ArrayList<>();
			
			// get granded uri list to filter the app bar
			SysPrivilegeMgr spMgr = new SysPrivilegeMgr();
			List<String> uriList = new ArrayList<String>();
			spMgr.getGrandedURIList(principal.getRoleCode(), uriList);
			
			// AppBar - Actions
			appBarActions.add(dynamic.getActions(conn, ui, templateType, uriList));
			
			// refine fields 
			for (Field item : template.getItems()) {
				if ("rights".equals(item.getId())) {
					item.setItems(dao.genRightMap(null));
				}
			}
			
			template.setType(templateType);
			template.setTitle("ROLE.ADD.TITLE");
			
			// AppBar
			AppBar appBar = new AppBar(appBarTitle, appBarActions);
			
			// translate
			appBar.getNameCodes(names);
			template.getNameCodes(names);
			Map<String, String> lmap = Function2.getTranslateMap(request, names);
			appBar.translate(lmap);
			template.translate(lmap);
			
			JsonObject langMap = new JsonObject();
//			for (String name : additionalNames) {
//				langMap.addProperty(name, lmap.get(name));
//			}			

			Content content = new Content(template, roleMst, roleTmp);
			content.setLangMap(langMap);
			// Page
			Page page = new Page("/UserRoles/Details", "User Role Master");
	
			// Token
			String tokenID = Token.CsrfID(session);
			String tokenKey = Token.CsrfToken(session);
	
			resp = new Response(Constants.ActionTypes.CHANGE_PAGE, null, page, tokenID, tokenKey, appBar, content);			
		} catch (Exception ex) {
			Log.error(ex);
			
			resp = new Response(Constants.ActionTypes.PAGE_REDIRECT, "/Error", null);
		} finally {
			try {
				if (conn != null && !conn.isClosed()) conn.close();
			} catch (SQLException e) {
				// ignorable
			} finally {
				conn = null;
			}
		}
	
		return resp;
	}
}
