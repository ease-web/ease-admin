package com.eab.biz.needs;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.eab.biz.common.CommonManager;
import com.eab.common.Constants;
import com.eab.common.Function;
import com.eab.common.Function2;
import com.eab.common.Language;
import com.eab.common.Log;
import com.eab.common.Token;
import com.eab.common.Constants.ActionTypes;
import com.eab.dao.needs.Needs;
import com.eab.json.model.AppBar;
import com.eab.json.model.AppBarTitle;
import com.eab.json.model.Content;
import com.eab.json.model.Dialog;
import com.eab.json.model.DialogItem;
import com.eab.json.model.Field;
import com.eab.json.model.ListContent;
import com.eab.json.model.Option;
import com.eab.json.model.Page;
import com.eab.json.model.Response;
import com.eab.json.model.SearchCondition;
import com.eab.json.model.Template;
import com.eab.model.needs.NeedAttachBean;
import com.eab.model.profile.UserPrincipalBean;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

public class NeedMgr {
	
	HttpServletRequest request;
	HttpSession session;
	UserPrincipalBean principal;
	Language langObj;
	Gson gson;
	
	public NeedMgr(HttpServletRequest request) {
		super();
		this.request = request;
		this.session = request.getSession();
		this.principal = (UserPrincipalBean) session.getAttribute(Constants.USER_PRINCIPAL);
		this.langObj = new Language(principal.getUITranslation());
	}
	
	public Response applyToNewChannel(String needCode, String oldChannelCode, String newChannelCode, Dialog inDialog, boolean confirmOverwrite) throws Exception {
		if(inDialog != null)
			return getJson("", oldChannelCode, "", "", "", 0, 0 ,inDialog);//Show record locked message
		
		Needs needsDAO = new Needs();
		int version = 0;
		
		if (confirmOverwrite){	
			//Overwrite existing tentative record
			version = needsDAO.createNewVersion(principal, needCode, newChannelCode, oldChannelCode, true);
						
			if (version == 0)
				throw new Exception ("Error to create a new version");
			
			Field btnOk = new Field("confirm_cancel", "button", langObj.getNameDesc("BUTTON.OK").toString());
			Dialog dialog = new Dialog("ok", null, langObj.getNameDesc("COMPLETED").toString(), null, btnOk, 20);
			
			return getJson("", oldChannelCode, "", "", "", 0, 0 ,dialog);
		} else {
			if (needsDAO.isTentativeStatus(needCode, newChannelCode, principal)) {
				Field btnOk = new Field("/Needs/Apply", "submitButton", langObj.getNameDesc("BUTTON.OK").toString());
				Field btnCancel = new Field("confirm_cancel", "button", langObj.getNameDesc("BUTTON.CANCEL").toString());
				Dialog dialog = new Dialog("applyToDialog", langObj.getNameDesc("BUTTON.CONFIRM").toString(), langObj.getNameDesc("NEED_APPLY_MSG").toString(), btnOk, btnCancel, 50);
				
				session.setAttribute("NEED_APPLY_NEED_CODE", needCode);
				session.setAttribute("NEED_APPLY_NEW_CHANNEL_CODE", newChannelCode);
				
				return getJson("", oldChannelCode, "", "", "", 0, 0 ,dialog);
			} else {
				//Create a new version
				version = needsDAO.createNewVersion(principal, needCode, newChannelCode, oldChannelCode, false);
				
				if (version == 0)
					throw new Exception ("Error to create a new version");
				
				Field btnOk = new Field("confirm_cancel", "button", langObj.getNameDesc("BUTTON.OK").toString());
				Dialog dialog = new Dialog("ok", null, langObj.getNameDesc("COMPLETED").toString(), null, btnOk, 20);
				
				return getJson("", oldChannelCode, "", "", "", 0, 0 ,dialog);
			}
		}
	}
	
	public Response getJson(String criteria, String typeFilter, String statusFilter, String sortBy, String sortDir, int recordStart, int pageSize, Dialog inDialog) throws Exception {
		ArrayList<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
								
		//Get available channel for current login user  
	    List<Option> appBarTitleOption = new ArrayList<>();
	      
		Needs needsDAO = new Needs();
		appBarTitleOption = needsDAO.getUserChannelList(principal.getChannelList().toString(), principal.getCompCode(), langObj);
		
		if (typeFilter.equals("") && appBarTitleOption.size() > 0) 
			typeFilter = appBarTitleOption.get(0).getValue();
		
		if (recordStart != 0) 
			list = (ArrayList<Map<String, Object>>) session.getAttribute(Constants.SessionKeys.SEARCH_RESULT);
						
		//Get list
		if (list.size() == 0 && appBarTitleOption.size() > 0) {			
			list = this.getList(criteria, typeFilter, statusFilter, sortBy, sortDir);
			
			//Add result to session for "More" button use
			session.setAttribute(Constants.SessionKeys.SEARCH_RESULT, list);
		}
			
		//Get template
		List<Field> item = this.getTemplate();
		
		String action = "";
		
		//Action	
		CommonManager cMgr = new CommonManager(request);
		boolean isChangePage = cMgr.isChangePage();
		
		if ((criteria != "" || typeFilter != "" || statusFilter != "") && !isChangePage) {
			//Page content change
			action =  Constants.ActionTypes.CHANGE_CONTENT;
		} else {
			//First time load
			action =  Constants.ActionTypes.CHANGE_PAGE;
		}
		
	    //Page
		JsonObject pageValue = new JsonObject();
		pageValue.addProperty("criteria", criteria);
		pageValue.addProperty("typeFilter", typeFilter);//Channel Filter
		pageValue.addProperty("statusFilter", statusFilter);
		pageValue.addProperty("sortBy", sortBy);
		pageValue.addProperty("sortDir", sortDir);
		pageValue.addProperty("recordStart", recordStart);
		pageValue.addProperty("pageSize", pageSize);
		Page page = new Page("/Needs", langObj.getNameDesc("MENU.NEEDS").toString(), pageValue);
		
	    //Token	    
		String tokenID = Token.CsrfID(request.getSession());
		String tokenKey = Token.CsrfToken(request.getSession());

	    //Template		
		Template template = new Template();
		template.setItems(item);
	    
		//More
	    int offset = 0;
	    int size = 10;	  
	    
	    List<Object> shortedList = new ArrayList<>();
		offset = recordStart;
		 
		size = pageSize != 0 ? pageSize : Constants.SEARCH_RESULT_DEFAULT_PAGE_SIZE;
	    		
		if (list != null) {
			for (int j = offset; j < offset + size && j < list.size(); j++) {
				shortedList.add(list.get(j));
			}
		}

		//Content - values
		ListContent contentValues = new ListContent(shortedList, list != null ? list.size(): 0, offset > 0);
	    	    
	    //Content
	    Content content = new Content(template, contentValues);
	    	    
	    //AppBar - Title & channel Filter
		AppBarTitle appBarTitle ;

	    if (appBarTitleOption.size() > 1)  
	    	appBarTitle = new AppBarTitle("typeFilter", "picker", typeFilter, null, appBarTitleOption, null);
	    else 
	    	//if user can only access to one channel, show the title to "Needs" instead of a dropdown
			appBarTitle = new AppBarTitle(null, "label", "", langObj.getNameDesc("MENU.NEEDS").toString(), null, null);
		
	    //AppBar - Needs Status Filter
	    List<Option> appBarAction1_Option = new ArrayList<>();
	    appBarAction1_Option = Function2.getOptionsByLookupKey(request, "NEEDS_STATUS", langObj);
		Field appBarAction1_1 = new Field("statusFilter", "picker", "*", null, appBarAction1_Option);
	    
	    //AppBar - Search
		Field appBarAction1_2 = new Field("searchString", "searchButton", langObj.getNameDesc("NEED_APPBAR_SEARCH"), langObj.getNameDesc("SEARCH"), null);
	
	    //AppBar - Add actions
		List<Field> appBarAction1 = new ArrayList<>();
	    appBarAction1.add(appBarAction1_1);
	    appBarAction1.add(appBarAction1_2);
	    
	    //Define actions - S	    
		Field btnNewOk = new Field("/Needs/NewVersion", "submitButton", langObj.getNameDesc("BUTTON.OK").toString());
		Field btnNewCancel = new Field("confirm_cancel", "button", langObj.getNameDesc("BUTTON.CANCEL").toString());
		Dialog createNewVersionDialog = new Dialog("createNewVersionDialog", langObj.getNameDesc("CREATE_NEW_VER").toString(), langObj.getNameDesc("CREATE_NEW_VER_MSG").toString(), btnNewOk, btnNewCancel, 50);  
	    Field appNewVersion = new Field("createNewVersionDialog", "dialogButton", langObj.getNameDesc("CREATE_NEW_VER").toString(), createNewVersionDialog);
	    	    			   	    	    
		//Apply To
		Field btnApplyOk = new Field("/Needs/Apply", "submitButton", langObj.getNameDesc("BUTTON.OK").toString());
		Field btnApplyCancel = new Field("confirm_cancel", "button", langObj.getNameDesc("BUTTON.CANCEL").toString());
		
		List<Option> radioChannelOptions = new ArrayList<>();
						
		for (int i=0; i < appBarTitleOption.size(); i++) {
			if (!appBarTitleOption.get(i).getValue().equals(typeFilter)){
				radioChannelOptions.add(new Option(appBarTitleOption.get(i).getTitle(), appBarTitleOption.get(i).getValue()));
			}
		}
		
		List<Field> applyToDialogItems = new ArrayList<>();
		DialogItem di_radioButton = new DialogItem("radio_apply", DialogItem.Types.RADIOBUTTON, null, true, radioChannelOptions);
		DialogItem di_label = new DialogItem("label", DialogItem.Types.LABEL, langObj.getNameDesc("SELECT_CHANNEL_LABEL1").toString(), 0, 0);
		
		applyToDialogItems.add(di_label);
		applyToDialogItems.add(di_radioButton);
		
		Dialog applyToDialog = new Dialog("applyToDialog", langObj.getNameDesc("SELECT_CHANNEL").toString(), "", btnApplyOk, btnApplyCancel, applyToDialogItems, 30);    
	
		Field appApply = new Field("/Needs/Apply", "dialogButton", langObj.getNameDesc("APPLY_TO").toString(), applyToDialog);
	    //Define actions - E

	    //Status = Tentative only
		List<Field> appBarAction2 = new ArrayList<>();
		appBarAction2.add(appNewVersion);
	    
	    //Status = Effective
		List<Field> appBarAction3 = new ArrayList<>();
	    
		if (appBarTitleOption.size() > 1)  
		{
			appBarAction2.add(appApply);
	    	appBarAction3.add(appApply);
		}
	    	    
	    //AppBar - Actions	    
	    List<List<Field>> appBarActions = new ArrayList<>();
	    appBarActions.add(appBarAction1);
	    appBarActions.add(appBarAction2);
	    appBarActions.add(appBarAction3);

	    //AppBar	    
		AppBar appBar = new AppBar(appBarTitle, appBarActions);
		SearchCondition cond = SearchCondition.RetrieveSearchCondition(request);
		appBar.setValues(cond);
		
	    //Convert to Json  
		Gson gson = new GsonBuilder().create();
		
		Response response = new Response(action, null, page, tokenID, tokenKey, appBar, content);
		
	    
		if(inDialog != null)
			response.setDialog(inDialog);
		    
	    return response;	
	}
		
	public ArrayList<Map<String, Object>> getList(String criteria, String typeFilter, String statusFilter, String sortBy, String sortDir) throws Exception {		
		Needs needsDAO = new Needs();
		return needsDAO.getNeedList(request, principal, criteria, typeFilter, statusFilter, sortBy, sortDir, langObj);
	}
	
	public List<Field> getTemplate() throws Exception {	   
	    List<String> List = Arrays.asList("CODE", "NAME", "STATUS", "REMARKS");
	    List<String> ListType = Arrays.asList("text", "text", "text", "text");
	    List<String> ListId = Arrays.asList("need_code", "need_name", "status", "remark");
	    List<String> ListWidth = Arrays.asList("150px", "auto", "auto", "300px");
	    
	    int seq = 0;
		List<Field> template = new ArrayList<>();
	    
		for(String label: List) {
			Field field1 = new Field(langObj.getNameDesc(ListId.get(seq)), langObj.getNameDesc(label), ListType.get(seq), seq+1, seq+1, ListWidth.get(seq));
		    template.add(field1);
	    	
		    seq ++;
	    }

	    return template;
	}
	
 
	public Response changePage(String selectPageId, String targetPageId, String needCode, int version) {
		// Action
		String action = ActionTypes.RESET_VALUES;

		// Token
		String tokenID = Token.CsrfID(request.getSession());
		String tokenKey = Token.CsrfToken(request.getSession());

		JsonObject values = new JsonObject();
		JsonObject selectPage = new JsonObject();
		selectPage.addProperty("id", targetPageId);
		if(selectPageId.equals(Constants.NeedPage.TASK)){
			//TODO
			//NeedTaskMgr taskMgr = new NeedTaskMgr();
			//taskMgr.addTaskValues(values, -1, needCode, version);
		}
		values.add("selectPage", gson.toJsonTree(selectPage));
		values.addProperty("actionType", "merge");
		Content content = new Content(null, values);
		
		return new Response(action, null, null, tokenID, tokenKey, null, content);
	}
	
	public void updateNeedAttachFile(String prodCode, int version, String attachCode, String attachFile, String lang, String channelCode, String sectionCode) {

		try {
			Needs pd = new Needs();
			UserPrincipalBean principal = Function.getPrincipal(request);
			NeedAttachBean attach = pd.getNeedAttach(principal.getCompCode(), prodCode, version, lang, channelCode, attachCode, sectionCode);
			
			if (attach == null || attach.getAttachCode() == null) {
				// create
				attach.setCompCode(principal.getCompCode());
				attach.setNeedCode(prodCode);
				attach.setVersion(version);
				attach.setLangCode(lang);
				attach.setAttachCode(attachCode);
				// attach.setAttachFile(attachFile);
				attach.setCreateBy(principal.getUserCode());
				attach.setCreateDate(new Date(Calendar.getInstance().getTimeInMillis()));
				pd.addNeedAttach(attach, attachFile,channelCode, sectionCode);
			} else {
				if(attachFile == null || attachFile.equals("")){
					//delete attachment
					pd.deleteNeedAttach(attach,channelCode, sectionCode);
				}else{
					// update
					attach.setAttachCode(attachCode);
					attach.setUpdateBy(principal.getUserCode());
					attach.setUpdateDate(new Date(Calendar.getInstance().getTimeInMillis()));
					int update = pd.updateNeedAttach(attach, attachFile,channelCode, sectionCode);
				}
			}
		} catch (Exception e) {
			Log.error(e);
		}

	}

	public void needDataInitial(String compCode, String channelCode) throws Exception {
		Needs needsDAO = new Needs();
		needsDAO.needDataInitial(compCode, channelCode);
	}
	
}
