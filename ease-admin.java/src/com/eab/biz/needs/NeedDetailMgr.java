package com.eab.biz.needs;

import java.sql.Blob;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.json.JSONObject;

import com.eab.biz.common.CommonManager;
import com.eab.biz.dynamic.LockManager;
import com.eab.biz.tasks.TaskInfoMgr;
import com.eab.common.Constants;
import com.eab.common.JsonConverter;
import com.eab.common.Language;
import com.eab.common.Log;
import com.eab.common.Token;
import com.eab.common.Constants.ActionTypes;
import com.eab.dao.common.Functions;
import com.eab.dao.dynamic.Companies;
import com.eab.dao.dynamic.RecordLock;
import com.eab.dao.needs.Needs;
import com.eab.dao.tasks.Tasks;
import com.eab.json.model.AppBar;
import com.eab.json.model.AppBarTitle;
import com.eab.json.model.Content;
import com.eab.json.model.Dialog;
import com.eab.json.model.Field;
import com.eab.json.model.Option;
import com.eab.json.model.Page;
import com.eab.json.model.Response;
import com.eab.json.model.Template;
import com.eab.json.model.Field.Types;
import com.eab.model.dynamic.CompanyBean;
import com.eab.model.dynamic.RecordLockBean;
import com.eab.model.needs.NeedsBean;
import com.eab.model.products.ProductBean;
import com.eab.model.profile.UserPrincipalBean;
import com.eab.model.system.FuncBean;
import com.eab.security.SysPrivilegeMgr;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class NeedDetailMgr {

	HttpServletRequest request;
	HttpSession session;
	UserPrincipalBean principal;
	Language langObj;
	Gson gson;
	Needs needsDAO;
	NeedTabMgr needTabMgr;
	NeedFinEvaluationMgr needFinEvalMgr;
	NeedSelectionMgr needSelectionMgr;
	NeedPrioritizationMgr needsPriorMgr;
	NeedRiskAssessmentMgr needsRAMgr;
	NeedsConceptSellingMgr needConceptSellingMgr;
	TaskInfoMgr taskInfoMgr;

	public NeedDetailMgr(HttpServletRequest request) {
		super();
		this.request = request;
		this.session = request.getSession();
		this.principal = (UserPrincipalBean) session.getAttribute(Constants.USER_PRINCIPAL);
		this.langObj = new Language(principal.getUITranslation());
		this.gson = new GsonBuilder().enableComplexMapKeySerialization().setPrettyPrinting().create();
		this.needsDAO = new Needs();
		this.needTabMgr = new NeedTabMgr(request);
		this.needFinEvalMgr = new NeedFinEvaluationMgr(request);
		this.needSelectionMgr = new NeedSelectionMgr(request);
		this.needsPriorMgr = new NeedPrioritizationMgr(request);
		String module = request.getParameter("p1");
		this.taskInfoMgr = new TaskInfoMgr(request, module);
		this.needsRAMgr = new NeedRiskAssessmentMgr(request);
		this.needConceptSellingMgr = new NeedsConceptSellingMgr(request);
		gson = new GsonBuilder().enableComplexMapKeySerialization().setPrettyPrinting().create();
	}

	public NeedDetailMgr() {
		super();
	}

	public Response changePage(String selectPageId, String needCode, int version, String channelCode, boolean isMenuUpdate) throws Exception{
		// Action
		String action = ActionTypes.NEEDS_CHANGE_PAGE;

		// Token
		String tokenID = Token.CsrfID(request.getSession());
		String tokenKey = Token.CsrfToken(request.getSession());
		
		//values
		JsonObject values = new JsonObject();
		JsonObject selectPage = new JsonObject();
		selectPage.addProperty("id", selectPageId);
		
		if (selectPageId.startsWith("NAQ")) 
			selectPage.addProperty("uid", "NeedsFinEval");
		else if (selectPageId.startsWith("NAG")) 
			selectPage.addProperty("uid", "NeedsGuide");
		
		values.add("selectPage", gson.toJsonTree(selectPage));
		values.add("needCode", gson.toJsonTree(needCode));
		values.add("id", gson.toJsonTree(needCode));
		values.add("version", gson.toJsonTree(version));
		values.add("channelCode", gson.toJsonTree(channelCode));
		addValues(selectPageId, values, needCode, version, channelCode);
		
		if(needCode.equals("N0001"))
			addValues(Constants.NeedPage.NEEDS_CONCEPT_SELLING, values, needCode, version, channelCode);
		
		//Template
		Template template = null;
		
		if (isMenuUpdate){
			template = new Template();
	        template.setEmptyMessage(langObj.getNameDesc("MESSAGE_ASSIGN_TASK"));
	        template.setMenu(needsDAO.getMenu(needCode, principal, channelCode, version));
		}
		
		Content content = new Content(template, values);

		return new Response(action, null, null, tokenID, tokenKey, null, content);
	}

	public void addValues(String pageId, JsonObject values, String needCode, int version, String channelCode) throws Exception{
		switch(pageId){ 
			case Constants.NeedPage.TASK:
				taskInfoMgr.addValues(values, -1, needCode, version, channelCode);
				break;
			case Constants.NeedPage.DETAIL:
				addValues(values, needCode, version, channelCode);
				break;
			case Constants.NeedPage.NEEDS_TAB:
				needTabMgr.addValues(values, needCode, version, channelCode);
				break;
			case Constants.NeedPage.NEEDS_FIN_EVAL:
				needFinEvalMgr.addValues(values, needCode, version, channelCode);
				break;
			case Constants.NeedPage.NEEDS_SELECTION:
				needSelectionMgr.addValues(values, needCode, version, channelCode);
				break;
			case Constants.NeedPage.NEEDS_PRIORITIZATION:
				needsPriorMgr.addValues(values, needCode, version, channelCode);
				break;
			case Constants.NeedPage.NEEDS_CONCEPT_SELLING:
				needConceptSellingMgr.addValues(values, needCode, version, channelCode);
				break;
			default:
				Map<String, JSONObject> _sections = needsDAO.getNeedsSections(needCode, principal, version, pageId, channelCode);
				
				if (_sections != null && _sections.containsKey(pageId)) {
					// Get attachments
					Map<String, Object> _attachs = needsDAO.getNeedsAttachContent(needCode, principal, version, channelCode, pageId);
					JsonObject attachsSet = new JsonObject();
				
					// load image from db
					if (_attachs != null) {
						for (Entry<String, Object> entry : _attachs.entrySet()) {
							String key = entry.getKey();
	
							Blob blob = (Blob) _attachs.get(key);
							byte[] bdata = blob.getBytes(1, (int) blob.length());
							String b64String = new String(bdata);
							attachsSet.remove(key);
							attachsSet.add(key, gson.toJsonTree(b64String));
						}
					}
	
					values.add(Constants.NeedPage.ATTACHMENTS, attachsSet);
					values.add(pageId,JsonConverter.convertObj(_sections.get(pageId)).getAsJsonObject());
				}
		}
	}
	
	public Response newVersion(String needCode, String channelCode) throws Exception {
		Response resp = getNeedDetail(needCode, channelCode, -1, "TaskInfo", true);
		resp.setAction(Constants.ActionTypes.CHANGE_CONTENT);
		return resp;
		
	}
	public Response getNeedDetail(String needCode, String channelCode, int version, String selectPageId, boolean isNewVersion) throws Exception {		
		if (isNewVersion) {
			Needs needsDAO = new Needs();
			version = needsDAO.createNewVersion(principal, needCode, channelCode, channelCode, false);

			if (version == 0)
				throw new Exception("Error to create a new version");
		}

		String action = "";

		// Action
		action = Constants.ActionTypes.CHANGE_PAGE;

		// select page
		if (selectPageId == null || selectPageId.equals(""))
			selectPageId = "TaskInfo";

		boolean isNewItem = false;
		
		if (version < 1) {
			isNewItem = true;
			version = 1;
		}

		// Read only
		boolean isReadOnly = false;
		boolean isSuspend = false;
		boolean canAccess = false;
		boolean updateRight = false;
		
		// Need status
		Map<String, Object> needStatus = needsDAO.getNeedStatusById(needCode, channelCode, version, principal);
		
		// RecordLock
		Dialog lockDialog = null;
		RecordLock recordLock = new RecordLock();
		RecordLockBean recordLockBean = recordLock.newRecordLockBean("/Needs/Detail", needCode+channelCode, version);
		recordLock.getRecordLock(request, recordLockBean);
		
		if (recordLockBean.canAccess()) {
			if(!recordLockBean.isCurrentUserAccess()) 
				recordLock.addRecordLock(request, recordLockBean);
			
			canAccess = true;
		} else {
			LockManager lm = new LockManager(request);
			lockDialog = lm.lockDialog(recordLockBean);
		}
		
		if (!canAccess) {
			isReadOnly = true;
		} else if(needStatus!=null) {
			if (needStatus.containsKey("launch_status") && needStatus.get("launch_status").toString().equals("L")) {
				isReadOnly = true;
			} else if (needStatus.containsKey("status") && needStatus.get("status").toString().equals("E")) {
				if (needStatus.containsKey("need_ver") && ((int)needStatus.get("need_ver")) >= version) 
					isReadOnly = true;
			} else if(needStatus.containsKey("task_status")) {
				if (needStatus.get("task_status").toString().equals("S"))
					isReadOnly = true;
				
				if (needStatus.get("task_status").toString().equals("D"))
					isSuspend = true;
			}
		}
		
		// Channel name
		String channelName = Functions.getChannelName(channelCode, principal.getCompCode());
		//check access right
		SysPrivilegeMgr privilegeMgr = new SysPrivilegeMgr();
		ArrayList<FuncBean> funcList = privilegeMgr.getFunc(principal.getRoleCode(), "en");
		
		for (FuncBean funcBean: funcList){
			if (funcBean.getCode().equals("FN.NEEDS.UPD"))
				updateRight = true;
		}
		
		boolean canEdit = isSuspend || isReadOnly || !updateRight;
		
		// Page
		JsonObject pageValue = new JsonObject();
		pageValue.addProperty("id", needCode);
		pageValue.addProperty("version", version);
		pageValue.addProperty("selectPageId", selectPageId);
		pageValue.addProperty("channelCode", channelCode);
		Page page = new Page("/Needs/Detail", langObj.getNameDesc("MENU.NEEDS"), pageValue);

		// Token
		String tokenID = Token.CsrfID(request.getSession());
		String tokenKey = Token.CsrfToken(request.getSession());

		// Template
		Template template = new Template();
		template.setEmptyMessage(langObj.getNameDesc("MESSAGE_ASSIGN_TASK"));
		template.setMenu(needsDAO.getMenu(needCode, principal, channelCode, version));

		List<Field> sections = new ArrayList<Field>();

		// Template - Task Info
		Tasks tasksDao = new Tasks();
		Map<String, Object> task = tasksDao.getTaskById(-1, needCode, version, Tasks.FuncCode.NEED, channelCode, principal);
		taskInfoMgr.addSection(task, sections, false, isReadOnly || !updateRight);
		addSection(sections, true);
		template.setItems(sections);

		// Content - values
		JsonObject contentValues = new JsonObject();
		contentValues.add("needCode", gson.toJsonTree(needCode));
		contentValues.add("id", gson.toJsonTree(needCode));
		contentValues.add("channelCode", gson.toJsonTree(channelCode));
		contentValues.add("version", gson.toJsonTree(version));
		contentValues.add("readOnly", gson.toJsonTree(canEdit));
		JsonObject selectPage = new JsonObject();
		selectPage.addProperty("id", selectPageId);
		contentValues.add("selectPage", gson.toJsonTree(selectPage));
		
		// Add default language code
		Language language = new Language();
		contentValues.add("defaultLangCode", gson.toJsonTree(language.getDefaultLang()));

		// Add company languages
		Companies company = new Companies();
		CompanyBean compBean = company.getCompany(principal.getCompCode());
		HashMap<String, String> langCodes = company.getCompanyLangNameMap(compBean, request);
		JsonArray _langCodes = new JsonArray();
		_langCodes.add(language.getDefaultLang());//default language should be the first one of the array
		
		for (String key: langCodes.keySet()) {
			if (!key.equals(language.getDefaultLang()))
				_langCodes.add(key);
		}
		
		contentValues.add("langCodes", _langCodes);
		
		addValues(selectPageId, contentValues, needCode, version, channelCode);
		
		if (needCode.equals("N0001"))
			addValues(Constants.NeedPage.NEEDS_CONCEPT_SELLING, contentValues, needCode, version, channelCode);
		
		// Content
		Content content = new Content(template, contentValues);

		// Action1
		List<Field> appBarAction1 = new ArrayList<>();

		// Action1 - 1
		if (!isReadOnly && updateRight) {
			Field appBarAction1_1 = new Field("/Needs/Detail/Save", "submitChangedButton", "save", langObj.getNameDesc("BUTTON.SAVE"), null);
			appBarAction1.add(appBarAction1_1);
		}
		
		// Action1 - 2
		if(!needStatus.get("status").toString().equals("T") && updateRight){
			List<Field> appBarAction1_3Items = new ArrayList<>();
			Field btnNewOk = new Field("/Needs/NewVersion", "submitButton", langObj.getNameDesc("BUTTON.OK").toString());
			Field btnNewCancel = new Field("confirm_cancel", "button", langObj.getNameDesc("BUTTON.CANCEL").toString());
			Dialog createNewVersionDialog = new Dialog("createNewVersionDialog", langObj.getNameDesc("CREATE_NEW_VER").toString(), langObj.getNameDesc("CREATE_NEW_VER_MSG").toString(), btnNewOk, btnNewCancel, 50);  
			appBarAction1_3Items.add(new Field("createNewVersionDialog", "dialogButton", langObj.getNameDesc("CREATE_NEW_VER"), createNewVersionDialog));
			Field appBarAction1_3 = new Field("more", "iconMenu", appBarAction1_3Items);
			
			appBarAction1.add(appBarAction1_3);
		}

		// Actions
		List<List<Field>> appBarActions = new ArrayList<>();
		appBarActions.add(appBarAction1);

		// AppBar
		// AppBar - Title
		// Secondary Level
		// Object secondary = new Field("needCode", AppBarTitle.Types.LABEL,
		// "/Needs/Detail", null, "TODO");

		String pageTitle = "";

		List<NeedsBean> needs = needsDAO.getNeeds(needCode, channelCode, principal.getCompCode(), principal);
		List<Option> secondaryOpts = new ArrayList<Option>();
				
		for (NeedsBean need : needs) {
			String _title = need.getNeedName() + " v." + String.valueOf(need.getVersion()) + " (" + need.getTaskStatus().toLowerCase() + ")";
			Option opt = new Option(_title, String.valueOf(need.getVersion()));
			
			if (needStatus.containsKey("need_ver") && ((int)needStatus.get("need_ver")) == need.getVersion())
				opt.setHighLight(true);
				
			secondaryOpts.add(opt);
			
			if (version == need.getVersion())
				pageTitle = _title;
		}
		
		Object secondary = new Field("/Needs/Detail", Types.VERSIONPICKER, String.valueOf(version), secondaryOpts, null);

		// First Level
		AppBarTitle appBarTitle = new AppBarTitle(AppBarTitle.Types.LEVEL_TWO, null, null, secondary);
		CommonManager cMgr = new CommonManager(request);
		cMgr.setAppbarPrimary(appBarTitle, "/Needs", channelName);

		AppBar appBar = new AppBar(appBarTitle, appBarActions);

		// Convert to Json
		Gson gson = new GsonBuilder().create();
		Response response = new Response(action, null, page, tokenID, tokenKey, appBar, content);

		if (lockDialog != null)
			response.setDialog(lockDialog);

		return response;
	}

	public void addSection(List<Field> sections, boolean isReadOnly) {
		try {
			Log.debug("---------------------[NeedsDetailMgr - start adding section]--------------------------");
			List<Field> needDetailItems = new ArrayList<Field>();

			needDetailItems.add(new Field("needCode", Types.TEXT, null, langObj.getNameDesc("CODE"), 100, "100%", null, true, isReadOnly, null, null, null, 500));
			needDetailItems.add(new Field("needName", Types.TEXT, null, langObj.getNameDesc("NAME"), 200, "100%", null, false,isReadOnly, null, null, null, null));
			needDetailItems.add(new Field("launchDate", Types.READONLY, null, langObj.getNameDesc("INIT_LAUNCH_DATE"), 300, "100%", null, false, isReadOnly, null, null, null, null));

			Field sectionNeedDetail = new Field(Constants.NeedPage.DETAIL, Types.SECTION, langObj.getNameDesc("DETAIL"), true, needDetailItems);

			sections.add(sectionNeedDetail);

			Log.debug("---------------------[NeedsDetailMgr - end adding section]--------------------------");
		} catch (Exception e) {
			Log.error(e);
		}
	}

	public void addValues(JsonObject values, String needCode, int version, String channelCode) {
		try {

			JsonObject needObj = needsDAO.getNeedDetail(needCode, version, principal, channelCode, this.principal.getLangCode());
			
			if (needObj != null) 
				values.add(Constants.NeedPage.DETAIL, needObj);
		} catch (Exception e) {
			Log.error(e);
		}
	}

}
