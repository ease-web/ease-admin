package com.eab.biz.releases;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONObject;

import com.eab.common.Constants;
import com.eab.common.Function;
import com.eab.dao.dynTemplate.DynData;
import com.eab.dao.tasks.Tasks;
import com.eab.database.jdbc.DBAccess;
import com.eab.database.jdbc.DBManager;
import com.eab.model.profile.UserPrincipalBean;
import com.eab.model.tasks.TaskBean;

public class DocMgr{
	UserPrincipalBean principal;
	public DocMgr(UserPrincipalBean principal) {
		this.principal = principal;	
	}
	
	//3 types of data
	//1. dyn json : JSONObject
	//2. rate json: JSONArray
	//3. attachment: JSONArray
	public JSONObject getReleaseDocs(int taskId, String docCode){
		DynData dataDao = new DynData();
		Connection conn = null;
		DBManager dbm = null;
		JSONObject result = new JSONObject();
		try{
			conn =  DBAccess.getConnection();
			dbm = new DBManager();
			
			Tasks taskDao = new Tasks();
			Map<String, Object> task = taskDao.getTaskById(taskId, null, -1, null, null, principal);
			String module = dataDao.getModuleByFuncCode(conn, task.get("func_code").toString());

			//get rates and json of modules such as product
			JSONArray results = dataDao.getAllDynJson(taskId, dbm, conn);
			dbm.clear();
			
			JSONObject main = new JSONObject();
			JSONArray rates = new JSONArray();
			int size = results.length();
			
			String compCode = principal.getCompCode();
			
			//doc code for module
			String cbDocId = null;
			
			int version = -1;
			// 
			for(int i = 0; i < size; i++){
				JSONObject json = results.getJSONObject(i);
				if(version == -1){
					version = json.getInt("version");
					cbDocId = compCode + "_" + module + "_" + docCode + "_" + version;
				}
				if(docCode.equals(json.getString("id"))){
					JSONObject jFile = json.getJSONObject("json");
					jFile.put("version", version);
					main.put("id", cbDocId);
					main.put("json", convertFinalJson(jFile, module));
				}else{
					rates.put(json);
				}
			}
			result.put("main", main);
			
			//make id for rate
			int rateSize = rates.length();
			JSONArray _rates = new JSONArray();
			for(int i = 0; i < rateSize; i++){
				JSONObject rate = rates.getJSONObject(i);
				String rateId = rate.getString("id");
				rateId = cbDocId + "_" + rateId;
				//add id
				rate.put("id", rateId);
				JSONObject rateJson = rate.getJSONObject("json");
				//add type
				rateJson.put("type", "nfor_tab");
				rate.put("json", rateJson);
				
				_rates.put(rate);
			}
			result.put("rates", _rates);
			
			
			//get keys 
			HashMap<String, Object> primaryKeys = new HashMap<String, Object>();
			
			primaryKeys.put("comp_code", compCode);
			primaryKeys.put("module", module);
			primaryKeys.put("doc_code", docCode);
			primaryKeys.put("version", version);
			
			//get attachments
			JSONArray atts = dataDao.getAttachments(primaryKeys, dbm, conn);
			result.put("attachments", atts);
			dbm.clear();
			
			
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try {
				conn.close();
				conn = null;
				dbm.clear();
				dbm = null;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		return result;
	}
	
	public JSONObject convertFinalJson(JSONObject obj, String module){
		JSONObject finalJson = obj;
		try {
			String type = module;
			if("pdf".equalsIgnoreCase(type)){
				type = "pdfTemplate";
			}
			finalJson.put("type", type);
			finalJson.put("compCode", principal.getCompCode());
			finalJson.put("status", "A");
			//releaseDate
			finalJson.put("releaseDate", Function.getToday());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		return finalJson;
	}
	
	
	
	
}
