package com.eab.biz.releases;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.json.JSONObject;
import com.eab.biz.common.CommonManager;
import com.eab.biz.tasks.TaskMgr;
import com.eab.common.Constants;
import com.eab.common.Language;
import com.eab.common.Token;
import com.eab.json.model.Dialog;
import com.eab.dao.releases.ReleaseDetail;
import com.eab.dao.tasks.Tasks;
import com.eab.json.model.Field;
import com.eab.model.profile.UserPrincipalBean;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.eab.json.model.AppBar;
import com.eab.json.model.AppBarTitle;
import com.eab.json.model.Content;
import com.eab.json.model.DialogItem;
import com.eab.json.model.Option;
import com.eab.json.model.Page;
import com.eab.json.model.Response;
import com.eab.json.model.SearchCondition;
import com.eab.json.model.Template;
import com.eab.model.releases.ReleasesDetailBean;

public class ReleaseDetailMgr {
	
	HttpServletRequest request;
	HttpSession session;
	UserPrincipalBean principal;
	Language langObj;
	
	public ReleaseDetailMgr(HttpServletRequest request) {
		super();
		this.request = request;
		this.session = request.getSession();
		this.principal = (UserPrincipalBean) session.getAttribute(Constants.USER_PRINCIPAL);
		this.langObj = new Language(principal.getUITranslation());
	}
	
	public ReleaseDetailMgr() {
		super();
	}
	
    public Response getJson(String releaseID) throws Exception {
        return getJson(releaseID, false, "", "", 0, 0, null, false);
    }
    
    public Response getJson(String releaseID, Dialog dialog) throws Exception {
        return getJson(releaseID, false, "", "", 0, 0, dialog, false);
    }
    public Response getJson(String releaseID, boolean change_content, String sortBy, String sortDir, int recordStart, int pageSize, Boolean reloadStatus) throws Exception {
    	return getJson(releaseID, change_content, sortBy, sortDir, recordStart, pageSize, null, reloadStatus);
    }
    
	public Response getJson(String releaseID, boolean change_content, String sortBy, String sortDir, int recordStart, int pageSize, Dialog inDialog, Boolean reloadStatus) throws Exception {
 
		ReleaseDetail releaseDAO = new ReleaseDetail();
		
		ArrayList<Map<String, Object>> details = releaseDAO.getReleaseDetail(request, principal, releaseID, langObj);
		
		Map<String, Object> detail = null;
		
		if (details != null && details.size() > 0) {
			detail = details.get(0);
		}
		
		ArrayList<Map<String, Object>> tasks = null;

		String filterKey = releaseID + " " + sortBy + " " + sortDir;
		String curFilterKey = session.getAttribute(Constants.SessionKeys.FILTER_KEY) != null ? (String) session.getAttribute(Constants.SessionKeys.FILTER_KEY) : null;

		if (recordStart > 0 && filterKey.equals(curFilterKey)) {
			Object temp = session.getAttribute(Constants.SessionKeys.SEARCH_RESULT);
			tasks = temp != null && temp instanceof ArrayList ? (ArrayList<Map<String, Object>>) temp: null;
		} 
		Boolean isBaseDiff = false;
		if (tasks == null || reloadStatus ) {
			tasks = releaseDAO.getTaskList(request, principal, releaseID, sortBy, sortDir, langObj);
//TODO : for checking base version difference when release			
//			Map<String, Integer> mstVerMap = releaseDAO.getMstVerMap();
//			Map<String, Integer> trxVerMap= releaseDAO.getTrxVerMap();
//			for(int i=0;i<tasks.size();i++){
//				Map<String, Object> task = tasks.get(i);
//				String compCode = (String) task.get("comp_code");
//				String prodCode = (String) task.get("prod_code");
//				String version = (String) task.get("version");
//				String type = (String) task.get("type");  
//				int prodVer = -1; 
//				String prefix = "";
//				switch (type){
//					case "Products":
//						prefix  = "prod";
//						break;
//					case "Needs":
//						prefix  = "need";
//						break;
//					case "Benefit Illustrations":
//						prefix  = "bi";
//						break;
//					 	 
//				}
//				 
//				String prodVerKey = prefix+"_"+ compCode+"_"+prodCode;
//				if( mstVerMap.get(prodVerKey) != null){
//					prodVer = mstVerMap.get(prodVerKey);
//					 
//				}
//				 
//				int trxVer = -1;
//				String trxVerKey = prefix+"_"+ compCode+"_"+prodCode+"_"+version;
//				if(  trxVerMap.get(trxVerKey) != null){
//					trxVer = trxVerMap.get(trxVerKey);
//					 
//				}
//				 
//				if(trxVer>0 && trxVer != prodVer)
//					isBaseDiff=true; 
//			}
			 
			session.setAttribute(Constants.SessionKeys.FILTER_KEY, filterKey);
			session.setAttribute(Constants.SessionKeys.SEARCH_RESULT, tasks);
		}
		
		//Get template
		TaskMgr taskmgr = new TaskMgr(request);
		List<Field> item = this.getReleaseDetailTemplate();
		List<Field> item2 = taskmgr.getTemplate();
		
		String action = "";	
		
		//Action		
		if(change_content){
			//Page content change
			action =  Constants.ActionTypes.CHANGE_CONTENT;
		} else {
			//First time load
			action =  Constants.ActionTypes.CHANGE_PAGE;
		}
		 
		//Page
		JsonObject pageValue = new JsonObject();
		pageValue.addProperty("releaseID", releaseID);
		pageValue.addProperty("sortBy", sortBy);
		pageValue.addProperty("sortDir", sortDir);
		//pageValue.addProperty("recordStart", recordStart);
		pageValue.addProperty("pageSize", pageSize);
		Page page = new Page("/Releases/Detail", langObj.getNameDesc("MENU.RELEASES"), pageValue);
		
	    //Token	    
		String tokenID = Token.CsrfID(request.getSession());
		String tokenKey = Token.CsrfToken(request.getSession());

	    //Template		
		Template template = new Template();
		template.setDetailItems(item);
		template.setItems(item2);
		template.setType("table");
		
		template.setEmptyMessage(langObj.getNameDesc("MESSAGE_ASSIGN_TASK"));
				
		//More
	    int offset = 0;
	    int size = 10;	  

		int taskListCount = tasks.size();
		
		List<Map<String,Object>> shortedTasks = new ArrayList<Map<String,Object>>();
		
		if (reloadStatus) {
			for (int j = 0; j<  Math.min(size, taskListCount); j++) 
				shortedTasks.add(tasks.get(j));
		} else {
			offset = recordStart; 
			for (int j = offset; j< offset + size && j<taskListCount; j++) 
				shortedTasks.add(tasks.get(j));			 
		}
		
		size = pageSize != 0 ? pageSize : Constants.SEARCH_RESULT_DEFAULT_PAGE_SIZE;
	    
	    Map<String, Object> temp1 = new HashMap<String, Object>();
	    temp1.put("releaseID", releaseID);
	    
	    Map<String, Object> temp2 = new HashMap<String, Object>();
	    temp2.put("keyList", temp1);
	    
	    ArrayList<Map<String, Object>> tempList = new ArrayList<Map<String, Object>>();
	    tempList.add(temp2);

	    String strOK = langObj.getNameDesc("BUTTON.OK");
	    String strCancel = langObj.getNameDesc("BUTTON.CANCEL");
	    
	    //Action button
	    Field btnCancel = new Field("confirm_cancel", "button", strCancel);
	    
		//Get detail information
		String releaseName = "";
		String remarks = "";
		long targetDate = 0;
		String statusCode = "";
		String scheduledDate = "";
		String scheduledTime = "";
		String isPublishing = "";
				
		try {
			releaseName = (String) detail.get("releaseName");
			remarks = (String) detail.get("remarks");
			targetDate = (long) detail.get("targetReleaseDate");
			statusCode = (String) detail.get("statusCode");
			scheduledDate = (String) detail.get("scheduledDate");
			scheduledTime = (String) detail.get("scheduledTime");
			isPublishing = (String) detail.get("isPublishing");
			
		} catch(Exception e) {}
	
	    //Edit dialog
	    Field btnOk = new Field("/Releases/Detail/Edit", "submitButton", strOK);
		
		DialogItem txtReleaseName = new DialogItem("releaseName", DialogItem.Types.TEXT, releaseName, true, "", langObj.getNameDesc("RELEASE_NAME"), 50);		
		DialogItem txtReleaseDate = new DialogItem("targetDate", DialogItem.Types.DATEPICKER, targetDate, true, "", langObj.getNameDesc("TARGET_REL_DATE"), this.principal.getDateFormat());
		DialogItem txtRemarks = new DialogItem("remark", DialogItem.Types.TEXT, remarks, false, "", langObj.getNameDesc("REMARK"), 100);
		List<Field> dialogItems = new ArrayList<>();
		dialogItems.add(txtReleaseName);
		dialogItems.add(txtReleaseDate);
		dialogItems.add(txtRemarks);

		Dialog editReleaseDialog = new Dialog("editReleaseDialog", langObj.getNameDesc("EDIT_RELEASE"), "", btnOk, btnCancel, dialogItems, 30);  
		
	    //Unassign dialog
		Field btnUnassignOk = new Field("/Releases/Detail/Unassign", "submitButton", strOK);
		Dialog confirmUnassignDialog = new Dialog("confirmUnassignDialog", langObj.getNameDesc("TASK_UNASSIGN"), langObj.getNameDesc("TASK_CONFIRM_UNASSIGN"), btnUnassignOk, btnCancel, 30); 
		
		//Reassign dialog
		Field btnReassignOk = new Field("/Releases/Detail/Reassign", "submitButton", strOK);
		Dialog confirmReassignDialog = new Dialog("confirmReassignDialog", langObj.getNameDesc("TASK_REASSIGN"), langObj.getNameDesc("TASK_CONFIRM_REASSIGN"), btnReassignOk, btnCancel, 30); 
	    
	    //Suspend dialog
		Field btnSuspendOk = new Field("/Releases/Detail/Suspend", "submitButton", strOK);
		Dialog confirmSuspendDialog = new Dialog("confirmSuspendDialog", langObj.getNameDesc("TASK_CONFIRM_SUSPEND"),langObj.getNameDesc("TASK_CONFIRM_SUSPEND"), btnSuspendOk, btnCancel, 30); 
	    
		//Get total for assigned tasks
//		ArrayList<String> taskList  = (ArrayList<String>) list.get(0).get("taskList");
				
		
		Map<String, Object> text = new HashMap<String, Object>();
		text.put("BUTTON_SUSPEND", langObj.getNameDesc("BUTTON.SUSPEND"));
		text.put("BUTTON_UNASSIGN", langObj.getNameDesc("BUTTON.UNASSIGN"));
		text.put("BUTTON_REASSIGN", langObj.getNameDesc("BUTTON.REASSIGN"));
		text.put("BUTTON_SUSPEND", langObj.getNameDesc("BUTTON.SUSPEND"));
		text.put("BUTTON_EDIT", langObj.getNameDesc("BUTTON.EDIT"));
		text.put("BUTTON_ADD", langObj.getNameDesc("BUTTON.ADD"));
		text.put("MENU_TASKS", langObj.getNameDesc("MENU.TASKS"));
		text.put("RELEASE_DETAILS", langObj.getNameDesc("RELEASE_DETAILS"));
				
		String isRecordLocked = "N";
		
//		if(inDialog != null)
//			isRecordLocked = "Y";
		
	    //Content - values
	    ReleasesDetailBean contentValues = new ReleasesDetailBean(detail, shortedTasks, taskListCount, offset > 0, releaseID, tempList, confirmUnassignDialog, confirmReassignDialog, confirmSuspendDialog, editReleaseDialog, text, isRecordLocked);
	    	    	    
	    //Content
	    Content content = new Content(template, contentValues);
	    
	    //AppBar
	    AppBar appBar = getAppBar(releaseName, statusCode, taskListCount , scheduledDate, scheduledTime, isPublishing, isRecordLocked, isBaseDiff);
		SearchCondition cond = SearchCondition.RetrieveSearchCondition(request);
		appBar.setValues(cond);
	    
	    //Convert to Json  
		Gson gson = new GsonBuilder().create();
		Response response = new Response(action, null, page, tokenID, tokenKey, appBar, content);

		if(inDialog != null)
			response.setDialog(inDialog);
		
	    return response;
	}
		
	public Response updateRelease(String releaseID, String releaseName, Date targetDate, String remarks) throws Exception {
		ReleaseDetail releaseDetailDAO = new ReleaseDetail();	
		releaseDetailDAO.updateRelease(releaseID, releaseName, targetDate, remarks, principal);
	
		return getJson(releaseID);
	}
			
	public JSONObject abortRelease(String releaseID) throws Exception {
		Map<String , Object> outputJson = new HashMap<String, Object>();
		ArrayList<Map<String, Object>> list = this.stopRelease(releaseID);
		
		outputJson.put("list", list);
		
	    JSONObject outputJSON = new JSONObject(outputJson);
	    
	    return outputJSON;		
	}
	
	public ArrayList<Map<String, Object>> stopRelease(String releaseID) throws Exception {	
		ArrayList<Map<String, Object>> output = new ArrayList<Map<String, Object>>();
		ReleaseDetail releaseDAO = new ReleaseDetail();
		Map<String, Object> outMap = new HashMap<String, Object>();
		
		outMap.put("detail" , releaseDAO.abortRelease(request, principal, releaseID));
		output.add(outMap);
		
		return output;
	}	
	
	public JSONObject deleteRelease(String releaseID) throws Exception {
		Map<String , Object> outputJson = new HashMap<String, Object>();
		ArrayList<Map<String, Object>> list = this.delRelease(releaseID);
		outputJson.put("list", list);
		
	    JSONObject outputJSON = new JSONObject(outputJson);
	    
	    return outputJSON;		
	}
	
	public ArrayList<Map<String, Object>> delRelease(String releaseID) throws Exception {	
		ArrayList<Map<String, Object>> output = new ArrayList<Map<String, Object>>();
		ReleaseDetail releaseDAO = new ReleaseDetail();
		Map<String, Object> outMap = new HashMap<String, Object>();
		
		outMap.put("detail" , releaseDAO.deleteRelease(request, principal, releaseID));
		output.add(outMap);
		
		return output;
	}		
	
	public JSONObject rescheduleRelease(String releaseID , String targetDate) throws Exception {
		Map<String , Object> outputJson = new HashMap<String, Object>();
		ArrayList<Map<String, Object>> list = this.reschRelease(releaseID, targetDate);
		
		outputJson.put("list", list);
		
	    JSONObject outputJSON = new JSONObject(outputJson);
	    	    
	    return outputJSON;		
	}
	
	public ArrayList<Map<String, Object>> reschRelease(String releaseID, String targetDate) throws Exception {	
		ArrayList<Map<String, Object>> output = new ArrayList<Map<String, Object>>();
		ReleaseDetail releaseDAO = new ReleaseDetail();
		Map<String, Object> outMap = new HashMap<String, Object>();
		
		outMap.put("detail" , releaseDAO.rescheduleRelease(request, principal, releaseID ,targetDate));
		output.add(outMap);
		
		return output;
	}		

	public AppBar getAppBar(String releaseName, String statusCode, int taskListCount, String scheduledDate, String scheduledTime, String isPublishing, String isRecordLocked, Boolean isBaseDiff) throws Exception{		
	    //AppBar - Title	    		
	    //Secondary Level
		Object secondary = new Field("releaseID", AppBarTitle.Types.LABEL, "/Releases/Detail", null, releaseName);
		
	    //First Level
		AppBarTitle appBarTitle = new AppBarTitle(AppBarTitle.Types.LEVEL_TWO, null, null, secondary);
		CommonManager cMgr = new CommonManager(request);
		cMgr.setAppbarPrimary(appBarTitle, "/Releases");
	    
	    String strOK = langObj.getNameDesc("BUTTON.OK");
	    String strCancel = langObj.getNameDesc("BUTTON.CANCEL");
			    
		//AppBar Dialog
		List<Option> testENVOptions = new ArrayList<>();
		ReleaseDetail releaseDAO = new ReleaseDetail();
		
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//Test Button
		Field testENVPositive = new Field("/Releases/Test", "submitButton", strOK);
		Field testENVNegative = new Field("test_cancel", "button", strCancel);
		
		ArrayList<Map<String , Object>> testENVList = releaseDAO.getTestENVList(request, principal);
		
		if(testENVList != null){
			for(Map<String, Object>testENV : testENVList){
				testENVOptions.add(new Option(testENV.get("envName").toString(), testENV.get("envID").toString()));
			}		
		}
		
		List<Field> testENVDialogItems = new ArrayList<>();
		DialogItem cdi_radioButton = new DialogItem("radio_ENV_Detail", DialogItem.Types.RADIOBUTTON, null, true, testENVOptions);
		testENVDialogItems.add(cdi_radioButton);
//		TODO: john: alert user when base version diff found		
//		Field baseDiffNegative = new Field("", "button", strOK);
//		List<Field> baseDiffDialogItems = new ArrayList<>();
//		Dialog baseDiffDialog = new Dialog("baseDiffDialog", langObj.getNameDesc("DIFFERENT_BASE_VERSION"), "", null, baseDiffNegative, baseDiffDialogItems, 30);
		
		Dialog testENVDialog = new Dialog("testENVDialog", langObj.getNameDesc("SELECT_TEST_ENV"), "", testENVPositive, testENVNegative, testENVDialogItems, 30);		
		Field appTest = null;
//		if(!isBaseDiff)
			appTest = new Field( "/Releases/Detail/Test", "dialogButton", langObj.getNameDesc("TEST"), testENVDialog);
//		else
//			appTest = new Field( "", "dialogButton", langObj.getNameDesc("TEST"),  baseDiffDialog );
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////
   
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//Release Button
		Field releasePositive = new Field("/Releases/Schedule", "submitButton", strOK);
		Field releaseNegative = new Field("release_cancel", "button", strCancel);
		
		List<Field> releaseDialogItems = new ArrayList<>();
	
		DialogItem di_ReleaseDate = new DialogItem("scheduleDate_Detail", DialogItem.Types.DATEPICKER, 0, true, langObj.getNameDesc("DATE"), langObj.getNameDesc("DATE"), this.principal.getDateFormat());
		DialogItem di_ReleaseTime = new DialogItem("scheduleTime_Detail", DialogItem.Types.TIMEPICKER, 0, true, langObj.getNameDesc("TIME"), langObj.getNameDesc("TIME"), this.principal.getTimeFormat());
		releaseDialogItems.add(di_ReleaseDate);
		releaseDialogItems.add(di_ReleaseTime);

		Dialog releaseDialog = new Dialog("releaseDialog", langObj.getNameDesc("SCHEDULE_RELEASE"), "", releasePositive, releaseNegative, releaseDialogItems, 30);
		
		Field appRelease = null;
//		if(!isBaseDiff)
			appRelease = new Field( "/Releases/Schedule", "dialogButton", langObj.getNameDesc("RELEASE"),  releaseDialog);
//		else
//			appRelease = new Field(  "baseDiffDialog", "dialogButton", langObj.getNameDesc("RELEASE"), baseDiffDialog);
		////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
	    //Reschedule
		Field reschedulePositive = new Field("/Releases/Schedule", "submitButton", strOK);
		Field rescheduleNegative = new Field("release_cancel", "button", strCancel);
		
		List<Field> rescheduleDialogItems = new ArrayList<>();
						
		DialogItem di_OriginReleaseLabel = new DialogItem("rescheduleLabel_Org", DialogItem.Types.LABEL, langObj.getNameDesc("RELEASE.RESCHEDULE.LABEL.ORIGIN"), 1, 0);
		DialogItem di_OriginReleaseDate = new DialogItem("rescheduleDate_Org", DialogItem.Types.TEXT, scheduledDate, false, "", langObj.getNameDesc("DATE"), 100);
		DialogItem di_OriginReleaseTime = new DialogItem("rescheduleTime_Org", DialogItem.Types.TEXT, scheduledTime, false, "", langObj.getNameDesc("TIME"), 100);
		DialogItem di_ReleaseLabel = new DialogItem("rescheduleLabel", DialogItem.Types.LABEL, "<br>" + langObj.getNameDesc("RELEASE.RESCHEDULE.LABEL.NEW"), 1, 0);
		DialogItem di_RescheduleDate = new DialogItem("scheduleDate_Detail", DialogItem.Types.DATEPICKER, null, true, langObj.getNameDesc("DATE"), langObj.getNameDesc("DATE"), 100);
		DialogItem di_RescheduleTime = new DialogItem("scheduleTime_Detail", DialogItem.Types.TIMEPICKER, null, true, langObj.getNameDesc("TIME"), langObj.getNameDesc("TIME"), 100);
		di_OriginReleaseDate.setDisabled(true);
		di_OriginReleaseTime.setDisabled(true);
		rescheduleDialogItems.add(di_OriginReleaseLabel);
		rescheduleDialogItems.add(di_OriginReleaseDate);
		rescheduleDialogItems.add(di_OriginReleaseTime);	
		rescheduleDialogItems.add(di_ReleaseLabel);
		rescheduleDialogItems.add(di_RescheduleDate);
		rescheduleDialogItems.add(di_RescheduleTime);

		Dialog rescheduleDialog = new Dialog("releaseDialog", langObj.getNameDesc("SCHEDULE_RELEASE"), "", reschedulePositive, rescheduleNegative, rescheduleDialogItems, 30);
		Field appReschedule = new Field("/Releases/Schedule", "dialogButton", langObj.getNameDesc("RESCHEDULE"), rescheduleDialog);
	    
	    //Export Note
	    Field appExportNote = new Field("/TaskRelease4", "submitSelectedButton", langObj.getNameDesc("EXPORT_NOTE"));
	    	    
	    //Abort
		Field btnAbortOk = new Field("/Releases/Abort", "submitButton", strOK);
		Field btnAbortCancel = new Field("confirm_cancel", "button", strCancel);
		com.eab.json.model.Dialog confirmAbortDialog = new com.eab.json.model.Dialog("confirmAbortDialog", langObj.getNameDesc("RELEASE_ABORT"), langObj.getNameDesc("RELEASE_CONFIRM_ABORT"), btnAbortOk, btnAbortCancel, 30);  
	    Field appAbort = new Field("A", "dialogButton", langObj.getNameDesc("ABORT"), confirmAbortDialog);
	    
	    //Delete
		Field btnDeleteOk = new Field("/Releases/Delete", "submitButton", strOK);
		Field btnDeleteCancel = new Field("confirm_cancel", "button", strCancel);
		com.eab.json.model.Dialog confirmDeleteDialog = new com.eab.json.model.Dialog("confirmDeleteDialog", langObj.getNameDesc("RELEASE_DELETE"), langObj.getNameDesc("RELEASE_CONFIRM_DELETE"), btnDeleteOk, btnDeleteCancel, 30);  
	    Field appDelete = new Field("D", "dialogButton", langObj.getNameDesc("DELETE"), confirmDeleteDialog);
	    
	    List<Field> appBarAction = new ArrayList<>();
	    
	    if ((statusCode.equals("T") || statusCode.equals("P")) && isPublishing.equals("N") && isRecordLocked.equals("N")) {//Release status = Pending or Testing && is publishing == false && record locked == false
	    	if (taskListCount > 0){
	    		appBarAction.add(appTest);
	    		appBarAction.add(appRelease);
	    	}
	    	//appBarAction.add(appExportNote);//TODO next phase
	    	appBarAction.add(appDelete);
	    } else if (statusCode.equals("S")) {//Release status = Scheduled
	    	appBarAction.add(appReschedule);
	    	appBarAction.add(appAbort);	    	
	    	//appBarAction.add(appExportNote);//TODO next phase
	    } else {
	    	//appBarAction.add(appExportNote);//All statues //TODO next phase
	    }
	   		
	    //AppBar - Actions	    
	    List<List<Field>> appBarActions = new ArrayList<>();
	    appBarActions.add(appBarAction);
	    
	    //AppBar
		AppBar appBar = new AppBar(appBarTitle, appBarActions);
	   
	    return appBar;
	}
	
	public List<Field> getReleaseDetailTemplate() throws Exception {
	    List<String> List = Arrays.asList("RELEASE_ID", "RELEASE_NAME", "STATUS", "UPD_BY", "CREATE_BY", "REMARKS");
	    List<String> ListId = Arrays.asList("releaseID", "releaseName", "status", "updateBy", "createBy" , "remarks");
	    
	    int seq = 0;
	    List<Field> template = new ArrayList<>();
	    
	    for(String label: List) {	    	
	    	Field field1 = new Field(ListId.get(seq),langObj.getNameDesc(label),null, seq+1, seq+1,null);
	    	template.add(field1);
		    
		    seq ++;
	    }

	    return template;
	}		
	
	public String assignRelease(List<Object> taskList, int newReleaseID) throws Exception{		
		Tasks taskDAO = new Tasks();
		taskDAO.assignTasks(newReleaseID, taskList, principal);
		
		return "";
	}

	public boolean UpdateStatus(String releaseID, String status) throws Exception {		
		return UpdateStatus(releaseID, status, "");
	}
	
	public boolean UpdateStatus(String releaseID, String status, String compCode) throws Exception {
		ReleaseDetail releaseDAO = new ReleaseDetail();
		UserPrincipalBean principal = new UserPrincipalBean();
		
		if(request == null){
			principal.setCompCode(compCode);
			principal.setUserName("SYSTEM");
			principal.setUserCode("SYSTEM");
		}else{
			HttpSession session = request.getSession();
			if(session!=null){
				principal = (UserPrincipalBean) session.getAttribute(Constants.USER_PRINCIPAL);}
			else{
				principal.setCompCode(compCode);
				principal.setUserName("SYSTEM");
				principal.setUserCode("SYSTEM");				
			}
		}

		return releaseDAO.UpdateStatus(principal, releaseID, status);
	}
		
}
