package com.eab.json.model;

import java.io.Serializable;

public class Condition implements Serializable {

	private String lookUp;
	private String value;

	public Condition(String lookUp, String value) {
		super();
		this.lookUp = lookUp;
		this.value = value;
	}

	public String getLookUp() {
		return lookUp;
	}

	public void setLookUp(String lookUp) {
		this.lookUp = lookUp;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
