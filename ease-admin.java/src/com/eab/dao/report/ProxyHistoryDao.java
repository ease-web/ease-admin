package com.eab.dao.report;

import java.sql.Connection;

import org.json.JSONArray;

import com.eab.common.Log;
import com.eab.database.jdbc.DBAccess;
import com.eab.database.jdbc.DBManager;
import com.eab.database.jdbc.DataType;

public class ProxyHistoryDao {
	
	Connection conn = null;
    DBManager dbm = null;
	
	public JSONArray getProxyHistory(java.sql.Date fromTime, java.sql.Date toTime) throws Exception {
		
		dbm = 	new DBManager();

		String sqlStatement = "select b.p_name, (select \"NAME\" from user_profile where user_id = a.proxy1) as proxy1, (select \"NAME\" from user_profile where user_id = a.proxy2) as proxy2, (select \"NAME\" from user_profile where agent_no = b.upline_no_2) as top, to_char( cast(a.start_dt as timestamp) at time zone 'UTC', 'yyyy-MM-dd hh24:mi:ss') as start_dt, to_char( cast(a.end_dt as timestamp) at time zone 'UTC', 'yyyy-MM-dd hh24:mi:ss') as end_dt, b.auth_group from API_AGT_PROXY_HIST a, (select upline_no_2, \"NAME\" as p_name, auth_group, user_id from user_profile) b where a.user_id = b.user_id AND ((start_dt BETWEEN ? AND ?) OR (end_dt BETWEEN ? AND ?) OR (? BETWEEN start_dt AND end_dt) OR (? BETWEEN start_dt AND end_dt)) order by b.p_name ASC";		
		
		dbm.param(fromTime, DataType.DATE);
		dbm.param(toTime, DataType.DATE);
		dbm.param(fromTime, DataType.DATE);
		dbm.param(toTime, DataType.DATE);
		dbm.param(fromTime, DataType.DATE);
		dbm.param(toTime, DataType.DATE);
		
		JSONArray resultArrayJson = null;

		try {
			conn = DBAccess.getConnectionApi();

			resultArrayJson = dbm.selectRecords(sqlStatement, conn);

		} catch (Exception ex) {
			Log.error(ex);
			throw ex;
		} finally {
			deallocate();
		}

		return resultArrayJson;

	}
	
	private void deallocate() throws Exception {
		dbm.clear();
		dbm = null;
		if (conn != null && !conn.isClosed())
			conn.close();
		conn = null;
	}
}
