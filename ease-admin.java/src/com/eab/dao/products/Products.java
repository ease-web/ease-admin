package com.eab.dao.products;

import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.json.JSONObject;
import com.eab.common.Function;
import com.couchbase.client.java.query.dsl.Functions;
import com.eab.common.Function;
import com.eab.common.JsonConverter;
import com.eab.common.Language;
import com.eab.common.Log;
import com.eab.dao.common.DynMenu;
import com.eab.dao.tasks.Tasks;
import com.eab.database.jdbc.DBAccess;
import com.eab.database.jdbc.DBManager;
import com.eab.database.jdbc.DataType;
import com.eab.database.jdbc.IOType;
import com.eab.json.model.Option;
import com.eab.json.model.ProductListCondition;
import com.eab.json.model.Template;
import com.eab.model.products.ProductAttachBean;
import com.eab.model.products.ProductBean;
import com.eab.model.profile.UserPrincipalBean;
import com.eab.model.tasks.TaskBean;
import com.eab.webservice.dynamic.DynamicFunction;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class Products {

	public final String NO_INSERT = "ERR_MSG.NO_INSERT";
	public final String NO_UPDATE = "ERR_MSG.NO_UPDATE";
	public final String DUPLICATE_CODE = "ERR_MSG.DUPLICATE_CODE";
	public final String CODE_WITHOUT_SPACE = "ERR_MSG.CODE_WITHOUT_SPACE";
	public final String RETURN_TEMPLATE = "RETURN_TEMPLATE";
	public final String RETURN_JSON = "RETURN_JSON";
	
	
	public boolean saveProdJson(UserPrincipalBean principal, Connection conn, JSONObject json, String prodCode, int version)throws SQLException{
		ResultSet rs = null;
		boolean isNew = false;
		DBManager dbm;
		try{
			if(conn == null){
				isNew = true;
				conn = DBAccess.getConnection();
			}
			dbm = new DBManager();
			String userCode = principal.getUserCode();
			String compCode = principal.getCompCode(); 
			boolean isProdExist = checkExist(prodCode, compCode, version, "prod_json");
			if(isProdExist){
				String sql = "update prod_json set "
							+ " json_file = " + dbm.param(json.toString(), DataType.TEXT) + ","
							+ " upd_by = " + dbm.param(userCode, DataType.TEXT) + ", "
							+ "upd_date = CURRENT_TIMESTAMP";
				return (dbm.update(sql) != -1);
			}else{
				String sqlInsert = "insert into prod_json"
									+" (comp_code, prod_code, version, json_file, upd_by, create_by, upd_date, create_date)"
									+" values ("
									+ dbm.param(compCode, DataType.TEXT) + ","
									+ dbm.param(prodCode, DataType.TEXT) + ","
									+ dbm.param(version, DataType.INT) + ","
									+ dbm.param(json.toString(), DataType.TEXT) + ","
									+ dbm.param(userCode, DataType.TEXT) + ","
									+ dbm.param(userCode, DataType.TEXT) + ","
									+ "CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)";
				return dbm.insert(sqlInsert);
			}
			
		}catch(Exception e){
			Log.error("saveProdJson error: " + e.getMessage());	
		}finally{
			dbm = null;
			if(isNew){
				conn.close();
				conn = null;		
			}
		}
		return false;
	}
	
	public boolean saveProdSection(UserPrincipalBean principal, Connection conn, JSONObject json, String prodCode, String sectionCode, int version) throws SQLException{
		//Log.error("saveProdSection: " + json);
		ResultSet rs = null;
		boolean isNew = false;
		DBManager dbm;
		try{
			if(conn == null){
				isNew = true;
				conn = DBAccess.getConnection();
			}
			dbm = new DBManager();
			String userCode = principal.getUserCode();
			String compCode = principal.getCompCode();
			String checkSql = "select json_file from prod_section "
								+ " WHERE comp_code = " + dbm.param(compCode, DataType.TEXT)
						    	+ " and prod_code = " + dbm.param(prodCode, DataType.TEXT)
						    	+ " and section_code = " + dbm.param(sectionCode, DataType.TEXT)
						    	+ " and version = " + dbm.param(version, DataType.INT);
			rs = dbm.select(checkSql, conn);
			if(rs.next()){
				dbm.clear();
				String sql = "UPDATE prod_section"
				    	+ " SET json_file = " + dbm.param(json.toString(), DataType.TEXT)   
				    	+ " , upd_date = CURRENT_TIMESTAMP"
				    	+ " , upd_by = " + dbm.param(userCode, DataType.TEXT)
				    	+ " WHERE comp_code = " + dbm.param(compCode, DataType.TEXT)
				    	+ " and prod_code = " + dbm.param(prodCode, DataType.TEXT)
				    	+ " and section_code = " + dbm.param(sectionCode, DataType.TEXT)
				    	+ " and version = " + dbm.param(version, DataType.INT);
				return dbm.update(sql, conn) != -1;
			}else{
				dbm.clear();
				String sqlInsert = " INSERT INTO prod_section (comp_code, prod_code, version, section_code, json_file, create_by, upd_by)"
						    		+ " VALUES (" 
						    		+ dbm.param(compCode, DataType.TEXT)  + ","
						    		+ dbm.param(prodCode, DataType.TEXT) + ","
						    		+ dbm.param(version, DataType.INT) + ","
						    		+ dbm.param(sectionCode, DataType.TEXT) + ","
						    		+ dbm.param(json.toString(), DataType.TEXT) + ","
						    		+ dbm.param(userCode, DataType.TEXT) + ","
						    		+ dbm.param(userCode, DataType.TEXT) + ")";
				return dbm.insert(sqlInsert, conn);
			}
			
		}catch(Exception e){
			Log.error("saveProdSection error: " + e.getMessage());	
		}finally{
			dbm = null;
			if(isNew){
				conn.close();
				conn = null;		
			}
		}
		return false;
	}
	
	
	public JSONObject getProdDetils(Connection conn, String prodCode, String compCode,int version) throws SQLException{
		JSONObject prod = new JSONObject();
		ResultSet rs = null;
		DBManager dbm;
		boolean isNew = false;
		try{
			dbm = new DBManager();
			if(conn == null){
				conn = DBAccess.getConnection();
				isNew = true;
			}
			String sql = " select section_code,"
						+" json_file,"
						+" create_date,"
						+" create_by,"
						+" upd_date,"
						+" upd_by"
						+" from prod_section"
						+" where"
						+" comp_code = " + dbm.param(compCode,  DataType.TEXT)
						+" and prod_code = " + dbm.param(prodCode,  DataType.TEXT)
						+" and version = " + dbm.param(version,  DataType.INT);
			rs = dbm.select(sql, conn);
			if (rs != null) {	
				while (rs.next()) {	
					Clob clob = rs.getClob("json_file");
					JSONObject section = Function.convertClobToJson(clob);
					String sectionKey = rs.getString("section_code");
					prod.put(sectionKey, section);
				}
			}
			
		}catch(Exception e){
			return null;
		}finally{
			dbm = null;
			rs = null;
			if(isNew){
				if(conn != null && !conn.isClosed())
					conn.close();
				conn = null;
			}
		}
		return prod;
	}

	public List<ProductBean> getProducts(String prodCode, String compCode, UserPrincipalBean principal) throws SQLException {
		ResultSet rs = null;
		List<ProductBean> products = new ArrayList<ProductBean>();
		DBManager dbm;
		Connection conn = null;
		
		try {
			if (prodCode != null && !prodCode.isEmpty() && compCode != null && !compCode.isEmpty()) {
				dbm = new DBManager();
				conn = DBAccess.getConnection();

				//get data
				String sql = " select comp_code,"
						   + " prod_code,"
						   + " version,"
						   + " name_desc,"
						   + " default_name_desc,"
						   + " task_status"
						   + " from v_prod_trx"
						   + " where prod_code = "+ dbm.param(prodCode, DataType.TEXT)
						   + " and lang_code = " + dbm.param(principal.getLangCode(), DataType.TEXT) 
						   + " and comp_code = " + dbm.param(compCode, DataType.TEXT)
						   + " order by version";
				
				rs = dbm.select(sql, conn);
				
				if (rs != null) {
					products =  new ArrayList<ProductBean>();
					
					while (rs.next()) {
						ProductBean product = new ProductBean();
						product.setProdCode(rs.getString("prod_code"));
						product.setProdName((rs.getString("name_desc")==null)?rs.getString("default_name_desc"):rs.getString("name_desc"));
						product.setVersion(rs.getInt("version"));
						product.setTaskStatus(rs.getString("task_status"));
						products.add(product);
					}

				}

			}
		} catch (Exception e) {
			Log.error("Products > getProducts" + e);
		} finally {
			dbm = null;
			rs = null;
			
			if (conn != null && !conn.isClosed())
				conn.close();
			
			conn = null;
		}
		return products;
	}

	public List<Option> getProductLine(Language langObj) throws SQLException {
		DBManager dbm;
		Connection conn = null;
		ResultSet rs = null;

		try {
			dbm = new DBManager();
			conn = DBAccess.getConnection();

			List<Option> optionList = new ArrayList<>();

			String sql = " select lookup_field,"
					   + " name_code"
					   + " from sys_lookup_mst"
					   + " where upper(lookup_key) = 'PRODUCT_LINE'"
					   + " order by seq";

			rs = dbm.select(sql, conn);

			if (rs != null) {
				while (rs.next()) {
					optionList.add(new Option(langObj.getNameDesc(rs.getString("name_code")), rs.getString("lookup_field")));
				}

				return optionList;
			}
		} catch (Exception e) {
			Log.error("Products > getProductLine" + e);
		} finally {
			dbm = null;
			rs = null;
			
			if (conn != null && !conn.isClosed())
				conn.close();
			
			conn = null;
		}

		return null;
	}

	public List<Option> getProductStatus(Language langObj) throws SQLException {
		DBManager dbm;
		Connection conn = null;
		ResultSet rs = null;
		try {
			dbm = new DBManager();
			conn = DBAccess.getConnection();

			List<Option> optionList = new ArrayList<>();

			String sql = " select lookup_field,"
					   + " name_code"
					   + " from sys_lookup_mst"
					   + " where upper(lookup_key) = 'PRODUCT_STATUS'"
					   + " order by seq";

			rs = dbm.select(sql, conn);

			if (rs != null) {
				while (rs.next()) {
					optionList.add(new Option(langObj.getNameDesc(rs.getString("name_code")), rs.getString("lookup_field")));
				}

				return optionList;
			}
		} catch (Exception e) {
			Log.error("Products > getProductStatus" + e);
		} finally {
			dbm = null;
			rs = null;

			if (conn != null && !conn.isClosed())
				conn.close();

			conn = null;
		}

		return null;
	}

	public JsonArray getMenu(String prodCode, UserPrincipalBean principal, int version, boolean isNewProduct) throws SQLException{
		JsonArray menuLists = DynMenu.getMenu(null, "PROD", null, principal, null, 1);
		//if new bi, disabled all menu item except task info and detail
		if (isNewProduct) {
			for (int i = 0; i < menuLists.size(); i++) {
				JsonArray menuList = menuLists.get(i).getAsJsonArray();
				
				for (int j = 0; j < menuList.size(); j++) {
					if (!(i == 0 && j <= 1)){
						JsonObject menuItem = menuList.get(j).getAsJsonObject();
						menuItem.addProperty("disabled", true);
					}					
				}
			}
		}

		return menuLists;
	}

	public JSONObject getProductTemplateJson(String templateType, UserPrincipalBean principal, boolean isNewProd) throws SQLException{
		JSONObject temp = (JSONObject)getProductTemplate(templateType, principal, isNewProd, RETURN_JSON);
		return temp;
	}
	public Template getProductTemplate(String templateType, UserPrincipalBean principal, boolean isNew) throws SQLException{
		Template temp = (Template)getProductTemplate(templateType, principal, isNew, RETURN_TEMPLATE);
		return temp;
	}
	public Object getProductTemplate(String templateType, UserPrincipalBean principal, boolean isNew, String returnType) throws SQLException {
		DBManager dbm;
		Connection conn = null;
		ResultSet rs = null;
		try {
			dbm = new DBManager();
			conn = DBAccess.getConnection();

			List<Option> optionList = new ArrayList<>();

			String sql = " select template_code, template_file"
					   + " from prod_template"
					   + " where type = " + dbm.param(templateType, DataType.TEXT)
					   + " and comp_code = " + dbm.param(principal.getCompCode(), DataType.TEXT);

			rs = dbm.select(sql, conn);

			if (rs != null && rs.next()) {
				String str = rs.getString("template_file");
				Log.debug("template str:" + str);
				if(RETURN_TEMPLATE.equalsIgnoreCase(returnType)){
					Gson gson = new Gson();
					Template templ = gson.fromJson(str, Template.class);
					return templ;
				}else if(RETURN_JSON.equalsIgnoreCase(returnType)){
					JSONObject json = new JSONObject(str);
					return json;
				}
			} else {
				Log.debug("empty list?" + rs.getFetchSize());
			}
		} catch (Exception e) {
			Log.error("Products > get product template" + e);
		} finally {
			dbm = null;
			rs = null;

			if (conn != null && !conn.isClosed())
				conn.close();

			conn = null;
		}
		return null;
	}
	
	public JsonObject getProductTemplateValue(String templateType, UserPrincipalBean principal, boolean isNew) throws SQLException {
		JsonObject templ = null;
		
		DBManager dbm;
		Connection conn = null;
		ResultSet rs = null;
		try {
			dbm = new DBManager();
			conn = DBAccess.getConnection();

			List<Option> optionList = new ArrayList<>();

			String sql = " select template_code, template_file"
					   + " from prod_template"
					   + " where  type = " + dbm.param(templateType, DataType.TEXT)
					   + " and comp_code = " + dbm.param(principal.getCompCode(), DataType.TEXT);

			rs = dbm.select(sql, conn);

			if (rs != null && rs.next()) {
				String str = rs.getString("template_file");
				Log.debug("template str:" + str);
				Gson gson = new Gson();
				templ = gson.fromJson(str, JsonObject.class);
			} else {
				Log.debug("empty list?" + rs.getFetchSize());
			}
		} catch (Exception e) {
			Log.error("Products > get product template" + e);
		} finally {
			dbm = null;
			rs = null;

			if (conn != null && !conn.isClosed())
				conn.close();

			conn = null;
		}
		return templ;
	}

	
    public int newVersion(UserPrincipalBean principal, String prodCode) throws SQLException {
		DBManager dbm;
		Connection conn = null;
		try {
			if (prodCode != null && !prodCode.isEmpty()) {
				dbm = new DBManager();
				conn = DBAccess.getConnection();
				
				String sql = "call p_prod_new_version(" + dbm.param(principal.getCompCode(), DataType.TEXT, IOType.IN) + "," + 
						dbm.param(prodCode, DataType.TEXT, IOType.IN) + "," + 
						dbm.param(principal.getUserCode(), DataType.TEXT, IOType.IN) + "," +
						dbm.param(null, DataType.INT, IOType.OUT) + ")";
				
				List<Object> rs= dbm.executeCall(sql, conn);
				return (Integer)rs.get(0);
			}
		} catch (Exception e) {
			Log.error("Products > newVersion" + e);
		} finally {
			dbm = null;
			if (conn != null && !conn.isClosed())
				conn.close();
			conn = null;
		}
		return -1;
	}

	
	public String clone(UserPrincipalBean principal, String prodCode, String destCompCode) throws SQLException {
		DBManager dbm;
		Connection conn = null;
		CallableStatement callableStatement = null;
		try {
			if (prodCode != null && !prodCode.isEmpty()) {
				dbm = new DBManager();
				conn = DBAccess.getConnection();
				
				String sql = "call p_prod_clone(" + dbm.param(principal.getCompCode(), DataType.TEXT, IOType.IN) + "," + 
						dbm.param(destCompCode, DataType.TEXT, IOType.IN) + "," + 
						dbm.param(prodCode, DataType.TEXT, IOType.IN) + "," + 
						dbm.param(principal.getUserCode(), DataType.TEXT, IOType.IN) + "," + 
						dbm.param(null, DataType.TEXT, IOType.OUT) + ")";
				
				List<Object> outData = dbm.executeCall(sql, conn);
				return (String) outData.get(0);
			}
		} catch (Exception e) {
			Log.error("Products > clone" + e);
		} finally {
			dbm = null;
			
			if (callableStatement != null)
				callableStatement.close();
			
			if (conn != null && !conn.isClosed())
				conn.close();
			conn = null;
		}
		return null;
	}
	public boolean checkExist(String prodCode, String compCode, int version, String table) throws SQLException {
		DBManager dbm;
		Connection conn = null;

		boolean found = false;
		try {
			if (prodCode != null && !prodCode.isEmpty() && compCode != null && !compCode.isEmpty()) {
				dbm = new DBManager();
				conn = DBAccess.getConnection();
				
				String sql = " select count(1) as cnt"
						   + " from " + table
						   + " where comp_code = " + dbm.param(compCode, DataType.TEXT) 
						   + " and prod_code = " + dbm.param(prodCode, DataType.TEXT);
				
				if (version > 0 )
					sql += " and version = " + dbm.param(version, DataType.INT);
		
				String cnt = dbm.selectSingle(sql, "cnt", conn);
		
				if (Integer.parseInt(cnt) > 0)
					return true;
			}
		} catch (Exception e) {
			Log.error("Products > checkExist" + e);
		} finally {
			dbm = null;
			
			if (conn != null && !conn.isClosed())
				conn.close();
			
			conn = null;
		}

		return found;
	}
	
    public boolean checkExist(String prodCode, String compCode, int version) throws SQLException {
		return checkExist(prodCode, compCode, version, "prod_trx");
	}

	
	public List<ProductBean> getProductList() throws SQLException {
		return searchProductList(null, null, null);
	}
	

	public List<ProductBean> searchProductList(ProductListCondition productListCondition, UserPrincipalBean principal, Language langObj) throws SQLException {
		DBManager dbm;
		Connection conn = null;
		ResultSet rs = null;

		List<ProductBean> productList = new ArrayList<>();

		try {
			dbm = new DBManager();
			conn = DBAccess.getConnection();

			String criteria = productListCondition != null ? productListCondition.getCriteria() : null;
			String productLine = productListCondition != null ? productListCondition.getProductLine() : null;
			String status = productListCondition != null ? productListCondition.getStatus() : null;
			String sortBy = productListCondition != null ? productListCondition.getSortBy() : null;
			String sortDir = productListCondition != null ? productListCondition.getSortDir() : null;

			String compCode = principal.getCompCode();
			String langCode = principal.getLangCode();

			// Condition
			String condition = " ";
			condition = DynamicFunction.addCompanyFilter(dbm, condition, null, compCode, "list");

			if (criteria != null && !criteria.isEmpty())
				condition += " and (UPPER(list.prod_code) like UPPER('%" + criteria + "%') or UPPER(name.name_desc) like UPPER('%" + criteria + "%'))";
			
			if (productLine != null && !productLine.isEmpty() && !productLine.equalsIgnoreCase("ALL"))
				condition += " and UPPER(list.prod_line_code) = UPPER('" + productLine + "')";
			
			if (status != null && !status.isEmpty() && !status.equalsIgnoreCase("ALL")) {
				if (status.equalsIgnoreCase("-R"))
					condition += " and UPPER(list.status_code) <> UPPER('" + status + "')";
				else
					condition += " and UPPER(list.status_code) = UPPER('" + status + "')";
			}

			// Order
			String order = "";
			if (sortBy != null && !sortBy.isEmpty()) {
				// Direction
				String dir = "";
				if (sortDir != null && !sortDir.isEmpty()) {
					switch (sortDir.toLowerCase()) {
					case "d":
						dir = " desc";
						break;
					case "a":
						dir = " asc";
						break;
					}
				}

				switch (sortBy.toLowerCase()) {
					case "prodcode":
						sortBy = "prod_code";
						break;
					case "prodname":
						sortBy = "prod_name";
						break;
					case "prodline":
						sortBy = "prod_line";
						break;
					case "status":
						sortBy = "status";
						break;
				}

				order = " order by " + sortBy + dir;
			}

			String sql = " select"
					   + " list.prod_code,"
					   + " name.name_desc as prod_name,"
					   + " name.name_desc as prod_name_default,"
					   + " line.name_desc as prod_line,"
					   + " list.status,"
					   + " list.status_code,"
					   + " list.version"
					  
					   + " from v_prod_list list"
					  
 					   + " left join v_prod_trx name"
					   + " on list.prod_code = name.prod_code"
					   + " and list.comp_code = name.comp_code"
					   + " and list.version = name.version"
					   + " and name.lang_code = '" + langCode + "'"
					  
					   + " left join"
					   + " (select pl.comp_code as comp_code,"
					   + " pl.pl_code as pl_code,"
					   + " pl.lang_code as lang_code,"
					   + " pl.name_desc as name_desc"
					   + " from v_prod_line pl"
					  
					   + " inner join v_prod_line_mst plm"
					   + " on pl.comp_code = plm.COMP_CODE"
					   + " and pl.pl_code=plm.pl_code) line"
					   + " on list.comp_code = line.comp_code"
					   + " and list.prod_line_code = line.pl_code"
					   + " and line.lang_code = '" + langCode + "'"
					  
					   + " where 1 = 1 " + condition + order;

			rs = dbm.select(sql, conn);

			if (rs != null) {
				while (rs.next()) {
					ProductBean product = new ProductBean();

					product.setId(rs.getString("prod_code"));
					product.setProdCode(rs.getString("prod_code"));
					product.setProdName((rs.getString("prod_name") == null)?rs.getString("prod_name_default"):rs.getString("prod_name"));
					product.setProdLine(langObj.getNameDesc(rs.getString("prod_line")));
					product.setStatus(langObj.getNameDesc(rs.getString("status")));
					product.setVersion(rs.getInt("version"));
					product.setIsNewVersion(rs.getString("status_code").equalsIgnoreCase("e"));
					product.setIsClone(true);

					productList.add(product);
				}
			}

		} catch (Exception e) {
			Log.error("Products > searchProductList" + e);
		} finally {
			dbm = null;
			rs = null;
			
			if (conn != null && !conn.isClosed())
				conn.close();
			
			conn = null;
		}

		return productList;
	}

	
	public JsonObject getProduct(String prodCode, UserPrincipalBean principal, int version, Language langObj) throws SQLException {
		DBManager dbm;
		Connection conn = null;
		ResultSet rs = null;
		String compCode = principal.getCompCode();
		JsonObject product = new JsonObject();
		try {

			if (prodCode != null && !prodCode.isEmpty() && compCode != null && !compCode.isEmpty()) {
				dbm = new DBManager();
				conn = DBAccess.getConnection();

				String sql = " select prod_code,"
						   + " prod_line,"
						   + " version,"
						   + " launch_date,"
						   + " expiry_date"
					       + " from v_prod_detail"
						   + " where prod_code = " + dbm.param(prodCode, DataType.TEXT) 
						   + " and comp_code = " + dbm.param(compCode, DataType.TEXT) 
						   + " and version=" + dbm.param(version, DataType.INT);

				rs = dbm.select(sql, conn);

				if (rs != null) {
					while (rs.next()) {
						product.add("covCode", JsonConverter.convertObj(rs.getString("prod_code")));
						product.add("productLine", JsonConverter.convertObj(rs.getString("prod_line")));
						dbm.clear();
						JsonObject names = JsonConverter.convertObj(getProdName(prodCode, version, principal)).getAsJsonObject();

						String channels = getProdChannels(prodCode, version, principal);
						
						if (channels != null)
							product.add("channels", JsonConverter.convertObj(channels));

						product.add("covName", JsonConverter.convertObj(names));
						product.add("version", JsonConverter.convertObj(rs.getInt("version")));

						if (rs.getTimestamp("launch_date") != null) {
							SimpleDateFormat dateTimeformatter = Function.getDateTimeFormat(principal);
							product.add("launchDate", JsonConverter.convertObj(dateTimeformatter.format(rs.getDate("launch_date"))));
						}

						if (rs.getTimestamp("expiry_date") != null)
							product.add("expDate", JsonConverter.convertObj(rs.getTimestamp("expiry_date").getTime()));
						
						// product name
						break;
					}
				}

			}
		} catch (Exception e) {
			Log.error("Products > getProduct" + e);

		} finally {
			dbm = null;
			rs = null;
			
			if (conn != null && !conn.isClosed())
				conn.close();
			
			conn = null;
		}

		return product;
	}

	
	public List<Option> getChannels(UserPrincipalBean principal) throws SQLException {
		DBManager dbm;
		Connection conn = null;

		try {
			dbm = new DBManager();
			conn = DBAccess.getConnection();

			List<Option> channels = new ArrayList<Option>();

			String sql = " select channel_code,"
					   + " channel_name"
					   + " from sys_channel_mst"
					   + " where comp_code = " + dbm.param(principal.getCompCode(), DataType.TEXT)
					   + " and status = 'A'";
			
			ResultSet rs = dbm.select(sql, conn);

			if (rs != null) {
				while (rs.next()) {
					channels.add(new Option(rs.getString("channel_name"), rs.getString("channel_code")));
				}

				return channels;
			}
		} catch (Exception e) {
			Log.error(e);
		} finally {
			dbm = null;
			
			try {
				if (conn != null && !conn.isClosed())
					conn.close();
			} catch (SQLException e) {
				Log.error(e);
			}
			
			conn = null;
		}

		return null;
	}

	
    public List<Option> getProdLineFilter(UserPrincipalBean principal, Language langObj) throws SQLException {	
		List<Option> options = new ArrayList<Option>();
		options.add(new Option(langObj.getNameDesc("PRODUCT_LINE_ALL"), "ALL"));
		List<Option> prodLinesList = getProdLines(principal);
		
		for(int i =0 ; i< prodLinesList.size();i++){ 
			options.add(prodLinesList.get(i));
		}
		
		return options;
	}

	
    public List<Option> getProdLines(UserPrincipalBean principal) throws SQLException {
		DBManager dbm;
		Connection conn = null;
		ResultSet rs = null;
		List<Option> prodLines = null;
		try {
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			prodLines = new ArrayList<Option>();

			String sql = " select pl.comp_code,"
					   + " pl.pl_code,"
					   + " pl.lang_code,"
					   + " pl.name_desc"
					   + " from v_prod_line pl"
					   
					   + " inner join v_prod_line_mst plm"
					   + " on pl.comp_code = plm.COMP_CODE"
					   + " and pl.pl_code = plm.pl_code "
					   
					   + " where pl.comp_code = " + dbm.param(principal.getCompCode(), DataType.TEXT)
					   + " and pl.lang_code = " + dbm.param(principal.getLangCode(), DataType.TEXT)
					   + " order by pl.name_desc ";
			
			rs = dbm.select(sql, conn);
			
			if (rs != null) {
				while (rs.next()) {
					prodLines.add(new Option(rs.getString("name_desc"), rs.getString("pl_code")));
				}
			}
		} catch (Exception e) {
			Log.error(e);
		} finally {
			dbm = null;
			rs = null;
			
			if (conn != null && !conn.isClosed())
				conn.close();
			
			conn = null;
		}
		return prodLines;
	}

	
    public JSONObject getProdName(String prodCode, int version, UserPrincipalBean principal) throws SQLException {
		DBManager dbm;
		Connection conn = null;
		ResultSet rs = null;
		JSONObject names = null;

		try {
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			names = new JSONObject();

			String sql = " select lang_code,"
					   + " name_desc"
					   + " from v_prod_name"
					   + " where prod_code = " + dbm.param(prodCode, DataType.TEXT)
					   + " and comp_code = " + dbm.param(principal.getCompCode(), DataType.TEXT)
					   + " and version = " + dbm.param(version, DataType.INT)
					   + " and name_type = " + dbm.param("PRODNAME", DataType.TEXT);

			rs = dbm.select(sql, conn);
			
			if (rs != null) {
				while (rs.next()) {
					names.put(rs.getString("lang_code"), rs.getString("name_desc"));
				}
			}
		} catch (Exception e) {
			Log.error(e);
		} finally {
			dbm = null;
			rs = null;
			
			if (conn != null && !conn.isClosed())
				conn.close();
			
			conn = null;
		}
		return names;
	}

	
    public String getProdChannels(String prodCode, int version, UserPrincipalBean principal) throws SQLException {
		DBManager dbm;
		Connection conn = null;
		String channels = null;
		
		try {
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			ResultSet rs = null;
			
			String sql = " select channel_code"
					   + " from v_prod_channel"
					   + " where prod_code = " + dbm.param(prodCode, DataType.TEXT)
					   + " and comp_code = " + dbm.param(principal.getCompCode(), DataType.TEXT)
					   + " and version = " + dbm.param(version, DataType.INT);
		
			rs = dbm.select(sql, conn);
			channels = "";
			
			if (rs != null) {
				while (rs.next()) {
					if (!rs.isFirst())
						channels += ",";
			
					channels += rs.getString("channel_code");
				}
			}

		} catch (Exception e) {
			Log.error(e);
		} finally {
			dbm = null;
			
			if (conn != null && !conn.isClosed())
				conn.close();
			
			conn = null;
		}
		return channels;
	}
    

	public String saveProduct(String prodCode, int version, TaskBean taskInfo, JSONObject prodDetail, Map<String, JSONObject> sections, int taskId, UserPrincipalBean principal)   throws SQLException{
		String errMsg = null;
		Connection conn = null;
		Language langObj = new Language(principal.getUITranslation());

		try {
			conn = DBAccess.getConnection();
			conn.setAutoCommit(false);
			Tasks taskMgr = new Tasks();
			
			if (taskInfo != null) {
				if (taskInfo.getTaskCode() == null || taskInfo.getTaskCode().equals("")) {
					if (!taskMgr.addTask(principal, taskInfo, conn)) {
						conn.rollback();
						conn.commit();
						
						return langObj.getNameDesc(NO_INSERT);
					}
				} else {
					if (!taskMgr.updateTask(principal, taskInfo, conn)) {
						conn.rollback();
						conn.commit();
						
						return langObj.getNameDesc(NO_UPDATE);
					}
				}
			}
			if (prodDetail != null) {
				errMsg = saveProdDetail(taskId, prodDetail, principal, conn);
				
				if(errMsg != null){
					conn.rollback();
					conn.commit();
					return errMsg;
				}
			}
			
			if (sections != null) {
				for (String key : sections.keySet()) {
					JSONObject section = sections.get(key);
					
					if (key.equals("prodFeatures")) {
						if (section.has("ccy") && section.has("saMultiple")) {
							List<String> ccy = Arrays.asList(section.getString("ccy").split(","));
							JSONObject sumAssuredMultiplier = section.getJSONObject("saMultiple");
							
							for (String _key : sumAssuredMultiplier.keySet()) {
								boolean found = false;
							
								for (int i = 0; i < ccy.size(); i++) {
									if (_key.equals(ccy.get(i))) {
										found = true;
										continue;
									}
								}
								
								if (!found)
									sumAssuredMultiplier.remove(_key);
							}
							section.put("saMultiple", sumAssuredMultiplier);
						}
					}
					
					errMsg = saveProdSection(prodCode, version, key, section, principal, conn);
					
					if (errMsg != null) {
						conn.rollback();
						conn.commit();
						return errMsg;
					}

				}
			}
			conn.commit();
			return null;
		}catch (Exception e) {
			Log.error(e);
			
			if (conn != null && !conn.isClosed())
				conn.rollback();
			
			return e.getMessage();
		} finally {
			if (conn != null && !conn.isClosed())
				conn.close();
			
			conn = null;
		}

	}

	
	public String saveProdDetail(int taskId, JSONObject prodDetail, UserPrincipalBean principal, Connection conn) throws SQLException {
		DBManager dbm;
		String prodCode = prodDetail.getString("covCode");
		String compCode = principal.getCompCode();
		int version = prodDetail.getInt("version");
		Language langObj = new Language(principal.getUITranslation());

		try {
			boolean isWhiteSpace = Pattern.compile("\\s").matcher(prodCode).find();
			
			if (isWhiteSpace)
				return langObj.getNameDesc(CODE_WITHOUT_SPACE);

			String orgProdCode = prodCode;
			
			if (taskId > -1) {
				Tasks tasksDAO = new Tasks();
				Map<String, Object> task = tasksDAO.getTaskById(taskId, null, -1, null, null, principal);
				orgProdCode = (String) task.get("item_code");
			}
			
			if (prodCode != null && !prodCode.isEmpty() && compCode != null && !compCode.isEmpty()) {
				dbm = new DBManager();

				if(!orgProdCode.equals(prodCode)){
					//check prodCode is unique
					if(checkExist(prodCode, principal.getCompCode(), -1))
						return langObj.getNameDesc(DUPLICATE_CODE);

					String sql = "update task_mst set item_code = " + dbm.param(prodCode, DataType.TEXT) + " where item_code = " + dbm.param(orgProdCode, DataType.TEXT);
					dbm.update(sql);
					dbm.clear();
					
					sql = "update prod_trx set prod_code = " + dbm.param(prodCode, DataType.TEXT) + " where prod_code = " + dbm.param(orgProdCode, DataType.TEXT);
					dbm.update(sql);
					dbm.clear();
					
					sql = "update prod_section set prod_code = " + dbm.param(prodCode, DataType.TEXT) + " where prod_code = " + dbm.param(orgProdCode, DataType.TEXT);
					dbm.update(sql);
					dbm.clear();
					
					sql = "update prod_name set prod_code = " + dbm.param(prodCode, DataType.TEXT) + " where prod_code = " + dbm.param(orgProdCode, DataType.TEXT);
					dbm.update(sql);
					dbm.clear();
					
					sql = "update prod_channel set prod_code = " + dbm.param(prodCode, DataType.TEXT) + " where prod_code = " + dbm.param(orgProdCode, DataType.TEXT);
					dbm.update(sql);
					dbm.clear();
					
					sql = "update prod_attach set prod_code = " + dbm.param(prodCode, DataType.TEXT) + " where prod_code = " + dbm.param(orgProdCode, DataType.TEXT);
					dbm.update(sql);
					dbm.clear();
				} 
				
				if (checkExist(prodCode, principal.getCompCode(), version)) {
					// update prod_trx record
					String sql = " update prod_trx"
							   + " set prod_line = " + dbm.param(prodDetail.getString("productLine"), DataType.TEXT)  // productLine
							   + ", expiry_date=" + ((prodDetail.has("expDate") && prodDetail.get("expDate").getClass() == java.lang.Long.class ) ? dbm.param(new Date(prodDetail.getLong("expDate")), DataType.DATE) : "NULL") // expiry date 
							   + ", upd_date = CURRENT_TIMESTAMP " 												//update date
							   + ", upd_by = " + dbm.param(principal.getUserCode(), DataType.TEXT) 				//update By
							   + " where comp_code=" + dbm.param(compCode, DataType.TEXT)							//comCode
							   + " and prod_code=" + dbm.param(prodCode, DataType.TEXT)						//covCode
							   + " and version=" + dbm.param((int) prodDetail.getInt("version"), DataType.INT);; // version
							
							if (dbm.update(sql, conn) < 1)
								return langObj.getNameDesc(NO_UPDATE);
				}else{
					//insert prod_trx record
					//check prodCode is unique
					if (checkExist(prodCode, principal.getCompCode(), -1))
						return langObj.getNameDesc(DUPLICATE_CODE);
					
					Log.error("prodDetail : " + prodDetail.toString());
					JSONObject lDate = Function.getDatePara("launchDate", prodDetail);
					Object lauchDateObj = lDate.get("value");
					
					JSONObject exDate = Function.getDatePara("expiryDate", prodDetail);
					Object exDateObj = exDate.get("value");

					String planType = prodDetail.getString("planType");
					
					String sql = "insert into prod_trx ("
							+ " comp_code,"
							+ " prod_code,"
							+ " version,"
							+ " prod_line,"
							+ " launch_date,"
							+ " expiry_date,"
							+ " base_version,"
							+ " launch_status,"
							+ " STATUS,"
							+ " CREATE_DATE,"
							+ " CREATE_BY,"
							+ " UPD_DATE,"
							+ " UPD_BY,"
							+ " PROD_TYPE)"
							+ " values ("
							+ dbm.param(compCode, DataType.TEXT) + " , " 							//comCode
							+ dbm.param(prodCode, DataType.TEXT) + " , " 							//covCode
							+ dbm.param(version, DataType.INT) + " , "								//version
							+ dbm.param(prodDetail.getString("productLine"), DataType.TEXT) + " , "	//productLine
							+ ((lauchDateObj.getClass() == String.class) ? "NULL" : dbm.param(lauchDateObj, DataType.DATE)) + " , "						//launch date
							+ ((exDateObj.getClass() == String.class) ? "NULL" :dbm.param(exDateObj, DataType.DATE)) + " , "								//expiry date 
							+ "NULL, "															//base version
							+ dbm.param("P", DataType.TEXT) + " , "									//launch status
							+ dbm.param("A", DataType.TEXT) + " , "					 				//status
							+ "CURRENT_TIMESTAMP, "													//create date
							+ dbm.param(principal.getUserCode(), DataType.TEXT) + " , "				//create by
							+ "CURRENT_TIMESTAMP, "													//update date
							+ dbm.param(principal.getUserCode(), DataType.TEXT) + " , " 			//update by
							+ dbm.param(planType, DataType.TEXT) + " ) ";							// plan type
					if (!dbm.insert(sql, conn))
						return langObj.getNameDesc(NO_INSERT);
				}

				// insert or update prod_name record
				dbm.clear();
				ResultSet rs = null;
				
				String sql = " select lang_code "
						   + " from prod_name"
						   + " where comp_code = " + dbm.param(compCode, DataType.TEXT)
						   + " and prod_code = " + dbm.param(prodCode, DataType.TEXT) 
						   + " and version = " + dbm.param(prodDetail.getInt("version"), DataType.INT)
						   + " and name_type = 'PRODNAME'" 
						   + " and status = 'A'";
				
				rs = dbm.select(sql, conn);

				List<String> mLangs = new ArrayList<String>();
				
				if (rs != null) {
					while (rs.next()) {
						mLangs.add(rs.getString("lang_code"));
					}
				}
				
				rs = null;
				JSONObject langs = prodDetail.getJSONObject("covName");
				for (String key : langs.keySet()) {
					dbm.clear();
				
					if (mLangs.contains(key)) {
						sql = " update prod_name"
							+ " set name_desc = " + dbm.param(langs.getString(key), DataType.TEXT) 	//langDesc
							+ " where comp_code = " + dbm.param(compCode, DataType.TEXT)			//comCode
							+ "	and prod_code = " + dbm.param(prodCode, DataType.TEXT)				//covCode
							+ "	and version = " + dbm.param(version, DataType.INT)					//version
							+ "	and name_type = " + dbm.param("PRODNAME", DataType.TEXT)			//name_type
							+ "	and lang_code = " + dbm.param(key, DataType.TEXT); 					//lang_code
						
						if (dbm.update(sql, conn) < 1)
							return langObj.getNameDesc(NO_UPDATE);
					} else if (!langs.getString(key).equals("")) {
						sql = "insert into prod_name values("
								+ dbm.param(compCode, DataType.TEXT) + " , " 						//comCode
								+ dbm.param(prodCode, DataType.TEXT) + " , " 						//covCode
								+ dbm.param(version, DataType.INT) + " , " 							//version
								+ dbm.param("PRODNAME", DataType.TEXT) + " , " 						//name_type
								+ dbm.param(key, DataType.TEXT) + " , " 							//lang_code
								+ dbm.param(langs.get(key).toString(), DataType.TEXT) + " , " 		//landDesc
								+ dbm.param("A", DataType.TEXT) + " , " 							//status
								+ "CURRENT_TIMESTAMP, "												//createDate
								+ dbm.param(principal.getUserCode(), DataType.TEXT) + " , " 		//createBy
								+ "CURRENT_TIMESTAMP, "												//updDate
								+ dbm.param(principal.getUserCode(), DataType.TEXT) + " ) ";		//updBy
						
						if (!dbm.insert(sql, conn))
							return langObj.getNameDesc(NO_INSERT);
					}
				}

				// insert channel
				dbm.clear();
				rs = null;
				
				sql = " select channel_code"
					+ " from prod_channel "
					+ " where comp_code = " + dbm.param(compCode, DataType.TEXT)
					+ " and prod_code = " + dbm.param(prodCode, DataType.TEXT)
					+ " and version = " + dbm.param(prodDetail.getInt("version"), DataType.INT)
					+ " and status = 'A'";
				
				rs = dbm.select(sql, conn);

				List<String> mChannels = new ArrayList<String>();
				
				if (rs != null) {
					while (rs.next()) {
						mChannels.add(rs.getString("channel_code"));
					}
				}
				
				String[] channels = {};
				
				if (prodDetail.has("channels") && !prodDetail.getString("channels").equals(""))
					channels = prodDetail.getString("channels").split(",");
				
				for (String channel : channels) {
					dbm.clear();
					
					if (!mChannels.contains(channel)) {
						sql = "insert into prod_channel values("
								+ dbm.param(compCode, DataType.TEXT) + " , "  					//comCode
								+ dbm.param(prodCode, DataType.TEXT) + " , "					//covCode
								+ dbm.param(version, DataType.INT) + " , "						//version
								+ dbm.param(channel, DataType.TEXT) + " , "						//channel_code
								+ dbm.param("A", DataType.TEXT) + " , "							//status
								+ "CURRENT_TIMESTAMP, "											//createDate
								+ dbm.param(principal.getUserCode(), DataType.TEXT) + " , "		//createBy
								+ "CURRENT_TIMESTAMP, " 										//updDate
								+ dbm.param(principal.getUserCode(), DataType.TEXT) + " ) ";	//updBy
					
						if (!dbm.insert(sql, conn))
							return langObj.getNameDesc(NO_INSERT);
					} else {
						mChannels.remove(channel);
					}
				}
				for (String channel : mChannels) {
					dbm.clear();
					
					sql = " delete from prod_channel"
						+ " where comp_code = " + dbm.param(compCode, DataType.TEXT)
						+ " and prod_code = " + dbm.param(prodCode, DataType.TEXT)
						+ " and version = " + dbm.param(version, DataType.INT)
						+ " and channel_code = " + dbm.param(channel, DataType.TEXT);
					
					if (dbm.delete(sql, conn) < 1)
						return langObj.getNameDesc(NO_UPDATE);
				}

				rs = null;
			}

		} catch (Exception e) {
			return e.getMessage();
		} finally {
			dbm = null;
		}

		return null;
	}
	

	private String saveProdSection(String prodCode, int version, String sectionCode, JSONObject section, UserPrincipalBean principal, Connection conn) throws SQLException {
		DBManager dbm;
		Language langObj = new Language(principal.getUITranslation());

		try {
			String compCode = principal.getCompCode();
			if (prodCode != null && !prodCode.isEmpty() && compCode != null && !compCode.isEmpty()) {
				dbm = new DBManager();
				String jsonFile = section.toString();
				
				String sql = " select count(1) as cnt"
						   + " from prod_section"
						   + " where prod_code = " + dbm.param(prodCode, DataType.TEXT)
						   + " and comp_code = " + dbm.param(compCode, DataType.TEXT)
						   + " and version = " + dbm.param(version, DataType.INT)
						   + " and section_code = " + dbm.param(sectionCode, DataType.TEXT)
						   + " and status = 'A'";
				
				int count = Integer.parseInt(dbm.selectSingle(sql, "cnt", conn));

				dbm.clear();
				
				if (count > 0) {
					sql = " update prod_section"
						+ " set json_file = " + dbm.param(jsonFile, DataType.CLOB)
						+ " ,upd_date = CURRENT_TIMESTAMP"
						+ " ,upd_by = " + dbm.param(principal.getUserCode(), DataType.TEXT)
						+ " where prod_code = " + dbm.param(prodCode, DataType.TEXT)
						+ " and comp_code = " + dbm.param(compCode, DataType.TEXT)
						+ " and version = " + dbm.param(version, DataType.INT)
						+ " and section_code=" + dbm.param(sectionCode, DataType.TEXT);

					if (dbm.update(sql, conn) < 1)
						return langObj.getNameDesc(NO_UPDATE);
				} else {
					sql = "insert into prod_section values("
							+ dbm.param(compCode, DataType.TEXT) + " , "							//compCode
							+ dbm.param(prodCode, DataType.TEXT) + " , "							//covCode
							+ dbm.param(version, DataType.INT) + " , "								//version
							+ dbm.param(sectionCode, DataType.TEXT) + " , "							//name_type
							+ dbm.param(jsonFile, DataType.TEXT) + " , "							//json_file
							+ dbm.param("A", DataType.TEXT) + " , "									//status
							+ "CURRENT_TIMESTAMP, " 												//createDate
							+ dbm.param(principal.getUserCode(), DataType.TEXT) + " , "				//createBy
							+ "CURRENT_TIMESTAMP, " 												//updDate
							+ dbm.param(principal.getUserCode(), DataType.TEXT) + " ) "; 			//update By
					
					if (!dbm.insert(sql, conn))
						return langObj.getNameDesc(NO_INSERT);
				}
			}
		} catch (Exception e) {
			return e.getMessage();
		} finally {
			dbm = null;
		}
		return null;
	}
	

	public Map<String, JSONObject> getProdSections(String prodCode, UserPrincipalBean principal, int version, String sectionId) throws SQLException {
		DBManager dbm;
		Connection conn = null;
		Map<String, JSONObject> sections = null;
		ResultSet rs = null;
		String compCode = principal.getCompCode();
		
		try {
			if (prodCode != null && !prodCode.isEmpty() && compCode != null && !compCode.isEmpty()) {
				dbm = new DBManager();
				conn = DBAccess.getConnection();
				
				String sql = " select section_code,"
						   + " json_file"
						   + " from prod_section "
						   + " where prod_code = " + dbm.param(prodCode, DataType.TEXT)
						   + " and comp_code = " + dbm.param(compCode, DataType.TEXT)
						   + " and version = " + dbm.param(version, DataType.INT)
						   + " and status = 'A'";
		
				if (sectionId != null && !sectionId.isEmpty())
					sql += " and section_code = " + dbm.param(sectionId, DataType.TEXT);
				
				rs = dbm.select(sql, conn);
				dbm.clear();
				
				if (rs != null) {
					sections = new HashMap<String, JSONObject>();
				
					while (rs.next()) {
						sections.put(rs.getString("section_code"), new JSONObject(rs.getString("json_file")));
					}
				}

			}
		} catch (Exception e) {
			Log.error(e);
		} finally {
			dbm = null;
			rs = null;
			
			if (conn != null && !conn.isClosed())
				conn.close();
			conn = null;
		}
		return sections;
	}

	
	public ProductAttachBean getProductAttach(String compCode, String prodCode, int version, String langCode, String attachCode) throws SQLException {
		DBManager dbm = null;
		Connection conn = null;
		ProductAttachBean attach = new ProductAttachBean();
		
		String sqlAttach = " select comp_code,"
						 + " prod_code,"
						 + " version,"
						 + " lang_code,"
						 + " create_date,"
						 + " create_by,"
						 + " upd_date,"
						 + " upd_by,"
						 + " attach_code,"
						 + " attach_file"
						 + " from prod_attach"
						 + " where comp_code = ?"
						 + " and prod_code = ?"
						 + " and version = ?"
						 + " and lang_code = ?"
						 + " and attach_code = ?";
		
		try {
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			
			dbm.param(compCode, DataType.TEXT);
			dbm.param(prodCode, DataType.TEXT);
			dbm.param(version, DataType.INT);
			dbm.param(langCode, DataType.TEXT);
			dbm.param(attachCode, DataType.TEXT);
			
			ResultSet rs = null;
			rs = dbm.select(sqlAttach, conn);

			if (rs.next()) {
				attach.setCompCode(rs.getString("comp_code"));
				attach.setProdCode(rs.getString("prod_code"));
				attach.setVersion(rs.getInt("version"));
				attach.setLangCode(rs.getString("lang_code"));
				attach.setCreateDate(rs.getDate("create_date"));
				attach.setCreateBy(rs.getString("create_by"));
				attach.setUpdateDate(rs.getDate("upd_date"));
				attach.setUpdateBy(rs.getString("upd_by"));
				attach.setAttachCode(rs.getString("attach_code"));
				attach.setAttachFile(rs.getBlob("attach_file"));
			}

		} catch (Exception e) {
			Log.error(e);
		} finally {
			dbm.clear();
			dbm = null;
			
			if (conn != null && !conn.isClosed())
				conn.close();
			
			conn = null;
		}
		return attach;
	}
	

	public int updateProductAttach(ProductAttachBean attach, String attachFile) throws SQLException {
		DBManager dbm;
		Connection conn = null;

		int update = -1;
		String sqlUpdate = " update prod_attach "
						 + " set attach_file = ?,"
						 + " upd_by = ?,"
						 + " upd_date = ?"
						 + " where comp_code = ?"
						 + " and prod_code = ?"
						 + " and version = ?"
						 + " and lang_code = ?"
						 + " and attach_code = ?";
		
		try {
			dbm = new DBManager();
			conn = DBAccess.getConnection();
		
			dbm.param(attachFile.getBytes(), DataType.BLOB);
			dbm.param(attach.getUpdateBy(), DataType.TEXT);
			dbm.param(attach.getUpdateDate(), DataType.DATE);
			dbm.param(attach.getCompCode(), DataType.TEXT);
			dbm.param(attach.getProdCode(), DataType.TEXT);
			dbm.param(attach.getVersion(), DataType.INT);
			dbm.param(attach.getLangCode(), DataType.TEXT);
			dbm.param(attach.getAttachCode(), DataType.TEXT);
			
			update = dbm.update(sqlUpdate, conn);
		} catch (Exception e) {
			Log.error(e);
		} finally {
			dbm = null;
			
			if (conn != null && !conn.isClosed())
				conn.close();
			conn = null;
		}
		return update;
	}
	

	public int deleteProductAttach(ProductAttachBean attach) throws SQLException {
		DBManager dbm;
		Connection conn = null;

		int update = -1;
		String sqlUpdate = " delete from prod_attach "
						 + " where comp_code = ?"
						 + " and prod_code = ?"
						 + " and version = ?"
						 + " and lang_code = ?"
						 + " and attach_code = ?";
		try {
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			
			dbm.param(attach.getCompCode(), DataType.TEXT);
			dbm.param(attach.getProdCode(), DataType.TEXT);
			dbm.param(attach.getVersion(), DataType.INT);
			dbm.param(attach.getLangCode(), DataType.TEXT);
			dbm.param(attach.getAttachCode(), DataType.TEXT);
			
			update = dbm.update(sqlUpdate, conn);
		} catch (Exception e) {
			Log.error(e);
		} finally {
			dbm = null;
			
			if (conn != null && !conn.isClosed())
				conn.close();
			
			conn = null;
		}
		return update;
	}
	

	public boolean addProductAttach(ProductAttachBean attach, String attachFile) throws SQLException {
		DBManager dbm = null;
		Connection conn = null;
		boolean update = false;
		String sqlInsert = " insert into prod_attach (comp_code, prod_code, version, lang_code, attach_code, attach_file, create_date, create_by, status) values(?, ?, ?, ?, ?, ?, ?, ?, ?)";

		try {
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			
			dbm.param(attach.getCompCode(), DataType.TEXT);
			dbm.param(attach.getProdCode(), DataType.TEXT);
			dbm.param(attach.getVersion(), DataType.INT);
			dbm.param(attach.getLangCode(), DataType.TEXT);
			dbm.param(attach.getAttachCode(), DataType.TEXT);
			dbm.param(attachFile.getBytes(), DataType.BLOB);
			dbm.param(attach.getCreateDate(), DataType.DATE);
			dbm.param(attach.getCreateBy(), DataType.TEXT);
			dbm.param("A", DataType.TEXT);
			
			update = dbm.insert(sqlInsert, conn);
		} catch (Exception e) {
			Log.error(e);
		} finally {
			dbm.clear();
			dbm = null;
			
			if (conn != null && !conn.isClosed())
				conn.close();
			
			conn = null;
		}
		return update;
	}
	

	public Map<String, Object> getProdStatusById(String prodCode, int version, UserPrincipalBean principal) throws SQLException{
		Map<String, Object> result = new HashMap<String, Object>();
		DBManager dbm;
		Connection conn = null;

		try {
			String compCode = principal.getCompCode();
			if (prodCode != null && !prodCode.isEmpty() && compCode != null && !compCode.isEmpty()) {
				dbm = new DBManager(); 
				conn = DBAccess.getConnection();
				
				String sql = " select status_code,"
						   + " version"
						   + " from v_prod_list"
						   + " where prod_code = " + dbm.param(prodCode, DataType.TEXT)
						   + " and comp_code = " + dbm.param(compCode, DataType.TEXT);
				
				ResultSet rs = dbm.select(sql, conn);
				dbm.clear();
				
				if(rs.next()){
					
					result.put("status",rs.getString("status_code"));
					result.put("prod_ver", rs.getInt("version"));
					
					sql = " select launch_status,"
						+ " task_status_code"
						+ " from v_prod_trx"
						+ " where prod_code = " + dbm.param(prodCode, DataType.TEXT)
						+ " and comp_code = " + dbm.param(compCode, DataType.TEXT)
						+ " and version = " + dbm.param(version, DataType.INT);
					
					ResultSet rs2 = dbm.select(sql, conn);
					
					if(rs2.next()){
						result.put("launch_status", rs2.getString("launch_status"));
						result.put("task_status", rs2.getString("task_status_code"));
					}
				}
			}
		} catch (Exception e) {
			Log.error(e);
		} finally {
			dbm = null;
			
			if (conn != null && !conn.isClosed())
				conn.close();
			
			conn = null;
		}

		return result;
	}
	

	public String updateProdMst(String prodCode, int version, String compCode, Connection conn) throws SQLException{
		DBManager dbm;

		try {
			if (prodCode != null && !prodCode.isEmpty() && compCode != null && !compCode.isEmpty() && version >0) {
				dbm = new DBManager();
				
				String sql = " select count(1) as cnt "
						   + " from prod_mst"
						   + " where prod_code = " + dbm.param(prodCode, DataType.TEXT)
						   + " and comp_code=" + dbm.param(compCode, DataType.TEXT);
				
				int count = Integer.parseInt(dbm.selectSingle(sql, "cnt"));
				dbm.clear();
				if(count > 0){
					sql = " update prod_mst set"
						+ " version = "+dbm.param(version, DataType.INT)
    				    + " ,upd_date = CURRENT_TIMESTAMP"
					    + " ,upd_by = " + dbm.param("SYSTEM", DataType.TEXT) 
					    + " where prod_code = " + dbm.param(compCode, DataType.TEXT)
					    + " and comp_code = " + dbm.param(compCode, DataType.TEXT);
					
					if (dbm.update(sql) > 0)
						return NO_UPDATE;
				}else{
					sql = "insert into prod_mst (comp_code, prod_code, version, prod_status, status, create_date, create_by, upd_date, upd_by) values (" +
							dbm.param(compCode, DataType.TEXT) + ", " +
							dbm.param(prodCode, DataType.TEXT) + ", " +
							dbm.param(version, DataType.INT) + ", " +
							dbm.param("E", DataType.TEXT) + ", " +
							dbm.param("A", DataType.TEXT) + ", " +
							"CURRENT_TIMESTAMP, " +
							dbm.param("SYSTEM", DataType.TEXT) + " , " +
							"CURRENT_TIMESTAMP, " +
							dbm.param("SYSTEM", DataType.TEXT) + ")" ;
					
					if (!dbm.insert(sql))
						return NO_INSERT;
				}
			}
		} catch (Exception e) {
			return e.getMessage();
		} finally {
			dbm = null;
		}

		return null;
	}
	

	public boolean updateProdLaunchStatus(Map<String,Object> release) throws SQLException{
		boolean success = false;
		DBManager dbm;
		Connection conn = null;

		try {
			String prodCode = release.get("ITEM_CODE").toString();
			int version = Integer.parseInt(release.get("VER_NO").toString());
			String compCode = release.get("COMP_CODE").toString();

			dbm = new DBManager();
			conn = DBAccess.getConnection();
			conn.setAutoCommit(false);

			if (prodCode != null && !prodCode.isEmpty() && compCode != null && !compCode.isEmpty() && version >0) {
				String sql = " update prod_trx set"
						   + " launch_status = 'L'"
						   + " where prod_code = " + dbm.param(prodCode, DataType.TEXT)
						   + " and version = " + dbm.param(version, DataType.INT)
						   + " and comp_code = " + dbm.param(compCode, DataType.TEXT);
				
				if (dbm.update(sql) > 0) {
					conn.commit();
					return true;
				}
				
				conn.rollback();
			}
		} catch (Exception e) {
			conn.rollback();
			Log.error(e);
		} finally {
			dbm = null;
			
			if (conn != null && !conn.isClosed())
				conn.close();
			
			conn = null;
		}

		return success;
	}
	
}
