package com.eab.dao.funds;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;

import com.eab.common.Log;
import com.eab.database.jdbc.DBAccess;
import com.eab.database.jdbc.DBManager;
import com.eab.database.jdbc.DataType;
import com.eab.json.model.Option;
import com.eab.model.profile.UserPrincipalBean;

public class Funds {
	
	
	//TODO:test for fundList
	public List<Option> getFundList(UserPrincipalBean principal) throws SQLException {	
		DBManager dbm;
		Connection conn = null;
		ResultSet rs = null;
		List<Option> fundListOptions = null;
		try {
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			fundListOptions = new ArrayList<Option>();

			String sql = "select comp_code, fund_code, lang_code, name_desc from v_fund_list where comp_code = " + dbm.param(principal.getCompCode(), DataType.TEXT) + " and lang_code = " + dbm.param(principal.getLangCode(), DataType.TEXT);

			rs = dbm.select(sql, conn);
			if (rs != null) {
				while (rs.next()) {
					fundListOptions.add(new Option(rs.getString("name_desc"), rs.getString("fund_code")));
				}
			}
		} catch (Exception e) {
			Log.error(e);
		} finally {
			dbm = null;
			rs = null;
			if (conn != null && !conn.isClosed())
				conn.close();
			conn = null;
		}
		return fundListOptions;
	}

//TODO: test for get fund list json
	public Map<String, JSONObject> getFundListJsonFile(String fund_code, UserPrincipalBean principal) throws SQLException {
		DBManager dbm;
		Connection conn = null;
		Map<String, JSONObject> sections = null;
		ResultSet rs = null;
		String compCode = principal.getCompCode();
		try {
			if (fund_code != null && !fund_code.isEmpty() && compCode != null && !compCode.isEmpty()) {
				dbm = new DBManager();
				conn = DBAccess.getConnection();
				String sql = "select fund_code, json_file from sys_fund_mst " + "where fund_code=" + dbm.param(fund_code, DataType.TEXT) + " and comp_code=" + dbm.param(compCode, DataType.TEXT) +  " and status='A'"; 
				rs = dbm.select(sql, conn);
				dbm.clear();
				if (rs != null) {
					sections = new HashMap<String, JSONObject>();
					while (rs.next()) {
						sections.put(rs.getString("fund_code"), new JSONObject(rs.getString("json_file")));
					}
				}

			}
		} catch (Exception e) {
			Log.error(e);
		} finally {
			dbm = null;
			rs = null;
			if (conn != null && !conn.isClosed())
				conn.close();
			conn = null;
		}
		return sections;
	}


}
