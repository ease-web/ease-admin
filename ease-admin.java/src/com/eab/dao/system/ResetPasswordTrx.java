package com.eab.dao.system;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import com.eab.common.Log;
import com.eab.database.jdbc.DBAccess;
import com.eab.database.jdbc.DBManager;
import com.eab.database.jdbc.DataType;

public class ResetPasswordTrx {
	static public int RESET_PWD_EXPIRE_HOUR = 12; 
	
	public boolean insert(String token, String userCode, String langCode, String loginUserCode) throws SQLException, Exception{
		boolean output = false;
		DBManager dbm = null;
		
		try {
			dbm = new DBManager();
			String sql = "insert into pwd_reset_trx(token, user_code, lang_code, create_by)"
					+ " values ("
					+ " " + dbm.param(token, DataType.TEXT)				//token
					+ ", " + dbm.param(userCode, DataType.TEXT)			//user_code
					+ ", " + dbm.param(langCode, DataType.TEXT)			//lang_code
					+ ", " + dbm.param(loginUserCode, DataType.TEXT)	//create_by
					+ ")";
			output = dbm.insert(sql);
		} catch(SQLException e) {
			throw e;
		} catch(Exception e) {
			throw e;
		} finally {
			dbm = null;
		}
		return output;
	}
	
	public HashMap<String, String> selectByToken(String token) throws SQLException, Exception {
		HashMap<String, String> output = null;
		DBManager dbm = null;
		ResultSet rs = null;
		Connection conn = null;
		ResultSetMetaData rsmd = null;
		ArrayList<String> colNames = null;
		
		try {
			conn = DBAccess.getConnection();
			dbm = new DBManager();
			String sql = "select token, user_code, lang_code, status, create_date, create_by "
						+ " from pwd_reset_trx "
						+ " where token = " + dbm.param(token, DataType.TEXT)
						+ "   and status = 'A'"
						+ "   and create_date + " + RESET_PWD_EXPIRE_HOUR + "/24 > sysdate";
			rs = dbm.select(sql, conn);
			if (rs != null) {
				output = new HashMap<String, String>();
				colNames = new ArrayList<String>();
				rsmd = rs.getMetaData();
				
				//Extract Column Name
				for (int i = 1; i <= rsmd.getColumnCount(); i++) {
					colNames.add( rsmd.getColumnName(i) );
				}
				
				while(rs.next()) {
					for(String name: colNames) {
						output.put(name, rs.getString(name));
					}
				}
			}
		} catch(SQLException e) {
			throw e;
		} catch(Exception e) {
			throw e;
		} finally {
    		dbm = null;
    		rs = null;
    		rsmd = null;
			if (conn != null && !conn.isClosed()) conn.close();
			conn = null;
    	}
		return output;
	}
	
	public boolean removeByToken(String token, Connection conn) throws SQLException, Exception {
		DBManager dbm = null;
		boolean output = false;
		
		try {
			dbm = new DBManager();
			String sql = "delete from pwd_reset_trx where token = " + dbm.param(token, DataType.TEXT);
			int count = dbm.delete(sql, conn);
			if (count == 1)
				output = true;
		} catch(SQLException e) {
			throw e;
		} catch(Exception e) {
			throw e;
		} finally {
    		dbm = null;
		}
		
		return output;
	}
	
	public boolean removeByUserCode(String userCode) throws SQLException, Exception {
		DBManager dbm = null;
		boolean output = false;
		
		try {
			dbm = new DBManager();
			String sql = "delete from pwd_reset_trx where user_code = " + dbm.param(userCode, DataType.TEXT);
			int count = dbm.delete(sql);
			if (count == 1)
				output = true;
		} catch(SQLException e) {
			Log.error(e);
			throw e;
		} catch(Exception e) {
			Log.error(e);
			throw e;
		} finally {
    		dbm = null;
		}
		
		return output;
	}
}
