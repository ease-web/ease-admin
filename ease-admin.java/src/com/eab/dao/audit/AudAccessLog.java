package com.eab.dao.audit;

import java.sql.Connection;
import java.sql.SQLException;

import com.eab.common.Function2;
import com.eab.common.Log;
import com.eab.database.jdbc.DBAccess;
import com.eab.database.jdbc.DBManager;
import com.eab.database.jdbc.DataType;

public class AudAccessLog {

	public boolean add(String userCode, String sessionId, String uri, String funcCode, String keyValue, boolean validAccess, String ipServAddr, String ipUserAddr) 
			 	throws SQLException{
		boolean result = false;
		Function2 func2 = new Function2();
		DBManager dbm = null;
		Connection conn = null;
		
		String privilege = "N";
		
		try {
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			conn.setAutoCommit(false);
			
			// * for empty value (for not null)
			if (userCode == null || userCode.length() == 0)
				userCode = "*";
			
			// "" for empty value
			if (sessionId == null) {
				sessionId = "";
			}
			if (uri == null) {
				uri = "";
			}
			if (funcCode == null) {
				funcCode = "";
			}
			if (keyValue == null) {
				keyValue = "";
			}
			if (validAccess) {
				privilege = "Y";
			} else {
				privilege = "N";
			}
			if (ipServAddr == null) {
				ipServAddr = "";
			}
			if (ipUserAddr == null) {
				ipUserAddr = "";
			}
			
			String sql = "insert into AUD_USER_ACCESS_LOG(user_code, session_id, access_time, access_uri, func_code, access_key_value, valid_access, serv_ip_addr, user_ip_addr)"
					+ " values(" + dbm.param(userCode, DataType.TEXT)
					+ ", " + dbm.param(sessionId, DataType.TEXT)
					+ ", sysdate"
					+ ", " + dbm.param(uri, DataType.TEXT)
					+ ", " + dbm.param(funcCode, DataType.TEXT)
					+ ", " + dbm.param(keyValue, DataType.TEXT)
					+ ", " + dbm.param(privilege, DataType.TEXT)
					+ ", " + dbm.param(ipServAddr, DataType.TEXT)
					+ ", " + dbm.param(ipUserAddr, DataType.TEXT)
					+ ")";
			result = dbm.insert(sql, conn);
			
			//remove record lock session
			dbm.clear();
			String sqlLock = "delete from aud_record_lock where user_code = ?";
			dbm.param(userCode, DataType.TEXT);
			dbm.delete(sqlLock,conn);
			
			
			if (result) {
				conn.commit();
			} else {
				conn.rollback();
			}
			
		} catch(Exception e) {
			Log.error(e);
		} finally {
			func2 = null;
			dbm = null;
			if (conn != null && !conn.isClosed()) conn.close();
			conn = null;
		}
		
		return result;
	}
}
