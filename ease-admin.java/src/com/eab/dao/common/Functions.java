package com.eab.dao.common;

import java.io.BufferedReader;
import java.io.Reader;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import com.eab.common.Language;
import com.eab.common.Log;
import com.eab.database.jdbc.DBAccess;
import com.eab.database.jdbc.DBManager;
import com.eab.database.jdbc.DataType;

public class Functions {
	public JSONArray getMstOptions(HttpServletRequest request, String key, String table, String comp_code){
		DBManager dbm = null;
		Connection conn = null;
		Language lang = null;
		ResultSet rs = null;
		JSONArray options = new JSONArray();
		try {
			lang = new Language();
			dbm = new DBManager();
			conn = DBAccess.getConnection();

			String sql = " select " + key + " from " + table + " where "
					   + " comp_code = " + dbm.param(comp_code, DataType.TEXT)
					   + " and status = " + dbm.param("A", DataType.TEXT);
			String keyStr = key.replaceAll(" ", "");
			String[] ary = keyStr.split(","); 
			
			 rs = dbm.select(sql, conn);
			 if (rs != null) {
					while (rs.next()) {
						JSONObject option = new JSONObject();
						option.put("value", rs.getString(ary[0]));
						option.put("title", rs.getString(ary[1]));
						options.put(option);
					}
				}
			 
		} catch (SQLException e) {
			Log.error("Functions getFunction 1 ----------->" + e);
		} catch (Exception e) {
			Log.error("Functions getFunction 2 ----------->" + e);
		} finally {
			dbm = null;
			lang = null;
			try {
				if (conn != null && !conn.isClosed()) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			conn = null;
		}
		return options;
	}
	
	public JSONArray getLookUpOptions(HttpServletRequest request, String key){
		DBManager dbm = null;
		Connection conn = null;
		Language lang = null;
		ResultSet rs = null;
		JSONArray options = new JSONArray();
		try {
			lang = new Language();
			dbm = new DBManager();
			conn = DBAccess.getConnection();

			String sql = " select sys_name_mst.name_desc, sys_lookup_mst.lookup_field"
					   + " from sys_lookup_mst inner join sys_name_mst on sys_lookup_mst.name_code = sys_name_mst.name_code where "
					   + " sys_lookup_mst.lookup_key = "
					   + dbm.param(key, DataType.TEXT)
					   + " and sys_name_mst.lang_code = "
					   + dbm.param(lang.getLang(request), DataType.TEXT);

			 rs = dbm.select(sql, conn);
			 if (rs != null) {
					while (rs.next()) {
						JSONObject option = new JSONObject();
						option.put("value", rs.getString("lookup_field"));
						option.put("title", rs.getString("name_desc"));
						options.put(option);
					}
				}
			 
		} catch (SQLException e) {
			Log.error("Functions getFunction 1 ----------->" + e);
		} catch (Exception e) {
			Log.error("Functions getFunction 2 ----------->" + e);
		} finally {
			dbm = null;
			lang = null;
			try {
				if (conn != null && !conn.isClosed()) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			conn = null;
		}
		return options;
	}
	
	public String convertClobToString(Clob clob) {
		String reString = "";
		try {
			Reader is = clob.getCharacterStream();
			BufferedReader br = new BufferedReader(is);
			String s = br.readLine();
			StringBuffer sb = new StringBuffer();
			while (s != null) {
				sb.append(s);
				s = br.readLine();
			}
			reString = sb.toString().trim();
		} catch(Exception e) {
			e.printStackTrace();
		}
		return reString;
	}
	public JSONObject convertClobToJson(Clob clob) throws Exception{
		String jsonStr = convertClobToString(clob);
		return new JSONObject(jsonStr);
	}

	public String getFunction(String url, HttpServletRequest request)
			throws SQLException {
		DBManager dbm = null;
		Connection conn = null;
		Language lang = null;

		try {
			lang = new Language();
			dbm = new DBManager();
			conn = DBAccess.getConnection();

			String sql = " select snm.name_desc as name "
					   + " from sys_func_mst sfm, sys_name_mst snm "
					   + " where sfm.func_uri = " + dbm.param(url, DataType.TEXT)
					   + " and sfm.name_code = snm.name_code and snm.lang_code = "
					   + dbm.param(lang.getLang(request), DataType.TEXT);

			return dbm.selectSingle(sql, "name", conn);
		} catch (SQLException e) {
			Log.error("Functions getFunction 1 ----------->" + e);
		} catch (Exception e) {
			Log.error("Functions getFunction 2 ----------->" + e);
		} finally {
			dbm = null;
			lang = null;

			if (conn != null && !conn.isClosed()) {
				conn.close();
			}
			conn = null;
		}
		return null;
	}

	private static List<String> dtFmtList;
	private static List<String> timeFmtList;

	public static List<String> getDateFmt() throws SQLException {
		DBManager dbm = null;
		Connection conn = null;
		ResultSet rs = null;
		if (dtFmtList != null) {
			return dtFmtList;
		} else {
			try {
				dbm = new DBManager();
				conn = DBAccess.getConnection();
				dtFmtList = new ArrayList<String>();
				String sql = " select NAME_CODE from sys_lookup_mst where lookup_key = 'DATEFORMAT'";
				rs = dbm.select(sql, conn);
				if (rs != null) {
					while (rs.next()) {
						dtFmtList.add(rs.getString("NAME_CODE"));
					}
				}
				return dtFmtList;
			} catch (SQLException e) {
				Log.error("Functions getDateFmt 1 ----------->" + e);
			} catch (Exception e) {
				Log.error("Functions getDateFmt 2 ----------->" + e);
			} finally {
				dbm = null;
				if (conn != null && !conn.isClosed()) {
					conn.close();
				}
				conn = null;
			}
		}
		return null;
	}

	public static List<String> getTimeFmt() throws SQLException {
		DBManager dbm = null;
		Connection conn = null;
		ResultSet rs = null;
		if (timeFmtList != null) {
			return timeFmtList;
		} else {
			try {
				dbm = new DBManager();
				conn = DBAccess.getConnection();
				timeFmtList = new ArrayList<String>();

				String sql = " select NAME_CODE from sys_lookup_mst where lookup_key = 'TIMEFORMAT'";
				rs = dbm.select(sql, conn);
				if (rs != null) {
					while (rs.next()) {
						timeFmtList.add(rs.getString("NAME_CODE"));
					}
				}
				return timeFmtList;
			} catch (SQLException e) {
				Log.error("Functions getTimeFmt 1 ----------->" + e);
			} catch (Exception e) {
				Log.error("Functions getTimeFmt 2 ----------->" + e);
			} finally {
				dbm = null;
				if (conn != null && !conn.isClosed()) {
					conn.close();
				}
				conn = null;
			}
			return null;
		}
	}

	public static String getChannelName(String channel_code, String comp_code)
			throws SQLException {
		DBManager dbm = null;
		Connection conn = null;

		try {
			dbm = new DBManager();
			conn = DBAccess.getConnection();

			String sql = " select channel_name " + " from sys_channel_mst "
					+ " where channel_code = "
					+ dbm.param(channel_code, DataType.TEXT)
					+ " and comp_code = " + dbm.param(comp_code, DataType.TEXT);

			return dbm.selectSingle(sql, "channel_name", conn);
		} catch (SQLException e) {
			Log.error("Functions getChannelName 1 ----------->" + e);
		} catch (Exception e) {
			Log.error("Functions getChannelName 2 ----------->" + e);
		} finally {
			dbm = null;

			if (conn != null && !conn.isClosed()) {
				conn.close();
			}
			conn = null;
		}

		return null;
	}

	public static List<String> getLangNameCode() throws SQLException {
		DBManager dbm = null;
		Connection conn = null;
		ResultSet rs = null;

		try {
			dbm = new DBManager();
			conn = DBAccess.getConnection();

			String sql = "select name_code from sys_lang_mst where status = 'A'";

			rs = dbm.select(sql, conn);
			if (rs != null) {
				List<String> nameCodes = new ArrayList<String>();
				while (rs.next()) {
					nameCodes.add(rs.getString("name_code"));
				}
				return nameCodes;
			}
			return null;
		} catch (SQLException e) {
			Log.error("Functions getLangNameCode 1 ----------->" + e);
		} catch (Exception e) {
			Log.error("Functions getLangNameCode 2 ----------->" + e);
		} finally {
			dbm = null;

			if (conn != null && !conn.isClosed()) {
				conn.close();
			}
			conn = null;
		}

		return null;
	}

}
