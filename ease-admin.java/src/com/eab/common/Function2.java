package com.eab.common;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.eab.dao.system.Name;
import com.eab.database.jdbc.DBAccess;
import com.eab.database.jdbc.DBManager;
import com.eab.database.jdbc.DataType;
import com.eab.json.model.Field;
import com.eab.json.model.Option;
import com.eab.model.profile.UserBean;
import com.eab.model.profile.UserPrincipalBean;
import com.eab.model.system.SysParamBean;

public class Function2 {

	public String getErrorMsg(String errCode) {
		String langCode = Constants.DEFAULT_LANG;
		String errMsg = null;

		try {
			errMsg = getErrorMsg(errCode, langCode);
		} catch(Exception e) {
			Log.error(e);
		}

		return errMsg;
	}

	public String getErrorMsg(String errCode, String langCode) {
		String message = null;
		DBManager dbm = null;

		try {
			dbm = new DBManager();

			String sql = " select name.name_desc "
					   + " from sys_error err, sys_name_mst name"
					   + " where err.error_code = " + dbm.param(errCode, DataType.TEXT)
					   + " and lang_code = " + dbm.param(langCode, DataType.TEXT)
					   + " and err.name_code = name.name_code";
			message = dbm.selectSingle(sql, "name_desc");			
		} catch (SQLException e) {
			Log.error("Function2 Function2 1 ----------->" + e);
		} catch (Exception e) {
			Log.error("Function2 Function2 2 ----------->" + e);
		} finally {
			dbm = null;
		}

		return message;
	}

	public String getCompCode(String userCode) {
		String compCode = null;
		DBManager dbm = null;

		try {
			dbm = new DBManager();
			String sql = " select comp_code from v_sys_user_all where user_code= " + dbm.param(userCode, DataType.TEXT);
			compCode = dbm.selectSingle(sql, "comp_code");
		} catch (SQLException e) {
			Log.error("Function2 getCompCode 1 ----------->" + e);
		} catch (Exception e) {
			Log.error("Function2 getCompCode 2 ----------->" + e);
		} finally {
			dbm = null;
		}

		return compCode;
	}

	public Map<String, SysParamBean> getSysParam() throws SQLException {
		HashMap<String, SysParamBean> sysParam = new HashMap<String, SysParamBean>();
		DBManager dbm = null;
		ResultSet rs = null;
		Connection conn = null;

		try {
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			String sql = "select param_code, param_value, name_code from sys_param where status='A'";
			rs = dbm.select(sql, conn);
			if (rs != null) {
				while (rs.next()) {
					SysParamBean bean = new SysParamBean();
					bean.setParamCode(rs.getString("param_code"));
					bean.setParamValue(rs.getString("param_value"));
					bean.setNameCode(rs.getString("name_code"));

					sysParam.put(rs.getString("param_code"), bean);
				}
			}
		} catch(SQLException e) {
			throw e;
		} catch(Exception e) {
			Log.error(e);
		} finally {
			dbm = null;
			rs = null;
			if (conn != null && !conn.isClosed()) conn.close();
			conn = null;
		}

		return sysParam;
	}
	
	public static boolean hasRecord(String table, String col, String value, DBManager dbm, Connection conn) {
		boolean hasRecord = false;
		ResultSet rs = null;
		
		try {
			String sql = null;
			try {
				sql = "select COUNT(*) AS rowcount from " + table + " where " + col + " = " +  dbm.param(value, DataType.TEXT);
			} catch (Exception e) {
				Log.error(e);
			}			
			rs = dbm.select(sql, conn);
			rs.next();
			int count = rs.getInt("rowcount");
			if (count > 0)
				hasRecord = true;
			rs.close();
		} catch (SQLException e) {
			Log.error("Function2 hasRecord 1 ----------->" + e);
		} catch (Exception e) {
			Log.error("Function2 hasRecord 2 ----------->" + e);
		} finally {
			dbm.clear();
		}
		return hasRecord;

	}
	public static boolean hasRecord(String table, HashMap<String,String> condMap, DBManager dbm, Connection conn) {
		HashMap<String,Object> condMap_ = new HashMap<String,Object>();
		for (Map.Entry<String, String> entry : condMap.entrySet())
		{
			condMap_.put(entry.getKey(), entry.getValue());
		}
		return hasRecordByType(table, condMap_, dbm, conn);
	}
	
	public static boolean hasRecordByType(String table, HashMap<String,Object> condMap, DBManager dbm, Connection conn) {
		
		boolean hasRecord = false;
		boolean newConn = false;
		
		if (table == null) 
			return hasRecord;
		
		ResultSet rs = null;
		try {
			String sql = null;
			if(dbm == null){
				dbm = new DBManager();
			}
			
			if(conn == null){
				newConn = true;
				conn = DBAccess.getConnection();
			}
			dbm.clear();
			
			String conditionStr = null;
				
				for (Map.Entry<String, Object> entry : condMap.entrySet())
				{
					if(conditionStr == null)
						conditionStr = entry.getKey() + "=? ";					
					else
						conditionStr = conditionStr + " and " + entry.getKey() + "=? "; 
					
					int type = (entry.getValue().getClass() == String.class) ? DataType.TEXT : DataType.INT;
					dbm.param(entry.getValue(), type);
				}
				
			sql = "select COUNT(*) AS rowcount from " + table + " where " + conditionStr;		
			rs = dbm.select(sql, conn);
			rs.next();
			int count = rs.getInt("rowcount");
			if (count > 0)
				hasRecord = true;
			rs.close();
		} catch (SQLException e) {
			Log.error("Function2 hasRecord1 1 ----------->" + e);
		} catch (Exception e) {
			Log.error("Function2 hasRecord1 2 ----------->" + e);
		} finally {
			dbm.clear();
			dbm = null;
			if(newConn){
				try {
					conn.close();
				} catch (SQLException e) {
					Log.error(e);
				}
			}
		}
		return hasRecord;

	}
	public static boolean hasRecord(String table, HashMap<String,String> posCondMap, HashMap<String,String> negCondMap, DBManager dbm, Connection conn) {
		
		boolean hasRecord = false;
		boolean newConn = false;
		
		if (table == null) {
			return hasRecord;
		}
		
		ResultSet rs = null;
		try {
			String sql = null;
			if(dbm == null){
				newConn = true;
				dbm = new DBManager();
				conn = DBAccess.getConnection();
			}
			
			String conditionStr = null;
				
				for (Map.Entry<String, String> entry : posCondMap.entrySet())
				{
					if(conditionStr == null)
						conditionStr = entry.getKey() + "=? ";					
					else
						conditionStr = conditionStr + " and " + entry.getKey() + "=? "; 
					
					dbm.param(entry.getValue(), DataType.TEXT);
				}
				
				String neg = null;
				for (Map.Entry<String, String> entry : negCondMap.entrySet())
				{
					if(neg == null)
						neg = entry.getKey() + "!=? ";					
					else
						neg = neg + " and " + entry.getKey() + "!=? "; 
					
					dbm.param(entry.getValue(), DataType.TEXT);
				}
				if(neg != null){
					conditionStr +=  " and " + neg;
				}
				
			sql = "select COUNT(*) AS rowcount from " + table + " where " + conditionStr;		
			rs = dbm.select(sql, conn);
			rs.next();
			int count = rs.getInt("rowcount");
			if (count > 0)
				hasRecord = true;
			rs.close();
		} catch (SQLException e) {
			Log.error("Function2 hasRecord2 1 ----------->" + e);
		} catch (Exception e) {
			Log.error("Function2 hasRecord2 2 ----------->" + e);
		} finally {
			dbm.clear();
			if(newConn){
				try {
					conn.close();
				} catch (SQLException e) {
					Log.error(e);
				}
			}
		}
		return hasRecord;

	}
	
	public static Object getSingleColumnFromRecord(String table, String col, String key, String value, int type, DBManager dbm, Connection conn) {
		Object field = null;
		ResultSet rs = null;
		try {
			String sql = null;
			try {
				sql = "select "+ col +" from " + table + " where " + key + " = " +  dbm.param(value, DataType.TEXT);
			} catch (Exception e) {
				Log.error(e);
			}			
			rs = dbm.select(sql, conn);
			if (rs.next()) {
				if (type == DataType.TEXT) {
					field = rs.getString(1);
				} else if (type == DataType.DATE) {
					field = rs.getDate(1);
				}
			}
		} catch (SQLException e) {
			Log.error("Function2 getSingleColumnFromRecord 1 ----------->" + e);
		} catch (Exception e) {
			Log.error("Function2 getSingleColumnFromRecord 2 ----------->" + e);
		} finally {
			try {
				rs.close();
			} catch (SQLException e) {
			}
			dbm.clear();
		}
		return field;

	}

	public Map<String, Object> backupSessionAttributes(HttpServletRequest request) {
		HttpSession session = request.getSession(false);
		Map<String, Object> output = new HashMap<String, Object>();
		Enumeration keys = null;

		try {
			if (session != null) {
				keys = session.getAttributeNames();
				while (keys.hasMoreElements()) {
					String key = (String) keys.nextElement();
					output.put(key, session.getAttribute(key));
				}
			}
		} catch(Exception e) {
			Log.error(e);
		}

		return output;
	}

	public void restoreSessionAttributes(HttpServletRequest request, Map<String, Object> attributes) {
		HttpSession session = request.getSession(true);		//Create session if not exist
		HttpServletRequest output = request;

		try {
			if (attributes != null && attributes.size() > 0) {
				for(Entry<String, Object> entry : attributes.entrySet()) {
					String key = entry.getKey();
					Object value = entry.getValue();
					session.setAttribute(key, value);
				}
			}
		} catch(Exception e) {
			Log.error(e);
		}
	}

	public void stdoutSessionAttributes(HttpServletRequest request) {
		HttpSession session = request.getSession(false);
		Enumeration keys = null;

		try {
			if (session != null) {
				keys = session.getAttributeNames();
				while (keys.hasMoreElements()) {
					String key = (String) keys.nextElement();
					Log.info("Session Key: " + key + ", Value: " + session.getAttribute(key));
				}
			}
		} catch(Exception e) {
			Log.error(e);
		}
	}

	public boolean hasLoginSession(String userCode) {
		boolean output = false;
		DBManager dbm = null;

		try {
			dbm = new DBManager();
			String sql = " select 1 as cnt from aud_user_log where user_code=" + dbm.param(userCode, DataType.TEXT) + " and success='Y' and logout_time is null";
			String result = dbm.selectSingle(sql, "cnt");
			if ("1".equals(result)) {
				output = true;
			}
		} catch(Exception e) {
			Log.error(e);
		} finally {
			dbm = null;
		}

		return output;
	}

	public int setLogoutTimeToAud(String ipAddr) {
		int count = 0;
		DBManager dbm = null;

		if (ipAddr != null && ipAddr.length() > 0) {
			try {
				dbm = new DBManager();
				String sql = "update aud_user_log set logout_time = sysdate where serv_ip_addr=" + dbm.param(ipAddr, DataType.TEXT) + " and logout_time is null";
				count = dbm.update(sql);
			} catch(Exception e) {
				Log.error(e);
			} finally {
				dbm = null;
			}
		}

		return count;
	}

	public String getFuncCode(String pathUrl) {
		String output = null;
		DBManager dbm = null;

		try {
			dbm = new DBManager();
			String sql = " select func_code from sys_func_mst where func_uri=" + dbm.param(pathUrl, DataType.TEXT) + " and status='A'";
			output = dbm.selectSingle(sql, "func_code");
		} catch(Exception e) {
			Log.error(e);
		} finally {
			dbm = null;
		}

		return output;
	}
	
	
	public Map<String, String> getFuncUriMap() throws SQLException{
		HashMap<String, String> uriMap = new HashMap<String, String>();
		
	 
		 
		Connection conn = null;
		DBManager dbm = null;
		ResultSet rs = null;
		 
				
		try {	
			dbm = new DBManager();
			conn = DBAccess.getConnection();
			
 
			String sql = " select func_code, func_uri from sys_func_mst where status='A'";
			 	rs = dbm.select(sql, conn); 
			
			while (rs.next()) { 
				
				uriMap.put(rs.getString("func_code"), rs.getString("func_uri"));
			}
			
			rs = null;
			dbm.clear();
			
			 
		} catch (SQLException e) {
			Log.error("{ReleaseDetail checkBaseVersion 1} ----------->" + e);
		} catch (Exception e) {
			Log.error("{ReleaseDetail checkBaseVersion 2} ----------->" + e);
		} finally {
			dbm = null;
			rs = null;
			
			if (conn != null && !conn.isClosed())
				conn.close();
				conn = null;
		}
		 
		return uriMap;
		
	}
	
	
	public String getFuncUri(String funcCode) { 
		String output = null;
		DBManager dbm = null;

		try {
			dbm = new DBManager();
			String sql = " select func_uri from sys_func_mst where func_code=" + dbm.param(funcCode, DataType.TEXT) + " and status='A'";
			output = dbm.selectSingle(sql, "func_uri");
		} catch(Exception e) {
			Log.error(e);
		} finally {
			dbm = null;
		}

		return output;
	}
	

	static public void translateField(HttpServletRequest request, Field field) throws Exception {	    
	    List<String> names = new ArrayList<String>();
	    field.getNameCodes(names);
	    translateFields(request, field.getItems(), names);
	}

	
	static public void translateFields(HttpServletRequest request, List<Field>fields) throws Exception {	    
	    List<String> names = new ArrayList<String>();
	    
	    for(Field field: fields) {
	    	field.getNameCodes(names);
	    }
	    
	    translateFields(request, fields, names);
	}
	
	static public void translateFields(HttpServletRequest request, List<Field>fields, List<String>names) throws Exception {	    
		Map<String, String> nameMap = getTranslateMap(request, names);
	    for(Field field: fields) {
	    	field.translate(nameMap);
	    }
	}
	
	static public Map<String,String> getTranslateMap(HttpServletRequest request, List<String> names) throws SQLException {
		HashMap<String, String> nameMap = new HashMap<String, String>();
		HashMap<String, Object> nameObMap = new HashMap<String, Object>();
		List<Name> nameList = new ArrayList<Name>();
		Language langObj;
		HttpSession session;
		UserPrincipalBean principal;
		
		int size = names.size();
		
		for(int i=0; i<size; i++){
			Name newName = new Name();
			newName.setCode(names.get(i));
			newName.setDesc(names.get(i));
			nameList.add(newName);
		}
		
		session = request.getSession();
		principal = (UserPrincipalBean) session.getAttribute(Constants.USER_PRINCIPAL);
		langObj = new Language(principal.getUITranslation());
		nameObMap = (HashMap<String, Object>) langObj.getNameMap();
		for (Map.Entry<String, Object> entry : nameObMap.entrySet()) {
		       try{
		    	   nameMap.put(entry.getKey(), (String) entry.getValue());
		          }catch(ClassCastException cce){
		        	  Log.error(cce);
		          }
		 }
		
	    /*
		Language langObj = new Language();
	    ArrayList<Name> nameList = new ArrayList<Name>();

	    for(String name: names) {
	    	if (!nameMap.containsKey(name)) {
		    	Name nameObj = new Name();
		    	nameMap.put(name, "");
		    	nameObj.setCode(name);
		    	nameList.add(nameObj); 
	    	}
	    }

	    if (langObj.getNameList(request, nameList) ) {
	    	for(Name dummy: nameList) {
	    		if (dummy.getDesc() != null && !dummy.getDesc().isEmpty())
	    			nameMap.replace(dummy.getCode(), dummy.getDesc());
	    	}
	    }
	    */
	    return nameMap;
	}


	public static List<Option> getOptionsByLookupKey(HttpServletRequest request, String lookUpKey, Language langObj) throws Exception {
		DBManager dbm = null;
		ResultSet rs = null;		
		List<Option> options = new ArrayList<Option>();
		Connection conn = null;
		
		try {
			conn = DBAccess.getConnection();
			dbm = new DBManager();
			
			String lookUpStr = "select NAME_CODE, LOOKUP_FIELD from SYS_LOOKUP_MST where LOOKUP_KEY = ? order by SEQ";
			dbm.param(lookUpKey, DataType.TEXT);
			
			rs = dbm.select(lookUpStr, conn);

			while(rs.next()) {				
				String lkupValue = rs.getString("LOOKUP_FIELD");
				String lkupTitle = langObj.getNameDesc(rs.getString("NAME_CODE"));

				Option option = new Option(lkupTitle, lkupValue);
				options.add(option);
			}
			
			Collections.sort(options, (a, b) -> a.getTitle().compareTo(b.getTitle()));
		} finally {
			try {
				rs.close();
				dbm.clear();
				
				rs = null;
				dbm = null;
	
				if (conn != null && !conn.isClosed()) 
					conn.close();
				
				conn = null;
			} catch (Exception ex) {
				
			}
		}
		
		return options;
	}
	
	
	public static String getUserName(String userCode) {
		String output = null;
		DBManager dbm = null;

		try {
			dbm = new DBManager();
			String sql = "select user_name from sys_user_mst where user_code = " + dbm.param(userCode, DataType.TEXT);
			output = dbm.selectSingle(sql, "user_name");
			
			if (output == null) 
				output = userCode;
		} catch(Exception e) {
			Log.error(e);
		} finally {
			dbm = null;
		}

		return output;
	}
	
	public boolean isSessionLogin(String sessionID) throws SQLException {
		boolean result = false;
		DBManager dbm = null;
		
		try {
			dbm = new DBManager();
			
			String sql = "select count(1) as cnt from aud_user_log where session_id=" + dbm.param(sessionID, DataType.TEXT) + " and success='Y' and logout_time is null";
			String count = dbm.selectSingle(sql, "cnt");
			
			if ("0".equals(count)) {
				result = false;		// Already Logout.
			} else {
				result = true;		// Still Login.
			}
		} catch(SQLException e) {
			throw e;
		} catch(Exception e) {
			Log.error(e);
		} finally {
			dbm = null;
		}
		
		return result;
	}
	
	public int setLogoutTimeByUser(String userCode) {
		int count = 0;
		DBManager dbm = null;

		if (userCode != null && userCode.length() > 0) {
			try {
				dbm = new DBManager();
				String sql = "update aud_user_log set logout_time = sysdate where user_code=" + dbm.param(userCode, DataType.TEXT) + " and logout_time is null";
				count = dbm.update(sql);
			} catch(Exception e) {
				Log.error(e);
			} finally {
				dbm = null;
			}
		}

		return count;
	}
}
