package com.eab.common;

import java.io.BufferedReader;
import java.io.Reader;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.sql.Clob;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import org.json.JSONArray;
import org.json.JSONObject;

import com.eab.dao.common.Functions;
import com.eab.database.jdbc.DataType;
import com.eab.model.RSAKeyPair;
import com.eab.model.profile.UserPrincipalBean;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;

public class Function {
	
	private static final String DATEFORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS";
	
	private static final String TYPE_PRODUCT = "product";
	private static final String TYPE_RIDER = "rider";
	
	public static String longToDate(Long sec){
		Date date = new Date(sec);
		SimpleDateFormat df = new SimpleDateFormat(Constants.dataFormats.DATE_FORMAT);
		return df.format(date);
	}
	
	public static String getToday(){
		DateFormat df = new SimpleDateFormat(Constants.dataFormats.DATE_FORMAT);
		Date today = Calendar.getInstance().getTime();        
		String reportDate = df.format(today);
		return reportDate;
	}
	public static int getSize(JSONObject json){
		Iterator<?> keys = json.keys();
		int size = 0;
		while( keys.hasNext() ) {
			String key = (String)keys.next();
			size ++;
		}
		return size;
	}
	
	public static JSONObject getDatePara(String key, JSONObject json){
		Object thisDate = new Object();
		int thisDateType = DataType.TEXT;
		if(json.has(key)){
			Object _thisDate = json.get(key);			
			if(_thisDate.getClass() == Long.class){
				thisDateType = DataType.DATE;
				Date utilDate = new Date((Long)_thisDate);
				java.sql.Date sDate = new java.sql.Date(utilDate.getTime());
				thisDate = sDate;
			}else if(_thisDate.getClass() == String.class){
				thisDateType = DataType.DATE;
				if("$today".equals((String)_thisDate)){
					thisDate = new java.sql.Date(Calendar.getInstance().getTime().getTime());
				}else if("$oneyearlater".equals((String)_thisDate)){
					Long thisMontSec =  Calendar.getInstance().getTime().getTime() + 356*24*60*60*1000;
					thisDate = new java.sql.Date(thisMontSec);
				}else{
					thisDateType = DataType.TEXT;
					thisDate = (String)"NULL";
				}
			}else{
				thisDate = (String)"NULL";
			}
		}else{
			thisDate = (String)"NULL";
		}
		JSONObject result = new JSONObject();
		result.put("type", thisDateType);
		result.put("value", thisDate);
		Log.error("getPara: " + result.toString());
		return result;
	}
	
	public static boolean isDataValid(JSONObject template){
		List VALID_TYP = Arrays.asList("text");
		try{
			if(template.has("type") && VALID_TYP.contains(template.getString("type"))){
				return true;
			}else{
				return false;
			}
		}catch(Exception e){
			return false;
		}
		
	}
	public static JSONObject convertClobToJson(Clob clob) throws Exception{
		String jsonStr = convertClobToString(clob);
		return new JSONObject(jsonStr);
	}
	
	public static JSONArray convertClobToJsonArray(Clob clob) throws Exception{
		String jsonStr = convertClobToString(clob);
		return new JSONArray(jsonStr);
	}
	
	public static String convertClobToString(Clob clob) {
		String reString = "";
		try {
			Reader is = clob.getCharacterStream();
			BufferedReader br = new BufferedReader(is);
			String s = br.readLine();
			StringBuffer sb = new StringBuffer();
			while (s != null) {
				sb.append(s);
				s = br.readLine();
			}
			reString = sb.toString().trim();
		} catch(Exception e) {
			e.printStackTrace();
		}
		return reString;
	}
	public static boolean canSkipMakerChecker(UserPrincipalBean principal) {
		return principal.getUserType().equals("A") || principal.getUserType().equals("S") ;
	}
	
	public static String GenerateRandomString(int length) {
		SecureRandom random = new SecureRandom();
		byte bytes[] = new byte[length];
		random.nextBytes(bytes);
		return new Base64().encodeAsString(bytes);
	}

	public static boolean isInteger(String s) {
		try {
			Integer.parseInt(s);
		} catch (NumberFormatException e) {
			return false;
		} catch (NullPointerException e) {
			return false;
		}
		// only got here if we didn't return false
		return true;
	}

	public static String GenerateRandomASCII(int length) {
		String characters = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

		// Random rng = new Random();
		java.security.SecureRandom rng = new java.security.SecureRandom();
		char[] text = new char[length];
		for (int i = 0; i < length; i++) {
			text[i] = characters.charAt(rng.nextInt(characters.length()));
		}
		return new String(text);
	}

	public static String getDateTimeNow() {
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(DATEFORMAT);
		sdf.setCalendar(calendar);
		return sdf.format(calendar.getTime());
	}

	public static long diffDateTimeNow(String dt) throws ParseException {
		Calendar d1 = Calendar.getInstance(); // Input DateTime
		Calendar d2 = Calendar.getInstance(); // Current DateTime
		SimpleDateFormat sdf = new SimpleDateFormat(DATEFORMAT);
		d1.setTime(sdf.parse(dt));
		long diff = d2.getTimeInMillis() - d1.getTimeInMillis();

		return (diff / 1000); // Second(s)
	}

	public static long diffDateTimeNow(Date dt) throws ParseException {
		Calendar d1 = Calendar.getInstance(); // Input DateTime
		Calendar d2 = Calendar.getInstance(); // Current DateTime
		d1.setTime(dt);
		long diff = d2.getTimeInMillis() - d1.getTimeInMillis();

		return (diff / 1000); // Second(s)
	}

	public static String decryptJsText(RSAKeyPair keypair, String encryptedText) {
		// String decryptedText = null;
		// RSA cryptoObj = new RSA();
		//
		// try {
		// cryptoObj.Import(keypair);
		// decryptedText = cryptoObj.decryptJS(encryptedText);
		// } catch (Exception e) {
		// Log.error(e);
		// }
		//
		// return decryptedText;
		return encryptedText;
	}

	public static String ByteToStr(byte[] bytes) {
		StringBuilder sb = new StringBuilder(bytes.length * 2);
		for (byte b : bytes)
			sb.append((char) b);
		return sb.toString();
	}

	public static byte[] StrToByte(String text) {
		return text.getBytes();
	}

	public static String ByteToHex(byte[] bytes) {
		return Hex.encodeHexString(bytes);
	}

	public static byte[] HexToByte(String s) {
		return new BigInteger(s, 16).toByteArray();
	}

	public static byte[] StrToBase64(String text) {
		return text.getBytes();
	}

	public static String Base64ToHex(byte[] bytes) {
		return Hex.encodeHexString(bytes);
	}

	public static byte[] HexToStr(String str) {
		int len = str.length();
		byte[] data = new byte[len / 2];
		for (int i = 0; i < len; i += 2) {
			data[i / 2] = (byte) ((Character.digit(str.charAt(i), 16) << 4) + Character.digit(str.charAt(i + 1), 16));
		}
		return data;
	}

	private static char[] HEX_ARRAY = "0123456789ABCDEF".toCharArray();

	public static String StrToHex(byte[] bytes) {
		char[] hexChars = new char[bytes.length * 2];
		for (int j = 0; j < bytes.length; j++) {
			int v = bytes[j] & 0xFF;
			hexChars[j * 2] = HEX_ARRAY[v >>> 4];
			hexChars[j * 2 + 1] = HEX_ARRAY[v & 0x0F];
		}
		return new String(hexChars);
	}

	public static String getClientIp(HttpServletRequest request) {
		// is client behind something?
		if (request.getHeader("X-FORWARDED-FOR") != null) {
			return request.getHeader("X-FORWARDED-FOR");
		}
		return request.getRemoteAddr();
	}

	public static String getServerIp(HttpServletRequest request) {
		String ipAddr = (request != null) ? request.getLocalAddr() : "0.0.0.0";
		if (Constants.SERVER_IP_ADDR != null && !Constants.SERVER_IP_ADDR.equals("0.0.0.0")) {
			ipAddr = Constants.SERVER_IP_ADDR;
		}
		return ipAddr;
	}

	public static int stringToInt(String num){
		int myNum = -1;

		try {
		    myNum = Integer.parseInt(num.toString());
		} catch(Exception e) {
			myNum = -1;
		} 
		return myNum;
	}
	
	public static String stringToHTMLString(String text) {
		StringBuffer sb = new StringBuffer(text.length());
		// true if last char was blank
		boolean lastWasBlankChar = false;
		int len = text.length();
		char c;

		for (int i = 0; i < len; i++) {
			c = text.charAt(i);
			if (c == ' ') {
				if (lastWasBlankChar) {
					lastWasBlankChar = false;
					sb.append("&nbsp;");
				} else {
					lastWasBlankChar = true;
					sb.append(' ');
				}
			} else {
				lastWasBlankChar = false;

				// HTML Special Chars
				if (c == '"')
					sb.append("&quot;");
				else if (c == '&')
					sb.append("&amp;");
				else if (c == '<')
					sb.append("&lt;");
				else if (c == '>')
					sb.append("&gt;");
				else if (c == '\n')
					// Handle Newline
					sb.append("&lt;br/&gt;");
				else {
					int ci = 0xffff & c;
					if (ci < 160)
						// nothing special only 7 Bit
						sb.append(c);
					else {
						// Not 7 Bit use the unicode system
						sb.append("&#");
						sb.append(new Integer(ci).toString());
						sb.append(';');
					}
				}
			}
		}
		return sb.toString();
	}

	public static String getBaseUrl(HttpServletRequest request) {
		return (request.getRequestURL()).substring(0, (request.getRequestURL()).length() - (request.getRequestURI()).length() + (request.getContextPath()).length()) + "/";
	}

	public static String getPathUrl(HttpServletRequest request) {
		return request.getServletPath();
	}

	public static int countString(String source, String target) {
		return (source == null || target == null) ? 0 : (source.length() - source.replaceAll(target, "").length());
	}

	public static UserPrincipalBean getPrincipal(HttpSession session) {
		if (session != null) {
			return (UserPrincipalBean) session.getAttribute(Constants.USER_PRINCIPAL);
		}

		return null;
	}

	public static UserPrincipalBean getPrincipal(HttpServletRequest request) {
		return getPrincipal(request.getSession(false));
	}

	public static String getBrowserURL(HttpServletRequest request) {
		return request.getScheme() + "://" + request.getServerName() + ("http".equals(request.getScheme()) && request.getServerPort() == 80 || "https".equals(request.getScheme()) && request.getServerPort() == 443 ? "" : ":" + request.getServerPort()) + request.getRequestURI() + (request.getQueryString() != null ? "?" + request.getQueryString() : "");
	}

	public static String getBrowserContextURL(HttpServletRequest request) {
		return request.getScheme() + "://" + request.getServerName() + ("http".equals(request.getScheme()) && request.getServerPort() == 80 || "https".equals(request.getScheme()) && request.getServerPort() == 443 ? "" : ":" + request.getServerPort()) + request.getContextPath();
	}

	private static String DEFAULT_TIME_FORMAT = "HH:mm";
	private static String DEFAULT_DATE_FORMAT = "dd/MM/yyyy";

	public static SimpleDateFormat getDateFormat(UserPrincipalBean principal) {
		try{
			if (principal != null) {

				String dateFmt = principal.getDateFormat();
				String timeFmt = principal.getTimeFormat();
				if (dateFmt != "" && Functions.getDateFmt().contains(dateFmt)) {
					dateFmt = dateFmt.replace("mm", "MM"); //mm for js, MM for java
					SimpleDateFormat dateformat = new SimpleDateFormat(dateFmt);
					
					if (timeFmt != "" && Functions.getTimeFmt().contains(timeFmt))
						dateformat.setTimeZone(TimeZone.getTimeZone(timeFmt.replace("MM", "mm")));
					
					return dateformat;
				} else {
					return new SimpleDateFormat(DEFAULT_DATE_FORMAT);
				}
			} else {
				return new SimpleDateFormat(DEFAULT_DATE_FORMAT);
			}
		}catch(Exception e){
			Log.error("DateFormat error ----------->" + e);
			return new SimpleDateFormat(DEFAULT_DATE_FORMAT);
		}
	}

	public static SimpleDateFormat getTimeFormat(UserPrincipalBean principal) {
		try{
			if (principal != null) {
				String timeFmt = principal.getTimeFormat();
				if (timeFmt != "" && Functions.getTimeFmt().contains(timeFmt)) {
					SimpleDateFormat timeformat = new SimpleDateFormat(timeFmt.replace("MM", "mm"));
					
					if (principal.getTimezone() != null && principal.getTimezone() != "")
						timeformat.setTimeZone(TimeZone.getTimeZone(principal.getTimezone()));
					
					return timeformat;
				} else {
					return new SimpleDateFormat(DEFAULT_TIME_FORMAT);
				}
			} else {
				return new SimpleDateFormat(DEFAULT_TIME_FORMAT);
			}
		}catch(Exception e){
			Log.error("TimeFormat error ----------->" + e);
			return new SimpleDateFormat(DEFAULT_TIME_FORMAT);
		}
	}

	public static SimpleDateFormat getDateTimeFormat(UserPrincipalBean principal) {
		try{
		if (principal != null) {
			String dateFmt = principal.getDateFormat();
			String timeFmt = principal.getTimeFormat();
			if (timeFmt != "" && Functions.getTimeFmt().contains(timeFmt) && dateFmt != "" && Functions.getDateFmt().contains(dateFmt)) {
				dateFmt = dateFmt.replace("mm", "MM"); //mm for js, MM for java
				timeFmt = timeFmt.replace("MM", "mm"); //MM for js, mm for java
				SimpleDateFormat datetimeformat = new SimpleDateFormat(dateFmt + " " + timeFmt);
				
				if (principal.getTimezone() != null && principal.getTimezone() != "")
					datetimeformat.setTimeZone(TimeZone.getTimeZone(principal.getTimezone()));
				
				return datetimeformat;
			} else {
				return new SimpleDateFormat(DEFAULT_DATE_FORMAT + " " + DEFAULT_TIME_FORMAT);
			}
		} else {
			return new SimpleDateFormat(DEFAULT_DATE_FORMAT + " " + DEFAULT_TIME_FORMAT);
		}
		}catch(Exception e){
			Log.error("DatetimeFormat error ----------->" + e);
			return new SimpleDateFormat(DEFAULT_DATE_FORMAT + " " + DEFAULT_TIME_FORMAT);
		}
	}
    
    public static Calendar convertISO8601(long timestamp) {
           ZonedDateTime utc = Instant.ofEpochMilli(timestamp).atZone(ZoneOffset.UTC);
           return GregorianCalendar.from(utc);
    }
    
    public static Calendar convertISO8601(String dateStr) {
           return javax.xml.bind.DatatypeConverter.parseDateTime(dateStr);
    }
    
    public static void changeTimezone(Calendar cal, String timezone) {
           cal.setTimeZone(TimeZone.getTimeZone(timezone));
    }
    
    
    
    public static Calendar setDayBegin(Calendar cal) {
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal;
	}
    
    public static Calendar setDayEnd(Calendar cal) {
		cal.set(Calendar.HOUR_OF_DAY, 23);
		cal.set(Calendar.MINUTE, 59);
		cal.set(Calendar.SECOND, 59);
		cal.set(Calendar.MILLISECOND, 999);
		return cal;
	}
    public static String cal2StrFull(Calendar cal) {
    	return cal.get(Calendar.YEAR)+"-"+(cal.get(Calendar.MONTH)+1)+"-"+cal.get(Calendar.DATE)
    	        +" "+cal.get(Calendar.HOUR_OF_DAY)+":"+cal.get(Calendar.MINUTE)+":"+cal.get(Calendar.SECOND)+":"+cal.get(Calendar.MILLISECOND)+" "+cal.getTimeZone().getDisplayName();
    }
    
    public static String cal2Str(Calendar cal) {
    	
    	int year = cal.get(Calendar.YEAR);
    	int month = cal.get(Calendar.MONTH)+1;
    	int date = cal.get(Calendar.DATE);
    	int hour = cal.get(Calendar.HOUR_OF_DAY);
    	int min = cal.get(Calendar.MINUTE); 
    	
        return year+"-"+(month<10?"0":"")+month+"-"+(date<10?"0":"") +date+" "+( hour<10?"0":"")+hour+":"+(min<10?"0":"") +min;
    }

	public static String cal2StrDate(Calendar cal) {

		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH)+1;
		int date = cal.get(Calendar.DATE);
		int hour = cal.get(Calendar.HOUR_OF_DAY);
		int min = cal.get(Calendar.MINUTE);

		return year+"-"+(month<10?"0":"")+month+"-"+(date<10?"0":"") +date;
	}

	public static Object jsonObject2Object(JSONObject jo, String str){
		if(jo != null) {
			String[] strAr = str.split("\\.");
			for (int i = 0; i < strAr.length; i++) {
				String key = strAr[i];
				if (jo.has(key)) {
					if (jo.get(key) instanceof JSONObject)
						jo = jo.getJSONObject(key);
					else if (jo.get(key) instanceof Object)
						return jo.get(key);
				} else
					break;
			}
		}
		return null;
	}



	public static String jsonObject2String(JSONObject jo, String str){
		if(jo != null) {
			String[] strAr = str.split("\\.");

			for (int i = 0; i < strAr.length; i++) {
				String key = strAr[i];
				if (jo.has(key)) {
					if (jo.get(key) instanceof JSONObject)
						jo = jo.getJSONObject(key);
					else if (jo.get(key) instanceof String)
						return jo.getString(key);
				} else
					break;
			}
		}
		return "";
	}
	
	public static java.sql.Date convertJavaDateToSqlDate(java.util.Date date) {
		return new java.sql.Date(date.getTime());
	}
	
	public static java.sql.Date sqlDateFromString(String value, String dateFormat, String targetTimeZoneId) throws ParseException {
		SimpleDateFormat format = new SimpleDateFormat(dateFormat);
		format.setTimeZone(TimeZone.getTimeZone(targetTimeZoneId));
		java.util.Date parsed = format.parse(value);
		
		return convertJavaDateToSqlDate(parsed);
	}
	
	public static JsonArray validateJsonArrayFormatForString(String jsonString) {
		JsonArray jsonBody = null;
		
		try {
			Gson gson = new GsonBuilder().setPrettyPrinting().create();
			// Convert JSON Body to Object
			JsonElement jsonElement = gson.fromJson(jsonString, JsonElement.class);
			jsonBody = jsonElement.getAsJsonArray();

		} catch (Exception ex) {
			Log.error(ex);
		}
		
		return jsonBody;
	}
     
}
