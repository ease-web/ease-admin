package com.eab.common;

public class ClassType extends Object{
	public final static int STRING = 1;
	
	public final static int INT = 2;

	public final static int DOUBLE = 3;
	
	public final static int CLOB = 4;
	
	public final static int BLOB = 5;
	
	public final static int DATE = 6;
	
	public final static int STRING_LIST = 7;
}
