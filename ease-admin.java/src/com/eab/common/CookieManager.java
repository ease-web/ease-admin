package com.eab.common;

import java.util.HashMap;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import com.eab.dao.system.CookieBean;

public class CookieManager {
	private HashMap<String, CookieBean> cooks = null;
	
	// Constructor
	public CookieManager() {
		this.cooks = new HashMap<String, CookieBean>();
	}
	
	public CookieManager(Cookie[] cookies) {
		this.cooks = new HashMap<String, CookieBean>();
		Log.debug("*** CookieManager --> Cookie Length ="+cookies.length);
		if (cookies != null && cookies.length > 0) {
			for (int i=0; i<cookies.length; i++) {
				Log.debug("*** CookieManager --> Domain ="+cookies[i].getDomain());
				Log.debug("*** CookieManager --> Path   ="+cookies[i].getPath());
				Log.debug("*** CookieManager --> Name   ="+cookies[i].getName());
				Log.debug("*** CookieManager --> Value  ="+cookies[i].getValue());
				// Only correct domain & path
				CookieBean obj = new CookieBean();
				obj.setName( cookies[i].getName() );
				obj.setValue( cookies[i].getValue() );
				obj.setMaxAge( cookies[i].getMaxAge() );
				obj.setPath( cookies[i].getPath() );
				obj.setComment( cookies[i].getComment() );
				this.cooks.put(cookies[i].getName().toLowerCase(), obj);	// All lowercase for easy to retrieve (when import only)
			}
		}
	}
	
	public CookieBean getData(String name) {
		CookieBean result = null;
		
		try {
			for (HashMap.Entry<String, CookieBean> entry : cooks.entrySet()) {
				Log.debug("*** CookieManager --> getData --> key="+entry.getKey()+"; value="+entry.getValue());
			}
			
			Log.debug("*** CookieManager --> getData --> name="+name);
			if (name != null && name.length() > 0) {
				result = this.cooks.get(name);
			}
			Log.debug("*** CookieManager --> getData --> result="+result);
		} catch (Exception e) {
			Log.error(e);
		}
		
		return result;
	}
	
	public boolean setCookie(String name, String value, String domain, int maxAge, String path) {
		boolean success = false;
		CookieBean cookie = null;
		
		try {
			if (name != null && value != null) {
				Log.debug("setCookie ---> name = "+name);
				Log.debug("setCookie ---> value = "+value);
				Log.debug("setCookie ---> domain = "+domain);
				Log.debug("setCookie ---> maxAge = "+maxAge);
				Log.debug("setCookie ---> path = "+path);
				
				cookie = new CookieBean();
				cookie.setName(name);
				cookie.setValue(value);
				cookie.setDomain(domain);
				cookie.setMaxAge(maxAge);
				cookie.setPath(path);
				
				
				
				this.cooks.remove(name);
				this.cooks.put(name, cookie);
				success = true;
			}
		} catch (Exception e) {
			Log.error(e);
		}
		
		return success;
	}
	
	public Cookie getCookie(String name) {
		Cookie result = null;
		CookieBean obj = null;
		
		try {
			if (name != null && name.length() > 0) {
				obj = this.cooks.get(name);
				if (obj != null) {
					result = new Cookie(obj.getName(), obj.getValue());
					result.setDomain(obj.getDomain());
					result.setMaxAge(-1);
					result.setPath("/");
				}
			}
		} catch (Exception e) {
			Log.error(e);
		}
		
		return result;
	}
	
	public boolean removeCookieAtResponse(String name, String domain, String path, HttpServletResponse response) {
		boolean success = false;
		
		Cookie cookie = new Cookie(name, "");
		cookie.setDomain(domain);
		cookie.setMaxAge(0);
		cookie.setPath(path);
		cookie.setHttpOnly(true);
		cookie.setSecure(true);
		response.addCookie(cookie);
		success = true;
		
		return success;
	}
	
	public int size() {
		return this.cooks.size();
	}
}
