package com.eab.common;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import com.eab.biz.system.SystemParameters;
import com.eab.database.jdbc.DBAccess;
import com.eab.database.jdbc.DBManager;
import com.eab.database.jdbc.DataType;
import com.eab.model.system.SysParamBean;

public class Email {
	private boolean authFlag = false;
	private int typeOfConnection = 0; // 0: none, 1: TLS, 2: SSL

	/***
	 * Get Email template by Template Code and Language Code.
	 * @param templateCode - Template Code (sys_email_template)
	 * @param langCode - Language Code (sys_lang_mst)
	 * @return Template Contents
	 */
	public String getTemplate(String templateCode, String langCode) {
		DBManager dbm = null;
		String output = null;
		
		try {
			dbm = new DBManager();
			String sql = "select template_file from sys_email_template "
						+ " where template_code=" + dbm.param(templateCode, DataType.TEXT)
						+ "   and lang_code=" + dbm.param(langCode, DataType.TEXT)
						+ "   and status='A'"
			;
			output = dbm.selectSingle(sql, "template_file");
		} catch(SQLException e) {
			Log.error(e);
		} catch(Exception e) {
			Log.error(e);
		} finally {
    		dbm = null;
    	}
		
		return output;
	}
	
	/***
	 * Create Email transaction (pending status) into database table sys_email_trx.
	 * @param langCode - Language Code
	 * @param from - From Email Address (comma as separater)
	 * @param to - To Email Address (comma as separater)
	 * @param subject - Email Subject
	 * @param body - Email Body (HTML format)
	 * @param funcCode - Related Function Code
	 * @param templateCode - Template Code
	 * @param userCode - Login User Code
	 * @return Email Transaction No
	 */
	public String createTransaction(String langCode, String from, String to, String subject, String body, String funcCode, String templateCode, String userCode) {
		String trxNo = null;
		DBManager dbm = null;
		
		try {
			trxNo = userCode + "-" + String.valueOf( (new Date()).getTime() );		//Format: {User_Code}-{Timestamp}    e.g. TAIMAN.CHAN-123952120
			
			dbm = new DBManager();
			String sql = "insert into SYS_EMAIL_TRX(trx_no, lang_code, from_addr, to_addr, subject, body, func_code, template_code, create_by, upd_date, upd_by) "
						+ " values("
						+ " " + dbm.param(trxNo, DataType.TEXT)				//trx_no
						+ ", " + dbm.param(langCode, DataType.TEXT)			//lang_code
						+ ", " + dbm.param(from, DataType.TEXT)				//from_addr
						+ ", " + dbm.param(to, DataType.TEXT)				//to_addr
						+ ", " + dbm.param(subject, DataType.TEXT)			//subject
						+ ", " + dbm.param(body, DataType.CLOB)				//body
						+ ", " + dbm.param(funcCode, DataType.TEXT)			//func_code
						+ ", " + dbm.param(templateCode, DataType.TEXT)		//template_code
						+ ", " + dbm.param(userCode, DataType.TEXT)			//create_by
						+ ", sysdate"										//upd_date
						+ ", " + dbm.param(userCode, DataType.TEXT)			//upd_date
						+ " )"
			;
			boolean success = dbm.insert(sql);
			
			// Set Transaction No to Null if insert is failed.
			if (!success)
				trxNo = null;
			
		} catch(Exception e) {
			Log.error(e);
		}
		
		return trxNo;
	}
	
	/***
	 * Process all pending email transaction.
	 * @param userCode - Operator's User Code
	 * @return True or False
	 * @throws SQLException
	 */
	public boolean sendAll(String userCode) throws SQLException {
		return send(null, userCode);
	}
	
	/***
	 * Process pending email transaction.
	 * @param trxNo - Email Transaction No.
	 * @param userCode - Operator's User Code
	 * @return True or False
	 * @throws SQLException
	 */
	public boolean send(String trxNo, String userCode) throws SQLException {
		boolean output = false;
		DBManager dbm = null;
		DBManager dbmExec = null;
		ResultSet rs = null;
		Connection conn = null;
		Connection connExec = null;
		String emailTrxNo = null;
		String langCode = null;
		String fromAddr = null;
		String toAddr = null;
		String subject = null;
		String body = null;
		String templateCode = null;
		String emailStatus = null;		//P = pending, W = working, S = sent, E = error
		
		try {
			dbm = new DBManager();
			dbmExec = new DBManager();
			conn = DBAccess.getConnection();
			conn.setAutoCommit(false);
			
			// Load email transaction and Lock record
			String sql = "select trx_no, lang_code, from_addr, to_addr, subject, body, template_code";
			sql += " from sys_email_trx";
			sql += " where 1=1";
			if (trxNo != null && trxNo.length() > 0) {
				sql += " and trx_no=" + dbm.param(trxNo, DataType.TEXT);
			}
			sql += " and email_status='P'";
			sql += " and status='A'";
			rs = dbm.select(sql, conn);
			if (rs != null) {
				output = true;
				while (rs.next()) {
					emailTrxNo = rs.getString("trx_no");
					langCode = rs.getString("lang_code");
					fromAddr = rs.getString("from_addr");
					toAddr = rs.getString("to_addr");
					subject = rs.getString("subject");
					body = rs.getString("body");
					templateCode = rs.getString("template_code");
					
					// Get DB connection for update email transaction
					connExec = DBAccess.getConnection();
					connExec.setAutoCommit(false);
					
					// Lock email record (prevent duplicated process
					dbmExec.clear();
					String sqlLock = "update sys_email_trx set email_status='W', sent_cnt=sent_cnt+1, upd_date=sysdate, upd_by=" + dbmExec.param(userCode, DataType.TEXT) + " where trx_no=" + dbmExec.param(emailTrxNo, DataType.TEXT) + " and email_status='P' and status='A'";
					
					// Only 1 record should be locked.
					if (dbmExec.update(sqlLock, connExec) == 1) {
						// Commit the status change
						connExec.commit();
						
						// Send mail
						if (sendMail(fromAddr, toAddr, null, SystemParameters.get("SMTP"), Integer.parseInt(SystemParameters.get("SMTP_PORT")), null, null, subject, body)) {
							// Emailed without error
							emailStatus = "S";		//Sent
						} else {
							// Email is failed
							emailStatus = "E";		//Error
						}
						dbmExec.clear();
						String sqlComplete = "update sys_email_trx set email_status=" + dbmExec.param(emailStatus, DataType.TEXT) + ", upd_date=sysdate, upd_by=" + dbmExec.param(userCode, DataType.TEXT) + " where trx_no=" + dbmExec.param(emailTrxNo, DataType.TEXT);
						dbmExec.update(sqlComplete, connExec);
						
						connExec.commit();
					} else {
						output = false;
						connExec.rollback();
					}
				}
			}
			
			conn.commit();
		} catch(Exception e) {
			Log.error(e);
			conn.rollback();
			connExec.rollback();
		} finally {
			rs = null;
			if (conn != null) {
				if (!conn.isClosed())
					conn.close();
				conn = null;
			}
			if (connExec != null) {
				if (!connExec.isClosed())
					connExec.close();
				connExec = null;
			}
		}
		
		return output;
	}
	
	/***
	 * Refer to http://www.blueshop.com.tw/board/FUM20141202112221MW3/BRD20141200000368683.html
	 */
	public boolean sendMail(String sender, String receiver, String ccList , String smtpHost, int port,
							String username, String passwd, String Subject, String messageText) {
		InternetAddress[] address = null;
		boolean sessionDebug = false;
		boolean status = false;
		
		try {
			// encoding
			System.setProperty("mail.mime.charset", "utf8");
		
			// properties
			java.util.Properties props = null;
			switch (typeOfConnection) {
				case 1: // TLS
				props = getPropertyInTls(smtpHost, port);
				break;
			case 2: // SSL
				props = getPropertyInSsl(smtpHost, port);
				break;
			default:
				props = getPropertyInNormalConnection(smtpHost, port);
				break;
			}
		
			// construct a mail session
			javax.mail.Session mailSession = javax.mail.Session.getDefaultInstance(props, null);
			mailSession.setDebug(sessionDebug);
			Message msg = new MimeMessage(mailSession);
			// mail sender
			msg.setFrom(new InternetAddress(sender));
			// mail recievers
			address = InternetAddress.parse(receiver, false);
			msg.setRecipients(Message.RecipientType.TO, address);
			// mail cc
			if (ccList != null && ccList.length() > 0) {
				msg.setRecipients(Message.RecipientType.CC, InternetAddress.parse(ccList));
			}
			// mail's subject
			msg.setSubject(Subject);
			// mail's sending time
			msg.setSentDate(new Date());
			MimeBodyPart mbp = new MimeBodyPart();
			// mail's charset
			mbp.setContent(messageText.toString(), "text/html; charset=utf8");
			Multipart mp = new MimeMultipart();
			mp.addBodyPart(mbp);
			msg.setContent(mp);
		
			if (getAuth()) {
				Transport transport = mailSession.getTransport("smtp");
				transport.connect(smtpHost, username, passwd);
				transport.sendMessage(msg, msg.getAllRecipients());
				transport.close();
			} else {
				Transport.send(msg);
			}
			status = true;
		} catch (Exception e) {
			Log.error("(Email) Exception--> " + e.getMessage());
			status = false;
		}
		
		return status;
	}
	
	private void setAuth(boolean flag){
		this.authFlag = flag;
	}

	private boolean getAuth(){
		return this.authFlag;
	}

	private boolean setTypeOfConnection (String type) {
		type = type.toLowerCase();
		if (type.compareTo("tls") == 0) {
			this.typeOfConnection = 1;
			return true;
		} else if(type.compareTo("ssl") == 0) {
			this.typeOfConnection = 2;
			return true;
		} else if(type.compareTo("none") == 0) {
			this.typeOfConnection = 0;
			return true;
		}
		return false;
	}

	private java.util.Properties getPropertyInNormalConnection (String smtpHost, int port) {
		java.util.Properties props = System.getProperties();

		// set smtp server and protocol
		props.put("mail.host", smtpHost);
		props.put("mail.transport.protocol", "smtp");
		props.put("mail.smtp.port", port);
		// authentication
		if (getAuth()) {
			props.put("mail.smtp.auth", "true");
		}
		return props;
	}

	private java.util.Properties getPropertyInTls (String smtpHost, int port) {
		System.out.println("tls");
		java.util.Properties props = System.getProperties();
		// set smtp server and protocol
		props.put("mail.host", smtpHost);
		props.put("mail.transport.protocol", "smtp");
		// authentication
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable","true");
		props.put("mail.smtp.port", port);
		return props;
	}

	private java.util.Properties getPropertyInSsl (String smtpHost, int port) {
		java.util.Properties props = System.getProperties();

		// set smtp server and protocol
		props.put("mail.host", smtpHost);
		props.put("mail.transport.protocol", "smtp");
		// authentication
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.socketFactory.port", port);
		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", port);
		return props;
	}
}
