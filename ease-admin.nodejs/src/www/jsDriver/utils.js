var debug = {
        isDebug: true,
        debugOnly: false,
        messages: ''
};

var errors = [];

var isCalledFromJava = false;

var returnResult = function(resultObj) {
    var resultJson = '';

    resultObj.errors = errors;

    if (debug.isDebug) {
        if (debug.debugOnly) {
            resultJson = JSON.stringify(debug);
        } else {
          resultObj.debug = Object.assign(debug, resultObj.debug || {});
          resultJson = JSON.stringify(resultObj);
        }
    } else {
        resultJson = JSON.stringify(resultObj);
    }
    if (isCalledFromJava) {
        result.resultJson = resultJson;
    } else {
        window.location = 'result://eabsystems.com/' + resultJson;
        return resultJson;
    }
};

// math = mathjs({
//     number: 'bignumber',
//     decimals: 32
// });

// math['import']({
//     trunc: function(number, dec) {
//         return Math[number < 0 ? 'ceil' : 'floor'](number * Math.pow(10, dec)) / Math.pow(10, dec);
//     }
// });

var addError = function(error) {
  var prepareMsgParam = function(msgParam) {
      var retVal = [];
      if (msgParam) {
        for (var i = 0; i < msgParam.length; i++) {
          retVal.push(msgParam[i] + '');
        }
        return retVal;
      }
  }

  error.msgParam = prepareMsgParam(error.msgParam);
  errors.push(error);
};

var runFunc = function(fnStr, para1, para2, para3, para4) {
    if (typeof fnStr == 'string' && fnStr != 'undefined') {
      try {
          var fn = eval('('+fnStr+')');
          if (typeof fn == 'function') {
              var res = fn(para1, para2, para3, para4);
          } else {
              debug.errFn = fn;
          }
      } catch (ex) {
          if (ex.errCode) {
              throw ex;
          } else {
              debug.ex = ex.message || ex;
              debug.exStr = ex.stack;
              debug.funcStr = fnStr;
          }
      }
      return res;
    } else if (fnStr && typeof fnStr =='function') {
      return fnStr(para1, para2, para3, para4);
    }
    return null;
};

var getCurrency = function(value, sign, decimals) {
    if (!isNumber(value)) {
        return value;
    }
    if (!isNumber(decimals)) {
        decimals = 2
    }
    var text = (sign && sign.trim().length > 0 ? sign : '') + parseFloat(value).toFixed(decimals);
    var parts = text.split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts.join(".");
};

var getCurAge = function(dob, planDetail) {
  if (planDetail.calcAgeBy == 'next') {

  } else if (planDetail.calcAgeBy == 'nearest') {

  } else {
    var date = dob.split('/');
    var year=Number(date[0]);
    var month=Number(date[1]);
    var day=Number(date[2]);
    var today=new Date();
    var age=today.getFullYear()-year;
    if(today.getMonth()<month || (today.getMonth()==month && today.getDate()<day)){age--;}

    return age;
  }
}

var isNumber = function(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
};
