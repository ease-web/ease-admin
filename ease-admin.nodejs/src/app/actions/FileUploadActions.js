var ConfigConstants = require('../constants/ConfigConstants');
var AppDispatcher = require('../dispatcher/AppDispatcher');
var ActionTypes = ConfigConstants.ActionTypes;

var ContentStore = require('../stores/ContentStore');
let HomeStore= require('../stores/HomeStore');

var FileUploadActions = {
  getAttachment: function(docCode, version, attId, callback) {

    var page = HomeStore.getCurrentPage();

    var param = {
      p0: convertObj2EncodedJsonStr({
        module: page.module,
        docCode: docCode,
        version: version,
        attId: attId
      })
    };
    getFile("/DynTemplate/getAttachment", param, function(data) {
      callback(data && data.attachment);
    });
  },

  saveFile: function(docCode, version, attId, file) {
    var page = HomeStore.getCurrentPage();
    var data = {
        values: file,
        module: page.module,
        docCode: docCode,
        version: version,
        attId: attId
    };
    saveDynDataOnServer("/DynTemplate/Detail/Save", data);
  },

  removeFile: function(docCode, version, attId, cb) {
    var page = HomeStore.getCurrentPage();
    var para = {
        docCode: docCode,
        version: version,
        attId: attId
    };
    var action = "/DynTemplate/RemoveAttachment";

    callServer(action, {
      p0: convertObj2EncodedJsonStr(para),
      p1: page.module
    }, cb);
  }
}
module.exports = FileUploadActions;
