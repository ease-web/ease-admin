var AppDispatcher = require('../dispatcher/AppDispatcher');
let SessionAlertStore = require('../stores/SessionAlertStore.js');
let ConfigConstants = require('../constants/ConfigConstants');
let ActionTypes = ConfigConstants.ActionTypes;

var SessionAlertActions = {
    extend() {
        var self = this;
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (xhttp.readyState == 4 && xhttp.status == 200) {
                self.reset();
                console.debug('[extend] session keeps alive.');
            }
        };
        xhttp.open("GET", ROOT_CONTEXT_PATH + "/Keepalive", true);
        xhttp.send();
    },
    
    reset() {
        var self = this;
        SessionAlertStore.reset();
        var objTimer = SessionAlertStore.getTimer();
        if (objTimer)
            clearInterval(objTimer);
        objTimer = setInterval(function(){var action=require('../actions/SessionAlertActions.js'); action.timer();}, ConfigConstants.SessionCheck_Millsec);
        SessionAlertStore.setTimer(objTimer);
        if (SessionAlertStore.getOpen() == true)
            self.close();
    },
    
    timer() {
        var self = this;
        var count = SessionAlertStore.reduce(1);    // decrease 1 minute
        var before = SessionAlertStore.getBefore();
        console.debug('[timer], session remains ', count, 'minutes.');
        
        if (count <= 0 && SessionAlertStore.getOpen() == true) {
            var objTimer = SessionAlertStore.getTimer();
            if (objTimer)
                clearInterval(objTimer);
            self.close();
        } else if (before >= count) {
            self.open();
        } 
    },
    
    open() {
        AppDispatcher.dispatch({
            actionType: ActionTypes.UPDATE_SNACKBAR,
            actionValue: true
        });
    },
    
    close() {
        AppDispatcher.dispatch({
            actionType: ActionTypes.UPDATE_SNACKBAR,
            actionValue: false
        });
    },
    
}

module.exports = SessionAlertActions;
