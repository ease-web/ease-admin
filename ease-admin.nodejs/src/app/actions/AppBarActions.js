let MasterTableActions = require('../actions/MasterTableActions.js');
let HomeStore = require('../stores/HomeStore.js');
let AppBarStore = require('../stores/AppBarStore.js');
let NeedDetailStore = require('../stores/NeedDetailStore.js');
let ContentStore = require('../stores/ContentStore.js');
let AppDispatcher = require('../dispatcher/AppDispatcher.js');
let ConfigConstants = require('../constants/ConfigConstants.js');
let MasterTableStore = require('../stores/MasterTableStore.js');
let ActionTypes = ConfigConstants.ActionTypes;
var DialogActions = require('../actions/DialogActions');
var ProductActions = require('../actions/ProductActions');


module.exports = {
  updateCriteria(value, delay) {
    var values = AppBarStore.getValues();
    values["criteria"] = value;
    AppDispatcher.dispatch({
      actionType: ActionTypes.AppBar.UPDATE_VALUES,
      values: values
    });

    if (!value || value.length >= ConfigConstants.SearchMinLenght || value.length == 0) {
      MasterTableActions.refreshMasterTable(0, {
        filter: values
      }, delay);
    }

  },

  showSearchBar(flag) {
    // console.trace('ober seachr', flag);
    AppDispatcher.dispatch({
      actionType: ActionTypes.AppBar.SHOW_SEARCH_BAR,
      value: flag
    });
  },

  submitSearchField(id, value) {
    var filters = AppBarStore.getValues();
    var sortInfo = MasterTableStore.getSortInfo();
    var start = 0;
    var page = HomeStore.getCurrentPage();
    var para = cloneObject(filters);
    // refine criteria
    var critera = value;
    para['criteria'] = critera && critera.length >= ConfigConstants.SearchMinLenght ? critera : "";

    // set the sort info
    for (let k in sortInfo) {
      para[k] = sortInfo[k];
    }

    // set record start and page size
    para['recordStart'] = start;
    para['pageSize'] = MasterTableStore.getPageSize();
    para['rows'] = ContentStore.getPageContent().values.id;
    HomeStore.updateBackPageValue(para);
    var data = {
      p0: convertObj2EncodedJsonStr(para),
      p1: page.module,
      p2: page
    }
    
    callServerWithWaiting(ConfigConstants.SearchLatency, id, data);
  },

  updateFilter(id, value) {
    var values = AppBarStore.getValues();
    values[id] = value;

    AppDispatcher.dispatch({
      actionType: ActionTypes.AppBar.UPDATE_VALUES,
      values: values
    })
    MasterTableActions.refreshMasterTable(0, {
      filter: values
    });
  },

  reloadPageFromServer() {
    MasterTableActions.refreshMasterTable(0);
  },

  changeVersion(action, version) {

    var orgVersion = ContentStore.getPageContent().values.version;
    if(checkChanges(orgVersion, version)){
      var id = ContentStore.getPageContent().values.id;

      var channelCode = ContentStore.getPageContent().values.channelCode;

      var page = HomeStore.getCurrentPage();
      HomeStore.updateBackPageValue(para);
      var para = {
        id:id,
        version:version,
        channelCode:channelCode
      };

      callServer(action, {
        p0: convertObj2EncodedJsonStr(para),
        p1: page.id
      });
    }

  },

  submit(action) {
    var page = HomeStore.getCurrentPage();
    var para = {
      p0: page.module,
      p1: page.id,
      p10: convertObj2EncodedJsonStr(page)
    }
    callServer(action, para)
  },

  // submit action with selected row
  submitSelected(action) {
    var page = HomeStore.getCurrentPage();
    var rows = MasterTableStore.getSelectedRows();

    if (action) {
      var para = {
        p0: convertObj2EncodedJsonStr({rows:rows}),
        p1: page.module,
        p2: page.id,
        p10: convertObj2EncodedJsonStr(page)
      }
      console.debug('handle submit =', para);
      callServer(action, para)
    }

    AppDispatcher.dispatch({
      actionType: ActionTypes.MasterTable.SELECTION_CHANGE,
      rows: []
    });
  },

  submitCurrentIdAfterConfirm(action) {

  },

  // submit action and record id to server
  submitCurrentId(action) {
    var page = HomeStore.getCurrentPage();
    var content =  ContentStore.getPageContent();
    var recordId = null;
    if (content.values) {
      recordId = content.values.id;
    } else if (content.changedValues){
      recordId = content.changedValues.id;
    }

    if (action) {
      var para = {
        p0: recordId,
        p1: page.module,
        p2: page.id,
        p10: convertObj2EncodedJsonStr(page)
      }
      console.debug("submitCurrentId", para);
      callServer(action, para)
    }
  },
  submitCurrentTaskId(action) {
    var page = HomeStore.getCurrentPage();
    var rows = {rows:ContentStore.getPageContent().values.taskId};
    if (rows && action) {
      var para = {
        p0: convertObj2EncodedJsonStr(rows),
        p1: page.module,
        p2: page.id,
        p10: convertObj2EncodedJsonStr(page)
      }
      callServer(action, para)
    }

  },

  back(target, appBarLevel){
    var content = ContentStore.getPageContent();
    var _back = function(){
      var title = AppBarStore.getTitle();
      var target = (title && title.id)?title.id :undefined;
      if(appBarLevel == 2) {
        
        AppDispatcher.dispatch({
          actionType: ActionTypes.BACK_PAGE,
          target: target
        });
      }else if(appBarLevel == 3) {
        AppDispatcher.dispatch({
          actionType: "BACK_TWO_PAGE",
          target: target
        });
      }
      
    };
    if(content){
      var {
        valuesBackup,
        changedValues
      } = content;

      if (valuesBackup && changedValues && checkChanges(valuesBackup, changedValues)) {
        DialogActions.showDialog({
          title: getLocalizedText('DIALOG.TITLE.WARNING', 'Warning'),
          message: getLocalizedText('DIALOG.MSG.UNSAVE_CHANGES', 'Changes are not yet saved, are you sure to leave and abort the changes?'),
          width: 50,
          actions: [{
            type: 'cancel',
            title: getLocalizedText('BUTTON.CANCEL', 'Cancel'),
            secondary: true,
          }, {
            type: 'customButton',
            title: getLocalizedText('BUTTON.CONFIRM', 'Confirm'),
            primary: true,
            handleTap: function( values ) {
              _back();
            }
          }]
        });
      }else{
        _back();
      }
    }else{
      _back();
    }
  },

  findSection(item, id) {
    if (!item.type || item.type.toLowerCase() == 'section') {
      if (item.id == id) {
        return item;
      }
    } else if (item.items) {
      for(var i in item.items) {
        var section = this.findSection(item.items[i], id);
        if (section) {
          return section;
        }
      }
    }
    return null;
  },

  submitCurrentIdWithConfirm(action){
    var self = this;
    var actionLower = action.toLowerCase();
    var msg = '';
    var msgTxt = '';
    var title = '';
    var actionTypeA = 'approve';
    var actionTypeD = 'delete';
    var actionTypeR = 'reject';
    var actionTypeRv = 'revert';


    if(actionLower.indexOf(actionTypeA) > -1){
      title ='DIALOG.MSG.APPROVE.TITLE';
      msg = 'DIALOG.MSG.APPROVE';
      msgTxt = 'Are you sure to approve?';
    } else if(actionLower.indexOf(actionTypeD) > -1){
      title ='DIALOG.MSG.DELETE.TITLE';
      msg = 'DIALOG.MSG.DELETE';
      msgTxt = 'Are you sure to delete this record?';
    }else if(actionLower.indexOf(actionTypeR) > -1){
      title ='DIALOG.MSG.REJECT.TITLE';
      msg = 'DIALOG.MSG.REJECT';
      msgTxt = 'Are you sure to reject?';
    }else if(actionLower.indexOf(actionTypeRv) > -1){
      title ='DIALOG.MSG.REVERT.TITLE';
      msg = 'DIALOG.MSG.REVERT';
      msgTxt = 'Are you sure to revert?';
    }else{
      msg = 'DIALOG.MSG.UNKNOWN';
      msgTxt = 'Unknown action type, Are you sure to revert?';
    }

    msg = getLocalizedText(msg, msgTxt);
    title = getLocalizedText(title, 'Alert');
    var btNev = getLocalizedText('BUTTON.CANCEL', 'Cancel');
    var btPos = getLocalizedText('BUTTON.CONFIRM', 'Confirm');
    //var actionFunction = self.submitCurrentId(action);
    alert(msg, title, btNev, btPos, function(values) {
      console.debug("AppBarActions submitCurrentId");
      self.submitCurrentId(action);
    });
  },

  submitProdBNC(){
    var path = '/Products/Detail/saveProdBNC';
    var module = 'product';
    var values = ContentStore.getPageContent().changedValues;
    var nbcValues = values.ProdBrochureNCover;
    submitAttachment(path, nbcValues, module, values.id, values.version);
  },
  // submit action and changed record to server
  submitChangedValues(action, fromMenu) {
    var page = HomeStore.getCurrentPage();
    var values = cloneObject(ContentStore.getPageContent().changedValues);
    var orgValues = ContentStore.getPageContent().values;
    var template = ContentStore.getPageContent().template;
    var errorMsg = false;
    var changed = false;
    var changedAttachment = false;
    var changedValues = null;
    //save action for BNC
    var isBNC = ContentStore.isBNC
    if(action == '/Needs/Detail/Save'){
      //check record is locked by other users
      //var isLock = NeedDetailStore.isLock();

      var sectionId = (checkExist(values,'selectPage.id'))?values.selectPage.id:"";
      var targetPageId = values.selectPageId?values.selectPageId:values.selectPage.id;

      //get the current section
      let _template = null;
      for(var i in template.items){
        if(template.items[i].id == sectionId){
          _template = template.items[i];
        }
      }

      //template validation
      if (!isEmpty(_template)) {
        if (sectionId=='TaskInfo'||sectionId=='NeedsDetail') {
          errorMsg = validate(_template, values[sectionId]);
        } else {
          errorMsg = _template.validate(values[sectionId]);
        }
      }
      let isCSPageChange=(sectionId=="NeedsConceptSelling"||values.selectPageId=="NeedsConceptSelling") ? true:false;

      //check changes of values and attachments
      changed = checkChanges(orgValues[sectionId], values[sectionId]);
      if(isEmpty(orgValues["Attachments"]) && !isEmpty(values[sectionId])){
        changedAttachment = values["Attachments"];
      }else if(!isEmpty(orgValues["Attachments"]) && !isEmpty(values["Attachments"])){
        changedAttachment = foundDiff(orgValues["Attachments"], values["Attachments"]);
      }

      if((!changed && (!changedAttachment || isEmpty(changedAttachment)))){
        if((!isEmpty(targetPageId) && sectionId!=targetPageId) ||isCSPageChange){
            values['skip'] = true;
        }else if(!errorMsg){
          errorMsg = "No Changes";
        }
      }
      if (action && values && (values['skip'] || (!errorMsg && (changed || changedAttachment)))) {
        if(sectionId=="NeedsPrior"){
            delete values["Attachments"];
        }

        if(values["Attachments"]){
          if(!changedAttachment){
              delete values["Attachments"];
          }else{
            values["Attachments"] = changedAttachment;
          }

        }

        var path = '/Needs/Detail/SaveAttachment';//TODO
        var module = 'needsTab';//TODO
        submitAttachment(path, values, module, "NeedTab", 1);


      } else {
        if (errorMsg && !values['panelSave']) {
          alert(errorMsg, 'Error', 'OK');
        }
        values['selectPageId'] = undefined;
        values['panelSave'] = undefined;
      }

    } else if (action == '/Env/TestConn'){
      var para = {
        p0: convertObj2EncodedJsonStr(values),
        p1: page.module,
        p2: page.id,
      }
      callServer(action, para)
      return;
    } else if (action == '/DynTemplate/Detail/Save') {
      // Dynamic template details save
      var submitValues = null;

      // find section, validate and check changes
      var sectionTemplate = this.findSection(template, values.selectPage.id);
      if (sectionTemplate) {
        errorMsg = validate(sectionTemplate, values[sectionTemplate.id]);
        if (!errorMsg) {
          changed = checkChanges(orgValues[sectionTemplate.id], values[sectionTemplate.id]);
          if (changed) {
            submitValues = {};
            submitValues[sectionTemplate.id] = values[sectionTemplate.id];
          }
        }
      } else {
        errorMsg = "section not found!";
      }

      if (!errorMsg) {
        var taskId = !!orgValues ? orgValues.taskId : null;
        var curSection = !!orgValues && !!orgValues.selectPage 
        var data = {
          values: submitValues,
          module: page.module,
          page: page.id,
          docCode: orgValues.id,
          taskId: orgValues.taskId,
          version: orgValues.version,
          curSection: orgValues.selectPage.id,
          nextSection: values.selectPageId
        };
        saveDynDataOnServer(action, data);
      }
      if (errorMsg) {
        alert(errorMsg, 'Error', 'OK');
      }
      // }
    }else{
      if(checkChanges(orgValues, values)){
        var para = {
          p0: convertObj2EncodedJsonStr(values),
          p1: page.module
        }
        callServer(action, para)
        return;
      } 
    }
  },
}
