var ConfigConstants = require('../constants/ConfigConstants');
var AppDispatcher = require('../dispatcher/AppDispatcher');
var ActionTypes = ConfigConstants.ActionTypes;

var AppStore = require('../stores/AppStore');
var HomeStore = require('../stores/HomeStore');
var ContentStore = require('../stores/ContentStore');
var DialogActions = require('../actions/DialogActions')

var MenuActions = {
  updateMenuState: function(mstate) {
    AppDispatcher.dispatch({
      actionType: ActionTypes.UPDATE_MENU_STATE,
      menuState: mstate
    })
  },

  changePage(page, changeCallback) {
    var change = function() {
      if (typeof changeCallback == 'function') {
        changeCallback();
      }

      if(page.id == "/Logout"){
        window.loggedIn = false;
        window.location = "./Logout";
      } else {
        AppDispatcher.dispatch({
          actionType: ActionTypes.CHANGE_PAGE,
          page: page
        });
      }
    }

    var content = ContentStore.getPageContent();

    if (content) {
      var {
        template,
        valuesBackup,
        changedValues
      } = content;
      
      if (valuesBackup && changedValues && checkChanges(valuesBackup, changedValues)) {
        DialogActions.showDialog({
          title: getLocalizedText('DIALOG.TITLE.WARNING', 'Warning'),
          message: getLocalizedText('DIALOG.MSG.UNSAVE_CHANGES', 'Changes are not yet saved, are you sure to leave and abort the changes?'),
          width: 50,
          actions: [{
            type: 'cancel',
            title: getLocalizedText('BUTTON.CANCEL', 'Cancel'),
            secondary: true,
          }, {
            type: 'customButton',
            title: getLocalizedText('BUTTON.CONFIRM', 'Confirm'),
            primary: true,
            handleTap: function( values ) {
              change();
            }
          }]
        })
      } else {
        change();
      }
    } else {
      change();
    }
  }
}
module.exports = MenuActions;
