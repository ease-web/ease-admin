
const AWSUploadActions = {
	upload: function(data) {
		// callServerToUploadFormData("/AWSUpload", data);
		uploadFile("/AWSUpload", data);
	},
	getStatus: function() {
		let para = {
			p0: convertObj2EncodedJsonStr(Object.assign({}))
		};
		callServer("/AWSStatus", para);
	},
	changeVersion: function(awsVersionId) {
		let para = {
			p0: convertObj2EncodedJsonStr(Object.assign({}, {version: awsVersionId}))
		};
		callServer("/AWSChangeVersion", para);
	}
};

module.exports = AWSUploadActions;
