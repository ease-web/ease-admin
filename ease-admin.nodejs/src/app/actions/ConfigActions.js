var ConfigConstants = require('../constants/ConfigConstants');
var ActionTypes = ConfigConstants.ActionTypes;
var AppDispatcher = require('../dispatcher/AppDispatcher');
var TemData = require('../actions/TemData');
var AppStore = require('../stores/AppStore');
var ContentStore = require('../stores/ContentStore');
var ConfigActions = {
	changePageContent(content){
		AppDispatcher.dispatch({
			actionType: ActionTypes.CHANGE_CONTENT,
			content: content
		});
	},

	init: function() {
		setTimeout(function() {
			callServer("/InitApp", "");
		}, 500);
	},

	login: function(loginID, password, lang) {
		var result = true;
		password = encryptPassword(password);
		callServer("/Login/Auth",
			"data_001="+loginID+"&lang="+lang+"&data_004="+ new Date().getTimezoneOffset() /60
		// +"&data_003="+captcha
		);
	},

	reloadLogin: function(lang){
		console.debug("reloadLogin", lang);
		callServer("/Login","lang="+lang);
	},

	termsAccept: function() {
		callServer("/Login/Terms/Accept");
	},

	termsDecline: function() {
		callServer("/Login/Terms/Decline");
	},
	getAbout: function(){
		callServer("/About");
	},
	getSettings: function(){
		callServer("/Settings");
	},

	changeSettings: function(action) {
		var changedValues =  ContentStore.getPageContent().changedValues;
		if (action) {
			var para = {
				p0: convertObj2EncodedJsonStr(changedValues),
			}
			callServer(action, para);
		}
	}
};

module.exports = ConfigActions;
