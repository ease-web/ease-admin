var ConfigConstants = require('../constants/ConfigConstants');
var AppDispatcher = require('../dispatcher/AppDispatcher');
var HomeStore = require('../stores/HomeStore.js');
var ActionTypes = ConfigConstants.ActionTypes;

var NeedActions = {
  getNeedList() {
     var data = {
 			action:'getNeedList'
 		 };
     var page = HomeStore.getCurrentPage();
     var param = {
       p0:convertObj2EncodedJsonStr(data),
       p10: convertObj2EncodedJsonStr(page)
     };

 	   callServer("/Needs", param);
  },

  getNeedDetail(needCode, channelCode){
    var data = {
     needCode : needCode,
     channelCode : channelCode
    };
    var page = HomeStore.getCurrentPage();
    var param = {
      p0:convertObj2EncodedJsonStr(data),
      p10: convertObj2EncodedJsonStr(page)
    };

    callServer("/Needs/Detail", param);
  },

  updateSelectedRow(rows) {
    var _rows = {
      rows: rows
    };

    AppDispatcher.dispatch({
      actionType: ActionTypes.NEED_APPBAR_CHANGE,
      rows: _rows
    })
  },

  reflesh(){
    AppDispatcher.dispatch({
      actionType: ActionTypes.REFLESH
    });
  }
}

module.exports = NeedActions;
