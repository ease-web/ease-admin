var ConfigConstants = require('../constants/ConfigConstants');
var AppDispatcher = require('../dispatcher/AppDispatcher');
var HomeStore = require('../stores/HomeStore.js');
var ActionTypes = ConfigConstants.ActionTypes;

var BeneIllusActions = {
  getBIList() {
     var data = {
 			action:'getBIList'
 		 };
     var page = HomeStore.getCurrentPage();
     var param = {
       p0:convertObj2EncodedJsonStr(data),
       p10: convertObj2EncodedJsonStr(page)
     };

 	   callServer("/BeneIllus", param);
  },

  goBiDetail(bi, backPage) {
      var data = {
        id: bi.biCode,
        version: bi.version
      };
      callServer("/BeneIllus/Detail", {
        p0: convertObj2EncodedJsonStr(data),
      });
    },

  updateSelectedRow(rows) {

    console.log(rows);
    var _rows = {
      rows: rows
    };

    AppDispatcher.dispatch({
      actionType: ActionTypes.NEED_APPBAR_CHANGE,
      rows: _rows
    })
  },

  reflesh(){
    AppDispatcher.dispatch({
      actionType: ActionTypes.REFLESH
    });
  }
}

module.exports = BeneIllusActions;
