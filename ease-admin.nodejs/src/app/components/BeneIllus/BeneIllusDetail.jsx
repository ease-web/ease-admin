let React = require('react');
let mui = require('material-ui');
let StylePropable = mui.Mixins.StylePropable;
let BeneIllusDetailStore = require('../../stores/BeneIllusDetailStore.js');
let ContentStore = require('../../stores/ContentStore.js');
let DynMenu = require('../DynamicUI/DynMenu.jsx');


let BeneIllusDetail = React.createClass({
  mixins: [React.LinkedStateMixin, StylePropable],

  componentDidMount() {
    // console.debug('homepage did mount');
    ContentStore.addChangeListener(this.onContentChange );
    BeneIllusDetailStore.addChangeListener(this.onPageChange);

  },

  componentWillUnmount: function() {
    ContentStore.removeChangeListener(this.onContentChange);
    BeneIllusDetailStore.removeChangeListener(this.onPageChange);
  },

  shouldComponentUpdate: function(nextProps, nextState) {
    if(checkChanges(this.state.currentPage, nextState.currentPage)){
        return true;
    }
    if(checkChanges(this.state.menuList, nextState.menuList)){
      return true;
    }
    return false;
  },
  onContentChange(){
    var content = ContentStore.getPageContent();
    var {
      template,
      values
    } = content;

    template.checkChanges = this._checkChanges;

    this.setState({
      menuList: template.menu
    });
  },


  onPageChange(){
    let pageId = BeneIllusDetailStore.getCurrentPage();
    let uiPage = BeneIllusDetailStore.getUiPage();
    let currentPage = {id:pageId, uid: uiPage};
    this.setState({currentPage: currentPage});
  },

  getInitialState() {
    var content = ContentStore.getPageContent();
    var {
      template,
      values
    } = content;

    let currentPage = values['selectPage'];
    template.checkChanges = this._checkChanges;
    return {
      menuList: template.menu,
      currentPage: currentPage
    };
  },

  _checkChanges(orgValues, values) {
    var _sid = BeneIllusDetailStore.getCurrentPage();
    let _orgValues = ContentStore.getPageContent().values;
    var isChange = checkChanges(_orgValues[_sid], values[_sid])?true:false;

    var changedValues = undefined
    if(_orgValues.isNew){
      changedValues = {
        TaskInfo: values.TaskInfo,
        BiDetail: values.BiDetail
      }
    }else{
      changedValues = {};
      changedValues[_sid] = values[_sid];
    }

    if(changedValues != undefined){

      changedValues['id'] = _orgValues.id;
      changedValues['version'] = _orgValues.version;
      changedValues['selectPage'] = values['selectPage'];
      changedValues['selectPageId'] = values['selectPageId'];
      if(_orgValues.taskId && _orgValues.taskId instanceof Array)
        changedValues['taskId'] = _orgValues.taskId[0];
      if(_orgValues.isNew){
        if(values['selectPage'].id == 'TaskInfo'){
          changedValues['ProdDetail'] = _orgValues['ProdDetail'];
        }else{
          changedValues['TaskInfo'] = _orgValues['TaskInfo'];
        } 
      }
    }

    return {
      change: isChange,
      changedValues: changedValues
    };
  },

  getStyle(){
    return ({
      containerStyle:{
        position: 'relative',
        width: 'calc(100% - 280px)',
        height: 'calc(100vh - 58px)',
        display: 'inline-block',
        overflow: 'hidden'
      },
      pageStyle:{
        width: '100%',
        height: 'calc(100vh - 58px)',
        overflowY: 'auto'
      }
    });
  },

  render: function() {
    var self = this;
    let styles = this.getStyle();

    let {
      menuList,
      currentPage
    } = this.state;
    let uiPage = null;

    if (currentPage.uid){
      uiPage = {id: currentPage.uid};
    }
    let page = getPageDom(uiPage?uiPage:currentPage.id=='TaskInfo'?{id:'BiTaskInfo'}:currentPage);

    return (
      <div className="PageContent">
        <DynMenu
          key={"biDynMenu"}
          ref={"biDynMenu"}
          show={true}
          module={"/BeneIllus/Detail"}
          items={menuList}
          style={{width: "280px"}}/>
        <div style={styles.containerStyle}>
            <div style={styles.pageStyle}>
              {page}
            </div>
        </div>
      </div>
    );
  }
});

module.exports = BeneIllusDetail;
