/** In this file, we create a React component which incorporates components provided by material-ui */

let React = require('react');
let mui = require('material-ui');
let StylePropable = mui.Mixins.StylePropable;
let BeneIllusDetailStore = require('../../stores/BeneIllusDetailStore.js');
let ContentStore = require('../../stores/ContentStore.js');
let CurrentColumn = require('../DynamicUI/DynEditableColumn.jsx');

let FlatButton = mui.FlatButton;
let sectionId = 'BiDetail';

let BiDetail = React.createClass({
  mixins: [React.LinkedStateMixin, StylePropable],

  componentDidMount() {
    // console.debug('homepage did mount');
    ContentStore.addChangeListener(this.onContentChange);
  },

  componentWillUnmount: function() {
    ContentStore.removeChangeListener(this.onContentChange);
  },

  shouldComponentUpdate: function(nextProps, nextState) {
    return true;
  },

  _validate(values){
    var content = ContentStore.getPageContent();
    var templateItems = content.template.items;
    var isChanged = false;

    if(values && content.values){
      if(isEmpty(values[sectionId]) && isEmpty(content.values[sectionId])){
        return {
          error: true,
          errorMsg: 'Please enter all madatory data'
        }
      }else if(!isEmpty(values[sectionId]) && isEmpty(content.values[sectionId])){
        isChanged = true;
      }else if(values[sectionId] && content.values[sectionId]){
        isChanged = checkChanges(values[sectionId], content.values[sectionId]);
      }
    }
    var self = this;

    if (!isChanged) {
      return {
        error: false
      };
    }

    for(var i in templateItems){
      var item = templateItems[i];
      if(item.id == sectionId){
        var errorMsg = validate(item, values);
        if(!errorMsg){
          return ({error: false});
        }else{
          return({
            error: true,
            errorMsg: errorMsg
          })
        }
      }
    }
    return ({
      error: true,
      errorMsg: 'unknown error'
    })
  },

  onPageChange(){
    let section = this.getSection(this.state.template);
    this.setState({section: section});
  },


  onContentChange() {
    var content = ContentStore.getPageContent();

    var {
      template,
      values,
      changedValues
    } = content;

    if(!changedValues){
      changedValues = content.changedValues = cloneObject(values);
    }
    template.validate = this._validate;
    this.setState({
      template:template,
      values: values,
      changedValues: changedValues,
      section: this.getSection(template)
    })
  },

  getInitialState() {
    var {
      template,
      values
    } = ContentStore.getPageContent();

    let section = this.getSection(template);

    //initial values
    if(!checkExist(values, sectionId)){
      values[sectionId]={};
    }
    var currPage = BeneIllusDetailStore.getCurrentPage();
    var changedValues = ContentStore.getPageContent().changedValues = cloneObject(values);
    template.validate = this._validate;

    ContentStore.getPageContent().valuesBackup = values;

    return {
      template: template,
      values: values,
      changedValues: changedValues,
      section: section
    };
  },

  getSection(template){
    var templateItems = template.items;
    for (let i in templateItems) {
      var section = templateItems[i];
      if (section.id == sectionId) {
        return section;
      }
    }
    return template.items[1];
  },

  render: function() {
    var self = this;
    var section = this.state.section;

    let dataContainerStyle = {
      width:'100%',
      height:"calc(100vh - 58px)",
      overflowY:"auto",
    };

    return (
      <CurrentColumn
        style={dataContainerStyle}
        show = {true}
        template = {section}
        values = { this.state.values[sectionId] }
        changedValues = { this.state.changedValues[sectionId] }
        id = "current"
        ref = "current" />
    );
  }
});

module.exports = BiDetail;
