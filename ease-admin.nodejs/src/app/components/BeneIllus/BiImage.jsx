let React = require('react');
let mui = require('material-ui');
let StylePropable = mui.Mixins.StylePropable;
let BeneIllusDetailStore = require('../../stores/BeneIllusDetailStore.js');
let ContentStore = require('../../stores/ContentStore.js');

let Dropzone = require('../CustomView/DropZone.jsx');
let TextField = mui.TextField;
let IconButton = mui.IconButton;
let Paper = mui.Paper;
let FloatingActionButton  = mui.FloatingActionButton;
import Add from 'material-ui/lib/svg-icons/content/add';
let sectionId = 'BiImage';

let ImageBox = React.createClass({

  componentWillUnmount(){
  },

  componentDidMount(){
  },

  getImage(id){
    var {
      changedValues
    } = this.props;
    let attachments = changedValues.Attachments;
    return attachments[id];
  },

  setImg(newFile) {
    var {
      changedValues,
      index
    } = this.props;
    var items = changedValues[sectionId].items;
    let imageId = items[index].image;
    let attachments = changedValues.Attachments;
    attachments[imageId] = newFile.url;
    this.forceUpdate();
  },

  removeAttachment(){
    var {
      changedValues,
      index,
      onListUpdate
    } = this.props;
    var items = changedValues[sectionId].items;
    let imageId = items[index].image;
    let attachments = changedValues.Attachments;

    delete attachments[imageId];
    items.splice(index, 1);
    onListUpdate();
  },

  validate(){
    var {
      changedValues,
      index
    } = this.props;
    var items = changedValues[sectionId].items;
    if(items[index].id == ''){
      return "This field is required.";
    }

    for(var i in items){
      if(Number(i)!=Number(index)){
        if(items[index].id == items[i].id){
          return "Image Id should be unique";
        }
      }
    }
    return null;
  },

  render: function(){
    let {
      id,
      isLock,
      values,
      changedValues,
      index
    } = this.props;

    var self = this;

    var errorMsg = this.validate();
    var items = changedValues[sectionId].items;

    //let image = this.getImage(changedValues)
    let image = this.getImage(items[index].image);

    let imgObj = (
      <div style={{display: 'table-cell', verticalAlign: 'middle', width: '230px'}}>
        <img style={{ width: 'auto', height: 'auto', maxWidth: '224px', maxHeight: '224px'}} src={image}/>
      </div>
    );


    let dropZ = (
      <Dropzone
        style={{width: '100%', margin: 'auto', display: 'table-cell', verticalAlign: 'middle', textAlign: 'center'}}
        onDrop={function(res) {
          var invalid = [];

          //File type checking
          var actualFileType = res.file.type;
          if (actualFileType == 'image/png' || actualFileType == 'image/jpeg') {
            var image = new Image();
            image.src = res.imageUrl;
            var width = image.width;
            var height = image.height;

            if ( res.file.size >2*1048576) {
               invalid.push("Your file size exceed " + 2 + " MB.");
            }

          }else{
              invalid.push("Your file is not " + requireFiletype.substring(requireFiletype.indexOf('/')+1, requireFiletype.length).toUpperCase()+ " type.");
          }


          if (invalid.length == 0) {
            this.updateImg({
              url: res.imageUrl,
            });
          } else {
            var errorMsg = "";
            for (var i = 0; i < invalid.length; i++) {
              errorMsg += invalid[i] + "\n";
            }
            alert(errorMsg, "Import failed");
          }
        }.bind({updateImg: this.setImg})}
        className="dropzone"
        accept={"image/*"} >
        {imgObj}
      </Dropzone>
    );


    return(
      <div style={{cursor:'pointer', textAlign: 'center', width:'260px', display: 'inline-block', marginRight:((Number(index)+1)%3)==0?'0px':'16px', marginBottom:'24px', verticalAlign:'top', position: 'relative', paddingRight:'24px', paddingTop: '24px'}}>
        {isLock?null:<IconButton iconClassName="material-icons" style={{position: 'absolute', right: '0px', top: '0px', zIndex: '500'}} onClick={function(){self.removeAttachment()}}>cancel</IconButton>}
        <Paper style={{ background: '#FFFFFF', color: 'rgba(0, 0, 0, 0)', border: 'solid', position: 'relative', display: 'table', width: '230px', height: '230px' }}>
          {isLock?imgObj:dropZ}
        </Paper>
        <TextField
          value={items[index].id}
          floatingLabelText={"Image Id"}
          errorText={errorMsg}
          style={{width: '230px', height: '48px', marginTop: '16px'}}
          floatingLabelStyle={{top:'12px'}}
          errorStyle={{textAlign: 'left', bottom: '14px'}}
          underlineFocusStyle={{bottom: '16px'}}
          underlineDisabledStyle={{bottom: '16px'}}
          underlineStyle={{bottom: '16px'}}
          onChange={function(e){
            items[index].id=e.target.value;
            self.forceUpdate();
          }} />
      </div>
    );
  }

});

let BiImage = React.createClass({
  mixins: [React.LinkedStateMixin, StylePropable],

  componentDidMount() {
    ContentStore.addChangeListener(this.onContentChange);
  },

  componentWillUnmount: function() {
    ContentStore.removeChangeListener(this.onContentChange);
  },

  shouldComponentUpdate: function(nextProps, nextState) {
    return true;
  },


  onContentChange() {
    var content = ContentStore.getPageContent();

    var {
      template,
      values,
      changedValues
    } = content;

    if(!changedValues){
      changedValues = content.changedValues = cloneObject(values);
    }

    this.setState({
      template:template,
      values: values,
      changedValues: changedValues,
    })
  },
  onUpdate(){
    this.forceUpdate();
  },

  getInitialState() {
    var {
      template,
      values
    } = ContentStore.getPageContent();

    var isLock = BeneIllusDetailStore.isLock();

    //initial values
    if(!checkExist(values, sectionId)){
      values[sectionId]={};
    }
    var currPage = BeneIllusDetailStore.getCurrentPage();
    var changedValues = ContentStore.getPageContent().changedValues = cloneObject(values);

    ContentStore.getPageContent().valuesBackup = values;
    return {
      template: template,
      values: values,
      changedValues: changedValues,
      isLock: isLock
    };
  },

  _genItems(changedValues){
    let items = [];
    let {
      isLock
    } = this.state;

    var self = this;
    if(changedValues[sectionId].items){
      for(var i in changedValues[sectionId].items){
          items.push(<ImageBox isLock={isLock} changedValues={changedValues} index={Number(i)} onListUpdate={self.onUpdate}/>);
      }
    }
    if(isEmpty(changedValues[sectionId].items)){
      return(
        <div style={{width: '100%', height: 'calc(100% - 58px)', display: 'table'}}>
          <div style={{textAlign: 'center', verticalAlign: 'middle', display: 'table-cell'}}>
            There is no values in this page.
          </div>
        </div>
      )
    }
    return items;
  },

  addImg(newFile) {
    var {
      changedValues
    } = this.state;

    if(!changedValues.Attachments) changedValues.Attachments = {};

    var uid = getUniqueId();

    if(!changedValues[sectionId].items) changedValues[sectionId].items = [];
    changedValues[sectionId].items.push({
      id:'',
      image: uid
    });

    changedValues.Attachments[uid] = newFile.url;
    this.forceUpdate();
  },


  render: function() {
    var self = this;
    var {
      isLock,
      changedValues
    } = this.state;

    let dataContainerStyle = {
      width:'100%',
      height:"calc(100vh - 58px)",
      overflowY:"auto",
    };

    let items = this._genItems(changedValues);

    let addButton = (
      <Dropzone
         style={{position: 'absolute', bottom: '24px', right: '24px'}}
        onDrop={function(res) {
          var invalid = [];

          //File type checking
          var actualFileType = res.file.type;
          if (actualFileType == 'image/png' || actualFileType == 'image/jpeg') {
            var image = new Image();
            image.src = res.imageUrl;
            var width = image.width;
            var height = image.height;

            if ( res.file.size >2*1048576) {
               invalid.push("Your file size exceed " + 2 + " MB.");
            }

          }else{
              invalid.push("Your file is not " + requireFiletype.substring(requireFiletype.indexOf('/')+1, requireFiletype.length).toUpperCase()+ " type.");
          }


          if (invalid.length == 0) {
            this.updateImg({
              url: res.imageUrl,
            });
          } else {
            var errorMsg = "";
            for (var i = 0; i < invalid.length; i++) {
              errorMsg += invalid[i] + "\n";
            }
            alert(errorMsg, "Import failed");
          }
        }.bind({updateImg: this.addImg})}
        className="dropzone"
        accept={"image/*"} >
        <FloatingActionButton>
          <Add/>
        </FloatingActionButton>
      </Dropzone>
    );


    return (
      <div style={{padding: '24px', position: 'relative', height: 'calc(100vh - 58px)'}}>
        {items}
        {isLock?null:addButton}
      </div>
    );
  }
});

module.exports = BiImage;
