let React = require("react");
let mui = require("material-ui");
let StylePropable = mui.Mixins.StylePropable;

let DynMasterTable = require("../../DynamicUI/DynMasterTable.jsx");

let ContentStore = require('../../../stores/ContentStore.js');
let MasterTableActions = require('../../../actions/MasterTableActions.js')

module.exports = React.createClass({
  mixins: [React.LinkedStateMixin, StylePropable],

  getInitialState() {
    return {
      content: ContentStore.getPageContent(),
    };
  },

  componentDidMount() {
    ContentStore.addChangeListener(this.onChange);

    if (this.state.content == null || this.state.content.template == null || this.state.content.values == null) {
      this._reload();
    }
  },

  componentWillUnmount() {
    ContentStore.removeChangeListener(this.onChange);
  },

  componentDidUpdate() {
    if (this.state.content == null || this.state.content.template == null || this.state.content.values == null) {
      this._reload();
    }
  },

  _reload() {
    if (!this.gettingContent) {
      this.gettingContent = true;
      MasterTableActions.refreshMasterTable(0, null, false);
    }
  },

  gettingContent: false,

  onChange() {
    this.gettingContent = false;
    this.setState({
      content: ContentStore.getPageContent(),
    });
  },

  render() {
    if (this.state.content == null || this.state.content.template == null || this.state.content.values == null) {
      this.state.content = ContentStore.getPageContent();
    }
    if (this.state.content == null || this.state.content.template == null || this.state.content.values == null) {
      return <div className="PageContent"></div>;
    } else {
      var windowHeight = window.innerHeight;

      var {
        template,
        values
      } = this.state.content

      return(
        <div className="PageContent">
          <DynMasterTable
            id = {template.id}
            list = {values.list}
            total = {values.total}
            template = {template}
            show = {true}
            height = { (windowHeight - 170) + "px"}
            />
        </div>
      );
    }
  }
});
