/** Settings page */
let React = require('react');
let mui = require('material-ui');

let FlatButton = mui.FlatButton;
let TextField = mui.TextField;
let RadioGroup = require('../CustomView/DynRadioGroup.jsx');

let ConfigActions = require('../../actions/ConfigActions.js');
let ContentStore = require('../../stores/ContentStore.js');
let DynActions = require('../../actions/DynActions.js');

let MySettings = React.createClass({

  componentDidMount() {
    ContentStore.addChangeListener(this.onChange);
  },

  componentWillUnmount: function() {
    ContentStore.removeChangeListener(this.onChange);
  },
  gettingContent: false,
  onChange() {
    this.gettingContent = false;
    this.setState({pageContent: ContentStore.getPageContent()})
  },

  setValues(newValues){
    DynActions.changeValues(newValues);
  },
  getInitialState() {
    var pageContent = ContentStore.getPageContent();
    var changedValues = pageContent ? pageContent.changedValues : null;
    return {
      pageContent: pageContent,
      changedValues: changedValues,
      data_001: "",
      data_002: "",
      data_003: "",
    };
  },
  saveButtonStyle: function(){
    return {};
  },
  handleSubmitTap(e, target) {
    var values = ContentStore.getPageContent().changedValues;
    values['data_001'] = this.state.data_001;
    values['data_002'] = this.state.data_002;
    values['data_003'] = this.state.data_003;
    this.setState({data_001: "", data_002: "", data_003: ""});

    if (target) {
      ConfigActions.changeSettings(target.id)
    }
  },
  saveChange: function(e, target){

  },

  passwordValid:function(password){
    if(password[0] == password[1] || password[2] == password[1] || password[0] == password[2])
      return false;
    else {
      return true;
    }
  },
  getStyle(){
    var style = {
      'buttonBgColor':'#FAFAFA',
      'buttonGreen':'#2DB154',
      'buttonRed':'#F1453D',
    }
    return style;
  },
  onPassChange01(e){
    this.setState({ data_001: e.target.value});
    //this.changeValues("data_001", e.target.value);
  },
  onPassChange02(e){
    this.setState({ data_002: e.target.value});
    //this.changeValues("data_002", e.target.value);
  },
  onPassChange03(e){
    this.setState({ data_003: e.target.value});
    //this.changeValues("data_003", e.target.value);
  },
  changeValues(id, value){

    var values = ContentStore.getPageContent().changedValues;
    var newValues = cloneObject(values);
    newValues[id] = value;
    this.setValues(newValues);
  },
  render: function() {
    var self = this;
    var pageContent = this.state.pageContent;
    var contentInStore = ContentStore.getPageContent();
    pageContent = contentInStore;
    if (pageContent == null || pageContent.template == null) {
      if (!this.gettingContent) {
        this.gettingContent = true;
        ConfigActions.getSettings();
      }
      return <div className="PageContent"></div>;
    } else {
      if (!pageContent) return null;
      var template = pageContent.template;
      var title = template.title;
      var values = pageContent.changedValues;
      var leng = template.items.length;
      var content = [];
      // this.refs["bt-"+i].getValue()
      for (let i=0; i<leng; i++){
        var item = template.items[i];

        switch(item.type){
          case "radioGroup":
            var onRGChange = function(e, newSelection){
              var newValues = cloneObject(values);
              newValues[this.id] = newSelection;
              self.setValues(newValues);
            }.bind( { id: item.id} )
            var value = values[item.id];
            content.push(<h2 key={item.title}>{item.title}</h2>);
            content.push(<RadioGroup onChange={onRGChange} key={"rg"+item.id} id={item.id} ref={item.id} template={item} value={value} />);
            break;

          case "txtFieldGroup":
            var txtFieldGroup = [];
            content.push(<h2 key={item.title}>{item.title}</h2>);

            for (let j in item.items){

              var subItem = item.items[j];
              var tf = "";
              var type = "password";

              switch (subItem.id) {
                case "data_001":
                  if(this.state.data_001==""){
                    type = "text";
                  }
                  var tf =
                    (<TextField
                      key = {subItem.id}
                      ref = "data_001"
                      style = { {width: '240px', marginRight:'20px', display:'inline-block' } }
                      floatingLabelStyle = { {top:'28px', lineHeight:'24px'} }
      								underlineStyle = { {bottom:'22px'} }
                      floatingLabelText = {subItem.title}
                      type={type}
                      value= { this.state.data_001 }
                      onChange = { self.onPassChange01 } />)

                  break;
                case "data_002":
                  if(this.state.data_002==""){
                    type = "text";
                  }
                  var tf =
                    (<TextField
                      key = {subItem.id}
                      ref = "data_002"
                      style = { {width: '240px', marginRight:'20px', display:'inline-block' } }
                      floatingLabelStyle = { {top:'28px', lineHeight:'24px'} }
      								underlineStyle = { {bottom:'22px'} }
                      floatingLabelText = {subItem.title}
                      type={type}
                      value= { this.state.data_002 }
                      onChange = { self.onPassChange02 } />)
                  break;
                case "data_003":
                  if(this.state.data_003==""){
                    type = "text";
                  }
                  var tf =
                    (<TextField
                      key = {subItem.id}
                      ref = "data_003"
                      style = { {width: '240px', marginRight:'20px', display:'inline-block' } }
                      floatingLabelStyle = { {top:'28px', lineHeight:'24px'} }
      								underlineStyle = { {bottom:'22px'} }
                      floatingLabelText = {subItem.title}
                      type={type}
                      value= { this.state.data_003 }
                      onChange = { self.onPassChange03 } />)
                  break;
                default:

              }
              txtFieldGroup.push(tf);
              var hidden = <input name="a" className="hidden" type="password"/>
               txtFieldGroup.push(hidden);
            }

            content.push(txtFieldGroup);
            break;
          case "button":
            var button =
            (<FlatButton
              key={"bt_"+item.id}
              labelStyle={{color:this.getStyle().buttonGreen}}
              backgroundColor={this.getStyle().buttonBgColor}
              style={{color:this.getStyle().buttonGreen}}
              id={item.id}
              target={item}
              onTouchTap={this.handleSubmitTap}>
              {item.title}
            </FlatButton>);
            content.push(<div className="saveBt">{button}</div>);
            break;

        }

      }

      return (
            <div className="settings">{content}</div>
      );

    }
  }
});

module.exports = MySettings;
