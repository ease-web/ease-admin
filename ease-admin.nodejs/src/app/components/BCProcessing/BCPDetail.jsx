/** Business Continuous Processing Detail page */
let React = require('react');
let mui = require('material-ui');

let Dialog = mui.Dialog;
let DynMasterTable = require("../DynamicUI/DynMasterTable.jsx");

let BCProcessingActions = require('../../actions/BCProcessingActions.js');
let DynActions = require('../../actions/DynActions.js');
let MasterTableActions = require('../../actions/MasterTableActions.js');
let MenuActions = require('../../actions/MenuActions.js');
let ContentStore = require('../../stores/ContentStore.js');
let AppBarStore = require('../../stores/AppBarStore.js');
let MenuItem = mui.MenuItem;

let BCPDetail = React.createClass({

  componentDidMount() {
    ContentStore.addChangeListener(this.onChange);
  },

  componentWillUnmount: function () {
    ContentStore.removeChangeListener(this.onChange);
  },

  gettingContent: false,

  onChange() {
    this.gettingContent = false;
    this.setState({ pageContent: ContentStore.getPageContent() })
  },

  setValues(newValues) {
    DynActions.changeValues(newValues);
  },

  getInitialState() {
    var pageContent = ContentStore.getPageContent();
    var changedValues = pageContent ? pageContent.changedValues : null;
    return {
      selectedRow: -1,
      pageContent: pageContent,
      changedValues: changedValues,
      open: false
    };
  },
  handleRowSelection(rows) {
    var pageContent = this.state.pageContent;
    this.setState({ selectedRow: rows.length > 0 ? rows[0] : -1 });
  },
  handleTapRow(e, row) {
    const bcpContent = ContentStore.getBcpContent();
    const pageContent = this.state.pageContent;
    const status = pageContent.values.list[row].status.toUpperCase();
    if(status != "SUCCESS" && status != "READY" && status !="PENDING" ) {
      BCProcessingActions.getBCPDetailError(pageContent.values.list[row].type, bcpContent.policyNumber, pageContent.values.list[row].taken_by_batch_no, pageContent.values.list[row].updateBy);
    }
    
  },

  changeValues(id, value) {
    var values = ContentStore.getPageContent().changedValues;
    var newValues = cloneObject(values);
    newValues[id] = value;
    this.setValues(newValues);
  },

  render: function () {
  
    var self = this;
    var windowHeight = window.innerHeight;
    var pageContent = this.state.pageContent;
    var contentInStore = ContentStore.getPageContent();
    pageContent = contentInStore;

    var bcpContent = ContentStore.getBcpContent();

    if (pageContent == null || pageContent.template == null) {
      if (!this.gettingContent) {
        this.gettingContent = true;
        BCProcessingActions.getStatus();
      }
      return <div className="PageContent"></div>;
    } else {
      if (!pageContent) return null;

      var template = pageContent.template;
      var title = template.title;
      var values = pageContent.changedValues;

      var content = [];
      // this.refs["bt-"+i].getValue()

      var menuItems = [];
      for (var j = 0; j < 3; j++) {
        menuItems.push(<MenuItem
          value={j}
          key={j}
          primaryText={j} />
        );
      }

      return (
        <div className="settings">
          <div style={{display: 'flex'}}>
            {content}
          </div>
          <DynMasterTable
            id={'bcpStatus'}
            page={'BCPDetail'}
            list={values.list}
            total={values.list.length}
            template={template}
            show={true}
            selectable={false}
            multiSelectable={false}
            height={(windowHeight - 100) + "px"}
            handleTapRow={this.handleTapRow}
            handleRowSelection={this.handleRowSelection}
            submitDisable = {bcpContent.submitDisable}
            policyNumber = {bcpContent.policyNumber}
            selectedType = {bcpContent.selectedType}
            sort = {false}
             />
        </div>
      );
    }
    
  }
});

module.exports = BCPDetail;
