/** Business Continuous Processing page */
let React = require('react');
let mui = require('material-ui');

let FlatButton = mui.FlatButton;
let Dialog = mui.Dialog;
let DynMasterTable = require("../DynamicUI/DynMasterTable.jsx");

let BCProcessingActions = require('../../actions/BCProcessingActions.js');
let DynActions = require('../../actions/DynActions.js');
let MasterTableActions = require('../../actions/MasterTableActions.js');
let MenuActions = require('../../actions/MenuActions.js');
let ContentStore = require('../../stores/ContentStore.js');
let AppBarStore = require('../../stores/AppBarStore.js');
let AppStore = require('../../stores/AppStore.js');
let MenuItem = mui.MenuItem;

let BCProcessing = React.createClass({

  componentDidMount() {
    ContentStore.addChangeListener(this.onChange);
  },

  componentWillUnmount: function () {
    ContentStore.removeChangeListener(this.onChange);
  },

  gettingContent: false,

  onChange() {
    var pageContent = ContentStore.getPageContent();
    let dialog_status = false;
    if(pageContent) {
      if(pageContent.changedValues) {
        let changedValues = pageContent.changedValues;
        if(changedValues.result && IsJsonString(changedValues.result)) {
          const result = JSON.parse(changedValues.result);
          if(result.msg) {
            dialog_status = true
          }
        }
      }
    }
    this.setState({ 
      pageContent: pageContent,
      dialogopen: dialog_status
    })
  },

  setValues(newValues) {
    DynActions.changeValues(newValues);
  },

  getInitialState() {
    var pageContent = ContentStore.getPageContent();
    var changedValues = pageContent ? pageContent.changedValues : null;

    return {
      selectedRow: -1,
      resubmitDisable: true,
      pageContent: pageContent,
      changedValues: changedValues,
      dialogopen: false
    };
  },
  handleRowSelection(rows) {
    if(!rows.length) {
      this.setState({ resubmitDisable: true });
      return;
    }

    var pageContent = this.state.pageContent;
    if(pageContent.values.list[rows].status.toUpperCase() === "SUCCESS" || pageContent.values.list[rows].status.toUpperCase() === "READY") {
      this.setState({ resubmitDisable: true });
    } else {
      this.setState({ resubmitDisable: false });
    }
    // this.setState({ selectedRow: rows.length > 0 ? rows[0] : -1 });
  },
  handleTapRow(e, row) {
    var pageContent = this.state.pageContent;
    const bcpContent = ContentStore.getBcpContent();
    BCProcessingActions.getBCPDetail(pageContent.values.list[row].type.toUpperCase(), bcpContent.policyNumber);
  },
  handleReSubmit() {
    
    let bcpContent = ContentStore.getBcpContent();
    BCProcessingActions.resubmit('resubmit', bcpContent ,function(res) {

      // MenuActions.changePage({
      //   disabled: false,
      //   id: "/BCProcessing",
      //   module: "bcpRequest",
      //   seq: 2750,
      //   title: "Business Continuous Processing"
      // });


      // setTimeout(function() {
      //   let values = AppBarStore.getValues();
      //   values["criteria"] = bcpContent.policyNumber;
      //   values["useDefault"] = true;
      //   MasterTableActions.refreshMasterTable(0, {
      //     filter: values
      //   }, false);
      // }, 1500);
      
    })
  },

  changeValues(id, value) {
    var values = ContentStore.getPageContent().changedValues;
    var newValues = cloneObject(values);
    newValues[id] = value;
    this.setValues(newValues);
  },

  CreateDialog(values) {
    let self = this;
    if(IsJsonString(values)) {
      let valuesJSON = JSON.parse(values);
      // if(!valuesJSON.status) {
      if(valuesJSON) {
        

        return (<Dialog
                open={self.state.dialogopen}
                actions={<FlatButton
                  label="OK"
                  primary={true}
                  onClick={function () {
                    BCProcessingActions.changeErrorMsg(null);
                    // self.setState({ dialogopen: false })
                  }}
                />}
                modal={false}
                onRequestClose={function () {
                  BCProcessingActions.changeErrorMsg(null);
                  // self.setState({ dialogopen: false })
                }}
                title={valuesJSON.title}
              >
                {valuesJSON.msg}
              </Dialog>
              );
        // return valuesJSON.msg;
      }
      
    }
    return null;
  },

  render: function () {
  
    var self = this;
    var windowHeight = window.innerHeight;
    var pageContent = this.state.pageContent;
    var contentInStore = ContentStore.getPageContent();
    pageContent = contentInStore;

    var bcpContent = ContentStore.getBcpContent();

    if (pageContent == null || pageContent.template == null) {
      if (!this.gettingContent) {
        this.gettingContent = true;
        BCProcessingActions.getStatus();
      }
      return <div className="PageContent"></div>;
    } else {
      if (!pageContent) return null;

      var template = pageContent.template;
      var title = template.title;
      var values = pageContent.changedValues;

      var content = [];
      // this.refs["bt-"+i].getValue()

      var menuItems = [];
      for (var j = 0; j < 3; j++) {
        menuItems.push(<MenuItem
          value={j}
          key={j}
          primaryText={j} />
        );
      }

      var dialog = [];
      if(this.state.dialogopen) {
        dialog.push(this.CreateDialog(values.result));
      }
      
      return (
        <div className="settings">
          <div style={{display: 'flex'}}>
            {content}
          </div>
          <DynMasterTable
            id={'bcpStatus'}
            page={'bcp'}
            list={values.list}
            total={values.list.length}
            template={template}
            show={true}
            selectable={true}
            multiSelectable={false}
            height={(windowHeight - 100) + "px"}
            handleTapRow={this.handleTapRow}
            handleRowSelection={this.handleRowSelection}
            // submitDisable = {bcpContent.submitDisable}
            submitDisable = {this.state.resubmitDisable}
            policyNumber = {bcpContent.policyNumber}
            selectedType = {bcpContent.selectedType}
            handleReSubmit = {this.handleReSubmit}
            sort = {false}
             />
          {dialog}
        </div>
      );
    }
    
  }
});

module.exports = BCProcessing;
