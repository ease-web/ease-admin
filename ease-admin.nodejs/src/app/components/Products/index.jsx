
let React = require('react');
let mui = require('material-ui');
let StylePropable = mui.Mixins.StylePropable;
let ProductStore = require('../../stores/ProductStore.js');
let ContentStore = require('../../stores/ContentStore.js');
let DynMenu = require('../DynamicUI/DynMenu.jsx');
let CurrentColumn = require('../DynamicUI/DynEditableColumn.jsx');
let BNC = require('./BNC.jsx');

let Products = React.createClass({
  mixins: [React.LinkedStateMixin, StylePropable],

  componentDidMount() {
    // console.debug('homepage did mount');
    ContentStore.addChangeListener(this.onContentChange);
    ProductStore.addChangeListener(this.onPageChange);
  },

  componentWillUnmount: function() {
    ProductStore.removeChangeListener(this.onPageChange);
    ContentStore.removeChangeListener(this.onContentChange);
  },

  shouldComponentUpdate: function(nextProps, nextState) {
    return true;
  },

  onPageChange() {
    // var section = this.getSection(ProductStore.getCurrentPage(), this.state.template);
    var currPage = ProductStore.getCurrentPage();
    this.setState({curSection: currPage.id});
  },

  onContentChange() {
    var {
      template,
      values,
      changedValues
    } = ContentStore.getPageContent();

    var {
      curSection,
      sections
    } = this.state;

    // var section = sections[curSection]  //this.getSection(ProductStore.getCurrentPage(), template);
    // template.validate = this._validate;
    // template.checkChanges = this._checkChanges;

    var orgValues = this.state.values;
    var newState = {
      // template: template,
      // menuList: template.menu,
      // section: section,
      // changedValues: changedValues
    };

    if (checkChanges(values.id, orgValues.id) || checkChanges(values.version, orgValues.version)){
      newState.values = ContentStore.getPageContent().values = cloneObject(values);
      newState.changedValues = ContentStore.getPageContent().changedValues = cloneObject(values);
    } else if(!changedValues){
      newState.changedValues = ContentStore.getPageContent().changedValues = cloneObject(values);
    }

    this.setState(newState);
  },

  _getMenu(config, sections) {
    var menu = [];
    for (var i in config.items) {
      var sect = config.items[i];

      if (sect && sect.type) {
        if (sect.type == 'sectionGroup') {
          var meunItemGroup = {};
          for (var key in sect) {
            if (key == "items") {
              meunItemGroup.items = [this._getMenu(sect, sections)];
            } else {
              meunItemGroup[key] = sect[key];
            }
          }
          meunItemGroup.type = "L"
          menu.push(meunItemGroup);
        } else if (sect.type == 'section') {
          var menuItem = {};
          for (var key in sect) {
            if (key == "items") {
              sections[sect.id] = sect;
            } else {
              menuItem[key] = sect[key];
            }
          }
          menuItem.type = ""
          menu.push(menuItem);
        }
      }
    }
    return menu;
  },

  getInitialState() {
    var {
      template,
      values
    } = ContentStore.getPageContent();

    var sections = {};
    var menu = this._getMenu(template, sections);
    var curSection = menu[0].id;
    var section = sections[curSection];

    // var section = template.items[0];
    // var currPage = ProductStore.getCurrentPage();
    var changedValues = ContentStore.getPageContent().changedValues = (values && cloneObject(values)) || {};
    ContentStore.getPageContent().valuesBackup = values;
    // var taskStatusCode = (checkExist(changedValues, 'TaskInfo.statusCode'))?changedValues.TaskInfo.statusCode:undefined;

    // // validate
    // template.validate = this._validate;
    // // checkChanges
    // template.checkChanges = this._checkChanges;

    return {
      id: values.id?values.id:"",
      version: values.version?values.version:1,
      curSection: curSection,
      sections: sections,
      menuList: [menu],
      values: values,
      changedValues: changedValues
      // section: section
      // taskStatusCode: taskStatusCode
    };
  },

  // getSection(page, template){
  //   var templateItems = template.items;
  //   for (let i in templateItems) {
  //     var section = templateItems[i];
  //     if (section.id == page.id) {
  //       return section;
  //     }
  //   }
  // },

  render: function() {
    var section = this.state.sections[this.state.curSection];
    console.debug('[Products] - [render]', section);
    if (section) {
      let containerStyle = {
        width: 'calc(100% - 280px)',
        display: 'inline-block',
      };
      let dataContainerStyle = {
        width:'100%',
        height:"calc(100vh - 58px)",
        overflowY:"auto",
      };

      var values = this.state.values[this.state.curSection];
      var changedValues = this.state.changedValues[this.state.curSection];
      if (!values) {
        values = {}
      }
      if (!changedValues) {
        changedValues = this.state.changedValues[this.state.curSection] = {}
      }
      // var id = section.id;
      var content = (
        <CurrentColumn
          style={dataContainerStyle}
          show = {true}
          template = {section}
          values = { values }
          changedValues = { changedValues }
          rootValues = { this.state.changedValues }
          id = "current"
          ref = "current" />
      );

      return (
        <div className="PageContent">
          <DynMenu
            key={"prodDynMenu"}
            ref={"prodDynMenu"}
            show={true}
            module={"/Products/Detail"}
            items={this.state.menuList}
            style={{width: "280px", minWidth: "280px"}}/>
          <div style={containerStyle}>
            {content}
          </div>
        </div>
      );
    } else {
      return null;
    }
  }
});

module.exports = Products;
