let React = require('react');
let mui = require('material-ui');
let StylePropable = mui.Mixins.StylePropable;
let Transitions = mui.Styles.Transitions;

let IconButton = mui.IconButton;
let RaisedButton = mui.RaisedButton;
let FontIcon = mui.FontIcon;
let Toolbar = mui.Toolbar;
let ToolbarGroup = mui.ToolbarGroup;
let ToolbarTitle = mui.ToolbarTitle;
let SelectField = require('../CustomView/SelectField.jsx');

let ConfigPanel = require('../CustomView/ConfigPanel.jsx');
let ContentStore = require('../../stores/ContentStore.js');
let EABTextField = require('../CustomView/TextField.jsx');
let EABPickerField = require('../CustomView/PickerField.jsx');

let DynConfigPanelActions = require('../../actions/DynConfigPanelActions.js');
let DynConfigPanelStore = require('../../stores/DynConfigPanelStore.js');

import appTheme from '../../theme/appBaseTheme.js';

let DynConfigPanel = React.createClass({

  mixins: [React.LinkedStateMixin, StylePropable],

  childContextTypes: {
    muiTheme: React.PropTypes.object,
  },

  getChildContext() {
    return {
      muiTheme: this.state.muiTheme,
    };
  },

  contextTypes: {
    muiTheme: React.PropTypes.object
  },

  propTypes:{
    id: React.PropTypes.string.isRequired,
    children: React.PropTypes.node,
    onPanelClose: React.PropTypes.func,
    onPanelOpen: React.PropTypes.func,
    onRequestChange: React.PropTypes.func,
    height:React.PropTypes.number,
    style: React.PropTypes.object,
    template: React.PropTypes.object,
    changedValues: React.PropTypes.object,
    values: React.PropTypes.object
  },

  componentDidMount() {
    // console.debug('homepage did mount');
    DynConfigPanelStore.addChangeListener(this.onPanelChange);
  },

  componentWillUnmount: function() {
    DynConfigPanelStore.removeChangeListener(this.onPanelChange);
  },

  shouldComponentUpdate: function(nextProps, nextState) {
    if(this.state.open != nextState.open){
      return true;
    }
    let template = nextProps.template;
    if(template && template.configPanel && checkChanges(this.state.configPanel, template.configPanel)){
      return true;
    }
    return false;
  },

  onPanelChange(){
    let open = DynConfigPanelStore.getPanelState();
    this.setState({open:open});
  },

  getDefaultProps() {
    return {
      //open: false
    }
  },

  componentWillReceiveProps(nextProps){
    let template = nextProps.template;
    this.state.values = nextProps.values?nextProps.values:{};
    this.state.changedValues = nextProps.changedValues?nextProps.changedValues:{};
    this.state.template = (template && template.configPanel)?template.configPanel:{};
  },

  getInitialState: function() {
    DynConfigPanelStore.updatePanel(false);
    return {
      open: false,
      muiTheme: this.context.muiTheme ? this.context.muiTheme : appTheme.getTheme()
    }
  },


  getStyles() {
    let theme = this.state.muiTheme;
    let height = this.props.height?this.props.height:500;
    return {
      contentStyle: {
        color: '#0000000',
        top: '0px'
      },
      topBarStyle:{
        height: '58px',
        display: 'block',
        overflow:'hidden'
      },
      topBarTitleStyles:{
        paddingRight:'16px',
        verticalAlign: 'super',
        fontSize: '24px',
        display: 'inline-block',
        position: 'relative'
      },
      panelBodyStyle:{
        display:'flex',
        position:'relative',
        height:(height-65)+'px',
        width:'100%'
      },
      leftPanelStyle:{
        display: 'inline-block',
        position: 'relative',
        width:'calc(65% - 1px)',
        height: (height-65)+'px',
        overflowY: 'auto'
      },
      borderStyle:{
        display: 'inline-block',
        position: 'relative',
        width:'2px',
        height: (height-65)+'px',
        background: '#AAAAAA'
      },
      rightPanelStyle:{
        display: 'inline-block',
        position: 'relative',
        width:'calc(35% - 1px)',
        height: (height-65)+'px',
        overflowY: 'auto'
      },
      pickerStyle: {
        width: 'auto',
        height: '56px',
      }
    }
  },

  handleSelectionChange(id, value) {
    //AppBarActions.updateFilter(id, value);
  },

  _genItem(its, item, values, changedValues) {
    var self = this;
    // current value
    if (!values) {
      values = this.state.values;
    }
    // changed value
    if (!changedValues) {
      changedValues = this.state.changedValues;
    }

    if (!showCondition(item, values, changedValues, null)) {
      return;
    }

    if (item.type.toUpperCase() == 'TEXT' || item.type.toUpperCase() == 'TEXTFIELD') {
      its.push(
        <EABTextField
          ref = {item.id}
          key = {"ctn_"+ item.id}
          template = { item }
          values = { values }
          changedValues = { changedValues }
          />
      );
    }
    else if (item.type.toUpperCase() == 'PICKER') {
      its.push(
        <EABPickerField
          ref = {item.id}
          key = {"ctn_"+ item.id}
          template = { item }
          values = { values }
          changedValues = { changedValues }
          />
      );
    }
    else if (item.type.toUpperCase() == 'PICKER') {
      its.push(
        <EABPickerField
          ref = {item.id}
          key = {"ctn_"+ item.id}
          template = { item }
          values = { values }
          changedValues = { changedValues }
          />
      );
    }
  },

  _genTopBar(){
    //find topBar
    let template = this.state.template;
    let topBar = (template && template.topBar)?template.topBar:null;
    let changedValues = this.state.changedValues;
    //start

    let styles = this.getStyles();
    let actions = [];
    let title = null;
    if(!isEmpty(topBar)){
      for(var i in topBar.items){
        let item = topBar.items[i];
        let itemId = this.props.id +'-tb-'+i;
        if(item.type == 'title'){
          if(!isEmpty(item.title)){
            title = <ToolbarTitle text={item.title} style={{color:'#000000'}}/>
          }else if(!isEmpty(item.id) &&!isEmpty(item.options)){
            title = <SelectField
              ref = {itemId}
              key = {itemId}
              template = {item}
              changedValues = {changedValues}
              style = { styles.pickerStyle }
              iconStyle={{ fill: styles.contentStyle.color}}
              labelStyle = { this.mergeStyles(styles.contentStyle, { paddingRight: '28px'}) }
              underlineStyle={{display:'none'}}
              handleChange={this.handleSelectionChange}/>
          }
        }else{
          switch(item.type){
            case 'label':
              actions.push(
                <ToolbarTitle text={item.title} style={{color:'#000000'}}/>
              )
              break;
            case 'button':
              actions.push(
                <RaisedButton key={itemId} style={{display:'inline-block'}} label={item.title}/>
              )
              break;
          }
        }
      }
    }


    return (
      <div key={this.props.id+'-topbar'} style={styles.topBarStyle}>
        <Toolbar key="appToolBar" style={{padding:"0px", position:'relative', background: '#FFFFFF'}} >
          <ToolbarGroup key={this.props.id+'-topbar-left'} style={{width:'65%', display:'flex'}}>
            <FontIcon
              key='closeBtn'
              style = {{padding: '0px 15px 0 24px'}}
              color = "#000000"
              className="material-icons"
              onClick={this.hidePanel} >
              close
            </FontIcon>
            {title}
          </ToolbarGroup>
          <ToolbarGroup key={this.props.id+'-topbar-right'} style={{width:'35%', display:'flex'}}>
            {actions}
          </ToolbarGroup>
        </Toolbar>
      </div>
    );
  },

  _genLeftPanel(){
    let template = this.state.template;
    var leftPanel = (template && template.leftPanel)?template.leftPanel:null;
    let styles = this.getStyles();
    let items = [];

    if(!isEmpty(leftPanel)){
      for(let i in leftPanel.items){
        let item = leftPanel.items[i];
        this._genItem(items, item, this.props.values, this.props.changedValues);
      }
    }

    return(
      <div style={styles.leftPanelStyle}>
        {items}
      </div>
    );


  },
  _genRightPanel(){
    //let rightPanel = this.props.template.rightPanel;
    let template = this.state.template;
    var rightPanel = (template && template.rightPanel)?template.rightPanel:null;
    let styles = this.getStyles();
    let items = [];

    if(!isEmpty(rightPanel)){
      for(let i in rightPanel.items){
        let item = rightPanel.items[i];
        this._genItem(items, item, this.props.values, this.props.changedValues);
      }
    }

    return(
      <div style={styles.rightPanelStyle}>
        {items}
      </div>
    );

  },

  hidePanel(){
    //this.refs["dcp-"+this.props.id].close();
    DynConfigPanelActions.closePanel();
  },

  render: function() {
    var self = this;
    var {
      id,
      height,
      onPanelClose,
      onPanelOpen,
      onRequestChange,
      ...others
    } = this.props;

    var {
      open
    } = this.state;

    let styles = this.getStyles();
    var topBar = this._genTopBar();
    var leftPanel = this._genLeftPanel();
    var rightPanel = this._genRightPanel();

    return (
      <ConfigPanel key={"dcp-"+id} ref={"dcp-"+id} id={id} open={open} height={height?height:500} onPanelClose={onPanelClose} onPanelOpen={onPanelOpen} onRequestChange={onRequestChange}>
        {topBar}
        <div key={"dcp-"+id+"-panel"} style={styles.panelBodyStyle}>
          {leftPanel}
          <div style={styles.borderStyle}/>
          {rightPanel}
        </div>
      </ConfigPanel>
    );
  }

});

module.exports = DynConfigPanel;
