/** In this file, we create a React component which incorporates components provided by material-ui */

let React = require('react');
let ReactDOM = require('react-dom');

let mui = require('material-ui');
let StylePropable = mui.Mixins.StylePropable;
import appTheme from '../../theme/appBaseTheme.js';
let brace = require('brace');

let MasterTableStore = require('../../stores/MasterTableStore.js');
let MasterTableActions = require('../../actions/MasterTableActions.js');

let AssignPageActions = require('../../actions/AssignPageActions.js');
let AssignContentStore = require('../../stores/AssignContentStore.js');


let ConfigConstants = require('../../constants/ConfigConstants.js');
let DynActions = require('../../actions/DynActions.js');
var DialogActions = require('../../actions/DialogActions')
let IconButton = mui.IconButton;
let IconMenu = mui.IconMenu;
let MenuItem = mui.MenuItem;
let FontIcon = mui.FontIcon;
let Colors = mui.Styles.Colors;
let FlatButton = mui.FlatButton;
let ToolTip = mui.Tooltip;



import List from 'material-ui/lib/lists/list';
import Divider from 'material-ui/lib/divider';
import ListItem from 'material-ui/lib/lists/list-item';


let DynList = React.createClass({

  mixins: [React.LinkedStateMixin, StylePropable],

  contextTypes: {
    muiTheme: React.PropTypes.object
  },

  childContextTypes: {
    muiTheme: React.PropTypes.object,
  },

  getChildContext() {
    return {
      muiTheme: this.context.muiTheme?this.context.muiTheme:appTheme.getTheme(),
    };
  },

  propTypes:{
    // id: React.PropTypes.string.isRequired,
    // height: React.PropTypes.string,
    list: React.PropTypes.array.isRequired,
    subheaderStyle: React.PropTypes.object
    // template: React.PropTypes.object.isRequired,
    // total: React.PropTypes.number.isRequired,
    // selectable: React.PropTypes.bool,
    // multiSelectable: React.PropTypes.bool,
    // contextMenu: React.PropTypes.array,
    // show: React.PropTypes.bool,
    // handleTapRow: React.PropTypes.func,
    // handleRowSelection: React.PropTypes.func,
    // assignPage: React.PropTypes.bool,
  },

  getDefaultProps() {
    return {
      selectable: false,
      multiSelectable: false,
      contextMenu: null,
      show: true,
      height: '500px',
      assignPage: false,
    }
  },

  getInitialState() {
    // let template = this.props.template;
    // var sortInfo = (this.props.assignPage)?AssignContentStore.getSortInfo():MasterTableStore.getSortInfo();
    // var selectedRows = (this.props.assignPage)?AssignContentStore.getSelectedRows():MasterTableStore.getSelectedRows();
    // var selectedRowNumbers = (this.props.assignPage)?AssignContentStore.getSelectedRowNumbers():MasterTableStore.getSelectedRowNumbers();
    return {
    //   muiTheme: this.context.muiTheme,
    //   sortedCol: sortInfo.sortBy,
    //   sortedDirection: sortInfo.sortDir,
    //   selectedRows: selectedRows,
    //   fields: this.initFields(template),
    //   tooltipState: {},
    //   selectedRowNumbers: selectedRowNumbers,
    }
  },

  getStyles() {
    return {
      tableHeaderColumnStyle: {
        fontWeight:'bold',
        paddingRight: '0px',
        cursor: 'pointer',
        overflowX: 'hidden',
        color: appTheme.palette.text1Color
      },
      tableColumnStyle: {
        paddingRight: '0px',
        fontSize: '14px',
        position: 'relative',
        overflow: 'visible'  // for tooltip
      },
      cellStyle: {
        width: '100%',
        display: 'inline-block',
        overflow: 'hidden',
        whiteSpace: 'nowrap',
        textOverflow: 'ellipsis',
      }
    }
  },

//   initFields(template) {
//     var fields = [];
//     for (let i in template.items) {
//       var item = template.items[i];
//       if (item.listSeq && item.title) {
//         fields.push(item);
//       }
//     }

//     fields = fields.sort(function(a,b) {
//       var aa = a.listSeq;
//       var bb = b.listSeq;
//       return (aa < bb) ? -1 : (aa > bb) ? 1 : 0;
//     });

//     return fields;
//   },

  componentDidMount() {
    // console.debug('homepage did mount');
    if(this.props.assignPage){
      AssignContentStore.addChangeListener(this.onChange);
    }else{
      MasterTableStore.addChangeListener(this.onChange);
    }
  },

  componentWillUnmount() {
    if(this.props.assignPage){
      AssignContentStore.removeChangeListener(this.onChange);
    }else{
      MasterTableStore.removeChangeListener(this.onChange);
    }

  },

  onChange() {
    var sortInfo = (this.props.assignPage)?AssignContentStore.getSortInfo():MasterTableStore.getSortInfo();
    var selectedRows = (this.props.assignPage)?AssignContentStore.getSelectedRows():MasterTableStore.getSelectedRows();
    var selectedRowNumbers = (this.props.assignPage)?AssignContentStore.getSelectedRowNumbers():MasterTableStore.getSelectedRowNumbers();
    this.setState({
      selectedRows: selectedRows,
      sortedCol: sortInfo.sortBy,
      sortedDirection: sortInfo.sortDir,
      selectedRowNumbers : selectedRowNumbers })
  },

  componentWillReceiveProps(nextProps) {
    // this.setState({ fields: this.initFields(nextProps.template)})
  },

//   handleTapHeader(e, row, col) {
//     var direction = this.state.sortedDirection;
//     var multiSelectable = !!this.props.multiSelectable;
//     if (multiSelectable && col == 1) return;
//     var column = col - (multiSelectable?2:1);
//     var fid = this.state.fields[column].id;

//     if (this.state.sortedCol == fid) {
//       if (direction == 'A') {
//         direction = 'D';
//       } else {
//         direction = 'A';
//       }
//     } else {
//       direction = 'A';
//     }

//     if(this.props.assignPage){
//       AssignPageActions.updateSortInfo(fid, direction);
//     }else{
//       MasterTableActions.updateSortInfo(fid, direction);
//     }

//     //var sortedList = this._sortList(this.state.sortedList, fid, direction);
//     //this.setState( {sortedList: sortedList, sortedCol: column, sortedDirection: direction});
//   },

//   handleTapRow(e, row) {
//     let {
//       list,
//       id,
//     } = this.props;
//     if((id == "releaseDetails" ||id == "tasks")  && !list[row].accessible){
//       DialogActions.showDialog({
//         title: getLocalizedText('DIALOG.TITLE.WARNING', 'Warning'),
//         message: getLocalizedText('DIALOG.MSG.ACCESS_DENIED', 'dsadsad'),
//         width: 50,
//         actions: [{
//           type: 'OK',
//           title: getLocalizedText('BUTTON.OK', 'OK'),
//           secondary: true,
//         }]
//       });
//       return null;
//     }
//     if(!this.props.assignPage){
//       if (this.props.handleTapRow){
//         this.props.handleTapRow(e, row);
//       } else {
//         MasterTableActions.selectRow(row);
//       }
//     }
//   },

//   // only the checkbox field
//   handleRowSelection(rows) {

//     if (this.props.handleRowSelection) {
//       if(this.props.assignPage){
//         AssignPageActions.updateSelectedRow(rows, this.props.list, this.props.handleRowSelection(rows));
//       }else{
//         MasterTableActions.updateSelectedRow(rows, this.props.list, this.props.handleRowSelection(rows));
//       }
//     } else {
//       if(this.props.assignPage){
//         AssignPageActions.updateSelectedRow(rows, this.props.list, false);
//       }else{
//         MasterTableActions.updateSelectedRow(rows, this.props.list, false);
//       }
//     }
//   },
  // IsJsonString(str) {
  //   try {
  //       JSON.parse(str);
  //   } catch (e) {
  //       return false;
  //   }
  //   return true;
  // },

  // _renderMsg(msg) {
  //   if(this.IsJsonString(msg)) {
  //     msg = msg.replace(/{/g, "{\n    ");
  //     msg = msg.replace(/}/g, "\n}");
  //     msg = msg.replace(/:/g, ":  ");
  //     msg = msg.replace(/,/g, ",\n    ");
  //     console.warn('parseToJson');
  //     consolw.warn(msg);
  //   }
  //   return msg;
  // },

  _genList(list) {
    let items = [];

    // sort
    list.sort(function(a ,b) {
      if(a.listSeq && b.listSeq) {
        return a.listSeq - b.listSeq;
      } else {
        return 0;
      }
      
    })
    // for(let data of list) {
    //   let ser = [];
    //   if(data.listSeq) {

    //   }
    // }

    for(let data of list) {
        let sub_item = [];
        if(data.type.toUpperCase() == "LISTITEM") {
            sub_item.push(<ListItem
                // primaryText = {this._renderMsg(data.msg)}
                primaryText = {data.msg}
            />)
        }


        
        items.push(
            <div>
                <List 
                id = {data.listSeq}
                key = {data.listSeq}
                subheader={data.listSeq.toString() + "." + data.title.toString()} 
                subheaderStyle = {this.props.subheaderStyle} 
                >
                
                {sub_item} 

                </List>
                <div style={{height: "50px"}} />
                {/* <Divider inset={true} /> */}
            </div>
            
        )
        
    }

    return <div>{items}</div>;
  },

  

  render: function() {
    var self = this;
    let {
    //   id,
    //   height,
    //   show,
    //   selectable,
    //   multiSelectable,
      list,
    //   template,
    //   assignPage,
    //   contextMenu,

    //   content,

      ...other
    } = this.props;

    let List = this._genList(list);

      return (
        <div style={{height:'100%'}}>
          {List}
        </div>)
    
    // return null;
  }
});

module.exports = DynList;
