// var React = require('react');
// var mui = require('material-ui');
// let TextField = mui.TextField;
// let RaisedButton = mui.RaisedButton;
// let Paper = mui.Paper;
// let FontIcon = mui.FontIcon;
// let Colors = mui.Styles.Colors;
// var PasswordActions = require('../actions/PasswordActions.js');
// var PasswordStore = require('../stores/PasswordStore.js');
// var AppStore = require('../stores/AppStore.js');

// import appTheme from '../theme/appBaseTheme.js';
// let Typography = mui.Styles.Typography;

// var ForgotPassword = React.createClass({
//     childContextTypes: {
//         muiTheme: React.PropTypes.object
//     },

//     getChildContext() {
//         return {
//             muiTheme: appTheme.getTheme()
//         }
//     },

//     propTypes: {
// 		userCode: React.PropTypes.string
// 	},
//   componentDidMount() {
//     PasswordStore.addChangeListener(this.onChange);
//   },

//   componentWillUnmount: function() {
//     PasswordStore.removeChangeListener(this.onChange);
//   },
//     getInitialState: function() {
//         return {
//             data_001: "",
//             data_002: "",
//             result:[PasswordStore.getResult()],
//             showDialog : false
//         }
//     },
//     gettingContent: false,
//     onChange() {
//       this.setState({
//         result: PasswordStore.getResult(),
//         template : PasswordStore.getTemplate()
//       });
//     },
//     handleChange: function(e, index, value) {
//         this.setState({value:value});
//     },

//     render() {
//         var self = this;
//         let theme=this.getChildContext().muiTheme;
//         var message = null;
//         var title = null;
//         var subtitle = null;
//         var icon = null;
//         var textfield1 = null;
//         var textfield2 = null;
//         var btn1 = null
//         var pop = null;

//         let style = {
//            width: '500px',
//            textAlign: 'center'
//         };
//         let titleStyle= {
//             fontSize: 25,
//             paddingTop: '32px',
//             paddingBottom: '20px',
//         };
//         let subTitleStyle= {
//             fontSize: 20,
//             paddingTop: '8px',
//             color: Typography.textLightBlack,
//         };
//         let messageStyle = {
//             fontSize: 16,
//             color: 'red',
//             width: '100%'
//         }
//         let pCenterStyle={
//             zIndex:'0'
//         };
//         let inputStyle= {
//             margin: '0 auto',
//             display: 'table',
//         }
//         let imageStyle= {
//           margin: 'auto',
//           width: '120px',
//           height: 'auto',
//           marginTop: '32px',
//         };
//         let textFieldStyle= {
//             width:'350px'
//         };
//         let buttonStyle={
//             float:'top' ,
//             paddingTop:'25px',
//             paddingBottom:'25px'
//         };
//         let popStyle={
//           borderRadius: '15px',
//           position: 'absolute',
//           paddingTop: '25px',
//           paddingBottom: '25px',
//           right: '-380px',
//           width: '380px',
//           background: '#f0f3f5',
//           bottom: '50px',
//           color: 'rgba(0, 0, 0, 0.54)',
//           fontSize: '20px',
//           lineHeight: '30px',
//           paddingLeft: '25px',
//           paddingRight: '25px'
//         };

//         if(this.state.template == null){
//           if (!this.gettingContent) {
//             this.gettingContent = true;
//             PasswordActions.sendcheckPassword("", "", window.location.href.split('req=')[1]);
//           }
//           return <div></div>;
//         } else {
//           title = (<div style={titleStyle}>{this.state.template.title}</div>);
//           subtitle = (<div style={subTitleStyle}>{this.state.template.subtitle}</div>);

//           if(this.state.result.code == "0"){
//             icon = (<img style={imageStyle} src= {'../web/img/success_icon.png'}/>);
//             textfield1 = (<br/>);
//             textfield2 = (<br/>);
//             btn1 = (
//               <div style={buttonStyle}>
//                 <RaisedButton
//                   label={this.state.template.btn}
//                   primary={true}
//                   onTouchTap={self.loginPage} />
//               </div>);
//           }else{
//             icon = (<img style={imageStyle} src= {'../web/img/reset_icon.png'}/>);
//             textfield1 = (
//               <div style={inputStyle}>
//                   <TextField
//                       label={this.state.template.floatPW}
//                       key="password"
//                       ref="password"
//                       type="password"
//                       hintText={this.state.template.hintsPW}
//                       hintStyle = { {top:'28px', lineHeight:'24px'} }
//                       floatingLabelText={this.state.template.floatPW}
//                       floatingLabelStyle = { {top:'28px', lineHeight:'24px'} }
//                       underlineStyle = { {bottom:'22px'} }
//                       onChange={self.textchange}
//                       style={textFieldStyle} />
//               </div>
//             );
//             textfield2 = (
//               <div style={inputStyle}>
//                   <TextField
//                       label={this.state.template.floatRPW}
//                       key="rePassword"
//                       ref="rePassword"
//                       type="password"
//                       hintText={this.state.template.hintsRPW}
//                       hintStyle = { {top:'28px', lineHeight:'24px'} }
//                       floatingLabelText={this.state.template.floatRPW}
//                       floatingLabelStyle = { {top:'28px', lineHeight:'24px'} }
//                       underlineStyle = { {bottom:'22px'} }
//                       onChange={self.textchange}
//                       style={textFieldStyle} />

//               </div>
//             );
//             btn1 = (
//               <div style={buttonStyle}>
//                   <RaisedButton
//                       label={this.state.template.btn}
//                       primary={true}
//                       onTouchTap={self.handleSend}
//                       disabled={!(this.state.result.pwLength&&this.state.result.pwLetter&&this.state.result.pwNumeric&&this.state.result.pwHistory&&this.state.result.pwUser&this.state.result.pwMatch)}/>
//               </div>
//             );

//             if (this.state.result.message) {
//                 message = <span style={messageStyle}>{this.state.result.message}</span>
//             }

//             let checkIcon = (<FontIcon className="material-icons" color={Colors.green500}>check_circle</FontIcon>);
//             let cancelIcon = (<FontIcon className="material-icons" color={Colors.red500}>cancel</FontIcon>);

//             if(this.state.showDialog){
//               pop = (
//                 <div style={popStyle}>
//                     { (this.state.result.pwLength) ? (<img src= {'../web/img/check_icon.png'}/>) : (<img src= {'../web/img/cancel_icon.png'}/>)} {this.state.template.popLength}<br/>
//                     { (this.state.result.pwLetter) ? (<img src= {'../web/img/check_icon.png'}/>) : (<img src= {'../web/img/cancel_icon.png'}/>)} {this.state.template.popChar}<br/>
//                     { (this.state.result.pwNumeric) ? (<img src= {'../web/img/check_icon.png'}/>) : (<img src= {'../web/img/cancel_icon.png'}/>)} {this.state.template.popNum}<br/>
//                     { (this.state.result.pwHistory) ? (<img src= {'../web/img/check_icon.png'}/>) : (<img src= {'../web/img/cancel_icon.png'}/>)} {this.state.template.popHistory}<br/>
//                     { (this.state.result.pwUser) ? (<img src= {'../web/img/check_icon.png'}/>) : (<img src= {'../web/img/cancel_icon.png'}/>)} {this.state.template.popPattern}<br/>
//                   { (this.state.result.pwMatch) ? (<img src= {'../web/img/check_icon.png'}/>) : (<img src= {'../web/img/cancel_icon.png'}/>)} {this.state.template.popMatch}<br/>
//                 </div>
//               ) ;
//             }
//           }



//           return (
//               <div>
//                   <div className="pCenter" style={pCenterStyle} >
//                       <div style={{float:'top', position:'relative'}}>
//                           <Paper zDepth={0} style={style}>



//                               {title}
//                               {subtitle}
//                               <br/>
//                               <br/>
//                               {icon}
//                               {textfield1}
//                               <input name="a" className="hidden" type="password"/>
//                               {textfield2}
//                               <input name="a" className="hidden" type="password"/>
//                               {message}
//                               {btn1}
//                           </Paper>
//                         {pop}

//                       </div>

//                   </div>
//               </div>
//           );
//       }
//     },

//     handleSend() {
//       var password = this.refs.password.getValue();
//       var confrimCassword = this.refs.rePassword.getValue();
//       PasswordActions.sendNewPassword(password, confrimCassword, window.location.href.split('req=')[1]);
//     },

//     loginPage() {
//         document.location="../";
//     },

//     onChangeData001(e){
//       this.setState({ data_001: e.target.value});
//     },

//     onChangeData002(e){
//       this.setState({ data_002: e.target.value});
//     },

//     textchange(){
//       this.state.showDialog = true;
//       PasswordActions.sendcheckPassword(this.refs.password.getValue(), this.refs.rePassword.getValue(),  window.location.href.split('req=')[1]);
//     }

// });

// module.exports = ForgotPassword;
