let React = require("react");
let mui = require("material-ui");
let StylePropable = mui.Mixins.StylePropable;
let DynMasterTable = require("../DynamicUI/DynMasterTable.jsx");
let NeedStore = require('../../stores/NeedStore.js');
let NeedActions = require('../../actions/NeedActions.js');
let ContentStore = require('../../stores/ContentStore.js');

let Needs = React.createClass({
  mixins: [React.LinkedStateMixin, StylePropable],

  propTypes: {

  },

  componentDidMount() {
    ContentStore.addChangeListener(this.onChange);

    if (this.state.content == null || this.state.content.template == null || this.state.content.values == null)
      this._reload();
  },

  componentWillUnmount: function() {
    ContentStore.removeChangeListener(this.onChange);
  },

  componentDidUpdate() {
    if (this.state.content == null || this.state.content.template == null || this.state.content.values == null)
      this._reload();
  },

  _reload() {
    if (!this.gettingContent) {
      this.gettingContent = true;
      NeedActions.getNeedList();
    }
  },

  gettingContent: false,
  onChange() {
    this.gettingContent = false;
    this.setState({
      content: ContentStore.getPageContent(),
    });
  },

  getInitialState() {
    return {
      content: ContentStore.getPageContent(),
    };
  },

  handleTapRow(e, row) {
    var needCode = this.state.content.values.list[row].need_code;
    var channelCode = this.state.content.values.list[row].channel_code;

    NeedActions.getNeedDetail(needCode, channelCode);
  },

  handleRowSelection(rows) {
    var content = this.state.content;
    var statusList = [];

    if (rows == 'all'){
      for (let i = 0; i < content.values.list.length; i++) {
        var status = content.values.list[i].status_code;

        if (status != undefined)
          statusList.push(status);
      }
    } else if (rows.length > 0) {
      for (let i = 0; i < rows.length; i++) {
        if (content.values.list[rows[i]] != undefined)
          statusList.push(content.values.list[rows[i]].status_code);
      }
    }

    var action;
    var hasTentative = statusList.indexOf("T") != -1 ? true : false;
    var hasEffective = statusList.indexOf("E") != -1 ? true : false;

    if (statusList.length == 1 && !hasTentative && hasEffective)
      action = 2;
    else if(!hasTentative && hasEffective)
      action = 3;
    else
      action = 1;

    if (rows != 'none' && statusList.length > 0)
      return action - 1;
    else
      return 0;
  },

  render() {
    var windowHeight = window.innerHeight;
    var content = this.state.content;
    var type_list = NeedStore.getTypeList();
    var status_list = NeedStore.getStatusList();

    if (content == null)
      this.state.content = content = ContentStore.getPageContent();

    if(content == null || content.template == null){
      return <div></div>;
    } else {
      return(
        <div className="PageContent">
          <DynMasterTable
            id = {'needs'}
            list = {content.values.list}
            total = {content.values.total}
            template = {content.template}
            show = {true}
            multiSelectable = {true}
            height = { (windowHeight - 170) + "px"}
            handleTapRow = {this.handleTapRow}
            handleRowSelection = {this.handleRowSelection} />
        </div>
      );
    }
  }
});

module.exports = Needs;
