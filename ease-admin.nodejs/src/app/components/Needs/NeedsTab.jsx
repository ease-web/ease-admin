var React = require('react');
let mui = require('material-ui');
let Paper = mui.Paper;

var ContentStore = require('../../stores/ContentStore.js');
let NeedDetailStore = require('../../stores/NeedDetailStore.js');
let DialogActions = require('../../actions/DialogActions.js');
let DynConfigPanelActions = require('../../actions/DynConfigPanelActions.js');

let NeedPageMinix = require('./NeedPageMinix.js');
let ItemMinix = require('../../mixins/ItemMinix.js');

import IconButton from 'material-ui/lib/icon-button';
import ChevronLeft from 'material-ui/lib/svg-icons/navigation/chevron-left';
import ChevronRight from 'material-ui/lib/svg-icons/navigation/chevron-right';
let PaperImgSet = require('../../components/CustomView/PaperImgSet.jsx');

let sectionId="NeedsTab";
let assmtIds=["finanEval","needsAnal", "riskAssess"];
let itemIds=["title","content", "button"];
var isClickedPaper=false;

let HFTitle = React.createClass({
	mixins: [ItemMinix],

	onUpdate(){
		this.forceUpdate();
	},


	getInitialState() {
		return {
			headingNo:0,
			displayArrowLeft:false,
			displayArrowRight:false,
		}
	},
	componentDidMount: function() {

		let self = this;
		//code for handle action of hover on arrow icons
		document.getElementById('needsLandingTitle').addEventListener("mouseover", function (e) {
			self.displayArrow(e,document.getElementById('needsLandingTitle').getBoundingClientRect());
		}, false);
		document.getElementById('needsLandingTitle').addEventListener("mousemove", function (e) {
			self.displayArrow(e,document.getElementById('needsLandingTitle').getBoundingClientRect());
		}, false);
		document.getElementById('needsLandingTitle').addEventListener("mouseout", function (e) {
			self.setState({displayArrowLeft:false, displayArrowRight:false})
		}, false);
	},

	displayArrow(e,box){

		let {
			headingNo
		} = this.state;
		this.setState({
			displayArrowLeft:(headingNo<1)?false:true,
			displayArrowRight:(headingNo>0)?false:true,
		});
	},

	shouldComponentUpdate: function(nextProps, nextState) {
    if( this.state.headingNo!=nextState.headingNo){
      return true;
    }
		if( this.state.displayArrowLeft!=nextState.displayArrowLeft){
			return true;
		}
		if( this.state.displayArrowRight!=nextState.displayArrowRight){
			return true;
		}
		return false;
	},
	openHFPanel(headerName, titleId){
		let {
			changedValues
		} = this.props;

		let self = this;

		if(!checkExist(changedValues[headerName], titleId+'.en')){
			changedValues[headerName][titleId]=NeedDetailStore.genLangCodesObj();
		}

		//self.setState({cfCValues:changedValues[type]});
		//if(!checkExist(changedValues[headerName][titleId], 'label')){
		//	changedValues[headerName][titleId]['label']=(titleId=='title')?'Headline':'Description';
		//}
		let configItem=changedValues[headerName][titleId];
		let validation = function(){
			let errorMsg = false;

			if(!configItem.en)
				errorMsg= "Some mandatory data do not input yet, if you click OK button, the configuration panel would be closed.";
			if(!errorMsg){
				console.log("validate");
			}else{
				console.log(errorMsg);
				let removeDialog = {
					id: "delete",
					title: getLocalizedText("DELETE","DELETE"),
					message: errorMsg,
					negative: {
						id: "delete_cancel",
						title: getLocalizedText("BUTTON.CANCEL","CANCEL"),
						type: "button"
					},
					positive:{
						id: "delete_confirm",
						title: getLocalizedText("BUTTON.OK","OK"),
						type: "customButton",
						handleTap: function(){
							DynConfigPanelActions.closePanel(true);
						}
					}
				}
				DialogActions.showDialog(removeDialog);
			}

			return errorMsg;
		};
		DynConfigPanelActions.openPanel(configItem, 'question', { Update: self.onUpdate});
	},

	_handleOnTouchTap (headingNo){
		this.setState({headingNo:headingNo})
	},

	getTitleId(type){
		return (type=='header')?'header':'header';//TODO
	},



	render(){
		let self = this;
		let {
      changedValues,
			type,
    } = this.props;

	 	if(this.isDebug()){
			console.log("render -needsTab-title-" + type, changedValues);
		}

		let titleId = 'title';//this.getTitleId(type.toLowerCase());
		let contentId = 'content';
		let buttonId = 'button';

		let headerName=(this.state.headingNo==0?'learnMore':'recommendProducts');

		let title = (checkExist(changedValues, headerName+"."+titleId+'.en') && !isEmpty(changedValues[headerName][titleId].en))?
				multilineText(changedValues[headerName][titleId].en):
				getLocalizedText(type.toUpperCase(),"Please click here to add title"  )//TODO
			 ;
	  let content = (checkExist(changedValues, headerName+"."+contentId+'.en') && !isEmpty(changedValues[headerName][contentId].en))?
				multilineText(changedValues[headerName][contentId].en):
				getLocalizedText(type.toUpperCase(),"Please click here to add content" )//TODO
			 ;
		let button =( headerName=="learnMore")? getLocalizedText("LEARN_MORE","Learn More" ): getLocalizedText("RECOMMEND_PRODUCTS","Recommend Products" ) ;
	 return(
				<div style={{float:'top', width:'100%', textAlign:'center', display: 'flex', alignItems: 'center'  }}>
					<div id='needsLandingTitle' style={{height:116,display: 'flex',alignItems: 'center', width:'100%',maxWidth:'944px' , margin:'0 auto'}}>
						<div  style={{ float:'left', width:'0%'}}>
							<IconButton  onTouchTap={function(){self._handleOnTouchTap(0)}} iconStyle={{  WebkitFilter:'invert(50%)', width:36, height:36}} style={{float:'left', width:48, height:48, padding:0, display:this.state.displayArrowLeft?'':'none'}}>
							<ChevronLeft  />
							</IconButton>
						</div>
						<div style={{float:'left', width:'100%'}}>
							<div className="configurable"  style={{ cursor:'pointer', fontWeight:500, fontSize:20, height:24, marginBottom:16}} onClick={function(){self.openHFPanel(headerName, titleId)}}>

									{title}

							</div>
							<div className="configurable" style={{cursor:'pointer', color:"rgba(0, 0, 0, 0.54)", fontSize:16, height:24, marginBottom:16,fontWeight:400}} onClick={function(){self.openHFPanel(headerName, contentId)}}>

									{content}

							</div>
							<div style={{cursor:'initial',color:"rgb(38, 150, 204)", fontSize:14, height:36,fontWeight:500}} >

									{button}

							</div>
						</div>
						<div   style={{float:'left', width:'0%'}}>
							<IconButton   onTouchTap={function(){self._handleOnTouchTap(1)}}  iconStyle={{  WebkitFilter:'invert(50%)', width:36 , height:36}} style={{float:'right', width:48, height:48, padding:0,  display:this.state.displayArrowRight?'':'none'}}>
							<ChevronRight />
							</IconButton>
						</div>
					</div>
				</div>
		);
	}
});




var NeedsTab = React.createClass({
	mixins: [NeedPageMinix],

	onUpdate(){
    this.forceUpdate();
  },

	//close color picker by clicking whiteSapce
	handleClick(){
		if(!isClickedPaper){
			this.refs.paper1.handleClose();
			this.refs.paper2.handleClose();
			this.refs.paper3.handleClose();

		}else
			isClickedPaper=false;
	},


	getInitialState() {
		let self = this;
		var content = ContentStore.getPageContent();
		var sectionId = NeedDetailStore.getCurrentPage();
		var {
			//template,
			values,
			changedValues,
			template
		} = content;

		if(!values[sectionId]){
			values[sectionId]={};
		}

		if(!isEmpty(values) && isEmpty(changedValues)){
			changedValues = content.changedValues = cloneObject(values);
		}

		if(!changedValues[sectionId]){
			changedValues[sectionId]={};
		}

		let validation = function(changedValues){
			let error = false;
			if(changedValues){

			}
			if(error){
				return "Please Enter All data";
			}
		};

		if(!template.items){
			template.items = [];
		}

		let foundTemplate = false
		for(let j in template.items){
			if(template.items[j].id == sectionId){
				template.items[j].validate = validation;
				let foundTemplate = true;
			}
		}

		if(!foundTemplate){
			template.items.push({id:sectionId, validate:validation});
		}


		return {
			values: values[sectionId],
			changedValues: changedValues[sectionId],
			isLock: NeedDetailStore.isLock(),
			sectionId: sectionId,
			validation: validation,
			assmtId:"",
			itemId:"",
		}
	},

	toggle(itemId,assmtId){
		this.setState({assmtId:"", itemId:itemId});
		let {
			headingNo,
			changedValues
		} = this.state;
		let headerId = (headingNo==0)?"learnMore":"recommendProducts";
		if(!checkExist(changedValues[assmtId][itemId], 'label')){
			changedValues[assmtId][itemId]['label']=(itemId=='title')?'Title':'Description';
		}
		if(assmtId){
			if(checkExist(changedValues, assmtId)){
				let configItem=changedValues[assmtId][itemId];


				let validation = function(){
					let errorMsg = false;

					if(!configItem.en)
						errorMsg= "Some mandatory data do not input yet, if you click OK button, the configuration panel would be closed.";
					if(!errorMsg){
						console.log("validate");
					}else{
						console.log(errorMsg);
						let removeDialog = {
							id: "delete",
							title: getLocalizedText("DELETE","DELETE"),
							message: errorMsg,
							negative: {
								id: "delete_cancel",
								title: getLocalizedText("BUTTON.CANCEL","CANCEL"),
								type: "button"
							},
							positive:{
								id: "delete_confirm",
								title: getLocalizedText("BUTTON.OK","OK"),
								type: "customButton",
								handleTap: function(){
									DynConfigPanelActions.closePanel(true);
								}
							}
						}
						DialogActions.showDialog(removeDialog);
					}

					return errorMsg;
				};

				DynConfigPanelActions.openPanel(configItem, 'question',  {});
			}
		}else{
			if(checkExist(changedValues, headerId)){
				DynConfigPanelActions.openPanel(changedValues[headerId][itemId], 'question');
			}
		}
	},
	openCfPanel( refNo,clickItem){
		let self = this;
		var {
			//template,
			changedValues
		} = self.state;

		var itemId=itemIds[clickItem]?itemIds[clickItem]:"";
 		self.toggle(itemId,assmtIds[refNo]);
		var assmtId=	assmtIds[refNo]?	assmtIds[refNo]:null;

		this.setState({assmtId:assmtId})
	},

	//do nothing if paper is clicked
	handleIsClickedPaper(){
		isClickedPaper=true;
	},



	render: function() {
		let imageStyle = {
      width: '120px',
      height: 'auto',
      marginBottom: '32px',
			display: 'flex',
			flexDirection: 'column',
    };

		let textStyle = {
			width: '50%',
			textAlign: 'center',
		};

		const style = {
		  height: 100,
		  width: 100,
		  margin: 20,
		  textAlign: 'center',
		  display: 'inline-block',
		};

		var bgColors=["rgb(230, 226, 223)", "rgb(209, 220, 230)","rgb(191, 207, 200)" ];
		var cnt=0;

		var self = this;
    let containerStyle = {
      flex: 1,
      position:'relative'
    };

    let {
      values,
      changedValues,
			headingNo,
			assmtId,
			displayArrowLeft,
			displayArrowRight
    } = this.state;

		let headerId = (assmtId!="")?assmtId: (headingNo==0)?"learnMore":"recommendProducts";

		var needsLandingTitle=null;
		var needsLandingContent=null;
		if(changedValues){
			if(checkExist(changedValues, 'needsLandingTitle.'+headingNo))
					needsLandingTitle=changedValues.needsLandingTitle[headingNo];
			if(checkExist(changedValues, 'needsLandingContent'))
					needsLandingContent=changedValues.needsLandingContent;
		}


		return (
			<div  onClick={ this.handleClick } style={{height:'calc(100vh - 56px)'}} >
				<div style={{ height:32}}/>
					<HFTitle changedValues={changedValues } type={'header'}/>
				<div style={{ height:16}}/>

				<div    id='paperArea'  style={{width:'100%' , textAlign:'center'}}>
					  <div style={{display: 'table', margin: '0 auto'}}>
						 {!needsLandingContent?null:
							 		needsLandingContent.map(function(landingContent ,cnt){
								  cnt++;
									var name=assmtIds[cnt-1]?assmtIds[cnt-1]:null;
									let _name = {}
									if(name && changedValues){
										_name = changedValues[name];
									}

									var dfTitle=""
									if(cnt==1)
										dfTitle="Financial Evaluation";
									else if(cnt==2)
									 	dfTitle="Needs Analysis"
									else if(cnt==3)
										dfTitle="Risk Assessment"
									let cTitle = (checkExist(_name, 'title'))?_name.title.en:dfTitle;
									let cContent = (checkExist(_name, 'content'))?_name.content.en:'Description';
									let cButton = (checkExist(_name, 'button'))?_name.button.en:'';

								 return(
									 <div key={name+'-paper'}  style={{float:'left',width:304, height: 392, marginLeft:'8px', marginRight:'8px'}}>
													<PaperImgSet
													 ref={"paper"+cnt}
													 key={name+'-paper-imageset'}
													 parent={sectionId}
													 id={name}
							 						 type = {"cover"}
							 						 title ={cTitle}
							 						 desc = {cContent}
													 startBtn={cButton}
													 color= {""}
							 						 button = { "DELETE"}
							 						 buttonAdd = {"ADD"}
							 		         require={{
							 		           width: 304,
							 		           height: 228,
							 		           fileType:'image/png',
							 		           sizeLimit:1*1048576,// 1MB:2MB in byte

							 	           }}
													 bgColor={checkExist(changedValues, name)? changedValues[name].bgColor :"red" }
													 openCfPanel={self.openCfPanel}
													 handleIsClickedPaper={self.handleIsClickedPaper}
													 refNo={cnt-1}
													 isLocked={NeedDetailStore.isLock()}

							 	           />
									 </div>
								 )
							 })
						 }
						</div>
				</div>
			<div>
		</div>
	</div>


		);
	},

	onChange() {
		var title = ContentStore.getPageContent().title;
		var error = ContentStore.getPageContent().error;

		if (title || error) {
			this.setState({
				title: title,
				error: error
			});
		}
	}
});

module.exports = NeedsTab;
