let React = require('react');
let mui = require('material-ui');
let AppStore = require('../../stores/AppStore.js');
let ContentStore = require('../../stores/ContentStore.js');
let NeedDetailStore = require('../../stores/NeedDetailStore.js');
let DialogActions = require('../../actions/DialogActions.js');
let MenuActions = require('../../actions/MenuActions.js');
let DynConfigPanelActions = require('../../actions/DynConfigPanelActions.js');
let NeedActions = require('../../actions/NeedActions.js');
let SortableList = require('../CustomView/SortableList.jsx');
let SelectionBoxSection = require('../CustomView/SelectionBoxSectionCS.jsx');
import AddCircleOutline from 'material-ui/lib/svg-icons/content/add-circle-outline';
let StylePropable = mui.Mixins.StylePropable;
let RaisedButton = mui.RaisedButton;
let Toolbar = mui.Toolbar;
let ToolbarGroup = mui.ToolbarGroup;
let ToolbarTitle = mui.ToolbarTitle;
let FloatingActionButton =mui.FloatingActionButton;
import ContentAdd from 'material-ui/lib/svg-icons/content/add';
var AppBarActions = require('../../actions/AppBarActions.js');
import Add from 'material-ui/lib/svg-icons/content/add';

let Tabs = mui.Tabs;
let Tab = mui.Tab;

let sectionId = 'NeedsConceptSelling';
let _isMount = false;

let debug=false;
let HFTitle = React.createClass({
	openHFPanel(titleId){
		let {
			changedValues,
			langCode
		} = this.props;

		let self = this;
		let onUpdate = function(){
			self.forceUpdate();
		};

		if(!checkExist(changedValues, titleId+'.en')){
			changedValues[titleId]=NeedDetailStore.genLangCodesObj();
		}

		if(!checkExist(changedValues[titleId], 'label')){
			changedValues[titleId]['label']='Category';
		}
		let selectedOption=self.getSelectedOption();
 		let defaultTitle=selectedOption?selectedOption['title'][this.props.langCode]:'defaultTitle';


		if(changedValues[titleId].en=='')
			changedValues[titleId].en=defaultTitle;

		let configItem=changedValues[titleId];
		let validation = function(){
      let errorMsg = false;

      if(!configItem.en)
        errorMsg= "Please input the mandatory field(s).";
      if(!errorMsg){
        console.log("validate");
      }else{
        console.log(errorMsg);
        let removeDialog = {
          id: "delete",
          title: getLocalizedText("ALERT","ALERT"),
          message: errorMsg,
          positive:{
            id: "delete_confirm",
            title: getLocalizedText("BUTTON.OK","OK"),
            type: "customButton",
            handleTap: function(){
              if(changedValues[titleId].en)
              	DynConfigPanelActions.closePanel(true);
            }
          }
        }
         DialogActions.showDialog(removeDialog);
      }

      return errorMsg;
    };

		DynConfigPanelActions.openPanel(changedValues, 'ConceptSelling', {validation : validation,  onUpdate:onUpdate});

	},

	getSelectedOption(){
		let content =ContentStore.getPageContent();
			let ncs=checkExist(content.changedValues,'NeedsConceptSelling')? content.changedValues.NeedsConceptSelling:'';
			let options = ncs.pages;
			let targetId='';
			if(ncs && ncs.pages[ncs.selectedPage]){
				targetId = ncs.pages[ncs.selectedPage].id;
			}
			for(let j in options){
				if(options[j].id ==targetId ){
					return options[j];
				}
			}
			return null;
	},

	deleteCategory(titleId){
		let self=this;


			let removeDialog = {
				id: "delete",
				title: getLocalizedText("DELETE","DELETE"),
				message: getLocalizedText("DELETE_ITEM","Delete this item?"),
				negative: {
					id: "delete_cancel",
					title: getLocalizedText("BUTTON.CANCEL","CANCEL"),
					type: "button"
				},
				positive:{
					id: "delete_confirm",
					title: getLocalizedText("BUTTON.OK","OK"),
					type: "customButton",
					handleTap: function(){
						let content=ContentStore.getPageContent();
						let ncs=checkExist(content.changedValues,'NeedsConceptSelling')? content.changedValues.NeedsConceptSelling:'';
						let options=ncs.pages;
						let targetId='';
						let selectedPage=ncs.selectedPage;

						if(ncs && options[selectedPage]){
							targetId =options[selectedPage].id;
						}
						for(let j in options){
							if(options[j].id ==targetId){
								//ContentStore.getPageContent().template.menu[1][0].items[0].splice(Number(j ),1);
								options.splice(Number(j),1);
								for(let k in options){
									options[k].seq = (Number(k)+1)*100;
								}
							}

						}

						ncs.selectedPage=Math.min(selectedPage, options.length-1);
						AppBarActions.submitChangedValues('/Needs/Detail/Save');
					}
				}
			}

			DialogActions.showDialog(removeDialog);

			this.forceUpdate();




	},

	render(){
		let self = this;
		let {
      changedValues,
			type,
			langCode,
    } = this.props;


		if(debug){
			console.log("render -NeedSelection-title-" + type, changedValues);
		}

		let titleId = type;
		let selectedOption=self.getSelectedOption();
		let defaultTitle=selectedOption?selectedOption.title.en:'defaultTitle';//TODO: depend on system language setting


		let title = (checkExist(changedValues, titleId+'.en') && !isEmpty(changedValues[titleId].en))?
				multilineText(changedValues[titleId].en):
				<span style={{color:'(0, 0, 0, 0.298039)'}}>
					{getLocalizedText(type.toUpperCase(),defaultTitle)}
				</span>;

		return(
			<div style={{textAlign:'center',fontSize:'16px',paddingBottom:'8px',paddingTop:'32px',height:100, }} >
				<div style={{'float':'left', paddingLeft:10, fontWeight:600, fontSize:18}}>
					{title}
				</div>
				<div style={{'float':'right'}}>
					<RaisedButton label="EDIT CATEGORY" onTouchTap={function(){self.openHFPanel(titleId)}}  style={{marginRight:10}}labelStyle={{color:'green'}}/>
					<RaisedButton label="DELETE" onTouchTap={function(){self.deleteCategory(titleId)}}  style={{marginRight:10}} labelStyle={{color:'red '}} />
				</div>
			</div>
		);
	}
});

let NeedsConceptSelling = React.createClass({
	mixins: [React.LinkedStateMixin, StylePropable],

	componentDidMount: function() {
		_isMount = true;
		 ContentStore.addChangeListener(this.onContentChange);
		 //this.refs.HFTitle.openHFPanel("header");

  },

  componentWillUnmount: function() {
		_isMount = false;
		 ContentStore.removeChangeListener(this.onContentChange);
  },


	onContentChange() {
			var content = ContentStore.getPageContent();
			var {
				values,
				changedValues
			} = content;
			let ncs=changedValues[sectionId];
			let selectedPage=Math.min(ncs.selectedPage, ncs.pages.length-1);
			ncs.selectedPage=selectedPage;
			let cValues = ncs.pages[selectedPage];//?changedValues[sectionId].pages[changedValues[sectionId].selectedPage]:changedValues[sectionId].pages[0];
			let items=content.template.menu[1][0].items;

			if(_isMount)
				this.setState({
					values: values[sectionId],
					changedValues: cValues
				});

	},

	getInitialState() {


		var content = ContentStore.getPageContent();
		var {
			//template,
			values,
			changedValues
		} = content;

		if(!values.selections){
			values.question = [];
		}

		if(!isEmpty(values) && isEmpty(changedValues)){
			changedValues = content.changedValues = cloneObject(values);
		}

		if(!changedValues[sectionId]){
			changedValues[sectionId]={
				selections:[]
			};
		}else if(!changedValues[sectionId].selections){
			changedValues[sectionId].selections = [];
		}

		let cValues = changedValues[sectionId].pages[changedValues[sectionId].selectedPage];

		let items=content.template.menu[1][0].items;

		return {
			isLock: NeedDetailStore.isLock(),
			values: values,
			changedValues: cValues,
			langCode:'en',
		}
	},

	getStyles(){
		return(
				{
					contentStyle:{
							width:'100%',
							//height: 'calc(100vh - 90px)',//58+32
							position: 'relative',
							overflowY: 'hidden'
					},
					listStyle:{
					  minHeight: 'calc(100vh - 232px)'
					},
					placeholderStyle:{
						fontStyle: 'italic',
						color: '#AAAAAA'
					}
				}
		)

	},
	handleTabsChange(value){
		if(NeedDetailStore.getLangCodes().indexOf(value)>-1){
			let cVal=this.state.changedValues;
			if(checkExist(cVal,'selections')&& checkExist(cVal.selections,'tabId')){
				this.state.changedValues.selections.tabId=value;
			}
		}
	},



	getNextId(selections){
		let self=this;
		let nextId = "r1";
		//get the latest id
		for(let i in selections){
			let _id = selections[self.state.langCode][i].id;
			if(_id){
				_id = _id.substring(1, _id.length);
				if(isNumeric(_id) && Number(_id) >= Number(nextId.substring(1, nextId.length)))
					nextId= "r"+(Number(_id)+1);
			}
		}
		return nextId;
	},

	render: function() {

		var self = this;
		let styles = this.getStyles();




    let {
      values,
      changedValues,
			deleteAction
    } = this.state;

		let tabSection=[];
		let langCodeArray=NeedDetailStore.getLangCodes();
		let handleActive=function(tab){
			self.setState({langCode:tab.props.route});
		};
		if(!changedValues)
			return (
				<div style={{height:'calc(100vh - 56px)', width:'calc(100vw )' , display:'table-cell', verticalAlign:'middle', textAlign:'center'}}>
						No Category
				</div>
			);
		for(var i =0 ; i < langCodeArray.length; i++){
			let langCode=langCodeArray[i];

			let pages=changedValues;
			if(!checkExist(changedValues, 'selections')){
				changedValues.selections = {};
			}
			if(!checkExist(changedValues.selections, langCode)){
				changedValues.selections[langCode] = {};
			}
			if(!checkExist(changedValues.selections[langCode], 'options')){
				changedValues.selections[langCode].options = [];
			}

			let selection = changedValues.selections[langCode]	;
			tabSection.push(
				 <Tab style={{width:'16.67%'}} value={langCode} label={getLocalizedText("LANG."+langCode.toUpperCase(),"LANG."+langCode.toUpperCase())} onActive={handleActive}  route= {langCode} >
					 <div  style={{textAlign:'center'  }}>
						 <div  style={{textAlign:'left' }} >
							 <SelectionBoxSection
								 parent={self}
									changedValues={selection}
									langCode={langCode}

									isSortable={true}
									sectionId={sectionId}
									disabled={this.state.isLock}/>
						 </div>
					 </div>
				 </Tab>

			);
		}
		let selections=checkExist(changedValues, 'selections')?changedValues.selections:'';
		if(selections&&!checkExist(selections, 'tabId'))
			selections.tabId=langCodeArray[0];

		if(debug && changedValues){
				console.log("NeedsConceptSelling - changedValues.selections", changedValues.selections);
		}
		return (
			<div key={"nedsSelection-content"} style={styles.contentStyle}>
					<div style={{height:'calc(100vh - 58px)', overflowY: 'auto'}}>
						<HFTitle ref='HFTitle' changedValues={changedValues} type="title" langCode={this.state.langCode}/>
						<Tabs style={{width:'100%', height:'calc(100vh - 58px - 41px - 60px)'}}onChange={this.handleTabsChange}  value={selections.tabId}>
							{tabSection}
						</Tabs>
					</div>
			</div>
		);
	},
});

module.exports = NeedsConceptSelling;
