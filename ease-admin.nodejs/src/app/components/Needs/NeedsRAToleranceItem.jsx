let React = require('react');
let mui = require('material-ui');

let ContentStore = require('../../stores/ContentStore.js');
let NeedDetailStore = require('../../stores/NeedDetailStore.js');

let DialogActions = require('../../actions/DialogActions.js');
let DynConfigPanelActions = require('../../actions/DynConfigPanelActions.js');

let ItemMinix = require('../../mixins/ItemMinix.js');

let Toolbar = mui.Toolbar;
let ToolbarGroup = mui.ToolbarGroup;
let ToolbarTitle = mui.ToolbarTitle;
let FontIcon = mui.FontIcon;
let TextField = mui.TextField;

let bgcolor = '#FAFAFA';

let RiskLevel = React.createClass({
  mixins: [ItemMinix],

	onUpdate(){
    this.forceUpdate();
  },

  openPanel(){
		DynConfigPanelActions.openPanel(this.props.changedValues, 'question', {onUpdate: this.onUpdate});
	},

  render: function(){
		let self = this;
		let {
			changedValues
		} = this.props;

    if(this.isDebug()){
      	console.log("NRATItemRow-render-level", changedValues);
    }

		return(
			<div className="configurable" style={{fontSize: '20px', paddingBottom: '12px', cursor: 'pointer'}} onClick={this.openPanel}>
			     {!isEmpty(changedValues.en)?changedValues.en:"Add Title"}
			</div>
		);
	}
});

let RiskDesc = React.createClass({
  mixins: [ItemMinix],

	onUpdate(){
    this.forceUpdate();
  },

  openPanel(){
		DynConfigPanelActions.openPanel(this.props.changedValues, 'question', {onUpdate: this.onUpdate});
	},

  render: function(){
		let self = this;
		let {
			changedValues
		} = this.props;

    if(this.isDebug()){
        console.log("NRATItemRow-render-desc", changedValues);
    }

		return(
			<div className="configurable" style={{fontSize: '14px', paddingTop: '12px', paddingBottom: '12px', cursor: 'pointer', color: '#BBBBBB'}} onClick={this.openPanel}>
			     {!isEmpty(changedValues.en)?changedValues.en:"Add Description"}
			</div>
		);
	}
});


let RiskScore = React.createClass({
  mixins: [ItemMinix],

  getInitialState() {
		return {
      error: false
    }
	},

	onUpdate(){
    this.forceUpdate();
  },

  removeItem(){
		let self = this;
		let {
			index,
			changedValues,
			onListUpdate
		} = this.props;

		let removeDialog = {
			id: "delete",
			title: getLocalizedText("DELETE","DELETE"),
			message: getLocalizedText("DELETE_ITEM","Delete this item?"),
			negative: {
				id: "delete_cancel",
				title: getLocalizedText("BUTTON.CANCEL","CANCEL"),
				type: "button"
			},
			positive:{
				id: "delete_confirm",
				title: getLocalizedText("BUTTON.OK","OK"),
				type: "customButton",
				handleTap: function(){
					changedValues.splice(index, 1);
					for(let j in changedValues){
						changedValues[j].seq = Number(j+1)*100;
					}
					onListUpdate();
				}
			}
		}

		DialogActions.showDialog(removeDialog);

	},

  requestChange(type, value){
    let {
      index,
      maxIndex,
      changedValues,
      onListUpdate
    } = this.props;

    changedValues[index][type] = Number(value);
    this.state.error=(Number(value) < Number(changedValues[index]['scoreFrom']))?true:false;

    if(index!=maxIndex && value){
      changedValues[index+1]['scoreFrom'] = Number(value)+1;
    }
    if(this.isMount()) onListUpdate();
  },


  render: function(){
		let self = this;
		let {
      index,
      maxIndex,
			changedValues
		} = this.props;


    if(this.isDebug()){
        console.log("NRATItemRow-render-desc", changedValues);
    }

    let isLock = NeedDetailStore.isLock();

		return(
      <div style={{textAlign: 'right', paddingTop: '12px', paddingBottom: '12px', position: 'relative'}}>
  			<div onClick={this.openPanel}>
          <span style={{paddingRight: '8px', fontSize: '16px'}}> From</span>
          <TextField
            style={{width:'48px', height:'32px', display: 'inline-block', cursor: 'pointer'}}
            inputStyle={{height:'initial'}}
            hintText="Score"
  					type="number"
  					disabled={true}
            value={changedValues[index].scoreFrom}/>
          <span style={{paddingLeft:'8px', paddingRight: '8px', fontSize: '16px'}}> To</span>
            <TextField
              style={{width:'48px', height:'32px', display: 'inline-block', cursor: 'pointer'}}
              inputStyle={{height:'initial'}}
              hintText="Score"
    					type="number"
    					disabled={isLock}
              value={changedValues[index].scoreTo}
              onChange={function(e){self.requestChange('scoreTo', e.target.value)}}/>
            {isLock?null:
              <FontIcon
               style = {{padding: '0px 12px 0 12px', verticalAlign: 'middle', display: 'inline-block', cursor:'pointer'}}
               color = {"#AAAAAA"}
               hoverColor={"#AAAAAA"}
               className="material-icons drag-area"
               onClick={this.removeItem}>
                 delete
             </FontIcon>
            }
  			</div>
        <div style={{position: 'absolute', right: '16px', bottom: '0px', color: '#FF0000'}}>
          {this.state.error?getLocalizedText("NEEDS_RA.SCORE_RANGE_ERROR","score to should greater or equal to score from."):''}
        </div>
      </div>
		);
	}
});


let NeedsRAToleranceItem = React.createClass({
	mixins: [ItemMinix],

	onUpdate(){
    this.forceUpdate();
  },

	propTypes:{
		index: React.PropTypes.number.isRequired,
		changedValues: React.PropTypes.array.isRequired,
		onClick: React.PropTypes.func
	},



	render: function() {

		var self = this;

		let {
      index,
      maxIndex,
      changedValues,
			onListUpdate,
			...others
		} = this.props;

    if(this.isDebug()){
      console.log("NRATItem-render", changedValues? changedValues[index]: undefined);
    }

		return (
			<div style={{marginTop: '12px', marginBottom: '12px' }} {...others}>
        <div style={{width:'calc(100% - 300px)', display: 'inline-block'}}>
          <RiskLevel changedValues={changedValues[index].riskTxt}/>
          <RiskDesc changedValues={changedValues[index].desc}/>
        </div>
        <div style={{width:'300px', display: 'inline-block'}}>
          <RiskScore changedValues={changedValues} index={index} maxIndex={maxIndex} onListUpdate={onListUpdate}/>
        </div>
			</div>
		);
	},
});

module.exports = NeedsRAToleranceItem;
