let React = require('react');
let mui = require('material-ui');

let ContentStore = require('../../stores/ContentStore.js');
let NeedDetailStore = require('../../stores/NeedDetailStore.js');

let NeedPageMinix = require('./NeedPageMinix.js');
let ItemMinix = require('../../mixins/ItemMinix.js');

let RaisedButton = mui.RaisedButton;
let Toolbar = mui.Toolbar;
let ToolbarGroup = mui.ToolbarGroup;
let ToolbarTitle = mui.ToolbarTitle;
let Paper = mui.Paper;
let DialogActions = require('../../actions/DialogActions.js');
let DynConfigPanelActions = require('../../actions/DynConfigPanelActions.js');

let sectionId = 'NeedsPrior';

let HFTitle = React.createClass({
	mixins: [ItemMinix],

	onUpdate(){
		this.forceUpdate();
	},

	openHFPanel(){
		let {
			changedValues
		} = this.props;

		let self = this;

		if(!checkExist(changedValues, 'header.en')){
			changedValues['header']=NeedDetailStore.genLangCodesObj();
			changedValues['header'].id="prior-header";
		}
		let header=changedValues['header'];
		let validation = function(){
			let errorMsg = false;

			if(!header.en)
				errorMsg= "Some mandatory data do not input yet, if you click OK button, the configuration panel would be closed.";
			if(!errorMsg){
				console.log("validate");
			}else{
				console.log(errorMsg);
				let removeDialog = {
					id: "delete",
					title: getLocalizedText("DELETE","DELETE"),
					message: errorMsg,
					negative: {
						id: "delete_cancel",
						title: getLocalizedText("BUTTON.CANCEL","CANCEL"),
						type: "button"
					},
					positive:{
						id: "delete_confirm",
						title: getLocalizedText("BUTTON.OK","OK"),
						type: "customButton",
						handleTap: function(){
							DynConfigPanelActions.closePanel(true);
						}
					}
				}
				DialogActions.showDialog(removeDialog);
			}

			return errorMsg;
		};

		//self.setState({cfCValues:changedValues[type]});
		DynConfigPanelActions.openPanel(header, 'question', {onUpdate: self.onUpdate});
	},


	render(){
		let self = this;
		let {
      changedValues
    } = this.props;

		if(this.isDebug()){
			console.log("render -NeedsPrior-title-" , changedValues);
		}

		let title = (checkExist(changedValues, 'header.en') && !isEmpty(changedValues['header'].en))?
				multilineText(changedValues['header'].en):
				<span style={{color:'(0, 0, 0, 0.298039)'}}>
					{getLocalizedText("header","Please click here to add  header")}
				</span>;

		return(
			<div
				className="configurable"
				style={{	textAlign:'center',
				fontSize:'16px',
				paddingBottom:'16px',
				paddingTop:'32px',
				cursor: 'pointer'}} onClick={function(){self.openHFPanel()}}>
				{title}
			</div>
		);
	}
});


let NeedsPriorization = React.createClass({
	mixins: [ItemMinix],

	componentDidMount: function() {
		 ContentStore.addChangeListener(this.onContentChange);
  },

  componentWillUnmount: function() {
		 ContentStore.removeChangeListener(this.onContentChange);
  },


	onContentChange() {

		var content = ContentStore.getPageContent();
		var {
			//template,
			values,
			changedValues
		} = content;

		if(this.isMount())
			this.setState({
				//template:template,
				values: values[sectionId],
				changedValues: changedValues[sectionId]
			});
	},


	getInitialState() {


		var content = ContentStore.getPageContent();
		var {
			//template,
			values,
			changedValues
		} = content;



		if(!isEmpty(values) && isEmpty(changedValues)){
			changedValues = content.changedValues = cloneObject(values);
		}


		return {
			values: values[sectionId],
			changedValues: changedValues[sectionId],
		}
	},

	getStyles(){
		return(
				{
					contentStyle:{
							width:'100%',
							height: 'calc(100vh - 90px)',//58+32
							position: 'relative',
							overflowY: 'hidden'
					},

					listStyle:{
					  minHeight: 'calc(100vh - 232px)'
					},
					placeholderStyle:{
						fontStyle: 'italic',
						color: '#AAAAAA'
					}
				}
		)

	},

	genItems(){
		let its = [];
		let self = this;

		const paperStyle = {
			width: 720,
		  height: 98,
		 	boxShadow: '0px 2px 4px #000000',
		 	backgroundColor:'#fafafa',
		};
		const imgStyle ={
			width: 147,
		  height: 98,
			float:'left',
		}

		const txtStyle={
		 height:97,
		 verticalAlign:'middle',
		 display:'table-cell',
		}

		const titleStyle={
		 fontSize:24,
		 paddingLeft:24,

		}
		const descStyle={
		 fontSize:14,
		 paddingLeft:24,

		}

		var cVal = ContentStore.getPageContent().changedValues;
		var selections = (cVal.NeedsSelection && cVal.NeedsSelection.selections)? cVal.NeedsSelection.selections : "";
		for(let i in selections){
			let selection = selections[i];
		  if(checkExist(selection, 'options')){
					for(let j in selection.options){
						let option=selection.options[j];
						let title= option.title.en;
						let desc= option.desc.en;
						let fileName=option.image;
						let image=cVal["Attachments"][fileName];
						its.push(
							<div className="drag-area">
								<Paper style={paperStyle}  >
									<img src={image} style={imgStyle}></img>
									<div style={txtStyle}>
										<font style={titleStyle}>{title?title:getLocalizedText("ICON_BOX.ADD_TITLE", "Add Title")}</font>
										<div style={{height:6}}></div>
										<font style={descStyle}>{desc?desc:getLocalizedText("ICON_BOX.ADD_DESC", "Add Description")}</font>
									</div>
								</Paper>
								<div style={{height:16}}></div>
							</div>
						);
					}
			}
		}

		return its;
	},

	render: function() {

		var self = this;
		let styles = this.getStyles();

    let {
      values,
      changedValues,
			cfValues,
			cfCValues,
			deleteAction
    } = this.state;
		if(this.isDebug() && changedValues){
			console.log("NP - changedValues.selections", changedValues.selections);
		}

		let items = this.genItems();

		const outerStyle ={
			display:'table',
		 	margin:'0 auto',
		}

		return (
			<div key={"np-content"} style={styles.contentStyle}>
				<div style={{height:'calc(100vh - 58px)', overflowY: 'auto'}}>
					<HFTitle changedValues={changedValues}/>
					<div style={outerStyle}>
				 		{items}
					 </div>
				</div>
			</div>
		);
	},
});

module.exports = NeedsPriorization;
