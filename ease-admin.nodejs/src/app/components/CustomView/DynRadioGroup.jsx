let React = require('react');
let mui = require('material-ui');
let dom = require('react-dom');
let RadioButtonGroup = mui.RadioButtonGroup;
let RadioButton = mui.RadioButton;
let DynRadioGroup = React.createClass({
  propTypes:{
    template: React.PropTypes.object.isRequired,
    id:React.PropTypes.string,
    onChange: React.PropTypes.func,
    value: React.PropTypes.string
  },

  render: function() {
    var template = this.props.template;
    var value = this.props.value;
    var style_rb = {
      fontSize:'14px',
      display: 'inline-block',
      width:'240px',
      marginRight:'20px',
      verticalAlign: 'top',
    };
    var items = template.options;
    var rg = [];
    var rbs = [];
    for (let i in items){
      var item = items[i];
      rbs.push(<RadioButton
        key={item.value}
        className="inline-blk"
        value={item.value}
        label={item.title}
        style={style_rb}/>);
    }
    return (<RadioButtonGroup
      key={template.id}
      ref={template.id}
      name={template.id}
      valueSelected={value}
      onChange={this.props.onChange}>
      { rbs }
    </RadioButtonGroup>);
  },

  getValue: function(){
    return this.refs[this.props.template.id].getSelectedValue();
  },
});
module.exports = DynRadioGroup;
