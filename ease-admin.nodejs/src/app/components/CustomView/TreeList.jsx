let React = require('react');
let mui, {Tabs, Tab, List, ListItem, FontIcon, TextField} = require('material-ui');

import NavigationExpandMore from 'material-ui/lib/svg-icons/navigation/expand-more';
import NavigationExpandLess from 'material-ui/lib/svg-icons/navigation/expand-less';
import ActionClass from 'material-ui/lib/svg-icons/action/class';
import ActionCode from 'material-ui/lib/svg-icons/action/code';
//let {NavigationExpandMore, NavigationExpandLess, ActionClass, ActionCode} = require('material-ui/lib/svg-icons');

import EABFieldBase from './EABFieldBase.js';

module.exports = React.createClass({

  contextTypes: {
    muiTheme: React.PropTypes.object,
  },
  //for passing default theme context to children
  childContextTypes: {
    muiTheme: React.PropTypes.object,
  },

  getChildContext() {
    return {
      muiTheme: this.state.muiTheme,
    };
  },

  propTypes: {
    useIcon: React.PropTypes.bool,
    style: React.PropTypes.object,
    className: React.PropTypes.string,
    tree: React.PropTypes.array,
    onTouchItemTap: React.PropTypes.func
  },

  getInitialState() {
    var tree = this.props.tree;
    for (var i in tree) {
      this.searchCriteria(tree[i], "");
    }
    return {
      criteria: "",
      tree: tree,
      useIcon: this.props.useIcon
    };
  },

  genListItem(itemList, item) {
    var self = this;
    var searching = !!this.state.criteria;
    if (!searching || item._show) {
      if (!item.items) {
          itemList.push(<ListItem
            key={"it_"+itemList.length}
            primaryText={item._primaryText}
            secondaryText={item._secondaryText}
            secondaryTextLines={item._secondaryText?2:null}
            leftIcon={this.state.useIcon?<ActionCode />:null}
            onTouchTap={function(e) {
              self.props.onTouchItemTap(this, e);
            }.bind(item)}
            />)
      } else if (item.items) {
        var subItems = [];
        for(var v in item.items) {
          this.genListItem(subItems, item.items[v]);
        }
        itemList.push(<ListItem
          key={"hd_"+itemList.length}
          primaryText={item._primaryText}
          secondaryText={item._secondaryText}
          secondaryTextLines={item._secondaryText?2:null}
          primaryTogglesNestedList={!item.value}
          leftIcon={this.state.useIcon?<ActionClass />:null}
          initiallyOpen={searching}
          onTouchTap={function(e) {
            console.log('click nested item:', e);
            if (item.value) {
              self.props.onTouchItemTap(this, e);
            }
          }.bind(item)}
          nestedItems={subItems}
          />)
      }
    }
  },

  find(criteria, listItem) {
      var searchTerms = criteria.split(' ');
      var {
        descKey,
        contentKey
      } = this.props;

      var match = false;
      var matchIndex = void 0,
          matchTermLength = void 0;
      var inDesc = false;
      if (searchTerms[0] !== '') {
          searchTerms.forEach(function (searchTerm) {
              var content = listItem[contentKey] ? listItem[contentKey] : '';
              matchIndex = content.toLowerCase().indexOf(searchTerm.toLowerCase());
              if (matchIndex !== -1) {
                  match = true;
                  matchTermLength = searchTerm.length;
              } else if (descKey && listItem[descKey]){
                content = listItem[descKey] ? listItem[descKey] : '';
                matchIndex = content.toLowerCase().indexOf(searchTerm.toLowerCase());
                if (matchIndex !== -1) {
                  match = true;
                  inDesc = true;
                  matchTermLength = searchTerm.length;
                }
              }
          });
      }

      if (match) {
          return { inDesc: inDesc, highlight: [matchIndex, matchTermLength] };
      } else {
          return false;
      }
  },

  modifyItem (listItem, result) {
    var {
      descKey,
      contentKey
    } = this.props;

    if (result && !result.inDesc) {
        var left = listItem[contentKey].substring(0, result.highlight[0]);
        var middle = listItem[contentKey].substring(result.highlight[0], result.highlight[0] + result.highlight[1]);
        var right = listItem[contentKey].substring(result.highlight[0] + result.highlight[1]);
        listItem._primaryText = <span>{left}<span
          style = {{ display: 'inline-block', backgroundColor: 'rgba(255,235,59,0.5)', padding: '3px' }}
          >{middle}</span>{right}</span>
        listItem._secondaryText = listItem[descKey];
    } else if (result && result.inDesc) {
        var left = listItem[descKey].substring(0, result.highlight[0]);
        var middle = listItem[descKey].substring(result.highlight[0], result.highlight[0] + result.highlight[1]);
        var right = listItem[descKey].substring(result.highlight[0] + result.highlight[1]);
        listItem._secondaryText = <span>{left}<span
          style = {{ display: 'inline-block', backgroundColor: 'rgba(255,235,59,0.5)', padding: '3px' }}
          >{middle}</span>{right}</span>
        listItem._primaryText = listItem[contentKey];
    } else {
        listItem._primaryText = listItem[contentKey];
        listItem._secondaryText = listItem[descKey];
    }
  },

  searchCriteria(item, criteria) {
    var res = false;
    var self = this;
    if (criteria) {
      res = self.find(criteria, item);
    }
    self.modifyItem(item, res);
    var found = !!res;
    if (item.items) {
      for (var i in item.items) {
        found |= self.searchCriteria(item.items[i], criteria);
      }
    }
    item._show = found;
    return found;
  },

  search() {
    var self = this;
    var {
      criteria,
      tree,
    } = this.state;

    for (var i in tree) {
      self.searchCriteria(tree[i], criteria);
    }
    self.forceUpdate();
  },

  handleSearch(e) {
    var self = this;
    if (self.searchId) {
      clearTimeout(self.searchId);
    }
    self.searchId = setTimeout(self.search, 1000);
    self.setState({
      criteria: e.target.value
    })
  },

  componentWillReceiveProps(newProps) {
    var tree = newProps.tree;
    for (var i in tree) {
      this.searchCriteria(tree[i], this.state.criteria);
    }
    this.setState ({
      tree: tree,
      useIcon: newProps.useIcon
    });
  },

  render() {
    var self = this;
    var {
      style,
      className
    } = this.props;

    var {
      criteria,
      tree,
    } = this.state;

    var listItems = [];
    for(var v in tree) {
      self.genListItem(listItems, tree[v]);
    }

    return <div>
      <TextField hintText={'Search'}
        fullWidth={true}
        style={{
          margin: '12px',
          width: "calc(100% - 24px)"
        }}
        onChange={self.handleSearch}
        ></TextField>
      <List
        className={className}
        style={style}>
        {listItems}
      </List>
    </div>
  }


})
