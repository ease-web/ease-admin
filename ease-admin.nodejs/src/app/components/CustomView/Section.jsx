let React = require('react');
let mui = require('material-ui');
let {RadioButton, TextField, FontIcon, IconButton, FlatButton, Divider, Toolbar, ToolbarTitle, ToolbarGroup} = mui;

import EABFieldBase from './EABFieldBase.js';

let EABViewOnlyField = require('./ViewOnlyField.jsx');
// let FunctionEditor = require('../CustomView/FunctionEditor.jsx');

import appTheme from '../../theme/appBaseTheme.js';
let SortableList = require('./SortableList.jsx');
let ContentStore = require('../../stores/ContentStore.js');

let HeaderBar = React.createClass({
  getInitialState() {
    return {
      hasError: this.props.hasError,
    }
  },

  updateState(newState) {
    var changed = false;
    for (var k in newState) {
      if (newState[k] != this.state[k]) {
        changed = true;
        break;
      }
    }
    if (changed) {
      this.setState(newState);
    }
    return changed;
  },

  render(){
    var {
      hasError
    } = this.state;

    var {
      title,
      handleChange,
      handleClose
    } = this.props;

    return (
      <Toolbar className={ "Toolbar" }>
        <ToolbarTitle text={<div style={{
              lineHeight:'56px',
              display:'inline-block', verticalAlign: 'top',
              paddingLeft: 24,
              fontSize: 20,
              color: appTheme.palette.alternateTextColor,
              whiteSpace: 'nowrap',
              textOverflow: 'ellipsis',
              overflow: 'hidden'
            }}>{title}</div>
          }>
        </ToolbarTitle>
        <ToolbarGroup style={{float: 'right'}}>
          <FlatButton
            label={ "Done" }
            style={ { float: "right"} }
            secondary={ true }
            disabled={!!hasError}
            onTouchTap={handleChange}/>
          <FlatButton
            label={ "Cancel" }
            style={ { float: "right"} }
            secondary={ true }
            onTouchTap={handleClose}/>
        </ToolbarGroup>
      </Toolbar>
    )
  }
})


let FloatingPage = React.createClass({

  getInitialState() {
    var {
      changedValues
    } = this.props;

    return {
      errorMsg: false,
      changedValues: cloneObject(changedValues)
    }
  },

  genItems(){
    var self = this;
    var {
      template,
      genItemsFunc
    } = this.props;

    var {
      items
    } = template;

    var {
      changedValues
    } = this.state;

    var sortedItems = items?items.filter((value)=>{return value.detailSeq;}).sort((a,b)=>{return(a.detailSeq>b.detailSeq)}):[];
    var fields = [];

    var validation = (id, value)=>{
      if (self.showCondItems[id]) {
        self.forceUpdate();
      } else {
        var error = false;
        var items = template.items
        for(var i in items){
          var item = items[i];
          if(validate(item, changedValues) != false){
            error = true;
            break;
          }
        }
        self.refs.headerBar.updateState({hasError: error});
      }
    }
    for(var i in sortedItems){
      genItemsFunc(fields, sortedItems[i], changedValues, changedValues, validation, i, this.showCondItems);
    }

    return fields;
  },

  render(){
    var self = this;

    var {
      template,
      index
    } = this.props;

    var {
      changedValues
    } = this.state;


    var {
      changedValues
    } = this.state;

    var hasError = false;

    var items = this.props.template.items
    for(var i in items){
      var item = items[i];
      if(validate(item, changedValues) != false){
        hasError = true;
      }
    }

    this.showCondItems = {};

    var itemsComp = this.genItems();

    return (
      <div key={template.id+"_fs"+index} className={ "FloatingPage" }
        style={{
          display:'flex'
        }}>
        <HeaderBar ref={'headerBar'}
          hasError = {hasError}
          title = {getLocalText('en', template.title || template.hints)}
          handleChange={function() {
            self.props.handleChange(changedValues);
          }}
          handleClose={function() {
            self.props.handleChange();
          }}></HeaderBar>
        <div key={'content'}
          className={ "Content" }>
          {itemsComp}
        </div>
      </div>
    )
  }
})

let EABSection = React.createClass({
  mixins: [EABFieldBase],

  getInitialState() {
    var {
      template,
      width,
      handleChange,
      rootValues,
      changedValues
    } = this.props;

    var {
      items,
      subType
    } = template

    this.columns = [];
    this._genFields(items);

    console.debug("table", rootValues, changedValues);
    return {
      rootValues: rootValues || changedValues,
      open: false,
      isNew: false,
      editing: -1
    }
  },

  _genFields(items){
    var allItems = [];

    var getAllItem = function(its) {
      for (var i in its) {
        var item = its[i]
        if (item.type && (item.type.toUpperCase() == 'HBOX')) {
          getAllItem(item.items);
        } else {
          allItems.push(item);
        }
      }
    }
    getAllItem(items);

    this.fields = allItems.length?allItems.filter((value)=>{return value.listSeq;}).sort((a,b)=>{return(a.listSeq>b.listSeq)}):[];
  },


  getStyle(){
    return {
      container: {display: 'flex', flexWrap: 'no-wrap', justifyContent: 'flex-end'},
      iconColumn: {flexBasis: '48px'},
      dataColumn: {flex:1}
    }
  },

  _genColumnHeader(){
    var styles = this.getStyle();
    return (
      <div style={ this.mergeStyles(styles.container, {fontWeight: 'bold', lineHeight: '32px'}) }>
        <div style={ this.mergeStyles(styles.iconColumn, {fontWeight: 'bold'}) }/>
        {
          this.fields.map((field)=>{
            return <div style={ this.mergeStyles(styles.dataColumn, {fontWeight: '300', fontSize: '16px', paddingRight: '24px'}) }>
              <span className="fixWidthSpan" style={{width:'100%'}}>{field.title}</span>
            </div>
          })
        }
        <div style={ this.mergeStyles(styles.iconColumn, {fontWeight: 'bold'}) }/>
        <div style={ this.mergeStyles(styles.iconColumn, {fontWeight: 'bold'}) }/>
      </div>
    );
  },

  _genColumnRows(cValue){
    var self = this;
    var styles = this.getStyle();
    var { template } = this.props;
    var rows = [];
    for(var i in cValue){
      var value = cValue[i];
      var fieldItems = [];
      for(var j in this.fields){
        var field = this.fields[j];

        var cell = self._getDisplayValue(field, value[field.id], value);

        fieldItems.push(
          <div key={field.id + '-' + i + '-' + j} style={ styles.dataColumn }>{cell}</div>
        );
      }
      if(!fieldItems.length){
        fieldItems.push(
          <div key={template.id + '-' + i + '-index'} style={ styles.dataColumn }>
            { template.hints + (template.hints?'-':'') + (Number(i)+1) }
          </div>
        )
      }
      rows.push(
        <div key={"r_"+template.id+"_"+i} style={ this.mergeStyles(styles.container, {alignItems: 'center'}) }>
          <div className="drag-area" style={ styles.iconColumn }>
            <IconButton>
              <FontIcon className="material-icons"
                style={{color:appTheme.palette.text1Color}}>reorder</FontIcon>
            </IconButton>
          </div>
          {fieldItems}
          <div style={ styles.iconColumn }>
            <IconButton onClick={function() {
                // self.refs.editor.open(this.index, false);
                self.setState({
                  open: true,
                  editing: this.index
                })
            }.bind({index: i})}>
              <FontIcon className="material-icons"
                style={{color:appTheme.palette.text1Color}}>edit</FontIcon>
            </IconButton>
          </div>
          <div style={ styles.iconColumn }>
            <IconButton onClick={
                function(e)  {
                  e.preventDefault();
                  e.stopPropagation();
                  alert('Confirm Deletion?', 'Confirm', "NO", "YES", function(e) {
                    this.cValue.splice(this.index, 1);
                    self.forceUpdate()
                  }.bind(this))
                }.bind({index:i, cValue:cValue})
              }>
              <FontIcon className="material-icons"
                style={{color:appTheme.palette.text1Color}}>delete</FontIcon>
            </IconButton>
          </div>
        </div>
      )
    }
    return rows;
  },

  addRow(){
    this.setState({
      open: true,
      editing: -1,
      isNew: true
    })
  },

  render() {
    var self = this;
    var {
      template,
      fields,
      width,
      handleChange,
      rootValues,
      deleteAble,
      ...others
    } = this.props;

    console.debug("table render template", template, this.props.changedValues);

    var {
      changedValues,
      open,
      isNew,
      editing,
      muiTheme
    } = this.state;

   if(!this.props.changedValues[template.id] || this.props.changedValues[template.id] == null){
      if(template.static){
        changedValues[template.id] = template.values;
      }
    }

    var {
      id,
      type,
      title,
      placeholder,
      disabled,
      value,
      min,
      max,
      multiLine,
      interval,
      subType,
      items
    } = template;

    var cValue = changedValues[id] = self.getValue([]);
    console.log(changedValues, ContentStore.getPageContent());
    var errorText = (typeof(template.validation)=='function')?template.validation(changedValues):validate(template, changedValues);

    placeholder = placeholder || "No records found."

    var colHeaders = this._genColumnHeader();
    var colRows = this._genColumnRows(cValue);

    var title = getLocalText("en", title);
    return <div
      className={this.props.className || "DetailsItem"}>
      { title ? <span style={{
        fontSize: '20px',
        lineHeight: '40px',
        fontWeight: 300
      }}>{title}</span>:null }
      <div>
        {colHeaders}
        <div style={{width: '100%', borderBottom: 'solid 1px'}}/>
        {
          colRows.length?
          <SortableList
            id={id}
            items={colRows}
            changedValues={cValue}
            handleSort={function(){
              self.forceUpdate();
            }}
          />
          :
          <div style={{width: '100%', textAlign: 'center'}}>
            {getLocalText('en', placeholder)}
          </div>
        }
        {open?<FloatingPage
          ref="editor"
          template={ template }
          index={editing}
          changedValues={(isNew || editing == -1)?{}:cValue[editing]}
          genItemsFunc={ this.props.genItemsFunc }
          handleChange={(value)=>{
            if (value) {
              if (isNew) {
                cValue.push(value);
              } else {
                cValue[editing] = value;
              }
            }
            this.setState({
              open: false,
              editing: -1,
              isNew: false
            })
          }}/>:null}
      </div>
    </div>
  }
});

module.exports = EABSection;
