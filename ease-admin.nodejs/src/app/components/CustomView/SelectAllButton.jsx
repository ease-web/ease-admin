let React = require('react');
let mui = require('material-ui');

let FlatButton = mui.FlatButton;

import EABFieldBase from './EABFieldBase.js';

module.exports = React.createClass({
  mixins: [EABFieldBase],

  styles: {
    buttonStyle: {
      position: 'absolute',
      top: 4,
      right: 24,
      padding: '0 16px'
    }
  },

  getInitialState() {
    var presentation = this.props.template.presentation;
    var changedValues = this.props.changedValues;
    var keys = presentation.split(',');
    var allSelect = true;
    var value = false;
    for (let k in keys) {
      var key = keys[k];
      allSelect = allSelect && changedValues[key] == 'Y';
    }
    if(allSelect)
      value = true;
      
    return {
      value : value
    };
  },

  render() {
    var self = this;
    var {
      template,
      changedValues,
      ...others
    } = this.props;

    var {
      id,
      presentation
    } = template;
    return <FlatButton
      style={ this.styles.buttonStyle }
      onClick = {
        function() {
          var value = !self.state.value;
          var keys = presentation.split(',');
          for (let k in keys) {
            var key = keys[k];
            changedValues[key] = value?'Y':'N';
          }
          self.state.value = value;
          if (typeof self.props.requestChange == 'function') {
            self.props.requestChange(keys, value)
          }
          self.handleChange(changedValues);
        }
      }
      >{ self.state.value?getLocalizedText('BUTTON.DESELECT_ALL'):getLocalizedText('BUTTON.SELECT_ALL')}
    </FlatButton>
  },
});
