let React = require('react');
let mui = require('material-ui');
let StylePropable = mui.Mixins.StylePropable;
let ConfigConstants = require('../../constants/ConfigConstants.js')
let AppStore = require('../../stores/AppStore.js');
let DatePicker = mui.DatePicker;

import EABFieldBase from './EABFieldBase.js';

let ViewTextField = React.createClass({
  mixins: [EABFieldBase],


  render() {
    var self = this;
    var {
      template,
      values,
      width,
      mode,
      handleChange,
      ...others
    } = this.props;

    var {
      changedValues,
      muiTheme
    } = this.state;

    var {
      id,
      type,
      placeholder,
      min,
      max,
      value,
      disabled,
      presentation,
      subType
    } = template;

    if (!presentation) {
      var user = AppStore.getUser();
      presentation = user.dateFormat;
    }
    var tvalue = self.getValue(undefined)

    if (tvalue == '$today') tvalue = (new Date()).getTime();
    if (tvalue == '$oneyearlater') tvalue = (new Date()).getTime() + (365 * 24 * 60 * 60 * 1000);

    var cValue = tvalue ? new Date(tvalue):undefined;
    changedValues[id] = tvalue;

    if (!mode) mode = "landscape"
    var errorText = validate(template, changedValues);

    return <div
      className={"DetailsItem" + this.checkDiff() }>
        <DatePicker
          key = {id}
          ref = {id}
          className = { "ColumnField" }
          mode = {mode}
          floatingLabelText = { this.getTitle() }
          errorStyle = {{lineHeight:'16px'}}
          floatingLabelStyle = { {top:'28px', lineHeight:'24px'} }
          underlineStyle = { {bottom:'22px'} }
          textFieldStyle = { { width: width?width:"100%", height: '80px', display:'block', backgroundColor:null } }
          underlineDisabledStyle = { this.getStyles().disabledUnderline }
          errorText = { errorText }
          hintText= { placeholder }
          disabled = { disabled }
          focusColor = { muiTheme.textField.focusColor }
          value = { cValue }
          formatDate = {
            function (date) {
              if (date && date instanceof Date) {
                return date.format(presentation?presentation:ConfigConstants.DateFormat);
              }
              return ""
            }
          }
          onChange = {
            function(e, value) {
              // TODO tranc the date by remove the time
              self.requestChange(value);
            }
          }
          onBlur = {
            function(e) {
              // TODO tranc the date by remove the time
              self.handleBlur(cValue);
            }
          }
          {...others}
          />
      </div>
  }
});

module.exports = ViewTextField;
