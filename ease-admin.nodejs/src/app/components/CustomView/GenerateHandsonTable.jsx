let React = require('react');
let ReactDOM = require('react-dom');
let mui = require('material-ui');
let RaisedButton = mui.RaisedButton;

import HandsonTableBase from './HandsonTableBase.js';

var GenerateHandsonTable = React.createClass({

  mixins: [HandsonTableBase],

  _keys: [],

  _afterChange(changes, source){
    if(changes){
      for(var i=changes.length-1; i>=0; i--){
        var _change = changes[i];
        if(_change[0] === 0){//row
          console.log("afterChange", _change, source);
        }
      }
    }
  },

  _validateKey(value, callback){
    // var cnt = 0;
    // var _keyObjs = this.props.gridData[0];
    // for(var i in _keyObjs){
    //   if(_keyObjs[i] == value){
    //     cnt++;
    //   }
    // }
    // callback(cnt == 0);
    callback(true);
  },

  _cellValid(row, col, prop) {
    var cellProperties = {type: 'text', validator: this._validateKey};

    if (row === 0 ) {
      cellProperties.type = 'dropdown';
      cellProperties.source = this._keys || [];
    }else{
      var {
        template,
        gridData
      } = this.props;
      var key = gridData[0]? gridData[0][col.toString()]:null;
      var templateItem = this.searchItemByKey(key, template);
      if(templateItem){
        var {
          type,
          subType,
          options
        } = templateItem;

        if(type.toUpperCase() == "PICKER"){
          var _source = [];
          cellProperties.type = "dropdown";
          for(var i in options){
            _source.push(options[i].title);
            cellProperties.source = _source;
          }
        }else if(type.toUpperCase() == "TEXT" && subType && subType.toUpperCase() == "NUMBER"){
          cellProperties.type="numberic";
          cellProperties.format="0[.00000]";
        }
      }
    }

    return cellProperties;
  },

  updateGrid(props) {
    if(!props) props = this.props;

    var {
      template,
      colHeaders,
      gridData
    } = props;

    var titleSize = colHeaders.length;
    var colHeadersId = this.colHeadersId=[];
    var columns = [];
    for(var i=0; i<titleSize; i++){
      colHeadersId.push(i.toString());
      columns.push({
        width: 100,
        data: i.toString(),
        type: 'text',
        readOnly: false
      });
    }

    var _keys = this._keys = [];
    var tempItems = template.items;
    for(var i in tempItems){
      _keys.push(tempItems[i].id);
    }


    this.hot.updateSettings({
      rowHeaders: function(index){
        if(index == 0){
          return "key";
        }else{
          return index;
        }
      },
      colHeaders: colHeaders,
      columns: columns,
      allowRemoveRow: true,
      allowInsertRow: true,
      contextMenu: false,
      fixedRowsTop: 1,
      data: gridData || {},
      cells: this._cellValid,
      afterChange: this._afterChange
    });

    this.initContextMenu(1);
  },

  searchItemByKey(key, template){
    if(!key) return 0;
    var items = template.items;
    for(var i in items){
      var item = items[i];
      if(item.id == key){
        return item;
      }
    }
    return 0;
  },

  searchKey(_title, template){
    if(!_title)  return "";
    var tempItems = template.items;
    for(var i in tempItems){
      var tempItem = tempItems[i];
      var {
        title,
        id
      } = tempItem;
      if(title.toUpperCase().split(",").indexOf(_title.toUpperCase())>-1){
        return id;
      }
    }
    return "";
  },

  componentDidMount: function () {
    this.createGrid();
    this.updateGrid();
  },

  componentWillReceiveProps: function (nextProps) {
    this.updateGrid(nextProps);
  },


  render: function () {

    return (
      <div style={this.tableStyle}  ref="handsontable"></div>
    );
  }
});

module.exports = GenerateHandsonTable;
