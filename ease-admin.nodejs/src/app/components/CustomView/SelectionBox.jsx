let React = require('react');
let mui = require('material-ui');
let IconButton = mui.IconButton;
let ContentStore = require('../../stores/ContentStore.js');
let Dropzone = require('../CustomView/DropZone.jsx');
//let DynActions = require('../../actions/DynActions.js');
let Paper = mui.Paper;
import AddCircleOutline from 'material-ui/lib/svg-icons/content/add-circle-outline';
import Add from 'material-ui/lib/svg-icons/content/add';
import Info from 'material-ui/lib/svg-icons/action/info';
import EABFieldBase from './EABFieldBase.js';


let IconBox = React.createClass({

  mixins: [EABFieldBase],

  propTypes: {
    onClickBody: React.PropTypes.func,
    onDrop: React.PropTypes.func,
    isAdd: React.PropTypes.bool,
    type: React.PropTypes.string.isRequired,
    size: React.PropTypes.number
  },

  getInitialState: function() {
    return {
    }
  },

  getImage(){
    var changedValues = ContentStore.getPageContent().changedValues;
    if(!checkExist(changedValues, "Attachments")){
      changedValues["Attachments"]={};
    }

    let image ="";
    let cValue = this.props.changedValues;
    if(checkExist(cValue, 'image')){
      let fileName = cValue.image;
      if(checkExist(changedValues, "Attachments."+fileName)){
        image=changedValues["Attachments"][fileName];
      }
    }

    return image;
  },

  setImg(newFile) {
    if(this.props.onDrop){
      this.props.onDrop(newFile.url);
    }else{
      var changedValues = ContentStore.getPageContent().changedValues;
      if(!checkExist(changedValues, "Attachments")){
        changedValues["Attachments"]={};
      }

      let cValue = this.props.changedValues;
      if(checkExist(cValue, 'image')){
        let fileName = cValue.image;
        changedValues["Attachments"][fileName] = newFile.url;
      }
    }
    this.props.forceRefresh();
  },

  removeFile() {
    var changedValues = ContentStore.getPageContent().changedValues;
    if(!checkExist(changedValues, "Attachments")){
      changedValues["Attachments"]={};
    }

    let cValue = this.props.changedValues;
    if(checkExist(cValue, 'image')){
      let fileName = cValue.image;
      changedValues["Attachments"][fileName] = "";
    }
  },

  getSize(size){
    let height = (size)? size: 200;
    let width = (size)? size: 200;

    return({height: height, width: width});
  },

  getStyle() {
    var style = {
      'buttonBgColor':'#FAFAFA',
      'buttonGreen':'#2DB154',
      'buttonRed':'#F1453D',
    }
    return style;
  },

  onUpdate(){
    this.forceUpdate();
  },

  render: function() {
    //var changedValues = ContentStore.getPageContent().changedValues;
    var {
      changedValues,
    } = this.state;

    let image = this.getImage();

    let {
      isAdd,
      size,
      type,
      isDragable,

      txtContent,
      txtTitelContent,

      gridZoneStyle,
      dragZoneStyle,
      imgZoneStyle,
      addZoneStyle,
      descZoneStyle,

      addIcBtn,
      infoIcBtn,
      disabled,
    } = this.props;

    let   style=gridZoneStyle.style?gridZoneStyle.style:"";
    let   paperStyle=gridZoneStyle.paperStyle?gridZoneStyle.paperStyle:"";

    let   dzStyle=dragZoneStyle.dzStyle?dragZoneStyle.dzStyle:"";
    let   viewDImageStyle=dragZoneStyle.viewDImageStyle?dragZoneStyle.viewDImageStyle:"";

    let   icBtnStyle=addZoneStyle.icBtnStyle?addZoneStyle.icBtnStyle:"";
    let   icBtnIconStyle=addZoneStyle.icBtnIconStyle?addZoneStyle.icBtnIconStyle:"";
    let   addBtnStyle=addZoneStyle.addBtnStyle?addZoneStyle.addBtnStyle:"";


    let   descOuterStyle=descZoneStyle.descOuterStyle?descZoneStyle.descOuterStyle:"";
    let   descInnerStyle=descZoneStyle.descInnerStyle?descZoneStyle.descInnerStyle:"";

    var dropZ = '';

    //let imageSize = this.getSize(size);

    let require = {
      width: size,
      height: size,
      fileType:'image/png',
      sizeLimit:1*1048576,// 1MB:2MB in byte
    }
    let imgStyle = {
      cursor: 'pointer',
      width: '100%',
      height: 'auto'
    }

    let imgObj = (
      <img
        style={imgZoneStyle}
        src={image}
         />
    );
    let imgHeight = (type=='imageSelection')?(size*(236/314)+'px'):(148+'px');
    let addButton = (
      <div style={addBtnStyle} key="addBtn">
        {addIcBtn}
      </div>
    );


    dropZ = (
      <Dropzone
        style={dzStyle}
        onDrop={function(res) {
          if (this.require) {
            var requireWidth = this.require.width;
            var requireHeight = this.require.height;
          }
          var invalid = [];

          var requireFiletype=this.require.fileType;
          //File type checking
          var actualFileType = res.file.type;
          if (actualFileType == requireFiletype) {
            var image = new Image();
            image.src = res.imageUrl;
            var width = image.width;
            var height = image.height;
            // Width & Height checking
            /*if ((requireWidth || requireHeight) && res.imageUrl && res.file.type.indexOf('image') != -1) {
              if ((width && width != requireWidth) || (height && height != requireHeight)) {
                invalid.push("Your image is " + width + " x " + height + ". Required " + requireWidth + " x " + requireHeight + ".");
              }
            }*/
            // File size checking
            if ( res.file.size >this.require.sizeLimit) {
               invalid.push("Your file size exceed " + this.require.sizeLimit/(1024*1024) + " MB.");
            }

          }else{
              invalid.push("Your file is not " + requireFiletype.substring(requireFiletype.indexOf('/')+1, requireFiletype.length).toUpperCase()+ " type.");
          }


          if (invalid.length == 0) {
            this.updateImg({
              name: res.file.name,
              size: res.file.size,
              altText: '',
              caption: '',
              file: res.file,
              url: res.imageUrl,
            });
          } else {
            var errorMsg = "";
            for (var i = 0; i < invalid.length; i++) {
              errorMsg += invalid[i] + "\n";
            }
            alert(errorMsg, "Import failed");
          }
        }.bind({require: require, updateImg: this.setImg})}
        className="dropzone"
        accept={"image/*"} >
        {image?imgObj:addButton}
      </Dropzone>
    );


    let height = (type=='imageSelection')?((size-size*(236/314))+'px'):((size-148)+'px');
    let disableZone = (
      <div>
        {image?imgObj:addButton}
      </div>
    )
    return (
      <div className={isDragable?"drag-area":""} style={this.mergeAndPrefix({width:size, height: size, display: 'inline-block' , verticalAlign:'top'}, style)}>
        <Paper style={paperStyle}>
          <div style={viewDImageStyle}>
            {disabled?disableZone:dropZ}
          </div>
          {infoIcBtn}
          <div className={isDragable?"configurable":""} style={descOuterStyle} onClick={this.props.onClickBody}>
            <div style={descInnerStyle}>{ txtContent}</div>
          </div>
        </Paper>
      </div>
    );
  },

});

module.exports = IconBox;
