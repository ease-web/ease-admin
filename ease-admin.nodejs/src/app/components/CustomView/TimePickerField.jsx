let React = require('react');
let mui = require('material-ui');
let StylePropable = mui.Mixins.StylePropable;
let ConfigConstants = require('../../constants/ConfigConstants.js')
let AppStore = require('../../stores/AppStore.js');
let TimePicker = mui.TimePicker;

import EABFieldBase from './EABFieldBase.js';

let ViewTextField = React.createClass({
  mixins: [EABFieldBase],

  render() {
    var self = this;
    var {
      template,
      values,
      width,
      mode,
      format,
      handleChange,
      ...others
    } = this.props;

    var {
      changedValues,
      muiTheme
    } = this.state;

    var {
      id,
      type,
      placeholder,
      min,
      max,
      value,
      disabled,
      presentation,
      subType
    } = template;

    if (!presentation) {
      var user = AppStore.getUser();
      presentation = user.timeFormat;
    }

    var tvalue = self.getValue(undefined)
    var cValue = tvalue ? new Date(tvalue):undefined;

    var errorText = validate(template, changedValues);

    return <div
      className={"DetailsItem" + this.checkDiff() }>
        <TimePicker
          key = {id}
          ref = {id}
          className = { "ColumnField" }
          format= {format?format:"24hr"}
          floatingLabelText = { this.getTitle() }
          focusColor = { muiTheme.textField.focusColor }
          errorStyle = {{lineHeight:'16px'}}
          floatingLabelStyle = { {top:'28px', lineHeight:'24px'} }
          underlineStyle = { {bottom:'22px'} }
          textFieldStyle = { { width: width?width:"100%", height: '80px', display:'block', backgroundColor:null } }
          underlineDisabledStyle = { this.getStyles().disabledUnderline }
          errorText = { errorText }
          hintText= { placeholder }
          disabled = { disabled }
          value = { cValue }
          formatTime = {
            function (date) {
              if (date && date instanceof Date) {
                return date.format(presentation?presentation:ConfigConstants.TimeFormat);
              } else {
                return ""
              }
            }
          }
          onChange = {
            function(e, value) {
              self.requestChange(value);
            }
          }
          {...others}
          />
      </div>
  }
});

module.exports = ViewTextField;
