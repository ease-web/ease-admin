let React = require('react');
let mui = require('material-ui');

let MenuItem = mui.MenuItem;

import EABFieldBase from './EABFieldBase.js';
import SelectField from './SelectField.jsx';

let PickerField = React.createClass({
  mixins: [EABFieldBase],

  render: function() {
    var self = this;
    var {
      template,
      values,
      width,
      disabled,
      compact,
      handleChange,
      rootValues,
      ...others
    } = this.props;

    var {
      changedValues,
      options,
      muiTheme
    } = this.state;

    var {
      id,
      title
    } = template;

    var cValue = this.getValue("");
    disabled = disabled || template.disabled || disableTrigger(template, null, values, changedValues, rootValues)

    compact |= !title;

    return <div
      className={"DetailsItem" + this.checkDiff() }>
      <SelectField
        key = {id}
        ref = {id}
        className = { "ColumnField" }
        template = {template}
        options = {options}
        disabled = {disabled?true:false}
        value = {cValue}
        changedValues = {changedValues}
        rootValues = { rootValues }
        compact = { compact }
        title = { !compact?this.getTitle():null}
        style= {{width: width?width:'100%', height:compact?'60px':'80px', backgroundColor:null}}
        errorStyle = {{lineHeight:'16px', bottom:compact?'16px':'20px'}}
        floatingLabelStyle = { {top:compact?'18px':'28px', lineHeight:'24px'} }
        labelStyle = {{ lineHeight: compact?'60px':'80px', top: 0, paddingRight:'24px',
          whiteSpace: "nowrap",
          overflow: "hidden",
          textOverflow: "ellipsis"} }
        iconStyle = {{ top: compact?18:28 }}
        underlineStyle = { {bottom:compact?'16px':'22px'} }
        underlineDisabledStyle = { this.getStyles().disabledUnderline }
        focusColor = { muiTheme.textField.focusColor }
        handleChange = {
          function(id, value) {
            self.requestChange(value);
            let callback = self.props.callback;
            if(typeof(callback) == 'function')
              callback();
          }
        }
        onBlur = { function(e) {
            self.handleBlur(e.target.value);
          }
        }
        {...others}
        />
    </div>;
  }
});
module.exports = PickerField;
