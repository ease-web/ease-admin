let React = require('react');
let mui = require('material-ui');

let TextField = mui.TextField;
let FlatButton = mui.FlatButton;
let RightDialog = require('./RightDialog.jsx');

import EABFieldBase from './EABFieldBase.js';
import ReadOnlyField from './ReadOnlyField.jsx';
import appTheme from '../../theme/appBaseTheme.js';
import AppStore from '../../stores/AppStore.js';

let MultipleTextField = React.createClass({
  mixins: [EABFieldBase],

  styles: {
    buttonStyle: {
      marginLeft: 16,
      color: appTheme.palette.primary2Color,
      backgroundColor:appTheme.palette.background2Color,
      borderWidth: 1,
      borderStyle: 'solid',
      borderColor: appTheme.palette.borderColor
    }
  },

  getInitialState() {
    var id = this.props.template.id;
    var values = (id && this.props.values && this.props.values[id])?cloneObject(this.props.values[id]):{};
    return {
      show: false,
      values: values,
      showTime: 0
    };
  },

  show(){
    var d = new Date();
    var n = d.getTime();
    this.setState({show:true, showTime: n});

    var {
      id,
      items
    } =  this.props.template;
    if (!items) {
      items = AppStore.getLangList();
    }
    this.refs[id + '_' + items[0].id].focus();
  },

  hide(){
    var d = new Date();
    var n = d.getTime();
    if(n - 1000 > this.state.showTime){
      this.setState({show:false});
    }
  },

  cancel(){
    var id = this.props.template.id;
    var changedValues = this.state.changedValues;
    mergeObj(changedValues[id],this.state.values);
    this.setState({show:false, changedValues:changedValues});
  },

  save(){
    var id = this.props.template.id;
    this.setState({show:false, values:cloneObject(this.state.changedValues[id])});
  },

  render() {
    var self = this;

    var {
      template,
      handleChange,
      width,
      compact,
      disabled,
      ...others
    } = this.props;

    var {
      changedValues,
      muiTheme
    } = this.state;

    var {
      id,
      title,
      value,
      placeholder,
      items,
      multiLine
    } = template;

    compact |= !title;

    var cValue = self.getValue({});
    if(isEmpty(cValue)){
      cValue={};
    }

    if (!items) {
      items = AppStore.getLangList();
    }
    var dfIndex = 0;
    var langItems = [];
    for (let z in items) {
      var sItem = items[z];

      // TODO: check lang?

      langItems.push(
        <TextField
          autoFocus = {z == 0 ? true : false}
          key = {id + '_' + sItem.id}
          ref = {id + '_' + sItem.id}
          multiLine = {multiLine}
          className = { "ColumnField" }
          style = { {width: '100%', height:'80px', backgroundColor:null} }
          errorStyle = {{ lineHeight:'16px' }}
          floatingLabelStyle = {{ top:'28px', lineHeight:'24px' }}
          labelStyle = {{ lineHeight: '80px', top: 0} }
          underlineStyle = {{ bottom:'22px' }}
          floatingLabelText = { self.getTitle(sItem.title) }
          hintText = { sItem.placeholder }
          focusColor = { self.state.muiTheme.textField.focusColor }
          value = {cValue[sItem.id]}
          onChange = {
            function(e) {
              var value = e.target.value;
              cValue[this.id] = value;
              self.requestChange(cValue);
            }.bind({id: sItem.id})
          }
          onBlur = {
            function(e) {
              var value = e.target.value;
              cValue[this.id] = value;
              self.handleBlur(cValue);
            }.bind({id: sItem.id})
          }
          />);
    }
    console.debug("item::::", items, template);
    var item = items[dfIndex];

    var okBtn = getLocalizedText('BUTTON.DONE', "Done");
    var cancelBtn = getLocalizedText('BUTTON.CANCEL', "Cancel");

    var errorText = validate(template, changedValues);

    return (
        <div
          className={"DetailsItem" + this.checkDiff() }>
          <TextField
            key = {id}
            ref = {id}
            className = { "ColumnField" }
            style = { {width:width?width:'100%', height:compact?'60px':'80px', backgroundColor:null } }
            errorStyle = {{lineHeight:'16px', bottom:compact?'16px':'20px'}}
            floatingLabelStyle = { {top:'28px', lineHeight:'24px'} }
            labelStyle = {{lineHeight: compact?'60px':'80px', top: 0} }
            underlineStyle = {{ bottom: compact?'16px':'22px' }}
            floatingLabelText = { !compact ? this.getTitle() : null }
            errorText= { errorText }
            hintText= { placeholder }
            value = { cValue[item.id] }
            onFocus = { (!disabled)?this.show:null }
            onClick = { (!disabled)?this.show:null }
            disabled = {disabled}
            {...others}
            />
          <RightDialog
            title = { this.getTitle(null, {
              display: "inline-block",
              padding: "24px",
              lineHeight: "24px",
              fontSize: "24px"
            }) }
            show = { this.state.show }
            handleClose = {this.hide}>
              {langItems}

          </RightDialog>
      </div>
    );
  }
});

module.exports = MultipleTextField;
