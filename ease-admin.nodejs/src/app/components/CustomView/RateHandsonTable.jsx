let React = require('react');
let ReactDOM = require('react-dom');
let mui = require('material-ui');
let RaisedButton = mui.RaisedButton;

import HandsonTableBase from './HandsonTableBase.js';


var RateHandsonTable = React.createClass({

  mixins: [HandsonTableBase],

  _validateKey(value, callback){
    callback(typeof value == "string");
  },


  updateGrid(props) {

    if(!props) props = this.props;

    var prepareData = this.initData(props);
    if(!prepareData) return;

    var {
      colHeaders,
      colHeadersId,
      columns
    } = prepareData;

    //set key validation
    var colSize = columns.length;
    for(var j=0; j<colSize; j++){
      if(colHeaders[j] == "key"){
        columns[j].validator = this._validateKey;
      }
    }


    for(var i=1; i<=120; i++){
      colHeaders.push(i.toString());
      colHeadersId.push(i.toString());
      columns.push({
        width: 50,
        data: i.toString(),
        type: 'numeric',
        readOnly: false,
        format: '0[.]000',
      })
    }

    this.hot.updateSettings({
      data: props.gridData || {},
      rowHeaders: false,
      colHeaders: colHeaders,
      columns: columns,
      allowRemoveRow: true,
      allowInsertRow: true,
      contextMenu: false
    });

    this.initContextMenu();

  },



  componentDidMount: function () {
    this.updateGrid();
  },

  componentWillReceiveProps: function (nextProps) {
      this.updateGrid(nextProps);
  },

  render: function () {
    return (
      <div style={this.tableStyle}  ref="handsontable"></div>
    );
  }
});

module.exports = RateHandsonTable;
