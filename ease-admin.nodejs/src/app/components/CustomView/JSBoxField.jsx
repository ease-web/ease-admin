let React = require('react');
let mui, {Tabs, Tab, FontIcon, IconButton}  = require('material-ui');

let JavascriptField = require('./JavascriptField.jsx');
import appTheme from '../../theme/appBaseTheme.js';
// import {MuiTreeList} from 'react-treeview-mui'
let EABTreeList = require('./TreeList.jsx');
import EABFieldBase from './EABFieldBase.js';
// let EABDetailsList = require('../CustomView/DetailsList.jsx');
import List from 'material-ui/lib/lists/list';
import ListItem from 'material-ui/lib/lists/list-item';

let FuncEditorStore = require('../../stores/FuncEditorStore.js')

let DialogActions = require('../../actions/DialogActions.js');
let DynActions = require('../../actions/DynActions.js');

const FUNCTION_START = "function([para]) {";
const FUNCTION_END = "}";

let ParameterList = React.createClass({
  mixins: [EABFieldBase],

  styles: {
    boxStyle: {
      backgroundColor: appTheme.palette.borderColor,
      borderRadius: '16px',
      cursor:'pointer',
      fontSize: '14px',
      border: "1px solid white"
    }
  },

  getDefaultProps() {
    return {
      allowAdd: true
    }
  },

  showParaDialog() {
    var self = this;
    var cValue = self.getValue([]);
    var {
      template,
      changedValues,
      inDialog
    } = this.props;
    var cancelBtn = getLocalizedText('BUTTON.CANCEL', "Cancel");
    var saveBtn = getLocalizedText('BUTTON.SAVE', "Save");
    var deleteBtn = getLocalizedText('BUTTON.DELETE', "Delete");

    var dialogData = this.state.dialogData;
    var dialogValues = {}
    if (cValue && dialogData.index != undefined && typeof cValue[dialogData.index] == 'object') {
      dialogValues = cloneObject(cValue[dialogData.index]);
    } else {
      // if it is already in dialog, just add row
      if (inDialog) {
        cValue.push(dialogValues);
        changedValues[id] = cValue;
        self.forceUpdate();
        return;
      }
    }

    // construct the dialog
    DialogActions.showDialog({
      title: getLocalText('en', template.title || template.hints),
      message: dialogData.message,
      items: dialogData.items,
      values: dialogValues,
      rootValues: self.props.rootValues,
      actions: [{
        type: 'customButton',
        float: 'left',
        title: deleteBtn,
        disabled: !self.props.allowAdd,
        hide: dialogData.index == undefined,
        handleTap: function() {
          if (dialogData.index) {
            cValue.splice(dialogData.index, 1);
          }
          self.handleChange(self.state.changedValues);
        }.bind(dialogData)
      }, {
        type: 'cancel',
        title: cancelBtn
      }, {
        type: 'customButton',
        title: dialogData.doneTitle,
        handleTap: function( values ) {
          // if set it as default, reset others
          if (values.default == 'Y') {
            for (let i in cValue) {
              if (i != dialogData.index) {
                cValue[i].default = 'N';
              }
            }
          }
          // check if key value already exists
          var key = null;
          for (var i in template.items) {
            if (template.items[i].isKey) {
              key = template.items[i];
            }
          }
          if (key) {
            for (var i in cValue) {
              if ( i != dialogData.index && cValue[i][key.id] == values[key.id]) {
                alert(getLocalText('en', key.title) + " duplicate, please use another one", "Warning");
                return false;
              }
            }
          }
          // update new values
          if (dialogData.index) {
            cValue[dialogData.index] = values;
          } else {
            cValue.push(values);
          }
          changedValues[template.id] = cValue;
          self.handleChange(changedValues);
          return true;
        }.bind(dialogData)
      }]
    });
  },

  onUpdateParaComplete() {
    FuncEditorStore.removeChangeListener(this.onUpdateParaComplete);
    var self = this;
    var {
      updateParaFunc,
      changedValues
    } = this.props;

    if (updateParaFunc) {
      updateParaFunc(changedValues);
    }

    setTimeout(function() {
      self.showParaDialog();
    }, 100);

  },

  render() {
    var self = this;
    var {
      template,
      width,
      values,
      inDialog,
      handleChange,
      rootValues,
      allowAdd,
      ...others
    } = this.props;

    var {
      changedValues,
      muiTheme
    } = this.state;

    var {
      id,
      type,
      title,
      presentation,
      mandatory,
      items,
      value,
      min,
      max,
      interval,
      subType
    } = template;

    // var cValue = (id && changedValues && changedValues[id])?changedValues[id]:(value?value:{});

    var cValue = self.getValue([]);

    var oValue = (id && values && !isEmpty(values[id]))?values[id]:[];

    // var label = "add";
    var cancelBtn = getLocalizedText('BUTTON.CANCEL', "Cancel");
    var addBtn = getLocalizedText('BUTTON.ADD', "Add");
    var saveBtn = getLocalizedText('BUTTON.SAVE', "Save");
    var deleteBtn = getLocalizedText('BUTTON.DELETE', "Delete");

    var subItems = [];

    var sortedItems = items.sort(function(a,b) {
      var aa = a.detailSeq?a.detailSeq:0;
      var bb = b.detailSeq?b.detailSeq:0;
      return (aa < bb) ? -1 : (aa > bb) ? 1 : 0;
    });

    var handleTap = function() {

      self.state.dialogData = this;
      FuncEditorStore.addChangeListener(self.onUpdateParaComplete);
      DynActions.getFunctionPara(changedValues.inputParams);



    }
    var disabled = disableTrigger(template, null, null, cValue, rootValues);

    var rows = []
    if (cValue instanceof Array && cValue.length > 0) {
      var goThruItem = function(cell, values, retValue) {
        var cvalue = values[cell.id];
        if (cell.options) {
          for (var o in cell.options) {
            if (cell.options[o].value == cvalue) {
              cvalue = getLocalText('en', cell.options[o].title)
            }
          }
        }
        if (cell.subType && cell.subType == 'currency') {
          cvalue = getCurrency(cvalue, "$", 2);
        }
        retValue = retValue.replace('['+cell.id+']', cvalue);

        if (cell.items) {
          for (var ci in cell.items) {
            var scell = cell.items[ci];
            retValue = goThruItem(scell, sv, retValue);
          }
        }
        return retValue;
      }

      for (var c in cValue) {
        var sv = cValue[c];
        var valPresent = presentation;

        for (var ci in sortedItems) {
          var cell = sortedItems[ci];
          valPresent = goThruItem(cell, sv, valPresent);
        }

        // if (!header) {
        //   header = <div key={"hr"} className="row">{hcells}</div>
        // }
        if (c != "0") {
          rows.push(<span style={{display: "inline-block",
            verticalAlign: "top"}}>, </span>)
        }

        rows.push(<span key={"r"+c} className="inlineCell" style={self.styles.boxStyle} onClick={
          disabled?null:handleTap.bind({items: sortedItems, values: cValue, index: c, doneTitle: saveBtn })
        }>{valPresent}</span>)
      }
    }
    if (allowAdd) {
      rows.push(<IconButton
        style = { Object.assign({}, self.styles.boxStyle, { padding:0, width: '20px', height: '20px'}) }
        iconStyle = { {fontSize: "18px", color:appTheme.palette.themeGreen} }
        onClick = { handleTap.bind({items: sortedItems, values: cValue, doneTitle: addBtn}) }
        ><FontIcon className="material-icons" style={{fontSize: "18px", color:appTheme.palette.themeGreen}}>add</FontIcon>
      </IconButton>)
    }
    return <div key={"icl_"+id} className="inlineCompList">{rows}</div>
  }

});


module.exports = React.createClass({
  mixins: [EABFieldBase],

  init() {
    var self = this;

    var {
      functions
    } = FuncEditorStore.getData();

    this.setState({
      activeListItem: [],
      expandedListItems: [],
      paraList: this.initParaList(),
      funcList: functions,
      helper: 'p',
      toolsPanelIndex: 0
    })
  },

  initParaList() {

    var self = this;

    var {
      values,
      rootValues,
      inline,
      changedValues,
      template
    } = this.props;

    var {
      dataDict,
      defParams
    } = FuncEditorStore.getData();

    if (!dataDict || inline) {
      return {};
    }

    var params = [];

    for (var i in changedValues.inputParams) { // loopping the params
      var para = changedValues.inputParams[i];
      var obj = {};
      var value = null;
      if (para.type.toLowerCase() == 'object') {
        try {
          if (para.value) {
            value = JSON.parse(para.value);
          } else {
            value = {};
          }
        } catch (e) {
          value = {};
        }
      } else if (para.type.toLowerCase() == 'array'){
        try {
          if (para.value) {
            value = JSON.parse(para.value);
          } else {
            value = [];
          }
        } catch (e) {
          value = [];
        }
      } else if (para.type == 'planDetail' ||
          para.type == 'planDetails') {
          value = defParams[para.type];
      } else if (para.type == 'extraPara') {
        if (para.value && typeof para.value == 'string') {
          value = JSON.parse(para.value);
        } else {
          value = para.value;
        }
        value['planIllustProp'] = defParams.extraPara['planIllustProp'];
        value['templateFuncs'] = defParams.extraPara['templateFuncs'];

      } else if (para.type == 'quotation' ||
          para.type == 'planInfo') {

        if (isEmpty(para.value)) {
          para.value = value = {}//this.defaultData[para.type];
           //JSON.stringify(obj[para.id]);
        } else {
          if (para.value && typeof para.value == 'string') {
            value = JSON.parse(para.value);
          } else {
            value = para.value;
          }
        }
      } else {
        value = para.value; //|| null;
      }
      obj[para.id] = value;
      params.push(obj);
    }

    var paraList = {};

    var getParaItem = function(list, obj, parentId, isRate) {
      if (typeof obj == 'object') {
        for (var id in obj) {
          var subItems = [];
          if (id == 'planIllustProp' || id == 'templateFuncs') {
            getParaItem(subItems, defParams.extraPara[id], parentId + id + ".", id == 'rates')
          } else {
            if (!isRate) {
              getParaItem(subItems, obj[id], parentId + id + ".", id == 'rates')
            }
          }
          list.push({
            "title": id,
            "desc": dataDict && dataDict[id],
            "value": parentId + id,
            "items": subItems.length?subItems:null
          })
        }
      }
    }

    var paraList = [];
    for (var p in params) {
      getParaItem(paraList, params[p], "")
    }
    console.log('paraList', paraList);
    return paraList;
  },

  componentWillMount() {
    this.init();
  },

  onTouchText(item) {
    var self = this;
    var {
      template
    } = self.props;

    console.log('touch text: ', item);
    self.refs[template.id].refs.jsEditor.editor.insert(item.value)
    self.refs[template.id].refs.jsEditor.editor.focus();
  },

  getFunctionStr() {
    var self = this
    var {
      changedValues
    } = this.state;

    var para = ""
    if (changedValues.inputParams) {
      for (var i in changedValues.inputParams) {
        if (i != '0') para += ", ";
        para += changedValues.inputParams[i].id;
      }
      var func = null;
      eval("func = "+FUNCTION_START.replace('[para]', para) + "\n"
        + changedValues.funcBody
        + FUNCTION_END);
      return (func+"").replace(new RegExp('\n', 'g'), '').replace(/\s+/g,' ');
    }
    return "";
  },

  render() {
    var self = this;
    var {
      template,
      width,
      handleChange,
      disabled,
      style,
      inline,
      index,
      updateParaFunc,
      ...others
    } = this.props;

    var {
      changedValues,
      muiTheme,
      helper,
      paraList,
      funcList,
      errorMsg,
      toolsPanelIndex,
      activeListItem,
      expandedListItems
    } = this.state;

    var treelist = helper == 'p'?paraList:funcList;

    var {
      id,
      type,
      placeholder,
      value,
      min,
      max,
      interval,
      subType
    } = template;

    var cValue = changedValues["funcBody"] //self.getValue("");

    // if (this.refs[id] && this.refs[id])
    var height = window.innerHeight - 200;

    var itemList = paraList;
    if (this.state.searchResult) {
      itemList = this.state.searchResult;
    }
    var params = <ParameterList
        key = {id+"_"+index+"_ipl"}
        template = { {
          id: "inputParams",
          type: "ComplexList",
          subType: "inline",
          presentation: "[id]",
          title: {
              en: "Function Parameters"
          },
          hints: {
              en: "Parameter"
          },
          items:[{
            type: "hbox",
            presentation: 'column2',
            items:[{
              id: "id",
              title: "Parameter Name",
              type: "text",
              isKey: true,
              disabled: changedValues.default, // disable if it is default
              mandatory: true
            }, {
              id: "type",
              title: "Type",
              type: "picker",
              disabled: changedValues.default, // disable if it is default
              options: [{
                value: "object",
                title: "Object"
              }, {
                value: "array",
                title: "Array"
              }, {
                value: "text",
                title: "Text"
              }, {
                value: "number",
                title: "Number"
              }, {
                value: "quotation",
                title: "Quotation"
              }, {
                value: "planDetail",
                title: "Plan Detail"
              }, {
                value: "planDetails",
                title: "Plan Details Map"
              }, {
                value: "planInfo",
                title: "Input of Plan in Quotation"
              }, {
                value: "extraPara",
                title: "Extra Parameter for generate BI"
              }]
            }]
          }, {
            id: "value",
            title: "Test Value",
            type: "text",
            trigger: [{
              type: "showIfEqual",
              id: "type",
              value: "text"
            }]
          }, {
            id: "value",
            title: "Test Value",
            type: "text",
            subType: "number",
            trigger: [{
              type: "showIfEqual",
              id: "type",
              value: "number"
            }]
          }, {
            id: "value",
            title: "Test Value (Json)",
            type: "jsField",
            value: "{}",
            trigger: [{
              type: "showIfExists",
              id: "type",
              value: "object,quotation,planInfo,extraPara"
            }]
          }, {
            id: "value",
            title: "Test Value (Json)",
            type: "jsField",
            value: "[]",
            trigger: [{
              type: "showIfEqual",
              id: "type",
              value: "array"
            }]
          }]
        }}
        values = { changedValues }
        changedValues = { changedValues }
        allowAdd = { !changedValues.default }
        updateParaFunc = { updateParaFunc }
        handleChange = { function(cValues) {
          self.setState({
            paraList: self.initParaList()
          })
        }}
        />

    if (!inline) {
      var fstart = FUNCTION_START.split('[para]');
      return <div style={{
          display: "block",
          position: "relative",
          padding: "12px 24px 0px"
        }}>
        <div className="Script">{fstart[0]}{params}{fstart[1]}</div>
          <JavascriptField
            id = {id}
            key = {id}
            ref = {id}
            className = { "ColumnField" }
            disabled = {disabled}
            height = {height+"px"}
            width = {width?width:"calc(100% - 360px)"}
            style = {{
              display: 'inline-block'
            }}
            onBlur = {
              function(value, errorMsg) {
                changedValues.funcBody = value;
                changedValues[id] = self.getFunctionStr();
                self.state.errorMsg = errorMsg;
                self.props.handleChange(id, value, !!errorMsg);
              }
            }
            onFocus = { self.props.onJSEditorFocus }
            mode = "javascript"
            defaultValue = { cValue }
            {...others}
            />
            <div style={{
                display: "inline-block",
                width: 340,
                height: height,
                position: 'relative',
                marginLeft: '10px',
                overflowY: 'auto',
                border: '1px solid ' + appTheme.palette.borderColor
              }}>
              <Tabs key={"helperTab"}
                style={{ backgroundColor: 'transparent' }}
                onChange={ function(value) {
                  self.setState({
                    helper: value
                  })
                }}>
                <Tab key={"parameter"} label={"Parameters"} value={"p"}/>
                <Tab key={"formula"} label={"Functions"} value={"f"}/>
              </Tabs>
              <EABTreeList
                key={"tl"}
                style={{
                  height: 'calc(100% - 48px)',
                  width: '100%',
                  overflowY: 'auto'
                }}
                contentKey={"title"}
                descKey={"desc"}
                useIcon={helper!='f'}
                onTouchItemTap={this.onTouchText}
                tree={ helper!='f'?paraList:funcList }/>
          </div>
          <div className="Script">{FUNCTION_END}</div>
          {errorMsg?<div style={{
              color: muiTheme.textField.errorColor,
              height: '16px',
              fontSize: '12px'
            }}>{errorMsg}</div>:null}
        </div>
    } else {
      return <div style={Object.assign({
          display:"inline-block"
        }, style)}>
        <span style={{
          display: "block"
          }}>{changedValues.title}</span>
        <div className="Script" style={{
          display: "block",
          paddingLeft: 0
        }}>{changedValues.id+"("}{params}{")"}</div>
        </div>
    }
  }
});
