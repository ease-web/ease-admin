var React = require('react');
var SortableMixin = require('../../mixins/react-sortable-mixin.js');
let mui = require('material-ui');
let StylePropable = mui.Mixins.StylePropable;
let SortableListActions = require('../../actions/SortableListActions.js');

let SortableList = React.createClass({
  mixins: [React.LinkedStateMixin, StylePropable, SortableMixin],

  sortableOptions: { draggable: ".drag-elements",  handle: ".drag-area"},

  PropTypes: {
    id: React.PropTypes.string.isRequired,
    items: React.PropTypes.array.isRequired,
    disabledItems: React.PropTypes.array,
    changedValues: React.PropTypes.object.isRequired,
    horizontal: React.PropTypes.bool,
    disabled: React.PropTypes.bool,
    presentation: React.PropTypes.number,
    handleChange: React.PropTypes.func
  },

  getDefaultProps() {
    return {
      horizontal: false,
    }
  },

  componentWillUnmount() {

  },

  componentDidMount: function() {
    // Set items' data, key name `items` required
    this.setState({ items: this.props.items });
  },

  componentWillReceiveProps(newProps, newState) {
    // console.debug("componentWillReceiveProps :", this.props, newProps);
    this.setState({
      items: newProps.items,
      changedValues: newProps.changedValues
    });
  },

  contextTypes: {
    muiTheme: React.PropTypes.object
  },

  childContextTypes: {
    muiTheme: React.PropTypes.object
  },

  getChildContext() {
    return {
      muiTheme: this.state.muiTheme
    }
  },

  getInitialState: function() {
      let {
        id,
        items,
        values,
        changedValues
      } = this.props;

      return {
          items: items,
          values: values,
          changedValues: changedValues
      };
  },
  handleSort: function (e) {
    let {
      changedValues
    } = this.props;
    if(e.oldIndex>e.newIndex){
      let temp = [];
      for(let k=e.newIndex; k<=e.oldIndex; k++){
        temp.push(changedValues[k]);
      }
      for(let m=0; m<temp.length-1;m++){
        let new_seq = e.newIndex+m+1;
        changedValues[new_seq]=temp[m];
        changedValues[new_seq].seq = (new_seq+1)*100;
      }
      changedValues[e.newIndex]=temp[temp.length-1];
      changedValues[e.newIndex].seq = (e.newIndex+1)*100;
    }else if(e.oldIndex<e.newIndex){
      let temp = [];
      for(let k=e.oldIndex; k<=e.newIndex; k++){
        temp.push(changedValues[k]);
      }
      for(let m=0; m<temp.length-1;m++){
        let new_seq = e.oldIndex+m;
        changedValues[e.oldIndex+m]=temp[m+1];
        changedValues[new_seq].seq = (new_seq+1)*100;
      }
      changedValues[e.newIndex]=temp[0];
      changedValues[e.newIndex].seq = (e.newIndex+1)*100;
    }
    console.log("changedValues", changedValues);
    SortableListActions.reflesh();
    this.props.handleSort?this.props.handleSort(e):null;
  },

  _genItems(items){

    let horizontalStyle=null;
    let {
      horizontal,
      presentation,
      disabledItems,
      itemStyle,
      disabled
    } = this.props;
    if(horizontal){
      let width = presentation? (100/presentation)+'%':'auto';
      horizontalStyle={
        display: 'inline-block',
        width: width
      }
    }

    let rows = [];
    let {
      id
    } = this.props;
    for(let i=0; i<items.length; i++){
      let item = items[i];
      rows.push(<li className={((disabledItems && disabledItems.indexOf(i))>=0 || disabled)?"ignore-elements":"drag-elements"} key={'sli-'+id+'-'+i} style={this.mergeAndPrefix(horizontalStyle, itemStyle)}>{item}</li>);
    }
    return rows;
  },

  render: function() {
    var self = this;
    let {
      id,
      style,
      horizontal,
      presentation,
      ...others
    } = this.props;
    var _items = this._genItems(this.state.items);
    return (
      <ul style={this.mergeAndPrefix({listStyleType:'none', padding: '0px', margin: '0px'}, style)} key={'sl-'+id}>{_items}</ul>
    )
  }
});
module.exports = SortableList;
