let React = require('react');
let mui = require('material-ui');

import EABFieldBase from './EABFieldBase.js';
let DynConfigPanelActions = require('../../actions/DynConfigPanelActions.js');
let DialogActions = require('../../actions/DialogActions.js');
let NeedDetailStore = require('../../stores/NeedDetailStore.js');
let ContentStore = require('../../stores/ContentStore.js');
let IconBox = require('./SelectionBoxCS.jsx');
let SortableList = require('./SortableList.jsx');
let IconButton = mui.IconButton;
import AddCircleOutline from 'material-ui/lib/svg-icons/content/add-circle-outline';
import Add from 'material-ui/lib/svg-icons/content/add';
import Info from 'material-ui/lib/svg-icons/action/info';
import Delete from 'material-ui/lib/svg-icons/action/delete';
import Search from 'material-ui/lib/svg-icons/action/search';
let FloatingActionButton =mui.FloatingActionButton;
import ContentAdd from 'material-ui/lib/svg-icons/content/add';
var AppBarActions = require('../../actions/AppBarActions');

module.exports = React.createClass({

  mixins: [EABFieldBase],

  propTypes:{

  },



  //global variables for function onUpload() use
  deleteList:[],

  getIcBoxStyles(sectionId, type, size){

    let gridZoneStyle={};
    let dragZoneStyle={};
    let imgZoneStyle={};
    let addZoneStyle={};
    let descZoneStyle={};


    let cardWidth=241;

    let style={width:cardWidth, height:260,margin:'8px'};
    let paperStyle={background: '#FAFAFA' , textAlign: 'center' , boxShadow: '0px 2px 4px #000000'};
    gridZoneStyle={style, paperStyle};

    let viewDImageStyle={'background':'#E5E5E5' };
    let dzStyle={height:'196px', width: '100%', margin: 'auto'};
    dragZoneStyle={viewDImageStyle, dzStyle};

    imgZoneStyle={   width:cardWidth, height:'196px' };

    let addBtnStyle={width:cardWidth, height:'196px' };
    let icBtnStyle={width: 48, height: 48,  WebkitFilter:'invert(100%)'};
    let icBtnIconStyle={ width: imgZoneStyle.width, height: imgZoneStyle.height };
    addZoneStyle={icBtnStyle, icBtnIconStyle, addBtnStyle};




    let descOuterStyle={height:'64px', width:'100%', textAlign: 'left'};
    let descInnerStyle={   paddingLeft:14, paddingTop:12 , fontSize:'14px'};
    descZoneStyle= {descOuterStyle,descInnerStyle};


    return {
      gridZoneStyle,
      dragZoneStyle,
      imgZoneStyle,
      addZoneStyle,
      descZoneStyle,

    };

  },



  _genItem(changedValues, pickerArray){
    let items = [];
    let self=this;
    let cardWidth=241;
    let {
      disabled,
      langCode
    } = this.props;


    if(changedValues && !changedValues.options){
      changedValues.options =[];
    }
    let options = changedValues.options;
    let size = (checkExist(changedValues, 'boxRegularWidth'))? Number(changedValues.boxRegularWidth): 150;
    let type=changedValues.type;
    let sectionId=this.props.sectionId;


    let {
      gridZoneStyle,
      dragZoneStyle,
      imgZoneStyle,
      addZoneStyle,
      descZoneStyle,

    }=this.getIcBoxStyles(sectionId, type, size);

    let isDragable=false;
    let txtItem="title";
    let defaultTxt=getLocalizedText("ICON_BOX.ADD_TITLE", "Description") ;

    txtItem="title";
    isDragable=true;
    defaultTxt=getLocalizedText("ICON_BOX.ADD_NEED", "Description");
    for(let i in options){
      let openPanel = function(){
        let delAction = function(){
          let removeDialog = {
      			id: "delete",
      			title: getLocalizedText("DELETE","DELETE"),
      			message: getLocalizedText("DELETE_ITEM","Delete this item?"),
      			negative: {
      				id: "delete_cancel",
      				title: getLocalizedText("BUTTON.CANCEL","CANCEL"),
      				type: "button"
      			},
      			positive:{
      				id: "delete_confirm",
      				title: getLocalizedText("BUTTON.OK","OK"),
      				type: "customButton",
      				handleTap: function(){
                options.splice(Number(i),1);
      					for(let j in options){
      						options[j].seq = (Number(j)+1)*100;
      					}
      					DynConfigPanelActions.closePanel();
                self.onUpdate();
      				}
      			}
      		}

      		DialogActions.showDialog(removeDialog);

				}
        let changeImgIdAction = function(newFileName){
          let fileName = options[i].image;
          cValue["Attachments"][fileName] = cValue["Attachments"][newFileName];
          cValue["Attachments"][newFileName] = undefined;
        }
        //parent.setState({cfValues: options[i], deleteAction: delAction});
        DynConfigPanelActions.openPanel(options[i], 'ConceptSelling_'+langCode, {deleteAction: delAction, changeImgIdAction: changeImgIdAction} ,pickerArray );
      }

      let option=options[i];
      let txtContent= checkExist(option, txtItem+'.'+langCode) && !isEmpty(option[txtItem][langCode])? multilineText(option[txtItem][langCode]) :defaultTxt;
      let isShowInfoIc=true;//false;
      if(checkExist(option, 'conceptSelection')&&option.conceptSelection!='pleaseSelect'){
         isShowInfoIc=true;
      };


      let delBinAction = function(){
        let removeDialog = {
          id: "delete",
          title: getLocalizedText("DELETE","DELETE"),
          message: getLocalizedText("DELETE_ITEM","Delete this item?"),
          negative: {
            id: "delete_cancel",
            title: getLocalizedText("BUTTON.CANCEL","CANCEL"),
            type: "button"
          },
          positive:{
            id: "delete_confirm",
            title: getLocalizedText("BUTTON.OK","OK"),
            type: "customButton",
            handleTap: function(){
              options.splice(Number(i),1);
              for(let j in options){
                options[j].seq = (Number(j)+1)*100;
              }
              DynConfigPanelActions.closePanel();
              self.onUpdate();
            }
          }
        }

        DialogActions.showDialog(removeDialog);

      }
      let addIcBtn=[];
      addIcBtn.push(
        <div  style={{display:'table', height:'100%'}}>
           <div  style={{display:'table-cell', verticalAlign:'middle'}}>
            <IconButton
                iconStyle={{width: 48, height: 48,  WebkitFilter:'invert(100%)'}}
                style={{width:cardWidth, height:'196px'}}
                >
            </IconButton>
          </div>
        </div>
      );


      let deleteBtn=[];
      deleteBtn.push(
        <div>
          <IconButton  style={{float:'right'}} onTouchTap={delBinAction} iconStyle={{fill:"rgba(0, 0, 0, 0.54)", width:24, height:24}}>
            <Delete  />
          </IconButton>
        </div>
        );
      items.push(
				<IconBox key={this.props.id + '-ic-' + i} changedValues={option} size={size} type={type} disabled={disabled}
          ref={option.id}

          forceRefresh={this.onUpdate}
          isDragable={isDragable}
          txtContent={txtContent}

          gridZoneStyle={gridZoneStyle}
          dragZoneStyle={dragZoneStyle}
          imgZoneStyle={imgZoneStyle}
          addZoneStyle={addZoneStyle}
          descZoneStyle={descZoneStyle}

          addIcBtn={addIcBtn}
          infoIcBtn={deleteBtn}
          sectionId={this.props.sectionId}



          imageId={option.id}
          langCode={langCode}


          />
      )
    }
    return items;
  },

  getNewId(cValue){
    let maxId=0;
    let options =this.state.changedValues.options;//cValue.NeedsSelection.selections['en'][0].options;
    for(let i in options){
      let _id = options[i].id;
      if(maxId<Number(_id.substring(1, _id.length))){
        maxId=Number(_id.substring(1, _id.length));
      }
    }
    return 'r'+(maxId+1);

  },

  addIconSelectionn(url,fileName, fileType){
    let self=this;
    let {
      changedValues
    } = this.state;

    if(changedValues && !changedValues.options){
      changedValues.options =[];
    }
    var {
      id,
      options
    } = changedValues;

    let {
      langCode,
    } = this.props;


    let langCodesItems = NeedDetailStore.genLangCodesObj();
    let cValue = ContentStore.getPageContent().changedValues;
    let newId= this.getNewId(cValue);

    let ncs=cValue.NeedsConceptSelling;
    let pageId = ncs.pages[ncs.selectedPage].id;

    let imageId = 'image_cs_' + pageId +'_'+langCode+'_'+newId;
    cValue["Attachments"][imageId]= url;
    let sectionId=this.props.sectionId;

    let title=NeedDetailStore.genLangCodesObj();
    title[langCode]=fileName;
    options.push({title:title ,label:'File', fileType:fileType, id: newId, image:imageId, boxRegularWidth:241  });


    let delAction = function(){
      options.splice(options.length-1,1);
      cValue["Attachments"][imageId]=undefined;
      DynConfigPanelActions.closePanel();
      self.onUpdate();
    }

    let changeImgIdAction = function(newFileName){
      let fileName = options[options.length-1].image;
      cValue["Attachments"][fileName] = cValue["Attachments"][newFileName];
      cValue["Attachments"][newFileName] = undefined;
    }

    self.onUpdate();
  },

  handleSort(e){
    this.forceUpdate();
  },

  onUpdate(){
    this.forceUpdate();
  },
  validateFiles(loadFiles){
    //sort by filename asc
    loadFiles.sort(
      function (a, b) {
        if (a.name > b.name) {
          return 1;
        }
        if (a.name < b.name) {
          return -1;
        }
        return 0;
      }
    );
    var invalidDimensionFiles = "";
    var invalidTypeFiles = "";
    var invalidSizeFiles = "";
    let sizeUnit=1024*1024;
    let sizeLimit=2;
    let widthLimit=2048;
    let heightLimit=1536;
    for (var h = 0, lf; lf = loadFiles[h]; h++) {
        //validation
        let {
          src,
          type,
          size,
          width,
          height,
        }=lf;
        let name=lf.name;
        let isInvalid=false;

        if(type.match('image.*')){
          if(!(width==widthLimit && height==heightLimit)){
            invalidDimensionFiles =invalidDimensionFiles +name+', ';
            isInvalid=true;
          }
        }
        if (!(type.match('image.*')||type=='application/pdf')) {
          invalidTypeFiles = invalidTypeFiles+ name+', ';
          isInvalid=true;
        }
        if ( size>sizeLimit*sizeUnit) {
          invalidSizeFiles =invalidTypeFiles+ name+', ';
          isInvalid=true;
        }
        if(!isInvalid)
           this.addIconSelectionn(src , name, type);
        if(h==loadFiles.length-1){
          let content = ContentStore.getPageContent();
          let {
            values,
            changedValues
          } = content;

          let url = "/Needs/Detail/Save";
          AppBarActions.submitChangedValues(url);
        }
    }
    let invalid=[];
    if(invalidDimensionFiles)
      invalid.push("The dimension of file(s) "+invalidDimensionFiles.substring(0, invalidSizeFiles.length-2)+" is not " + widthLimit + " x " + heightLimit + ".\n");
    if(invalidTypeFiles)
      invalid.push("The type of file(s) "+invalidTypeFiles.substring(0, invalidSizeFiles.length-2)+" is not IMAGE or PDF type.\n");
    if(invalidSizeFiles)
      invalid.push("The size of file(s) "+invalidSizeFiles.substring(0, invalidSizeFiles.length-2)+" size exceed " + sizeLimit + " MB.\n");

    if(invalid.length>0){
       var errorMsg = "";
       for (var i = 0; i < invalid.length; i++) {
         errorMsg += invalid[i] ;
         errorMsg += "\n";
       }
       alert(errorMsg, "Import failed");
    }
  },

  onUpload(evt){
    let self=this;
    var files= evt.target.files;
     var loadFiles=[];
     let filesNum=files.length;
     for (var i = 0, f; f = files[i]; i++) {
       let cnt=i;
       let file=files[i];
       var reader = new FileReader();
       reader.onload = (function(theFile ) {
         return function(e ) {
         if(file.type.match('image.*')){
             let newImg=new Image();
             newImg.src = e.target.result;
             newImg.onload=function(){
                loadFiles.push(
                  {
                    src: newImg.src,
                    name:file.name,
                    type:file.type,
                    size:file.size,
                    width: newImg.width,
                    height: newImg.height,
                  }
                );
                if(loadFiles.length==filesNum){
                  self.validateFiles(loadFiles);
                }
              }
           }else{
               loadFiles.push(
                 {
                   src: e.target.result,
                   name:file.name,
                   type:file.type,
                   size:file.size,
                 }
               );
               if(loadFiles.length==filesNum){
                 self.validateFiles(loadFiles);
               }
           }
         };
       })(f);
       reader.readAsDataURL(f);
     }
  },

  checkImgSize(url){
    let newImg=new Image();
    newImg.src = url;
    newImg.onload=function(e){
      if( e.target.width!=56||e.target.height!=80)
        return false;
      else {
        return true
      }
    }
  },

  uploadFile(){
      document.getElementById("file_input_file_"+this.props.langCode).click();
  },

  ////MultiDel-S
    /*
  setDeleteMode(){
    let isMultiDel=this.state.isMultiDel;

    this.setState({isMultiDel:!isMultiDel});
  },
  addToDeleteList( boxId){
    let deleteList=this.deleteList

    if(boxId.length>0){
      if(this.state.isMultiDel){
        let targetNo=-1;
        for(let i in deleteList){
          if(deleteList[i]==boxId)
            targetNo=i;
        }
        if(targetNo>=0 ){
          this.deleteList.splice(Number(targetNo),1);
          return false;
        }else{
          this.deleteList.push(boxId);
          return true;
        }
        return false;

      }
    }
    return false;

    console.log(deleteList)


  },

  checkIfSelected( boxId){
    let deleteList=this.deleteList

    if(boxId.length>0){
      if(this.state.isMultiDel){
        let targetNo=-1;
        for(let i in deleteList){
          if(deleteList[i]==boxId)
            targetNo=i;
        }
        if(targetNo>=0 ){

          return false;
        }else{

          return true;
        }
        return false;

      }
    }
    return false;

    console.log(deleteList)


  },

  getInitialState() {

    ///get selectSortIdxfrom seledix
    return {
     isMultiDel:false,
     multiSelectStatus:false,
    };
  },

  alertDelete(){
    let self=this;
    if(this.deleteList.length>0){
      let removeDialog = {
        id: "delete",
        title: getLocalizedText("DELETE","DELETE"),
        message: getLocalizedText("DELETE_ITEM","Delete this item?"),
        negative: {
          id: "delete_cancel",
          title: getLocalizedText("BUTTON.CANCEL","CANCEL"),
          type: "button"
        },
        positive:{
          id: "delete_confirm",
          title: getLocalizedText("BUTTON.OK","OK"),
          type: "customButton",
          handleTap: function(){
            self.confirmMultiDel();
          }
        }
      }

      DialogActions.showDialog(removeDialog);
    }

  },
  confirmMultiDel(){
  deleteMode={this.state.isMultiDel}
  deleteFile={this.addToDeleteList}
  multiSelectStatus={this.checkIfSelected( option.id)}
    <div style={{position:'absolute' , left:40}}>
  <FloatingActionButton onTouchTap={this.alertDelete} secondary={true}style={{ marginRight: 20 , display:this.state.isMultiDel?'':'none'}}>
      <Done  />
</FloatingActionButton>
</div>


<FloatingActionButton onTouchTap={this.setDeleteMode} secondary={true}style={{ marginRight: 20 }}>
    <Delete  />
</FloatingActionButton>



    let deleteList=this.deleteList;
    let cValue = ContentStore.getPageContent().changedValues;
    let options= cValue.NeedsSelection.selections[this.props.langCode][0].options;

    for(let h in deleteList){
      let targetId=deleteList[h];
      for(let i in options){
        let optionId=options[i].id;
        if(optionId==targetId){
          options.splice(Number(i),1);
          for(let j in options){
            options[j].seq = (Number(j)+1)*100;
          }

        }
      }

    }
    this.deleteList=[];
      this.setState({isMultiDel:false})

    this.onUpdate();
  },
*/
  ////MultiDel-E

  render() {
    var self = this;
    let {
      changedValues
    } = this.state;

    let {
      isSortable
    } = this.props;

    let items = this._genItem(changedValues,self.props.pickerArray);

    let sectionId=this.props.sectionId;
    let horizontal, hasAddButton, handleSort, presentation=null;

    horizontal=true;
    hasAddButton=true;
    handleSort=this.handleSort;
    presentation=3;

    let sortableItems = (
      <SortableList
        disabled={this.props.disabled}
        id={this.props.id}
        items={items}
        changedValues={changedValues.options}
        horizontal={horizontal}
        hasAddButton={false}
        handleSort={handleSort}
		     />
    )

    return (
      <div style={{width:'100%' }}>
        <div style={{minHeight:'calc(100vh - 290px)'}}>
            {isSortable?sortableItems:items}
        </div>

        <div style={{position:'absolute' , right:40}}>
          <input type="file" multiple="multiple" onChange={this.onUpload}  id={"file_input_file_"+this.props.langCode} style={{display:'none'}}  />
            <FloatingActionButton onTouchTap={this.uploadFile} secondary={true}style={{ marginRight: 20 }}>
                  <ContentAdd  />
            </FloatingActionButton>
        </div>
      </div>
    );
  }
});
