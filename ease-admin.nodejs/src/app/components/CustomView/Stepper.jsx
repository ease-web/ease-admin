let React = require('react');
let mui = require('material-ui');

let FontIcon = mui.FontIcon;

import EABFieldBase from './EABFieldBase.js';
import appTheme from '../../theme/appBaseTheme.js';

let _isMount=false;

module.exports = React.createClass({
  mixins: [EABFieldBase],

  styles: {
    containerStyle:{
      background: '#2696CC',
      width: '144px',
      height: '48px',
      lineHeight: '48px',
      borderRadius: '4px',
      WebkitUserSelect: 'none'
    },
    buttonStyle:{
      display: 'inline-block',
      width: '48px',
      lineHeight: '48px',
      textAlign: 'center',
      verticalAlign: 'text-bottom',
      cursor: 'pointer'
    },
    numberStyle:{
      display: 'inline-block',
      background: '#67B5DB',
      width: '48px',
      textAlign: 'center',
      color: '#FFFFFF',
      fontSize: '21px'
    }
  },

  getInitialState: function() {
    let {
      minValue,
      maxValue,
      defaultValue,
      interval
    } = this.props;

    //initial maximum value if empty
    if(isEmpty(maxValue)){
      maxValue = 100;
    }

    //initial minimum value if empty
    if(isEmpty(minValue)){
      minValue = 0;
    }

    //initial interval
    if(isEmpty(interval)){
      interval = 1;
    }

    //initial defaultValue
    if(isEmpty(defaultValue)){
      defaultValue = minValue;
    }

    return {
      interval: interval,
      minValue: minValue,
      maxValue: maxValue,
      defaultValue: defaultValue
    }
  },

  componentWillReceiveProps(nextProps){
    let {
      minValue,
      maxValue,
      interval,
      defaultValue
    } = nextProps;

    //initial maximum value if empty
    if(isEmpty(maxValue)){
      maxValue = 100;
    }

    //initial minimum value if empty
    if(isEmpty(minValue)){
      minValue = 0;
    }

    //initial default value if empty
    if(isEmpty(defaultValue)){
      defaultValue = minValue;
    }

    //initial interval
    if(isEmpty(interval)){
      interval = 1;
    }

    this.setState({
      interval: interval,
      maxValue: maxValue,
      minValue: minValue
    });
  },

  onDemoChange(e, value){
    this.setState({stepperValue: value});
  },

  _handleChange(type){
    let {
      isDemo
    } = this.props;

    let {
      maxValue,
      minValue,
      interval,
      changedValues,
      defaultValue
    } = this.state;

    let cValue = changedValues?changedValues:defaultValue;

    if(type=='decrease'){
      if(cValue-Number(interval)<minValue){
        return;
      }else{
        this.state.changedValues = changedValues - Number(interval);
        this.requestChange(this.state.changedValues);
        if(_isMount)
          this.forceUpdate();
      }
    }else{
      if(cValue+Number(interval)>Number(maxValue)){
        return;
      }else{
        this.state.changedValues = changedValues + Number(interval);
        this.requestChange(this.state.changedValues);
        if(_isMount)
          this.forceUpdate();
      }
    }
  },

  componentDidMount: function() {
    _isMount = true;
    var interval;
    var self = this;
    var time = 500;
    var count = 0;
    var resetInterval = function(e){
      clearInterval(interval);
      time = 500;
      count = 0;
    }

    let incrementInterval = function(_time, type){
      return setInterval(function() {
        self._handleChange(type);
        count++;
        if(count%5==0){
          if(_time-100>0){
            _time=_time-100;
            clearInterval(interval);
            interval = incrementInterval(_time, type);
          }
        }
      },_time);
    }
    document.getElementById('addBtn').addEventListener('mousedown', function(e){
      interval = incrementInterval(time, 'add');
    });
    document.getElementById('addBtn').addEventListener('mouseup',resetInterval);
    document.getElementById('addBtn').addEventListener('mouseout',resetInterval);

    document.getElementById('decreaseBtn').addEventListener('mousedown', function(e){
      interval = incrementInterval(time, 'decrease');
    });
    document.getElementById('decreaseBtn').addEventListener('mouseup',resetInterval);
    document.getElementById('decreaseBtn').addEventListener('mouseout',resetInterval);
  },

  componentWillUnmount(){
    _isMount=false;
  },

  render() {
    var self = this;
    var {
      disabled,
      isDemo,
      ...others
    } = this.props;

    var {
      maxValue,
      minValue,
      interval,
      changedValues,
      defaultValue
    } = this.state;

    let cValue = changedValues?changedValues:defaultValue;

    return (
      <div style={this.styles.containerStyle}>
        <div id="decreaseBtn" style={this.styles.buttonStyle} onClick={function(){self._handleChange('decrease')}}> <FontIcon className="material-icons" color={"#FFFFFF"}>remove</FontIcon></div>
        <div style={this.styles.numberStyle}>{cValue}</div>
        <div id="addBtn" style={this.styles.buttonStyle} onClick={function(){self._handleChange('add')}}> <FontIcon className="material-icons" color={"#FFFFFF"}>add</FontIcon></div>
      </div>
    )
  }
});
