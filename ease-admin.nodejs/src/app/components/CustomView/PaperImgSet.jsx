let React = require('react');
let mui = require('material-ui');
let RaisedButton = mui.RaisedButton;
let ContentStore = require('../../stores/ContentStore.js');
let Dropzone = require('../CustomView/DropZone.jsx');
//let DynActions = require('../../actions/DynActions.js');
let Paper = mui.Paper;
import { SketchPicker } from 'react-color';
import ColorLens from 'material-ui/lib/svg-icons/image/color-lens';
import Dialog from 'material-ui/lib/dialog';
import IconButton from 'material-ui/lib/icon-button';
import Add from 'material-ui/lib/svg-icons/content/add';
var isOpened=false;
let PaperImgSet = React.createClass({
  propTypes: {
    parent: React.PropTypes.string,
    id:  React.PropTypes.string,
    type: React.PropTypes.string,
    title: React.PropTypes.string,
    desc: React.PropTypes.string,
    startBtn: React.PropTypes.string,
    button: React.PropTypes.string,
  },

  getInitialState: function() {
    var changedValues = ContentStore.getPageContent().changedValues;
    var id = this.props.id;
    var type = this.props.type;
    var parent = this.props.parent;
    //var data = changedValues[parent][id][type]? changedValues[parent][id][type]: "";

    var data ="";
    if(changedValues[parent]&&changedValues[parent][id]&&changedValues[parent][id]["bgImage"]){
      var fileName=changedValues[parent][id]["bgImage"];
      if( changedValues["Attachments"]&& changedValues["Attachments"][fileName]&&changedValues["Attachments"][fileName] ){
        data=changedValues["Attachments"][fileName]
      }
    }

    return {
      changedValues: changedValues,
      data: data,
      displayColorPicker: false,
      color: this.props.bgColor,
    }
  },


  componentDidMount() {
    ContentStore.addChangeListener(this.onValuesChange);
  },

  componentWillUnmount() {
    ContentStore.removeChangeListener(this.onValuesChange);
  },

  onValuesChange() {
    var content = ContentStore.getPageContent();
    var changedValues = content.changedValues;
    if(!checkChanges(changedValues, this.state.changedValues)){
      this.setState({
        changedValues:changedValues,
      })
    }
  },

  updateImg(newFile) {

    var id = this.props.id;
    var type = this.props.type;
    var parent = this.props.parent;
    var newValues = ContentStore.getPageContent().changedValues;


    //newValues[parent][id][type] = newFile.url;
    if(newValues[parent]&& newValues[parent][id]&& newValues[parent][id]["bgImage"]){
    var itemName=  newValues[parent][id]["bgImage"];
    if(  newValues["Attachments"]&&  newValues["Attachments"][itemName]&&  newValues["Attachments"][itemName])
      newValues["Attachments"][itemName] = newFile.url;
    }
    //DynActions.changeValues(newValues);
    this.setState({data:newFile.url})
  },

  removeFile() {

    var id = this.props.id;
    var type = this.props.type;
    var parent = this.props.parent;
    var newValues = ContentStore.getPageContent().changedValues;
  //  newValues[parent][id][type] = "";
  var itemName=  newValues[parent][id]["bgImage"];
  newValues["Attachments"][itemName]["attachContent"] = "";
    //DynActions.changeValues(newValues);

    this.setState({data:""})
  },



  handleClick() {
    if(this.state.displayColorPicker)
      this.saveBgColor();
   this.setState({ displayColorPicker: !this.state.displayColorPicker });
    isOpened=true;
 },

 saveBgColor(){
   var id = this.props.id;
   var type = this.props.type;
   var parent = this.props.parent;
   var newValues = ContentStore.getPageContent().changedValues;
   newValues[parent][id]['bgColor'] = this.state.color;
   //DynActions.changeValues(newValues);
   //this.setState({data:newFile.url})

 },

 handleClose() {
   this.setState({ displayColorPicker: false });
   this.saveBgColor();

 },
 //notify parent compents if clicked
closePickerByBlur(){
  if(isOpened){
    isOpened=false;

   }else{

      this.handleClose();
   }
   this.props.handleIsClickedPaper();
},
//notify parent compents if clicked
 handleChange(color) {
   let rgb=color.rgb;
   let colorCode = "rgba("+rgb.r+","+rgb.g+","+rgb.b+","+rgb.a+")";
   this.setState({ color: colorCode });
   this.props.handleIsClickedPaper();
 },

 handleChangeComplete() {
  this.setState({ displayColorPicker: false});
},

handleOnClick(clickItem) {
  //alert(itmeNo)
  this.props.openCfPanel( this.props.refNo,clickItem)
},

  getStyle() {
    var style = {
      'buttonBgColor':'#FAFAFA',
      'buttonGreen':'#2DB154',
      'buttonRed':'#F1453D',
    }
    return style;
  },

  render: function() {
    var {
      id,
      parent,
      title,
      desc,
      startBtn,
      button,
      buttonAdd,
      disabled,
      type,
      require,
      isLocked,
    } = this.props;

    //var changedValues = ContentStore.getPageContent().changedValues;
    var changedValues = this.state.changedValues;
    var data = this.state.data;






    var dropZ = '';
    var hasData = false;
    var buttonOb = '';
    if (data && data != '') {
      hasData = true;
    }
      buttonOb = (disabled)?<div style={{height:"24px"}}/>:(

        <div
          label={buttonAdd}
          style={{width:304, height:228 ,marginTop:'0px', marginBottom:'0px'}}
          backgroundColor = {this.getStyle().buttonBgColor}
          labelColor = {this.getStyle().buttonGreen} />
      );

      dropZ = (
        <div>
        <Dropzone
          onDrop={function(res) {
            if (this.require) {
              var requireWidth = this.require.width;
              var requireHeight = this.require.height;
            }
            var invalid = [];

            var requireFiletype=this.require.fileType;
            //File type checking
            var actualFileType = res.file.type;
            if (actualFileType.startsWith("image/")) {
              var image = new Image();
              image.src = res.imageUrl;
              var width = image.width;
              var height = image.height;
              // Width & Height checking
              if ((requireWidth || requireHeight) && res.imageUrl && res.file.type.indexOf('image') != -1) {
                if ((width && width != requireWidth) || (height && height != requireHeight)) {
                  invalid.push("Your image is " + width + " x " + height + ". Required " + requireWidth + " x " + requireHeight + ".");
                }
              }
              // File size checking
              if ( res.file.size >this.require.sizeLimit) {
                 invalid.push("Your file size exceed " + this.require.sizeLimit/(1024*1024) + " MB.");
              }

            }else{
                invalid.push("Your file is not PNG/JPG type.");
            }


            if (invalid.length == 0) {
              this.updateImg({
                name: res.file.name,
                size: res.file.size,
                altText: '',
                caption: '',
                file: res.file,
                url: res.imageUrl,
              });
            } else {
              var errorMsg = "";
              for (var i = 0; i < invalid.length; i++) {
                errorMsg += invalid[i] + "\n";
              }
              alert(errorMsg, "Import failed");
            }
          }.bind({require: require, updateImg: this.updateImg})}
          className="dropzone"
          accept={type == "cover" ? "image/*" : "application/pdf"} >
          {buttonOb}
          <div  >
            {data!="null"?
              <img
              style={{cursor:'pointer', position:'absolute', left:0, top:0, width:304, height:228, marginTop:0, marginBottom:0,  border:'0px solid black',}}
              className="productCoverImg"
              key={id + "img"}
              src={data}/>:
              <div    className="productCoverImg" style={{display:'table', cursor:'pointer', position:'absolute', left:0, top:0, width:304, height:228, marginTop:0, marginBottom:0,  border:'0px solid black',backgroundColor:'#9B9B9B'}}>
                <div style={{display: 'table-cell','vertical-align': 'middle'}}>
                    <Add/>
                </div>
              </div>
            }
          </div>

        </Dropzone>

        </div>
      );
    //}
//if it has data(cover / brochure)
    if (1==2) {
      if (type == "cover") {
        var imgnText = (
          <div className="bncItem">
            <img
              className="productCoverImg"
              key={id + "img"}
              src={data}
              onClick={this.removeFile } />
          </div>
        );
        dropZ = (
          <div className="bncItem">
            {imgnText}
            {buttonOb}
          </div>
        );
      } else {
        var encodedStr = data.substring(data.search(",") + 1, data.length);
        var type = data.substring(data.search(":") + 1, data.search(";"));
        var pdfnText = (
          <div
            className="bncItem">

          </div>
        );

      }
    }
    let disableStyle={
      'pointer-events': 'none'
    };
    var viewDImage = (<div style={ isLocked? disableStyle:{} }>{dropZ}</div>);


    return (
      <div>
        <Paper onClick={ this.closePickerByBlur } style={{backgroundColor:this.state.color, boxShadow: '0px 2px 4px #000000',}} >
          {viewDImage}
          <div style={{height:80 , width:'100%' }}>

            <div
              className="configurable"
              onClick={this.handleOnClick.bind(this, 0)}
              style={{cursor:'pointer',width:'100%', height:24, textAlign:'left', display:'block',left:0, padding:16, fontWeight:500, fontSize:20,maxWidth:304, wordBreak:'break-word'   }}
              > {title ? multilineText(title) : 'Title'}

            </div>

            <div className="configurable" onClick={this.handleOnClick.bind(this, 1)}
              style={{cursor:'pointer',width:'100%',  height:24, textAlign:'left',display:'block',left:0, padding:16, fontWeight:400, color:"rgba(0, 0, 0, 0.54)", fontSize:16,maxWidth:304, wordBreak:'break-word' }}>
               {desc ? multilineText(desc) : 'Description'}
            </div>
          </div>
          <div style={{height:84 , width:'100%', display:'table'}}>
            <div style={{paddingTop: 0, paddingBottom: 0, width:'100%', display:'table-row'}}>
              <div style={{ textAlign:'left' ,verticalAlign:'bottom', paddingLeft:4 ,paddingBottom:4, display:'table-cell' }}>
                <IconButton     onTouchTap={this.handleClick}  iconStyle={{fill:"rgba(0, 0, 0, 0.54)", width:24, height:24}}>
  								<ColorLens  />
  							</IconButton>
              </div>
              <div style={{width:'50%', textAlign:'right', display:'table-cell',verticalAlign:'bottom',paddingBottom:18, paddingRight:24, fontSize:14, fontWeight:500}}>
                  {startBtn}
              </div>
            </div>
          </div>
        </Paper>

        {this.state.displayColorPicker && !isLocked?
          <div onClick={  this.props.handleIsClickedPaper }style={{  position:'absolute', top:document.getElementById('paperArea').getBoundingClientRect().top,  display:'table', margin:'auto'}}>
                <SketchPicker
                  color={ this.state.color }
                     position="right"
                     display={ this.state.displayColorPicker }
                     onChange={ this.handleChange.bind(this) }
                     onClose={ this.handleClose.bind(this) }
                     onChangeComplete={ this.saveBgColor.bind(this) }
                  type="chrome" />
          </div>
          :null
        }
      </div>
    );
  },

});

module.exports = PaperImgSet;
