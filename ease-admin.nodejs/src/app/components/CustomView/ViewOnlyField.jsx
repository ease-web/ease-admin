let React = require('react');
let mui = require('material-ui');
let ContentStore = require('../../stores/ContentStore.js');

import ReadOnlyField from './ReadOnlyField.jsx';

module.exports = React.createClass({


  getInitialState() {
    var {
      reference,
      options
    } = this.props.template;

    if (reference && !this.props.template.options) {
      var {
        template
      } = ContentStore.getPageContent();

      var ref = reference.replace("@", "/")
      var path = ref.split("/");
      var refField = getFieldFmTemplate(template, path, 0, "options");

      if (refField && refField.options) {
        return {
          options: refField.options
        };
      }
    }

    return {
      options: options
    };
  },

  PropTypes: {
    template: React.PropTypes.object.isRequired,
    values: React.PropTypes.object.isRequired
  },

  render() {
    var self = this;
    var {
      template,
      width,
      values,
      ...others
    } = this.props;

    var {
      id,
      title,
      value
    } = template;

    var {
      options
    } = this.state;

    var oValue = (id && values && values[id])?values[id]:"";

    title = getLocalText('en', title);
    if (options) {
      for (var o in options) {
        var opt = options[o];
        if (opt.value == oValue) {
          oValue = getLocalText("en", opt.title);
        }
      }
    }

    return <div
      className={"DetailsItem"}>
        <ReadOnlyField
          key = {id}
          className = { "ColumnField" }
          style= {{width: width?width:'100%', backgroundColor:null}}
          floatingLabelText = { title }
          defaultValue = { oValue }
          {...others}
          />
      </div>
  }
});
