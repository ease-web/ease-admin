let React = require('react');
let mui = require('material-ui');

let FlatButton = mui.FlatButton;
let FontIcon = mui.FontIcon;
let Divider = mui.Divider;
let Tabs = mui.Tabs;
let Tab = mui.Tab;

import EABFieldBase from './EABFieldBase.js';
import ReadOnlyField from './ReadOnlyField.jsx';
import DialogActions from '../../actions/DialogActions.js';

import appTheme from '../../theme/appBaseTheme.js';

var _addBtnId = "sys_add_btn_id";

let DetailsBlock = React.createClass({
  mixins: [EABFieldBase],

  getInitialState() {
    var {
      collapsed
    } = this.props;

    return {
      collapsed: !!collapsed
    }
  },

  genItems(fields){
    var self = this;
    var {
      template,
      values,
      changedValues,
      genItemsFunc
    } = this.props;

    var {
      items
    } = template;

    var sortedItems = items?items.filter((value)=>{return value.detailSeq;}).sort((a,b)=>{return(a.detailSeq>b.detailSeq)}):[];

    var validation = (id, value)=>{
      if (this.showCondItems[id]) {
        self.forceUpdate();
      }
    }

    for(var i in sortedItems){
      genItemsFunc(fields, sortedItems[i], values, changedValues, validation, i, this.showCondItems);
    }
    return fields;
  },

  render() {
    var self = this;
    var {
      template,
      width,
      handleChange,
      handleDeleteRow,
      handleUpdateRow,
      deleteAble,
      changedValues,
      rootValues,
      index,
      discollapsible,
      isNoCard,
      ...others
    } = this.props;

    var {
      detailValue,
      collapsed,
      muiTheme
    } = this.state;

    var {
      id,
      type,
      title,
      placeholder,
      disabled,
      value,
      min,
      max,
      multiLine,
      items,
      interval,
      subType
    } = template;

    var testValues = changedValues;
    if (testValues[template.id]) {
      testValues={};
      testValues[template.id] = changedValues;
    }

    this.showCondItems = {};

    var errorText = (typeof(template.validation)=='function')?template.validation(changedValues):validate(template, changedValues);
    //console.log('render detail:', template);
    var rows = [];

      var titleControls = [];
      var subTitle = changedValues['title'] || changedValues['desc'];
      subTitle = (!isEmpty(subTitle) && (getLocalText('en', subTitle)+ " (id:"+changedValues['id']+")")) || changedValues['id']

      titleControls.push(<div key="titleDiv" className="title"
        style={{
          maxWidth: changedValues.default?"calc(100% - 50px)":"calc(100% - 220px)"
        }}
        onClick={changedValues.default?null:handleUpdateRow}>
          <div style={{
          whiteSpace: "nowrap",
          overflow: "hidden",
          textOverflow: "ellipsis",
          display: "inline-block",
          width: "100%"
        }}>{subTitle}</div>

      </div>)
      if (!changedValues.default) {
        titleControls.push(<FontIcon className="material-icons" style={{padding: '0 12px'}}>edit</FontIcon>);
      }

      if(!discollapsible){
        if (collapsed) {
          titleControls.push(<IconButton key="colBtn" style={{padding: '12px', float: 'right'}} onClick={
              function(e) {
                self.setState({
                  collapsed: !collapsed
                })
              }
            }><FontIcon className="material-icons">keyboard_arrow_down</FontIcon></IconButton>)
        } else {
          titleControls.push(<IconButton key="expBtn" style={{padding: '12px', float: 'right'}} onClick={
            function(e) {
              self.setState({
                collapsed: !collapsed
              })
            }
          }><FontIcon className="material-icons">keyboard_arrow_up</FontIcon></IconButton>)
        }
      }else{
        collapsed = false;
      }


      if (deleteAble && !changedValues.disabled && !changedValues.default) {
        titleControls.push(<FlatButton
          key="delBtn"
          label="Delete"
          style={{
            padding: '6px',
            float: 'right',
            marginRight: '24px'
          }}
          onClick={
            function(e) {
              e.preventDefault();
              e.stopPropagation();
              alert('Confirm Deletion?', 'Confirm', "NO", "YES", function(e) {
                if (typeof this.handle == 'function') {
                  this.handle(this.index);
                }
              }.bind({handle: handleDeleteRow, index:this.index}))
            }.bind({index:index})
          }>
        </FlatButton>)
      }

      rows.push(<div key={"dt_"+index} className="detailTitle">{titleControls}</div>)

      rows = this.genItems(rows);

      return(
        <div style={{
            transition: 'all 500ms cubic-bezier(0.445, 0.05, 0.55, 0.95) 0ms',
            margin: '8px 24px',
            paddingBottom: collapsed?'0px':'24px',
            maxHeight: collapsed?'48px':'10000px'
          }}>
          {rows}
        </div>
      )


    return null;
  }
});

module.exports = React.createClass({
  mixins: [EABFieldBase],

  styles: {
    subLineStyle: {
      backgroundColor: appTheme.palette.borderColor,
      height:1,
      margin:'20px 0'
    },
  },


  handleIndexChange(value){
    this.setState({selectedIndex: value});
  },

  render() {
    var self = this;
    var {
      template,
      width,
      values,
      handleChange,
      rootValues,
      collapsed,
      inDialog,
      ...others
    } = this.props;

    var {
      changedValues,
      selectedIndex,
      muiTheme
    } = this.state;

    var {
      id,
      type,
      title,
      presentation,
      allowAdd,
      mandatory,
      items,
      value,
      min,
      max,
      interval,
      subType
    } = template;

    // var cValue = (id && changedValues && changedValues[id])?changedValues[id]:(value?value:{});
    console.log('render DetailsTab');
    rootValues = rootValues || changedValues;
    var cValue = changedValues[id] = self.getValue({});

    var oValue = (id && values && !isEmpty(values[id]))?values[id]:{};

    // var label = "add";
    var cancelBtn = getLocalizedText('BUTTON.CANCEL', "Cancel");
    var addBtn = getLocalizedText('BUTTON.ADD', "Add");
    var saveBtn = getLocalizedText('BUTTON.SAVE', "Save");
    var deleteBtn = getLocalizedText('BUTTON.DELETE', "Delete");

    var subItems = [];
    var subTabs = [];


    var sortedItems = items.sort(function(a,b) {
      var aa = a.detailSeq?a.detailSeq:0;
      var bb = b.detailSeq?b.detailSeq:0;
      return (aa < bb) ? -1 : (aa > bb) ? 1 : 0;
    });

    var handleTap = function() {

      var dialogValues = {}
      if (cValue && this.index != undefined && typeof cValue[this.index] == 'object') {
        dialogValues = cValue[this.index];
      } else {
        // if it is already in dialog, just add row
        if (inDialog && cValue) {
          cValue[this.index] = dialogValues;
          this.forceUpdate();
          return;
        }
      }

      //console.debug('on handleTap add:', this, dialogValues);
      // construct the dialog
      DialogActions.showDialog({
        title: self.getTitle() || template.hints,
        message: this.message,
        items: [{
          "id": "id",
          "type": "text",
          "isKey": true,
          "detailSeq": 1,
          "mandatory": true,
          "title": {
            "en": "ID"
          }
        }, {
          "id": "title",
          "title": {
            "en": "Description"
          },
          "type": "multText",
          "detailSeq": 2,
          "items": [{
            "id":"en",
            "title": "English"
          }, {
            "id":"zh-tw",
            "title": "中文"
          }]
        }],
        values: dialogValues,
        rootValues: rootValues,
        actions: [{
          type: 'customButton',
          float: 'left',
          title: deleteBtn,
          hide: this.index == undefined,
          handleTap: function() {
            if (this.index) {
              delete cValue[this.index];
            }
            self.handleChange(self.state.changedValues);
          }.bind(this)
        }, {
          type: 'cancel',
          title: cancelBtn
        }, {
          type: 'customButton',
          title: this.doneTitle,
          handleTap: function( values ) {
            // if set it as default, reset others
            if (values.default == 'Y') {
              for (let i in cValue) {
                if (i != this.index) {
                  cValue[i].default = 'N';
                }
              }
            }
            if (this.index) { // exsiting record
              // update record including id
              if (this.index != values.id && cValue[values.id]) { // updated id duplicate
                alert("ID duplicate, please use another one", "Warning");
                return false;
              } else {
                delete cValue[this.index];
              }
            } else if (cValue[values.id]) { // new record but existing id
              alert("ID duplicate, please use another one", "Warning");
              return false;
            }

            // new record
            cValue[values.id] = values;
            self.state.selectedIndex = values.id;
            self.handleChange(changedValues);
            return true;
          }.bind(this)
        }]
      });
    }

    var keys = Object.keys(cValue)

    if (keys.length > 0) {
      for (var i in keys) {
        var cVal = cValue[keys[i]];
        if (typeof cVal == 'object'){
          var tabTitle = (!isEmpty(cVal.title) && (getLocalText('en', cVal.title)+ " (id:"+cVal.id+")")) || cVal.id;

          subTabs.push(<Tab key = {"Tab_" + id + "_"+i}
              label={<span style={{padding: '0px 12px'}}>{tabTitle}</span>}
              value={keys[i]}
              style={{borderColor: appTheme.palette.borderColor, borderBottom: selectedIndex==keys[i]?'3px solid ':'none'}}
            />
          )

          subItems.push(
              <DetailsBlock
                template = { template }
                values = { cVal }
                changedValues = { cVal }
                rootValues = { rootValues }
                deleteAble = { allowAdd }
                editable = { allowAdd }
                discollapsible = { true }
                index = {keys[i]}
                genItemsFunc = {this.props.genItemsFunc}
                handleUpdateRow = {
                  handleTap.bind({items: sortedItems, values: cValue, index: keys[i], doneTitle: addBtn})
                }
                handleDeleteRow = { function(row) {
                  delete cValue[row];
                  self.forceUpdate();
                }}
                handleChange = {function() {
                  self.forceUpdate();
                }}
                />

            )
        }
      }
    } else {
      subItems.push(
          <div style={{
            lineHeight: '48px',
            marginLeft: '24px'
          }}>
            No records found.
          </div>
      );
    }

    if(allowAdd){
      subTabs.push(
        <Tab
          label={
            <span style={{padding: '0px 12px'}}>
              <FontIcon
                className="material-icons"
                style={{color:appTheme.palette.text1Color}}>
                add
              </FontIcon>
            </span>
          }
          value={_addBtnId}
        />
      );
    }

    var errorMsg = validate(template, changedValues);
    errorMsg = errorMsg?<div style={{
      color: this.state.muiTheme.textField.errorColor,
      height: '16px',
      fontSize: '12px',
      position: 'absolute',
      left: '24px',
      top: '42px'
    }}>{errorMsg}</div>:null;

    var valueLen = oValue?oValue.length:0;
    var cValueLen = cValue?cValue.length:0;
    var dd = valueLen - cValueLen;
    if (dd < 0) dd = 0;

    return (
      <div key= {"ctn"+id} className="ListField" style={{position:'relative'}}>
        {this.getHeaderTitle()}
        <div style={{padding: '0px 24px', overflowX: 'auto'}}>
          <Tabs
            inkBarStyle={{display: 'none'}}
            tabItemContainerStyle={{width: 'initial'}}
            value={selectedIndex || (keys.length>0?keys[0]:addBtn)}
            onChange={(value)=>{
              if(value == _addBtnId){
                if(allowAdd === undefined || allowAdd){
                  var _handleTap = handleTap.bind({items: sortedItems, values: cValue, doneTitle: addBtn});
                  _handleTap();
                }
              }else{
                this.handleIndexChange(value);
              }
            }}>
            {subTabs}
          </Tabs>
        </div>
        <Divider/>

        {subItems[keys.indexOf(selectedIndex)>=0?keys.indexOf(selectedIndex):0]}
      </div>
    )
  }
});
