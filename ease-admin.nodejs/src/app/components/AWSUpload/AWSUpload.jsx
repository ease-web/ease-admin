/** Business Continuous Processing page */
let React = require('react');
let mui = require('material-ui');

let FlatButton = mui.FlatButton;
let Dialog = mui.Dialog;
let DynMasterTable = require("../DynamicUI/DynMasterTable.jsx");
import appTheme from '../../theme/appBaseTheme.js';

let AWSUploadActions = require('../../actions/AWSUploadActions.js');
let ContentStore = require('../../stores/ContentStore.js');
let AppBarStore = require('../../stores/AppBarStore.js');
let AppStore = require('../../stores/AppStore.js');
let MenuItem = mui.MenuItem;
let TextField = mui.TextField;

let AWSUpload = React.createClass({
  componentDidMount() {
    AWSUploadActions.getStatus();
    ContentStore.addChangeListener(this.onChange);
  },

  componentWillUnmount: function () {
    ContentStore.removeChangeListener(this.onChange);
  },

  onChange: function () {
    var pageContent = ContentStore.getPageContent();
    let dialog_status = false;
    if(pageContent) {
      if(pageContent.changedValues) {
        let changedValues = pageContent.changedValues;
        if(changedValues.result && IsJsonString(changedValues.result)) {
          const result = JSON.parse(changedValues.result);
          if(result.msg) {
            dialog_status = true
          }
        }
        let responseData = ((pageContent && pageContent.changedValues && pageContent.changedValues && pageContent.changedValues.map && pageContent.changedValues.map.data && pageContent.changedValues.map.data.map) ||
          (pageContent && pageContent.changedValues && pageContent.changedValues && pageContent.changedValues.map && pageContent.changedValues.map)) || {};
        this.setState({ 
          pageContent: pageContent,
          dialogopen: dialog_status,
          isUploading: changedValues.IS_UPLOADING,
          latest: this.transformToUIValuesFromResponse(responseData.latest, pageContent.changedValues),
          previous:  this.transformToUIValuesFromResponse(responseData.previous, pageContent.changedValues)
        })
      }
    }
  },

  transformToUIValuesFromResponse: function(row, changedValues) {
    if (row && row.map) {
      return {
        VERSION_ID: row.map.VERSION_ID_APP,
        VERSION_ID_AWS: row.map.VERSION_ID_AWS,
        IS_CURRENT: row.map.IS_CURRENT,
        IS_UPLOADING: changedValues.IS_UPLOADING || row.map.IS_UPLOADING
      }
    } else {
      return {};
    }
  },

  onChangeHandler: function (event) {
    this.setState({
      selectedFile: event.target.files[0],
      loaded: 0,
    });
  },

  onClickHandler: function () {
    AWSUploadActions.upload({
      file: this.state.selectedFile,
      fileName: this.state.fileName
    });
  },

  getStyle(){
    var style = {
      'buttonBgColor':'#FAFAFA',
      'width': '100px',
      'pageLayout': {
        margin: '24px'
      },
      'buttonStyle': {
        color: '#2DB154',
        width: '200px'
      },
      'changeVersionStyle': {
        margin: '12px 0',
        display: 'flex',
        alignItems: 'center'
      },
      'boldRecordStyle': {
        width: '472px',
        display: 'inline',
        fontWeight: 'bold',
        fontSize: '16px'
      },
      'recordStyle': {
        width: '472px',
        display: 'inline'
      },
      'contentStyle': {
        color: 'black',
        top: '0px'
      },
      'hintStyle': {
        color: 'lightgray',
        top: '0px'
      }
    }
    return style;
  },

  changeVersion: function(event) {
    let awsVersionId = event.currentTarget.value;
    AWSUploadActions.changeVersion(awsVersionId);
  },

  getVersionRow: function(rowValues, isUPloading) {
    if (rowValues && rowValues.VERSION_ID) {
      return (
        <div style={this.getStyle().changeVersionStyle}>
          <div style={rowValues && rowValues.IS_CURRENT !== 'Y' ? this.getStyle().recordStyle : this.getStyle().boldRecordStyle}>
            {rowValues ? rowValues.IS_CURRENT !== 'Y' ? rowValues.VERSION_ID : `${rowValues.VERSION_ID} (Current Version)` : null }
          </div>
          {isUPloading === 'Y' ? null : 
            rowValues && rowValues.IS_CURRENT !== 'Y'? 
              <FlatButton
                key={`${rowValues.VERSION_ID} btn-aws-ipa-changeVesion`}
                labelStyle={{color:this.getStyle().buttonGreen}}
                backgroundColor={this.getStyle().buttonBgColor}
                style={this.getStyle().buttonStyle}
                value={rowValues.VERSION_ID_AWS}
                id={`${rowValues.VERSION_ID} btn-aws-ipa-changeVesion`}
                onTouchTap={this.changeVersion}>
                {"Rollback Version"}
              </FlatButton>
              :
              null 
            }
        </div>
      )
    } else {
      return null;
    }
  },

  onChangeVersionNo: function(e) {
    this.setState({
      fileName: e && e.currentTarget && e.currentTarget.value
    });
  },

  render: function () {
    let latestObject = this.state && this.state.latest || {};
    let previousObject = this.state && this.state.previous || {};
    let isUploading = this.state && this.state.isUploading || false;
    return (
      <div style={this.getStyle().pageLayout}>
        <div>
          {this.getVersionRow(latestObject, latestObject.IS_UPLOADING)}
          {this.getVersionRow(previousObject, latestObject.IS_UPLOADING)}
        </div>
        {
          latestObject.IS_UPLOADING === 'Y' || isUploading === 'Y' ? <div> Uploading in progress ... </div> :
          <div>
            <TextField
              key = "versionTextField"
              style = {{ width: '200px', lineHeight:'56px', margin: '0 24px 0 0'}}
              inputStyle = {this.getStyle().contentStyle}
              hintText = {"Version No..."}
              hintStyle = {this.getStyle().hintStyle}
              onChange = {this.onChangeVersionNo}
            />
            <input type="file" name="file" onChange={this.onChangeHandler}/>
            <FlatButton
              key={"btn-aws-ipa-upload"}
              labelStyle={{color:this.getStyle().buttonGreen}}
              backgroundColor={this.getStyle().buttonBgColor}
              style={this.getStyle().buttonStyle}
              id={"btn-aws-ipa-upload"}
              onTouchTap={this.onClickHandler}>
              {"IPA File Upload"}
            </FlatButton>
          </div>
        }
      </div>
    )
  }
});

module.exports = AWSUpload;
