let React = require('react');
let mui = require('material-ui');

import FlatButton from 'material-ui/lib/flat-button';
import Dialog from 'material-ui/lib/dialog';
import RaisedButton from 'material-ui/lib/raised-button';
import AppBar from 'material-ui/lib/app-bar';
import appTheme from '../theme/appBaseTheme.js';

let AppStore = require('../stores/AppStore.js');
let MenuStore = require('../stores/MenuStore.js');
let HomeStore = require('../stores/HomeStore.js');

let Menu = require('./CustomView/Menu.jsx');
let DynToolbar = require('./DynamicUI/DynToolbar.jsx');
let CompntBase = require('../mixins/EABComponentBase.js');
let DynDialog = require('./DynamicUI/DynDialog.jsx');
let DynMsgDialog = require('./DynamicUI/DynMsgDialog.jsx');
let Loading = require('./CustomView/LoadingBlock.jsx');
let AssignPage = require('./CustomView/AssignPage.jsx');
let PreviewPdf = require('./CustomView/PreviewPdf.jsx');
let SessionAlert = require('./SessionAlert/SessionAlert.jsx');

let Homepage = React.createClass({
  mixins: [CompntBase],

  childContextTypes: {
    muiTheme: React.PropTypes.object,
  },

  getChildContext() {
    return {
      muiTheme: appTheme.getTheme(),
    };
  },

  componentDidMount() {
    AppStore.addChangeListener(this.onPathChange);
    HomeStore.addChangeListener(this.onChange);
    MenuStore.addChangeListener(this.onMenuStateChange);
  },

  componentWillUnmount() {
    AppStore.removeChangeListener(this.onPathChange);
    HomeStore.removeChangeListener(this.onChange);
    MenuStore.removeChangeListener(this.onMenuStateChange);
  },

  onPathChange() {
    var path = AppStore.getPath();
    if (path) {
      redirect(path);
    }
  },

  onChange() {
    this.setState({
      currentPage: HomeStore.getCurrentPage(),
    })
  },

  onMenuStateChange() {
    this.setState({menuOpen: MenuStore.getMenuState()})
  },

  getInitialState() {
    return {
      currentPage: HomeStore.getCurrentPage(),
      menuOpen: true,
    };
  },

  render() {
    var result;
    let page = getPageDom(this.state.currentPage);
    // container style
    var padding = MenuStore.getMenuState()?'280px':'0px';
    let containerStyle = {
      position: 'relative',
      paddingLeft: padding,
      minWidth: '720px',
      width: document.body.clientWidth+'px',
      transition: 'all 400ms cubic-bezier(0.23, 1, 0.32, 1) 0ms'
    };
    //    <Menu ref="leftMenu" overlay={this.state.deviceSize < 3} defaultOpen={this.state.deviceSize == 3}/>

    return (
      <div>
        <Menu />
        <div key="assign" style={containerStyle}>
          <AssignPage/>
        </div>
        <div key="preview" style={containerStyle}>
          <PreviewPdf/>
        </div>
        <div key="content" style={containerStyle}>
          <DynToolbar/>
          {page}
        </div>
        <DynDialog key="dynamic"/>
        <DynMsgDialog key="msgDialog" ref="msgDialog"/>
        <Loading/>
        <SessionAlert/>
      </div>
    );
  }
});

module.exports = Homepage;
