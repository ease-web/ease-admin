import Colors from 'material-ui/lib/styles/colors';
import ColorManipulator from 'material-ui/lib/utils/color-manipulator';
import Spacing from 'material-ui/lib/styles/spacing';
import zIndex from 'material-ui/lib/styles/zIndex';

import ThemeManager from 'material-ui/lib/styles/theme-manager';

module.exports = {
  spacing: Spacing,
  fontFamily: 'Roboto, sans-serif',
  zIndex: zIndex,
  palette: {
    primary1Color: "#2696CC",
    primary2Color: "#22B250",
    primary3Color: Colors.grey400,
    accent1Color: Colors.pinkA200,
    accent2Color: Colors.grey100,
    accent3Color: Colors.grey500,
    textColor: Colors.darkBlack,
    text1Color: Colors.lightBlack,
    alternateTextColor: Colors.white,
    canvasColor: Colors.white,
    borderColor: Colors.faintBlack, //"rgba(0,0,0,.12)", //_colors2.default.grey300,
    disabledColor: ColorManipulator.fade(Colors.darkBlack, 0.3),
    pickerHeaderColor: Colors.cyan500,
    clockCircleColor: ColorManipulator.fade(Colors.darkBlack, 0.07),
    labelColor: '#333333',
    fixedLabelColor: Colors.lightBlack,
    themeGreen: '#20A84C',
    background2Color: '#FAFAFA',
    hoverBackground: Colors.minBlack
  },

  theme: false,
  getTheme: function() {
    if (!this.theme) {
      var theme = ThemeManager.getMuiTheme(this);
      theme.radioButton.checkedColor = this.palette.themeGreen;

      theme.checkbox.checkedColor = this.palette.themeGreen;
      theme.checkbox.boxColor = this.palette.text1Color;

      theme.flatButton.textColor = this.palette.primary1Color;
      theme.flatButton.primaryTextColor = this.palette.textColor;
      theme.flatButton.secondaryTextColor = this.palette.alternateTextColor;

      theme.raisedButton.textColor = this.palette.themeGreen;
      theme.raisedButton.primaryColor = this.palette.primary1Color;

      theme.toolbar.backgroundColor = this.palette.primary1Color;
      theme.toolbar.iconColor = this.palette.alternateTextColor;
      theme.toolbar.menuHoverColor = this.palette.alternateTextColor;

      theme.textField.fixedLabelColor = this.palette.fixedLabelColor;

      theme.tabs.backgroundColor = "transparent";

      theme.tabs.selectedTextColor = ColorManipulator.fade(this.palette.primary1Color, 0.87);
      theme.tabs.textColor = this.palette.text1Color;
      theme.inkBar.backgroundColor = this.palette.primary1Color;
      this.theme = theme;
    }
    return this.theme;
  }
};
