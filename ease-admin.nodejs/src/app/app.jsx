(function () {
  let React = require('react');
  let ReactDom = require('react-dom');
  let injectTapEventPlugin = require('react-tap-event-plugin');
  require('./utils/DateFormater.js');
  require('./utils/CommonUtils.js');
  require('./utils/PageGenerator.js');
  require('./utils/FieldTrigger.js');

  let Welcome = require('./components/Welcome.jsx');

  //Needed for React Developer Tools
  window.React = React;

  //Needed for onTouchTap
  //Can go away when react 1.0 release
  //Check this repo:
  //https://github.com/zilverline/react-tap-event-plugin
  injectTapEventPlugin();

  ReactDom.render(<Welcome />, document.getElementById("content"));
})();
