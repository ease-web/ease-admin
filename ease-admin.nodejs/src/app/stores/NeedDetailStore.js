var AppDispatcher = require('../dispatcher/AppDispatcher.js');
var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');
var ConfigConstants = require('../constants/ConfigConstants');
// var HomeStore = require('../stores/HomeStore.js')

var DynConfigPanelStore = require('./DynConfigPanelStore.js');

var CURRENT_PAGE_CHANGE = 'NeedChangePage';
var ActionTypes = ConfigConstants.ActionTypes;

var _currentPage = '';
var _uiPage = '';
var _lock = '';
var _langCodes = ['en', 'zh-Hant'];
var _defaultLang = 'en';
var _reflesh = false;


var _boxSizeOptions=[{title:'Small', value:150},{title:'Medium', value:192},{title:'Large', value:261}];
var _boxSizeItem={id:'boxRegularWidth', title:getLocalizedText("CONFIG_PANEL.SIZE_OF_BOXES","Size of Boxes"), type:'PICKER', options: _boxSizeOptions, mandatory: true};

var _allowItem={id:'isSingleSelect', title:getLocalizedText("CONFIG_PANEL.SELECTION_ALLOW","Selection allow"), type:'SWITCH'};

var _formatOption=[{title:'Number', value:'number'},{title:'Currency', value:'currency'}];
var _formatItem={id:'format', title:getLocalizedText("CONFIG_PANEL.FORMAT","Format"), type:'PICKER', options: _formatOption, mandatory: true};

var _formatOption2=[{title:'Number', value:'number'},{title:'Currency', value:'currency'},{title:'Precentage', value:'percentage'}];
var _formatItem2={id:'format', title:getLocalizedText("CONFIG_PANEL.FORMAT","Format"), type:'PICKER', options: _formatOption2, mandatory: true};

var _tableNumOpt=[{title:'1',value:1},{title:'2',value:2}];
var _tableNumItem = {id:'tableNum', title:getLocalizedText("CONFIG_PANEL.TABLE_NUMBER","Number of Tables"), type:'PICKER', options: _tableNumOpt};

var _othSelectionOpt=[{title:'other',value:'Y'},{title:'no-other',value:'N'}];
var _othSelectionItem = {id:'otherSelection', title:getLocalizedText("CONFIG_PANEL.OTHER_SELECTION","Other Selection"), type: 'SWITCH'};

var _simpleEqtOption=[{title:'Equation', value:'Y'}, {title:'No Equation', value:'N'}];
var _simpleEqtItem = {id:'equation', title:getLocalizedText("CONFIG_PANEL.SIMPLE_EQUATION","Simple Equation"), type: 'SWITCH'};

var _segmentItem = {id:'segment', title:getLocalizedText("CONFIG_PANEL.SEGMENT_NUMBER","Number of Segments"), type: 'TEXT', mandatory: true, subType:'number'};

var _intervalItem = {id:'interval', title:getLocalizedText("CONFIG_PANEL.INTERVAL","Increment Value"), type: 'TEXT', mandatory: true, subType:'number', value: 1, validation:function(changedValues){if(changedValues['interval']<=0){return getLocalizedText("CONFIG_PANEL.SMALLER_THAN_ZERO","Cannot smaller than or equal 0")}else{return null;}}};
var _incrementItem = {id:'increment', title:getLocalizedText("CONFIG_PANEL.INTERVAL","Increment Value"), type: 'TEXT', mandatory: true, subType:'number', value: 1, validation:function(changedValues){if(changedValues['increment']<=0){return getLocalizedText("CONFIG_PANEL.SMALLER_THAN_ZERO","Cannot smaller than or equal 0")}else{return null;}}};

var _minValueValidation = function(changedValues){
	if(changedValues['maxValue'] && changedValues['minValue'] && Number(changedValues['minValue']) > Number(changedValues['maxValue'])){
		return getLocalizedText("CONFIG_PANEL.SMALLER_THAN_MAX","Cannot greater than maximum value");
	}else if(changedValues['minValue'] && Number(changedValues['minValue'])<0){
		return getLocalizedText("CONFIG_PANEL.SMALLER_THAN_ZERO","Cannot smaller than 0");
	}else{
		return null;
	}
}

var _maxValueValidation = function(changedValues){
	if(changedValues['maxValue'] && changedValues['minValue'] && Number(changedValues['maxValue']) < Number(changedValues['minValue'])){
		return getLocalizedText("CONFIG_PANEL.SMALLER_THAN_MIN","Cannot smaller than minimum value");
	}else if(changedValues['maxValue'] && Number(changedValues['maxValue'])<0){
		return getLocalizedText("CONFIG_PANEL.SMALLER_THAN_ZERO","Cannot smaller than 0");
	}else{
		return null;
	}
}

var _dValueValidation = function(changedValues){
	if(Number(changedValues['defaultValue']) || changedValues['defaultValue'] == "0"){
		if(changedValues['minValue'] && Number(changedValues['defaultValue']) < Number(changedValues['minValue'])){
			return getLocalizedText("CONFIG_PANEL.SMALLER_THAN_MIN","Cannot smaller than minimum value");
		}else if(changedValues['maxValue'] && Number(changedValues['defaultValue']) > Number(changedValues['maxValue'])){
			return getLocalizedText("CONFIG_PANEL.SMALLER_THAN_MAX","Cannot greater than maximum value");
		}else if(Number(changedValues['defaultValue'])<0){
			return getLocalizedText("CONFIG_PANEL.SMALLER_THAN_ZERO","Cannot smaller than 0");
		}
	}else if(changedValues['defaultValue']!='-'){
		return "should be integer or '-'";
	}
	return null;
};

var _minItem = {id:'minValue', title:getLocalizedText("CONFIG_PANEL.MIN","Minimum Value"), type: 'TEXT', mandatory: true, subType:'number', validation: _minValueValidation};
var _maxItem = {id:'maxValue', title:getLocalizedText("CONFIG_PANEL.MAX","Maximum Value"), type: 'TEXT', mandatory: true, subType:'number', validation: _maxValueValidation};
var _dValueItem = {id:'defaultValue', title:getLocalizedText("CONFIG_PANEL.DEFAULT_VALUE","Default Value"), type: 'TEXT', mandatory: true, validation: _dValueValidation};
var _calValue = {id:'calculationValue', title:getLocalizedText("CONFIG_PANEL.CAL_VALUE","Calculation Value"), type: 'TEXT', mandatory: false, subType: 'number'};

var _manualOption=[{title:'Manual', value:'Y'}, {title:'Non-Manual', value:'N'}];
var _manualItem = {id:'numberEditable', title:getLocalizedText("CONFIG_PANEL.MANUAL","Manual Input"), type: 'SWITCH', options: _manualOption};
//var _numberEdit = {id:'numberEditable', title:getLocalizedText("CONFIG_PANEL.NUMBER_EDITABLE","Number Editable"), type: 'SWITCH'};

var _manualNote = {id:'manualNode', value:getLocalizedText("CONFIG_PANEL.MANUAL_INPUT","If manual input is turned on, the user may enter a value bigger than the maximum value."), type:'NOTE'}

var _allowAdding = {id:'allowAdding', title:getLocalizedText("CONFIG_PANEL.SELECTION_ALLOW_ADDING","Add New Attachment"), type:'SWITCH', disabled: true, value: true};

var _showName = {id:'showName', title:getLocalizedText("CONFIG_PANEL.ATTACHMENT_NAME","Attachment names"), type:'SWITCH'};

var _attachmentOption = [{title:'Scan document',value:'scan'},{title:'Take photo',value:'camera'},{title:'Choose existing photo',value:'existing'},{title:'Handwrite',value:'handwriting'}];
var _attachmentItem = {id:'attachOpts', type:'CHECKBOXGROUP', options: _attachmentOption, presentation:'column1'};

var _othInputTypeOption = [{title:'Text', value: 'text'}, {title:'Currency', value: 'currency'}];
var _othInputTYpe = {id: 'otherInputType', type:'PICKER', options: _othInputTypeOption};

var _leftIcon = {id:'leftImage', value: "one_coin"};
var _rightIcon = {id:'rightImage', value: "three_coins"};

var csRelation = {id:'isRelationNeedsAly', title:getLocalizedText("CONFIG_PANEL.RELATE_NEED_ALY","Relational Needs"), type: 'SWITCH'};


let _inputTypeSections = [
	{
		id:'Selections',
		title:'Selections',
		items: [
			{
				value: 'imageSelection',
				title: 'Image Selection',
				src: 'imageSelection.png',
				items:[
					_boxSizeItem,
          _othSelectionItem,
          _allowItem,
					csRelation
				]
			},
			{
				value: 'iconSelection',
				title: 'Icon Selection',
				src: 'iconSelection.png',
				items:[
          _boxSizeItem,
					_othSelectionItem,
          _allowItem
        ]
			},
			{
				value: 'textSelection',
				title: 'Text Selection',
				src: 'textSelection.png',
        items:[
          _othSelectionItem,
          _allowItem
        ],
				subItems:[
					_minItem,
					_maxItem,
					_calValue
				]
			}
		]
	},
	{
		id:'Sliders',
		title:'Sliders',
		items: [
			{
				value: 'normalSlider',
				title: 'Normal Slider',
				src: 'normalSlider.png',
        items:[
          _formatItem,
					_minItem,
          _maxItem,
					_dValueItem,
					_intervalItem,
          _manualItem,
          _manualNote,
					//_numberEdit,
					_leftIcon,
					_rightIcon


        ]
			},
			{
				value: 'trackingSlider',
				title: 'Tracking Slider',
				src: 'trackingSlider.png',
        items:[
          _formatItem2,
					_minItem,
          _maxItem,
					_dValueItem,
					_intervalItem,
        ]
			},
			{
				value: 'segmentedSlider',
				title: 'Segmented Slider',
				src: 'segmentedSlider.png',
        items:[
          _formatItem2,
          _segmentItem,
          _intervalItem,
        ]
			}
		]
	},
	{
		id:'Others',
		title:'Others',
		items: [
			{
				value: 'stepper',
				title: 'Stepper',
				src: 'stepper.png',
				items:[
					_minItem,
					_maxItem,
					_dValueItem,
					_incrementItem
				]
			},
			{
				value: 'attachment',
				title: 'Attachment',
				src: 'attachment.png',
        items:[
					_attachmentItem,
					_allowAdding,
					_showName
        ]
			},
			{
				value: 'textbox',
				title: 'Text box',
				src: 'textbox.png',
        items:[

        ],
				disabled: true
			},
			{
				value: 'tables',
				title: 'Table(s)',
				src: 'tables.png',
        items:[
          _formatItem,
          _tableNumItem,
          _simpleEqtItem,
        ],
				disabled: true
			},
			{
				value: 'verticalBarCharts',
				title: 'Vertical Bar Chart(s)',
				src: 'verticalBarCharts.png',
        items:[

        ],
				disabled: true
			},
			{
				value: 'horizontalBarSlider',
				title: 'Horizonal Bar Slider',
				src: 'horizontalBarSlider.png',
        items:[
          _formatItem,
          _intervalItem,
          _maxItem,
					_minItem,
					_dValueItem
        ],
				disabled: true
			}
		]
	}
]

function _selectPage(currentPage) {
  _currentPage = currentPage.id;
	_uiPage = currentPage.uid;
	_reflesh = true;
}

function _getSelectPage(){
  return _currentPage;
}

function _getUiPage(){
	return _uiPage;
}

var NeedDetailStore = assign({}, EventEmitter.prototype, {

  emitChange: function() {

    this.emit(CURRENT_PAGE_CHANGE);
  },

  /**_selectPage
   * @param {function} callback
   */
  addChangeListener: function(callback) {
    // console.debug('home store add listener');

    this.on(CURRENT_PAGE_CHANGE, callback);
  },

  /**
   * @param {function} callback
   */
  removeChangeListener: function(callback) {

    this.removeListener(CURRENT_PAGE_CHANGE, callback);
  },

  getCurrentPage(){
    return _getSelectPage();
  },

	getUiPage(){
		return _getUiPage();
	},

  isLock(){
    if(_lock=='Y'){
      return true;
    }else{
      return false;
    }
  },

  updateLock(lock){
    if(lock){
      _lock='Y';
    }else{
      _lock='N';
    }
  },

	isReflesh(){
		if(_reflesh){
			_reflesh = false;
			return true;
		}
		return false;
	},

  getLangCodes(){
    return _langCodes;
  },

  genLangCodesObj(){
		let langCodesItems = {};
		for(let i in _langCodes){
			langCodesItems[_langCodes[i]]='';
		}
    return langCodesItems;
  },

	updateLangCode(langCodes){
		_langCodes = langCodes;
	},

	getDefaultLangCode(){
		return _defaultLang;
	},

	updateDefaultLangCode(defaultLangCode){
		_defaultLang = defaultLangCode;
	},

  //return section if id is pass, otherwise return whole sections
  getInputTypeSections(id){
    if(!isEmpty(id)){
      for(let i=0; i<_inputTypeSections.length; i++){
  			let section = _inputTypeSections[i];
  			for(let j=0; j<section.items.length; j++){
  				let item = section.items[j];
  				if(id == item.value)
  					return cloneObject(item);
  			}
  		}
      return null;
    }else{
      return cloneObject(_inputTypeSections);
    }
  }
});

 NeedDetailStore.dispatchToken = AppDispatcher.register(function(action) {
   switch(action.actionType) {
     case ActionTypes.CHANGE_PAGE:
				 if(action.data && action.data.content && action.data.content.values){
					 // update the currenct page
					 if(action.data.content.values.selectPage){
						 _selectPage(action.data.content.values.selectPage);
					 }

					 //update user can access the record or not
	        NeedDetailStore.updateLock(action.data.content.values.readOnly);

					 //update the company language codes
					 if(action.data.content.values.langCodes){
						 NeedDetailStore.updateLangCode(action.data.content.values.langCodes);
					 }

					 //update default langCode
					 if(action.data.content.values.defaultLangCode){
						 NeedDetailStore.updateDefaultLangCode(action.data.content.values.defaultLangCode);
					 }
				 }else{
					 NeedDetailStore.updateLock(false);
				 }


         NeedDetailStore.emitChange();
  			break;
 		case ActionTypes.NEEDS_CHANGE_PAGE:
       // AppDispatcher.waitFor([HomeStore.dispatchToken]);
       _selectPage(action.data.content.values.selectPage);
       NeedDetailStore.emitChange();
 			break;
 		default:
 			// no action
 	}
});

module.exports = NeedDetailStore;
