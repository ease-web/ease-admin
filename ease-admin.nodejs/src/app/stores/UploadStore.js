var AppDispatcher = require('../dispatcher/AppDispatcher.js');
var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');
var ConfigConstants = require('../constants/ConfigConstants');
var ActionTypes = ConfigConstants.ActionTypes;
var AppStore = require('../stores/AppStore.js');
var LoadingStore = require('../stores/LoadingStore.js');


var UPLOAD_STORE = 'UploadStore';

var _state = false;

function _updateState(state) {
    _state = state.isComplete && state.isTeminate?0:1;
}

var UploadStore = assign({}, EventEmitter.prototype, {

  emitChange: function() {
    this.emit(UPLOAD_STORE);
  },

  /**
   * @param {function} callback
   */
  addChangeListener: function(callback) {
    this.on(UPLOAD_STORE, callback);
  },

  /**
   * @param {function} callback
   */
  removeChangeListener: function(callback) {
    this.removeListener(UPLOAD_STORE, callback);
  },

  getState() {
    return _state;
  }
});

UploadStore.dispatchToken = AppDispatcher.register(function(action) {
  switch(action.actionType) {
    case ActionTypes.UPDATE_UPLOAD_STATE:
      if (action.data && action.data.uploadStatus) {
        _updateState(action.data.uploadStatus);
        UploadStore.emitChange();
      }
      break;
		default:
	}
});

module.exports = UploadStore;
