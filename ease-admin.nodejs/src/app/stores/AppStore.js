var AppDispatcher = require('../dispatcher/AppDispatcher.js');
var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');
var ConfigConstants = require('../constants/ConfigConstants');

var HomeStore = require('../stores/HomeStore.js');
var ContentStore = require('../stores/ContentStore.js');

var ActionTypes = ConfigConstants.ActionTypes;
let ProductActions = require('../actions/ProductActions.js');
var PATH_CHANGE_EVENT = 'AppChange';
var _path = 0;
var _user = 0;
var _saml = "";
var _loggedOn = "N";
var _langMap = {};
var _errorMsg = "";
var _version = "1";
var _langList = [];

function _initValues() {
  _path = 0;
  _user = 0;
  _saml = "";
  _loggedOn = "N";
  _langMap = {};
  _errorMsg = "";
  _langList = [];
}

function _updateLangList(list) {
  _langList = list;
}

function _updatePath(newPath) {
  _path = newPath;
}

function _updateUser(user) {
  _user = user;
}

function _updateLangMap(langMap) {
  _langMap = langMap;
}

function _updateSamlPath(saml) {
  _saml = saml;
}

function _updateLoggedOn(flag) {
  _loggedOn = flag;
}

function _updateErrorMsg(err) {
  _errorMsg = err;
}

function _updateVersion(ver) {
  _version = ver;
}

var AppStore = assign({}, EventEmitter.prototype, {

  emitChange: function() {
    this.emit(PATH_CHANGE_EVENT);
  },

  /**
   * @param {function} callback
   */
  addChangeListener: function(callback) {
    this.on(PATH_CHANGE_EVENT, callback);
  },

  /**
   * @param {function} callback
   */
  removeChangeListener: function(callback) {
    this.removeListener(PATH_CHANGE_EVENT, callback);
  },

  getPath: function() {
    return _path;
  },

  getUser: function() {
    return _user;
  },

  getLangMap: function() {
    return _langMap;
  },

  getSamlPath: function() {
    return _saml;
  },

  getLoggedOn: function() {
    return _loggedOn;
  },

  getErrorMsg: function() {
    return _errorMsg;
  },

  getVersion: function() {
    return _version;
  },

  getLangList: function() {
    return _langList;
  }
});

AppStore.dispatchToken = AppDispatcher.register(function(action) {
  switch (action.actionType) {
    case ActionTypes.CONFIG_LOGOUT: 
      _initValues();
      break;
    case ActionTypes.PAGE_REDIRECT:
      AppDispatcher.waitFor([HomeStore.dispatchToken, ContentStore.dispatchToken]);
      // console.debug('[AppStore] - [Dispatching] - [' + action.actionType + ']', action);
      if (action.path) {
        _updatePath(action.path);
      }
      if (action.path == '/Logout') {
          _updateUser(null);
          _updateLangMap(action.data.langMap);
          _updateLoggedOn(false);
      } else {
        if (action.data) {
          if (action.data.saml) {
            _updateSamlPath(action.data.saml);
          }
          if (action.data.user) {
            _updateUser(action.data.user);
          }
          if (action.data.langMap) {
            _updateLangMap(action.data.langMap);
          }
          if (action.data.loggedOn) {
            _updateLoggedOn(action.data.loggedOn);
          }
          if (action.data.errorMsg) {
            _updateErrorMsg(action.data.errorMsg);
          }
          if (action.data.version) {
            _updateVersion(action.data.version);
          }
          if (action.data.langList) {
            _updateLangList(action.data.langList);
          }
        }
      }
      AppStore.emitChange();
      break;
    default:
      // no action
  }
});

module.exports = AppStore;
