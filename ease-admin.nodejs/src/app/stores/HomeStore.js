let AppDispatcher = require('../dispatcher/AppDispatcher.js');
let EventEmitter = require('events').EventEmitter;
let assign = require('object-assign');
let ConfigConstants = require('../constants/ConfigConstants');

let MenuStore = require('../stores/MenuStore.js');
let ContentStore = require('../stores/ContentStore.js');
let MasterTableStore = require('../stores/MasterTableStore.js');
let ProductStore = require('../stores/ProductStore.js');
let TaskReleaseStore = require('../stores/TaskReleaseStore.js');
let TaskStore = require('../stores/TaskStore.js');

var CURRENT_PAGE_CHANGE = 'HomePageChange';
var ActionTypes = ConfigConstants.ActionTypes;
var _fromPage = undefined;
var _currentPage = undefined;
var moduleEqual = true;
function setModuleEqual(newPage){
  if(newPage.module){
    if(_currentPage.module === 'undefined')
      moduleEqual = true;
    else if(_currentPage.module == newPage.module){
      moduleEqual = true;
    }else{
      moduleEqual = false;
    }
  }else{
    moduleEqual = false;
  }
}

function _selectPage(currentPage) {
  _currentPage = currentPage;
  _updateFromPage(currentPage);
}

function _updateFromPage(currentPage){
  if(_fromPage == undefined || !Array.isArray(_fromPage)){
    _fromPage = [];
  }
  if( _fromPage.length>0 && currentPage.value && currentPage.value.isSubmit){
    var pageId = currentPage.id;
    var backPage = _fromPage[_fromPage.length-1];
    while(backPage!=null && backPage.id!=pageId){
      backPage=_backPage();
    }

  }
  if(_fromPage.length==0 || (_fromPage.length>0 && _fromPage[_fromPage.length-1].id!=currentPage.id)){
      _fromPage[_fromPage.length] = cloneObject(currentPage);
  }else if(_fromPage.length>0 && _fromPage[_fromPage.length-1].id == currentPage.id){
    _fromPage[_fromPage.length-1] = cloneObject(currentPage);
  }
  console.debug('[HomeStore] - [backPage] : ', _fromPage);
}

function _backPage(){
  if(_fromPage.length > 0)
    _fromPage.splice(_fromPage.length-1, 1);
  if(_fromPage.length == 0)
    return null;
  return _fromPage[_fromPage.length-1];
}

function clearFromPage(){
  _fromPage = [];
}


var HomeStore = assign({}, EventEmitter.prototype, {

  emitChange: function() {
    this.emit(CURRENT_PAGE_CHANGE);
  },

  /**
   * @param {function} callback
   */
  addChangeListener: function(callback) {
    // console.debug('home store add listener');
    this.on(CURRENT_PAGE_CHANGE, callback);
  },

  /**
   * @param {function} callback
   */
  removeChangeListener: function(callback) {
    this.removeListener(CURRENT_PAGE_CHANGE, callback);
  },

  getCurrentPage() {
    
    return _currentPage;
  },

  getBackPage() {
    if(_fromPage.length && _fromPage.length >=2)
      return _fromPage[_fromPage.length-2];
    return null;
  },

  updatePageId(id){
    _currentPage.value.id = id;
    _fromPage[_fromPage.length-1].value.id = id;
  },

  updateBackPageValue(newValue){
    if(_fromPage.length){
      _fromPage[_fromPage.length-1].value = newValue;
    }
  },

  removeBackPage() {
    return _backPage();
  }

});

HomeStore.dispatchToken = AppDispatcher.register(function(action) {
  // console.debug('[HomeStore] - [Dispatching]');

  switch(action.actionType) {
    case ActionTypes.CHANGE_MENU_PAGE:
      clearFromPage();
      break;
		case ActionTypes.CHANGE_PAGE:
      AppDispatcher.waitFor([MenuStore.dispatchToken, ContentStore.dispatchToken, MasterTableStore.dispatchToken, ProductStore.dispatchToken, TaskReleaseStore.dispatchToken, TaskStore.dispatchToken]);
      // console.debug('[HomeStore] - [Dispatching] - [' + action.actionType + ']', action);

      if (_currentPage.id != action.page.id) {
        // console.debug('[HomeStore->CHANGE_PAGE->emitChange]');
        _selectPage(action.page);
        HomeStore.emitChange();
      } else {
        //the dyn pages have some id but diff module, have to change the current page in homestore
        setModuleEqual(action.page);
        if(!moduleEqual){
            _selectPage(action.page);
        }else{
          _updateFromPage(action.page);
        }
        ContentStore.emitChange();
        MasterTableStore.emitChange();
        ProductStore.emitChange();
        TaskReleaseStore.emitChange();
        TaskStore.emitChange();
      }
      break;
    case ActionTypes.PAGE_REDIRECT:
      AppDispatcher.waitFor([MenuStore.dispatchToken, ContentStore.dispatchToken, MasterTableStore.dispatchToken, ProductStore.dispatchToken, TaskReleaseStore.dispatchToken, TaskStore.dispatchToken]);
      // console.debug('[HomeStore] - [Dispatching] - [' + action.actionType + ']', action);
      if (action.page) {
        _selectPage(action.page);
      }
      // HomeStore.emitChange(); // emit change on appStore
			break;
    case ActionTypes.BACK_PAGE:
      console.debug('[HomeStore] - [Dispatching] - [' + action.actionType + ']', action);
      if(action.target){
        clearFromPage();
        var param = {};
        if(_currentPage.module){
          param['p1'] = _currentPage.module;
        }
        callServer(action.target,param);
      }else{
        var page = HomeStore.removeBackPage();
        var backPage = HomeStore.getBackPage();
        
        var param = {};
        if(page && page.value){
          param['p0'] = convertObj2EncodedJsonStr(page.value);
        }
        if(page && page.module){
          param['p1'] = page.module;
        }
        if(backPage && backPage.id){
          param['p10'] = convertObj2EncodedJsonStr(backPage);
        }
        if(page.id == "/BCProcessing") {
          const bcpContent = ContentStore.getBcpContent();
          param['p11'] = convertObj2EncodedJsonStr({
            action: ActionTypes.CHANGE_PAGE, 
            action_chain: 
            {
              path: page.id, 
              callServer: true,
              data : {
                p0: convertObj2EncodedJsonStr({"criteria" : bcpContent.policyNumber ? bcpContent.policyNumber : null})
              }
              
            }
          });
        } else {
          param['p11'] = convertObj2EncodedJsonStr({action: ActionTypes.CHANGE_PAGE});
        }
        callServer(page.id, param);
      }
      break;
    case "BACK_TWO_PAGE":
      const page = HomeStore.removeBackPage();
      const backPage = HomeStore.getBackPage();
      const param = {};
      // param['p11'] = convertObj2EncodedJsonStr({action: ActionTypes.CHANGE_PAGE});
      if(backPage.id == "/BCProcessing") {
        const bcpContent = ContentStore.getBcpContent();
        param['p11'] = convertObj2EncodedJsonStr({
          action: ActionTypes.CHANGE_PAGE, 
          action_chain: 
          {
            path: backPage.id, 
            callServer: true,
            data : {
              p0: convertObj2EncodedJsonStr({"criteria" : bcpContent.policyNumber ? bcpContent.policyNumber : null})
            }
            
          }
        });
      }
      
      callServer(backPage.id, param);
      
      break;

    case ActionTypes.RELOAD_PAGE:
      console.debug('[HomeStore] - [Dispatching] - [' + action.actionType + ']', action);
      var page = HomeStore.getCurrentPage();
      var backPage = HomeStore.getBackPage();
      var param = {};
      if(page && page.value){
        param['p0'] = convertObj2EncodedJsonStr(page.value);
      }
      if(page && page.module){
        param['p1'] = page.module;
      }
      if(backPage && backPage.id){
        param['p10'] = convertObj2EncodedJsonStr(backPage);
      }
      param['p11'] = convertObj2EncodedJsonStr({action: ActionTypes.CHANGE_PAGE});
      callServer(page.id, param);
      break;
    case ActionTypes.AssignPage.ASSIGN_PAGE:
      break;
    case ActionTypes.RESET_VALUES:
      if(checkExist(action.data, 'content.values.id')){
        HomeStore.updatePageId(action.data.content.values.id);
      }
      break;
		default:
      if(action.page){
          _updateFromPage(action.page);
      }
			// no action
	}
});

module.exports = HomeStore;
