var AppDispatcher = require('../dispatcher/AppDispatcher.js');
var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');
var ConfigConstants = require('../constants/ConfigConstants');
var AppStore = require('../stores/AppStore.js')

var ActionTypes = ConfigConstants.ActionTypes;

var _template = [];
var _list = [];
var _fitler = [];

function _getUassignedTaskList(list, template) {
  _list = list;
  _template = template;
}

var TaskReleaseStore = assign({}, EventEmitter.prototype, {
  emitChange: function() {
    //this.emit(GET_UNASSIGNED_RELEASE_LIST);
  },

  /**_selectPage
   * @param {function} callback
   */
  addChangeListener: function(callback) {
    // console.debug('home store add listener');
    //this.on(GET_UNASSIGNED_RELEASE_LIST, callback);
  },

  /**
   * @param {function} callback
   */
  removeChangeListener: function(callback) {
    //this.removeListener(GET_UNASSIGNED_RELEASE_LIST, callback);
  },

  getList() {
      return _list;
  },

  getTemplate() {
      return _template;
  },

  setFilter(insearch){
	   _fitler = {search : insearch};
   },

   getFilter(){
	    return _fitler;
   }
});

ReleaseAddTaskStore.dispatchToken = AppDispatcher.register(function(action) {
  switch(action.actionType) {
		case ActionTypes.CHANGE_PAGE:
      if(action.page && action.page.id == "/Releases/Detail/Task"){
        _getUassignedTaskList(action.data.list, action.data.template);
        ReleaseAddTaskStore.emitChange();
      }
		break;
		default:
			// no action
	}
});

module.exports = ReleaseAddTaskStore;
