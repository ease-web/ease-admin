whenever sqlerror continue;
  /* Put DROP query here.  Please test using SQLPlus. */
  /* e.g.
     drop sequence BATCH_LOG_NO_SEQ;
  */
whenever sqlerror exit failure;

/* Put CREATE SEQUENCE query here */
/* provide DROP query for the CREATE or ALTER query */
/* e.g.
   create sequence BATCH_LOG_NO_SEQ
     minvalue 1
     maxvalue 999999999999999999999999999
     start with 1
     increment by 1
     cache 10;
*/

