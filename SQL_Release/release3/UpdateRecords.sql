
Update SYS_FUNC_MST set FUNC_URI = '/BCProcessing' where FUNC_CODE = 'FN.BCP.BCP';

Delete from SYS_NAME_MST where name_code in('BCP.DETAIL.RLS','BCP.DETAIL.WFI','BCP.DETAIL.PYMT','BCP.DETAIL.ERROR.RLS','BCP.DETAIL.ERROR.WFI','BCP.DETAIL.ERROR.PYMT','BCP.FUNC.SUBMISSION_RLS','BCP.FUNC.SUBMISSION_WFI','BCP.FUNC.SUBMISSION_PAYMENT');
Insert into SYS_NAME_MST VALUES('BCP.DETAIL.RLS', 'en', 'Submission (RLS)', 'A', 'Y');
Insert into SYS_NAME_MST VALUES('BCP.DETAIL.WFI', 'en', 'Document Upload (WFI)', 'A', 'Y');
Insert into SYS_NAME_MST VALUES('BCP.DETAIL.PYMT', 'en', 'Record Payment (DCR)', 'A', 'Y');
Insert into SYS_NAME_MST VALUES('BCP.DETAIL.ERROR.RLS', 'en', 'Error History (RLS)', 'A', 'Y');
Insert into SYS_NAME_MST VALUES('BCP.DETAIL.ERROR.WFI', 'en', 'Error History (WFI)', 'A', 'Y');
Insert into SYS_NAME_MST VALUES('BCP.DETAIL.ERROR.PYMT', 'en', 'Error History (PAYMENT)', 'A', 'Y');
Insert into SYS_NAME_MST VALUES('BCP.FUNC.SUBMISSION_RLS', 'en', 'Submission (RLS)', 'A', 'N');
Insert into SYS_NAME_MST VALUES('BCP.FUNC.SUBMISSION_WFI', 'en', 'Document Upload (WFI)', 'A', 'N');
Insert into SYS_NAME_MST VALUES('BCP.FUNC.SUBMISSION_PAYMENT', 'en', 'Record Payment (DCR)', 'A', 'N');

