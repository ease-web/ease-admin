-- for product setup-s
CREATE OR REPLACE FORCE EDITIONABLE VIEW "V_PROD_TRX" ("COMP_CODE", "PROD_CODE", "VERSION", "PROD_LINE", "LAUNCH_DATE", "EXPIRY_DATE", "BASE_VERSION", "LAUNCH_STATUS", "STATUS", "CREATE_DATE", "CREATE_BY", "UPD_DATE", "UPD_BY", "LANG_CODE", "PLAN_TYPE", "VERSION_DESC", "NAME_DESC", "TASK_STATUS", "TASK_STATUS_CODE", "PROD_DESC")
AS
  SELECT a."COMP_CODE",
    a."PROD_CODE",
    a."VERSION",
    a."PROD_LINE",
    a."LAUNCH_DATE",
    a."EXPIRY_DATE",
    a."BASE_VERSION",
    a."LAUNCH_STATUS",
    a."STATUS",
    a."CREATE_DATE",
    a."CREATE_BY",
    a."UPD_DATE",
    a."UPD_BY",
    a."LANG_CODE",
    a."PLAN_TYPE",
    pn1.name_desc
    || ' v.'
    || a.version  AS version_desc,
    pn1.name_desc AS name_desc,
    vtl.status task_status,
    vtl.status_code task_status_code,
    pn1.name_desc
    || ' ['
    || a.prod_code
    || ']' AS prod_desc
  FROM
    (SELECT vpl.*,
      slm.lang_code
    FROM
      (SELECT b.*
      FROM DYN_PUBLISH_MST a,
        prod_trx b
      WHERE a.comp_code = b.comp_code
      AND a.DOC_CODE    = b.PROD_CODE
      AND a.version     = b.version
      UNION
      SELECT c.*
      FROM prod_trx c
      WHERE c.status      = 'A'
      AND c.launch_status = 'P'
      AND c.version       = 1
      AND NOT EXISTS
        (SELECT NULL
        FROM DYN_PUBLISH_MST mst
        WHERE mst.comp_code = c.comp_code
        AND mst.doc_code    = c.PROD_CODE
        )
      ) vpl,
      sys_lang_mst slm
    WHERE vpl.status = 'A'
    AND slm.status   = 'A'
    ) a
  LEFT JOIN dyn_name_mst pn1
  ON a.comp_code    = pn1.comp_code
  AND a.PROD_CODE   =pn1.doc_code
  AND a.version     =pn1.version
  AND pn1.module    ='product'
  AND pn1.lang_code = a.lang_code
  AND pn1.status    = 'A'
  LEFT JOIN v_task_list vtl
  ON a.comp_code    = vtl.comp_code
  AND a.PROD_CODE   = vtl.item_code
  AND a.version     = vtl.ver_no
  AND vtl.func_code = 'FN.PRO.PRODUCTS';
-- for product setup-e